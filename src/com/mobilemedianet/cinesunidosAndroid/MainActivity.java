package com.mobilemedianet.cinesunidosAndroid;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.mobilemedianet.cinesunidosAndroid.activities.AcercaActivityFragment;
import com.mobilemedianet.cinesunidosAndroid.activities.ButacasActivityFragment;
import com.mobilemedianet.cinesunidosAndroid.activities.ComprarActivityFragment;
import com.mobilemedianet.cinesunidosAndroid.activities.ConfirmarActivityFragment;
import com.mobilemedianet.cinesunidosAndroid.activities.DetallesActivityFragment2;
import com.mobilemedianet.cinesunidosAndroid.activities.DetallesCinesActivityFragment;
import com.mobilemedianet.cinesunidosAndroid.activities.ListaActivityFragment;
import com.mobilemedianet.cinesunidosAndroid.activities.ListaCinesActivityFragment;
import com.mobilemedianet.cinesunidosAndroid.activities.ListaEstrenosActivityFragment;
import com.mobilemedianet.cinesunidosAndroid.activities.LoginActivityFragment;
import com.mobilemedianet.cinesunidosAndroid.activities.PagarActivityFragment;
import com.mobilemedianet.cinesunidosAndroid.activities.RegistroActivityFragment;
import com.mobilemedianet.cinesunidosAndroid.activities.SeleccionButacasActivityFragment;
import com.mobilemedianet.cinesunidosAndroid.managers.ApplicationStatus;
import com.mobilemedianet.cinesunidosAndroid.managers.ButacasManager;
import com.mobilemedianet.cinesunidosAndroid.managers.ConnectionManager;
import com.mobilemedianet.cinesunidosAndroid.managers.DetallesCompra;
import com.mobilemedianet.cinesunidosAndroid.managers.LoginManager;
import com.mobilemedianet.cinesunidosAndroid.managers.MovieManager;
import com.mobilemedianet.cinesunidosAndroid.managers.PrefManager;
import com.mobilemedianet.cinesunidosAndroid.objects.Movie;
import com.mobilemedianet.cinesunidosAndroid.utilities.Contador;
import com.mobilemedianet.cinesunidosAndroid.utilities.Fechas;
import com.mobilemedianet.cinesunidosAndroid.utilities.Metrics;

import net.londatiga.android.ActionItem;
import net.londatiga.android.QuickAction;

//import com.mobilemedianet.cinesunidosAndroid.managers.LocalyticsManager;

public class MainActivity extends FragmentActivity  implements ListaEstrenosActivityFragment.OnListaSelectedListener,
DetallesActivityFragment2.OnShowTimeSelectedListener, ComprarActivityFragment.OnPagarSelectedListener, 
SeleccionButacasActivityFragment.OnSeleccionButacasListener, PagarActivityFragment.OnPagarListener, 
ButacasActivityFragment.OnButacasListener, ListaCinesActivityFragment.OnListaCinesSelectedListener,
DetallesCinesActivityFragment.OnShowTimeSelectedListener, LoginActivityFragment.OnShowTimeSelectedListener, 
LoginActivityFragment.OnLoginListener{
    
	
	
	private static final int ID_SELECT     = 1;
	private static final int ID_REFRESH   = 2;	
	private static final int ID_LOGOUT = 3;	
	private static final int ID_ACERCA = 4;
	private static final int ID_COMPARTIR = 5;
	private static final int ID_ESTRENOS = 6;
	
	public static final int ID_DETAILS = 1;
	public static final int ID_CINEMAS = 2;
	
	public static final String ST_ESTRENOS = "estrenos";
	public static final String ST_CINES = "cines";
	public static final String ST_DETALLES = "detalles";
	public static final String ST_ACERCADE = "acercade";
	public static final String ST_LISTA = "lista";
	public static final String ST_LOGIN = "login";
	public static final String ST_COMPRAR = "comprar";
	public static final String ST_PAGAR = "pagar";
	public static final String ST_BUTACAS = "butacas";
	public static final String ST_SELECCIONBUTACAS = "seleccionbutacas";
	public static final String ST_CONFIRMAR = "confirmar";
	public static final String ST_DETALLESCINES = "detallescines";
	public static final String ST_REGISTRO = "registro";
	
	private static final String Flurry_API_Key = "GSW94MXD24S7D546NST6";
	
	
	
	private boolean cine = false;
	
	private String alertTimer = "Usted está realizando una operación con límite de tiempo, terminela o salga de ella para volver a intentar.";
	
	private static MainActivity instance = null;
	
	
	

	@Override
    public void onCreate(Bundle savedInstanceState) {
       
    	super.onCreate(savedInstanceState);    	

		//Flurry Start Session
		FlurryAgent.onStartSession(this);


    	if(ApplicationStatus.getStatus() == false){
    		Intent reLaunchMain=new Intent(this, SplashActivity.class);
    		reLaunchMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    		startActivity(reLaunchMain);
    	}
    	
    	instance = this;
    	Metrics.setContext(this);   
    	//LocalyticsManager.newLocalytics(this);
    	Fechas.getFecha();
    	requestWindowFeature(Window.FEATURE_NO_TITLE); 
    	ConnectionManager.setContext(this);
    	MovieManager.getInstance().setContext(this);
    	PrefManager.getInstance().setAplicationContext(this);
    	
    	
    	setContentView(R.layout.main_fragment);
    	
    	RelativeLayout home = (RelativeLayout) findViewById(R.id.botonHome);
    	RelativeLayout cines = (RelativeLayout) findViewById(R.id.botonCine);
    	RelativeLayout peliculas = (RelativeLayout) findViewById(R.id.botonPeli);
    	
    	cines.setOnClickListener(OnClickDoSomething(cines));
    	peliculas.setOnClickListener(OnClickDoSomething(peliculas));
    	home.setOnClickListener(OnClickDoSomething(home));
    	generarMenu();	
    	
    	Bundle bundle = getIntent().getExtras();
    	
    	if(bundle == null)
    		return;
    	
    	int action = bundle.getInt("action");
    	//int param = bundle.getInt("param");
		String param = bundle.getString("param");
    	switch (action) {
    	
    		
    		case ID_DETAILS :  
    			MovieManager.getInstance().setIndexSelection(param);
    	    	cambiarFragment(param, ST_DETALLES, DetallesActivityFragment2.getInstance(param, false));
    	    	cine = false;
    	    	break;   
    	    	
    		case ID_CINEMAS :
    			cambiarFragment("0", ST_CINES, ListaCinesActivityFragment.newInstance());
    			cine = true;
    			break;
    	    	
    		case ID_ACERCA :
    	    	cambiarFragment("0", ST_ACERCADE, AcercaActivityFragment.getInstance());
    	    	cine = false;
    	    	break;
    	    	
    		case ID_ESTRENOS :
    			cambiarFragment("0", "ST_ESTRENOS", ListaEstrenosActivityFragment.getInstance());
    			break;
    	    	
    			
    		}
    		
   
    	
    	
    }
	
	 View.OnClickListener OnClickDoSomething(final View v)  {
	        return new View.OnClickListener() {
	        	

	        	
	            @Override
				public void onClick(View v) {   
	            	
	            	FragmentManager fm = getSupportFragmentManager();
	            	
	            	
	            	ListaCinesActivityFragment cinesFragment = (ListaCinesActivityFragment) fm.findFragmentByTag(ST_CINES);
	            	
	            	
	            	if(v.getId() == R.id.botonCine){
	            		checkTimersOnBack(true);
	            		
	            		if(cinesFragment != null && cinesFragment.isVisible()){
	            			Toast.makeText(MainActivity.this, "Ya se encuentra en cines", Toast.LENGTH_SHORT).show();
	            			return;
	            		}
	            		
	            		cine = true;
	            		if(cinesFragment == null)
	            			cambiarFragment("0", ST_CINES, ListaCinesActivityFragment.newInstance());
	            		else
	            			fm.popBackStack(ST_CINES, 0);
	            		            		
	            	}else if(v.getId() == R.id.botonPeli){
	            		checkTimersOnBack(true);
	            		
	            		MainActivity.this.finish();
//	            		
	            	}else if(v.getId() == R.id.botonHome){
	            		
	            		if(cinesFragment != null && cinesFragment.isVisible()){
	            			Toast.makeText(MainActivity.this, "Ya se encuentra en home", Toast.LENGTH_SHORT).show();    
	            			return;	            			
	            		}
	            			
	            		checkTimersOnBack(true);
	            	
	            	
	            		if(cine == true){
	            			if(cinesFragment != null)
	            				fm.popBackStack(ST_CINES, 0);
	            			else
	            				cambiarFragment("0", ST_CINES, ListaCinesActivityFragment.newInstance());
//	            			
	            			return;	     
	            		}
	            		
	            		MainActivity.this.finish();
	            		
	            	}
	                 
	            }
	        };
	    }


	@Override
	protected void onStart() {
		super.onStart();

		FlurryAgent.init(this, Flurry_API_Key);
	}

	@Override
	protected void onPause() {
		super.onPause();

		FlurryAgent.onEndSession(this);
	}

	@Override
	protected void onStop() {
		super.onStop();

		FlurryAgent.onEndSession(this);
	}

	private void addDynamicFragment() {
        // TODO Auto-generated method stub
//    eliminarBackStacks();     
//  	if(orient){
  		
  		Object viewer = getSupportFragmentManager().findFragmentByTag(ST_LISTA);
    	Fragment fg = ListaActivityFragment.getInstance();
    	FragmentTransaction  transaction = getSupportFragmentManager().beginTransaction();   
    	
    	
  		if(viewer == null){  			
  			
  			transaction.add(R.id.Layout1, fg, ST_LISTA);
  			transaction.addToBackStack(ST_LISTA);
  			transaction.commit();   			
  		}else{  			
  			transaction.replace(R.id.Layout1, fg, ST_LISTA);   			
  			transaction.commit();
  		}
//    		}
//    	else {    		
//    		Object viewer = (Object) getSupportFragmentManager().findFragmentByTag(ST_LISTA);
//    		DetallesActivityFragment2 viewer2 = (DetallesActivityFragment2) getSupportFragmentManager().findFragmentByTag(ST_DETALLES);
//        	
//    		Fragment fg = ListaActivityFragment.newInstance();
//        	Fragment fg2 = DetallesActivityFragment2.getInstance(0);
//    		
//    		if(viewer == null){
//    			getSupportFragmentManager().beginTransaction().add(R.id.Layout1, fg, ST_LISTA).commit();
//    		}else{
//    			FragmentTransaction  transaction = getSupportFragmentManager().beginTransaction();
//      			transaction.replace(R.id.Layout1, fg, ST_LISTA); 
//      			transaction.commit();
//    		}
//    		
//    		if(viewer2 == null){
//    			getSupportFragmentManager().beginTransaction().add(R.id.Layout2, fg2, ST_DETALLES).commit();
//    		}else{
//    			FragmentTransaction  transaction = getSupportFragmentManager().beginTransaction();
//      			transaction.replace(R.id.Layout2, fg2, ST_DETALLES);  
//      			transaction.commit();
//    		}
//    			  		
//    	
//    		
//    	}
        
    }


//@Override
//public boolean onCreateOptionsMenu(Menu menu) {
//    MenuInflater menuInflater = getMenuInflater();
//    menuInflater.inflate(R.menu.main, menu);
//
//    // Calling super after populating the menu is necessary here to ensure that the
//    // action bar helpers have a chance to handle this event.
//    return super.onCreateOptionsMenu(menu);
//}

//@Override
//public boolean onOptionsItemSelected(MenuItem item) {
//    switch (item.getItemId()) {
//        case android.R.id.home:
//            Toast.makeText(this, "Tapped home", Toast.LENGTH_SHORT).show();
//            break;
//
//        case R.id.menu_refresh: 	
//        	
//        	final ListaActivityFragment viewer = (ListaActivityFragment) getSupportFragmentManager()
//            .findFragmentByTag(ST_LISTA);
//        	
//        	if(viewer == null)
//        		return super.onOptionsItemSelected(item);
//        	
////            Toast.makeText(this, "Fake refreshing...", Toast.LENGTH_SHORT).show();
//            getActionBarHelper().setRefreshActionItemState(true);
//            
//            new Thread(new Runnable() {             			
//        		public void run() {
//        			
//        			MovieManager.getInstance().refresh();
//        			getWindow().getDecorView().post(new Runnable() {
//                public void run() {
//                	getActionBarHelper().setRefreshActionItemState(false); 	
//                    viewer.inicializarLista();
//                	
//                }
//              });       			
//        	}}).start(); 
//            
//           
//            
//            break;
//        
//        case R.id.menu_share:
//            Toast.makeText(this, "Tapped share", Toast.LENGTH_SHORT).show();
//            break;
//        
//    }
//    return super.onOptionsItemSelected(item);
//}





@Override
public void onListaSelected(String movieId, boolean premier) {
	// TODO Auto-generated method stub
	MovieManager.getInstance().setIndexSelection(movieId);
	
	cambiarFragment(movieId, ST_DETALLES, DetallesActivityFragment2.getInstance(movieId, premier));

	cine = false;
	
	
}



@Override
public void showTimeSelected() {

	String showtimeId = MovieManager.getInstance().getShowTimeId();

	ComprarActivityFragment inst = ComprarActivityFragment.getInstance(showtimeId);
	
	if(inst == null){
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
		alertDialog.setTitle("CinesUnidos"); 
		alertDialog.setMessage("Lo sentimos mucho pero la aplicación ha encontrado un error en los datos, intente refrescar o reiniciar");
		alertDialog.setIcon(R.drawable.icono); 
		alertDialog.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog,int which) {
				dialog.cancel();				
				return;
			}
	      }); 
		return;
	}
	
	if(PrefManager.getInstance().checkLoginStatus() == false || LoginManager.getInstance().getLoginStatus() == false){
		cambiarFragment("0", ST_LOGIN, LoginActivityFragment.getInstance());
	}else{
		cambiarFragment("0", ST_COMPRAR, ComprarActivityFragment.getInstance(MovieManager.getInstance().getShowTimeId()));
	}
	
}


@Override
public void PagarSelected() {
	// TODO Auto-generated method stub
	if(DetallesCompra.getInstance().getSeleccionAsientos()){
		ButacasManager.getInstance().setBackFromMap(false);
		irAPagar(true);
	}else
		irAPagar(false);
		
	
		
	
	
}


private void irAPagar(boolean butacas) {
	if(butacas){
		
		Contador.getInstance().setTimer(300000);  // 5=300000 minutos para pagar
		cambiarFragment("0", ST_SELECCIONBUTACAS, SeleccionButacasActivityFragment.getInstance());
		}else{
			
			cambiarFragment("0", ST_PAGAR, PagarActivityFragment.getInstance());
		}
		
}

@Override
public void irAPagar2(boolean donde) {
	// TODO Auto-generated method stub
	if(donde == true){
		
		cambiarFragment("0", ST_PAGAR, PagarActivityFragment.getInstance());
	}else{
		
		cambiarFragment("0", ST_BUTACAS, ButacasActivityFragment.getInstance());
	}
}


@Override
public void irConfirmacion() {
	// TODO Auto-generated method stub
	
	cambiarFragment("0", ST_CONFIRMAR, ConfirmarActivityFragment.getInstance());
	
}

private void cambiarFragment(String index, String tag, Fragment newFragment){
		
	DetallesActivityFragment2 viewer = null;
	
	
		
	if(tag.equals(ST_DETALLES))
		viewer = (DetallesActivityFragment2) getSupportFragmentManager().findFragmentByTag(tag);

	
    if (viewer == null || !viewer.isInLayout()) {    	
    	
    	FragmentTransaction  transaction = getSupportFragmentManager().beginTransaction();    	
    	    	
//    	if(orient)
    		transaction.replace(R.id.Layout1, newFragment, tag);
//    	else
//    		transaction.replace(R.id.Layout2, newFragment, tag);	    	
    	
    	transaction.addToBackStack(tag);
    	
//    	transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);    	
    	
    	transaction.commit();    	

    } else {
    	viewer.mostrarDetalles(index);    	
      
    }
}

@Override
public void onBackPressed() {
	
    FragmentManager fm = getSupportFragmentManager();
   
    ConfirmarActivityFragment myFragment1 = (ConfirmarActivityFragment) fm.findFragmentByTag(ST_CONFIRMAR);
    
    //TODO arreglar el back en confirmacion
    
    if (fm.getBackStackEntryCount() > 1) {
    	
    	if(myFragment1 != null && myFragment1.isVisible()) 
      		goHome();
    	
    	checkTimersOnBack(false);  
    	
    	
        fm.popBackStack();
        
        
    }else{
    	
    	MainActivity.this.finish();  
    	
    }
//    }else{
//    	
//    	AlertDialog.Builder alertDialog = new AlertDialog.Builder(this); 
//        
//        alertDialog.setTitle("CinesUnidos"); 
//        
//        alertDialog.setMessage("¿Está seguro que desea salir?");
//       
//        alertDialog.setIcon(R.drawable.icono); 
//       
//        alertDialog.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog,int which) { 
//            	MainActivity.this.finish();     
//            	android.os.Process.killProcess(android.os.Process.myPid());
//            }
//        }); 
//        
//        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog,    int which) {            
//            
//            dialog.cancel();
//            }
//        });
// 
//       
//        alertDialog.show();    	

//    }
//    
//    if(indicadordepagina == 1)
//    	indicadordepagina = 0;
}






@Override
public void butacasListener() {
	
	
}




@Override
public void onListaCinesSelected(int cineId) {
	
//	MovieManager.getInstance().setIndexSelection(movieId);	
	cambiarFragment(String.valueOf(cineId), ST_DETALLESCINES, DetallesCinesActivityFragment.getInstance(cineId));
	
}



private class RefreshTask extends AsyncTask<String, Void, Integer> {
	 ProgressDialog pd;
	 boolean refresh = false;
	 boolean resultado;	 
	 private Movie pelicula;
	 @Override
	protected void onPreExecute() {		 
		 
		 pd = ProgressDialog.show(MainActivity.this, "Espere.", "Actualizando listas.", true, false);
		 
		 
   	}
    
	 @Override
	protected Integer doInBackground(String... params) {
		 
		resultado = MovieManager.getInstance().refresh();
		 Log.d("MainActivity-Res", "" + resultado);
		if(resultado == false)
			return 0;
		
		if(params[0].equals("refresh"))
			 refresh = true;
		
		MovieManager.getInstance().resetRankingSearch();
		
		 while((pelicula = MovieManager.getInstance().getNextMovieRank(0)) != null){
			 if(pelicula.isInCache())
				 pelicula.setPoster();    			 
		 }
        
        return 0;
    }	    

    @Override
	protected void onPostExecute(Integer result) {
    	
    	
    	if(resultado == false){
    		try {
   	    	 pd.cancel();
   	         pd = null;
   	        } catch (Exception e) { }    		
   	        Toast.makeText(MainActivity.this, "Fallo de conexión.", Toast.LENGTH_SHORT).show();
    		refreshDialog();
    		return;
    		    		
    	}
    	
    	MovieManager.getInstance().needToRefresh = false;	
    	
    	if(refresh == false)
    		addDynamicFragment();
    	else{	
    		FragmentManager fm = getSupportFragmentManager();
    		goHome();
  			
    	}
    	FragmentManager fm = getSupportFragmentManager();    	
    	ListaActivityFragment pelisFragment = (ListaActivityFragment) fm.findFragmentByTag(ST_LISTA);
    	
    	if(pelisFragment.isVisible())
    		ListaActivityFragment.getInstance().refreshList();
    	else
    		goHome();
    	try {
	    	 pd.cancel();
	         pd = null;
	        } catch (Exception e) { }
    	
    	
        
    }
    
    
}

private void refreshDialog () {
	AlertDialog.Builder builder = new AlertDialog.Builder(this);
	builder.setMessage("Error en la conexión, intente la recarga de nuevo---")
	.setTitle("¡Atención!")
	.setCancelable(false)
	.setNegativeButton("Recargar", new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			ConnectionManager.getInstance().clearCache(null);
			MovieManager.getInstance().needToRefresh = true;
			new RefreshTask().execute("");
			dialog.cancel();
		}
	});
	AlertDialog alert = builder.create();
	alert.show();
}

private void generarMenu(){	
	
	ActionItem refreshItem = new ActionItem(ID_REFRESH, "\nRefrescar\n", null);
	ActionItem estrenosCinemaItem = new ActionItem(ID_ESTRENOS, "\nPróximos Estrenos\n", null);
	ActionItem selectCinemaItem = new ActionItem(ID_SELECT, "\nCines Favoritos\n", null);	
	ActionItem compartirItem = new ActionItem(ID_COMPARTIR, "\nCompartir\n", null);
	ActionItem logoutItem = new ActionItem(ID_LOGOUT, "\nCerrar Sesión\n", null);
	ActionItem acercaDe = new ActionItem(ID_ACERCA, "\nAcerca de\n", null);
	
	
	
	
	final QuickAction quickAction = new QuickAction(this, QuickAction.VERTICAL);
	
	quickAction.addActionItem(refreshItem);
	quickAction.addActionItem(estrenosCinemaItem);
	quickAction.addActionItem(selectCinemaItem);
	quickAction.addActionItem(compartirItem);
	quickAction.addActionItem(logoutItem);	
	quickAction.addActionItem(acercaDe);
	
	
	quickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {			
		@Override
		public void onItemClick(QuickAction source, int pos, int actionId) {				
			
			Intent myIntent; 
			
			//here we can filter which action item was clicked with pos or actionId parameter
			if (actionId == ID_REFRESH) {
				if(checkScreenWithTimer()){
	        		mensajeError(alertTimer);
	        		return;
	        	}				
				if(PrefManager.getInstance().isAnyCinemaChecked() == false){
					mensajeError("No hay cines favoritos, por favor seleccione al menos un cine favorito.");
			        return;
				}	
				if(ConnectionManager.checkConn() == false){
					mensajeError("No hay conexión a internet.");
					return;
	        	}
				MovieManager.getInstance().needToRefresh = true;
				
				myIntent = new Intent(MainActivity.this, ListaActivity.class);
				myIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP); 
				myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		        startActivityForResult(myIntent, 0); 

		        MainActivity.this.finish();
				
//				myIntent = new Intent(MainActivity.this, DummyActivity.class);
//	            startActivityForResult(myIntent, 0); 
			} else if (actionId == ID_LOGOUT) {
				if(checkScreenWithTimer()){
	        		mensajeError(alertTimer);
	        		return;
	        	}				
				PrefManager.getInstance().cleanUserInfo();
				LoginManager.getInstance().setLoginStatus(false);
				goHome();
				
			} else if (actionId == ID_SELECT) {
				if(checkScreenWithTimer()){
	        		mensajeError(alertTimer);
	        		return;
	        	}				
				myIntent = new Intent(MainActivity.this, SeleccionCiudadesActivity.class);
				myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
	            startActivityForResult(myIntent, 0);    
	        } else if (actionId == ID_ACERCA) {
	        	if(checkScreenWithTimer()){
	        		mensajeError(alertTimer);
	        		return;
	        	}
	        	
	        	cambiarFragment("0", ST_ACERCADE, AcercaActivityFragment.getInstance());
	        } else if(actionId == ID_COMPARTIR){
	        	if(checkScreenWithTimer()){
	        		mensajeError(alertTimer);
	        		return;
	        	}
	        	     	
	        	Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
	    		sharingIntent.setType("text/plain");
	    		String shareBody = "Descarga CinesUnidos para Android visitando:\n\n www.cinesunidos.com/aplicativo";
	    		sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Descarga Cines Unidos para Android");
	    		sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
	    		startActivity(Intent.createChooser(sharingIntent, "Share via"));	        	
	        }else if (actionId == ID_ESTRENOS) {
	        	if(checkScreenWithTimer()){
	        		mensajeError(alertTimer);
	        		return;
	        	}
	        	
	        	if(ConnectionManager.checkConn() == false){
					mensajeError("No hay conexión a internet");
					return;
	        	}
	        	cambiarFragment("0", "ST_ESTRENOS", ListaEstrenosActivityFragment.getInstance());
	        }
			
		}
	});	
	
	RelativeLayout preferencias = (RelativeLayout) findViewById(R.id.botonMenu); 
	preferencias.setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View v) {
			quickAction.show(v);
		}
	});
	
	
}


public void goHome(){
	checkTimersOnBack(true);
	this.finish();
}

private void mensajeError(String error){
	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setTitle("CinesUnidos");
    builder.setMessage(error);       
    builder.setPositiveButton("OK",null);
    builder.create();
    builder.setIcon(R.drawable.icono);
    builder.show(); 
}

private void checkTimersOnBack(boolean cancelOrder){
	FragmentManager fm = getSupportFragmentManager();
	SeleccionButacasActivityFragment myFragment1 = (SeleccionButacasActivityFragment) fm.findFragmentByTag(ST_SELECCIONBUTACAS);
	ButacasActivityFragment myFragment2 = (ButacasActivityFragment) fm.findFragmentByTag(ST_BUTACAS);
	PagarActivityFragment myFragment3 = (PagarActivityFragment) fm.findFragmentByTag(ST_PAGAR);
	
	
	if (myFragment1 != null && myFragment1.isVisible()) {
		SeleccionButacasActivityFragment.getInstance().cancelTimer();
		ButacasManager.getInstance().cancelOrder(this);    		
	}else if(myFragment2 != null && myFragment2.isVisible()){    		
		ButacasActivityFragment.getInstance().cancelTimer();  
		RelativeLayout actionbar = (RelativeLayout) findViewById(R.id.actionBar);
        actionbar.setVisibility(View.VISIBLE);
        if(cancelOrder)
			ButacasManager.getInstance().cancelOrder(this); 
//		((TextView) findViewById(R.id.actBarButtonText)).setText("");    		
//		((RelativeLayout)findViewById(R.id.genericButtonActionBar)).setClickable(false);
	}else if (myFragment3 != null && myFragment3.isVisible()) {
		PagarActivityFragment.getInstance().cancelTimer();		
		ButacasManager.getInstance().setBackFromMap(true); 
	}
}

private boolean checkScreenWithTimer(){

		FragmentManager fm = getSupportFragmentManager();
		SeleccionButacasActivityFragment myFragment1 = (SeleccionButacasActivityFragment) fm.findFragmentByTag(ST_SELECCIONBUTACAS);
		ButacasActivityFragment myFragment2 = (ButacasActivityFragment) fm.findFragmentByTag(ST_BUTACAS);
		PagarActivityFragment myFragment3 = (PagarActivityFragment) fm.findFragmentByTag(ST_PAGAR);
		
		if (myFragment1 != null && myFragment1.isVisible()) {
			return true;		
		}else if(myFragment2 != null && myFragment2.isVisible()){    		
			return true;
		}else if (myFragment3 != null && myFragment3.isVisible()) {
			return true;		
		}
		
	return false;

}

@Override
public void regisrar() {
	// TODO Auto-generated method stub
	cambiarFragment("0", ST_REGISTRO, RegistroActivityFragment.getInstance());
		
}

@Override
public void onConfigurationChanged(Configuration newConfig) {
  super.onConfigurationChanged(newConfig);
 
  FragmentManager fm = getSupportFragmentManager();
  DetallesActivityFragment2 myFragment1 = (DetallesActivityFragment2) fm.findFragmentByTag(ST_DETALLES);
  SeleccionButacasActivityFragment myFragment2 = (SeleccionButacasActivityFragment) fm.findFragmentByTag(ST_SELECCIONBUTACAS);
  ButacasActivityFragment myFragment3 = (ButacasActivityFragment) fm.findFragmentByTag(ST_BUTACAS);
  DetallesCinesActivityFragment myFragment4 = (DetallesCinesActivityFragment) fm.findFragmentByTag(ST_DETALLESCINES);
  
  if(myFragment1 != null && myFragment1.isVisible()) 
		DetallesActivityFragment2.getInstance().configurationChanged();
  else if(myFragment2 != null && myFragment2.isVisible())
	  SeleccionButacasActivityFragment.getInstance().actualizar();  
  else if(myFragment3 != null && myFragment3.isVisible())
	  ButacasActivityFragment.getInstance().actualizar(); 
  else if(myFragment4 != null && myFragment4.isVisible()) 
	  DetallesCinesActivityFragment.getInstance().configurationChanged();
  
	
   
}

@Override
public void onResume(){
	super.onResume();
	
	if(ApplicationStatus.getStatus() == false){
		Intent reLaunchMain=new Intent(this, SplashActivity.class);
		reLaunchMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(reLaunchMain);
		this.finish();
	}
	/*if(LocalyticsManager.localyticsSession() == null){
		LocalyticsManager.newLocalytics(getApplicationContext());
    	LocalyticsManager.localyticsSession().open();
	}*/
	
}



}