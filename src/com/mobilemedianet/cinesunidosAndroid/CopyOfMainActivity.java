package com.mobilemedianet.cinesunidosAndroid;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mobilemedianet.cinesunidosAndroid.activities.AcercaActivityFragment;
import com.mobilemedianet.cinesunidosAndroid.activities.ButacasActivityFragment;
import com.mobilemedianet.cinesunidosAndroid.activities.ComprarActivityFragment;
import com.mobilemedianet.cinesunidosAndroid.activities.ConfirmarActivityFragment;
import com.mobilemedianet.cinesunidosAndroid.activities.DetallesActivityFragment2;
import com.mobilemedianet.cinesunidosAndroid.activities.DetallesCinesActivityFragment;
import com.mobilemedianet.cinesunidosAndroid.activities.ListaActivityFragment;
import com.mobilemedianet.cinesunidosAndroid.activities.ListaCinesActivityFragment;
import com.mobilemedianet.cinesunidosAndroid.activities.LoginActivityFragment;
import com.mobilemedianet.cinesunidosAndroid.activities.PagarActivityFragment;
import com.mobilemedianet.cinesunidosAndroid.activities.RegistroActivityFragment;
import com.mobilemedianet.cinesunidosAndroid.activities.SeleccionButacasActivityFragment;
import com.mobilemedianet.cinesunidosAndroid.managers.ButacasManager;
import com.mobilemedianet.cinesunidosAndroid.managers.ConnectionManager;
import com.mobilemedianet.cinesunidosAndroid.managers.DetallesCompra;
import com.mobilemedianet.cinesunidosAndroid.managers.LoginManager;
import com.mobilemedianet.cinesunidosAndroid.managers.MovieManager;
import com.mobilemedianet.cinesunidosAndroid.managers.PrefManager;
import com.mobilemedianet.cinesunidosAndroid.objects.Movie;
import com.mobilemedianet.cinesunidosAndroid.utilities.Contador;
import com.mobilemedianet.cinesunidosAndroid.utilities.Fechas;
import com.mobilemedianet.cinesunidosAndroid.utilities.Metrics;

import net.londatiga.android.ActionItem;
import net.londatiga.android.QuickAction;

public class CopyOfMainActivity extends FragmentActivity  implements ListaActivityFragment.OnListaSelectedListener,
DetallesActivityFragment2.OnShowTimeSelectedListener, ComprarActivityFragment.OnPagarSelectedListener, 
SeleccionButacasActivityFragment.OnSeleccionButacasListener, PagarActivityFragment.OnPagarListener, 
ButacasActivityFragment.OnButacasListener, ListaCinesActivityFragment.OnListaCinesSelectedListener,
DetallesCinesActivityFragment.OnShowTimeSelectedListener, LoginActivityFragment.OnShowTimeSelectedListener, 
LoginActivityFragment.OnLoginListener{
    
	
	private boolean orient;
	private int indicadordepagina = 0;
	
	private static final int ID_SELECT     = 1;
	private static final int ID_REFRESH   = 2;
	private static final int ID_LOGOUT = 3;
	private static final int ID_CACHE   = 4;
	private static final int ID_ACERCA = 5;
	public static boolean isListaVisible = true;
	public static boolean fromCarrusel = false;
	public static boolean fromDetails = false;
	public static boolean fromCinesFavoritos = false;
	
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
       
    	super.onCreate(savedInstanceState);    	
    	Metrics.setContext(this);    	
    	Fechas.getFecha();
//    	this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
    	requestWindowFeature(Window.FEATURE_NO_TITLE); 
    	ConnectionManager.setContext(this);
    	MovieManager.getInstance().setContext(this);
    	PrefManager.getInstance().setAplicationContext(this);
    	
    	
    	setContentView(R.layout.main_fragment);
    	
    	RelativeLayout home = (RelativeLayout) findViewById(R.id.botonHome);
    	RelativeLayout cines = (RelativeLayout) findViewById(R.id.botonCine);
    	RelativeLayout peliculas = (RelativeLayout) findViewById(R.id.botonPeli);
    	
    	cines.setOnClickListener(OnClickDoSomething(cines));
    	peliculas.setOnClickListener(OnClickDoSomething(peliculas));
    	home.setOnClickListener(OnClickDoSomething(home));
    	generarMenu();
    	
    	   	  	
    	   	
    	
    	
    	if(MovieManager.getInstance().needToRefresh == true){
    		new RefreshTask().execute("");
    		return;
    		
    	}
    	
    	if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT ||
    			fromCarrusel == true)
    		orient = true;
    	else
    		orient = false;
    	Log.i("isListaVisible", "" + isListaVisible);
    	if(isListaVisible && !orient){    		
//    		orient = true;
//    		addDynamicFragment();
    		Log.i("Rotado en oncreate", " ");
    		Intent myIntent = new Intent(this, CarruselActivity.class);
    		myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivityForResult(myIntent, 0);    
            
    		   		
    	}
    }
	
	 View.OnClickListener OnClickDoSomething(final View v)  {
	        return new View.OnClickListener() {
	        	

	        	
	            @Override
				public void onClick(View v) {   
	            	
	            	FragmentManager fm = getSupportFragmentManager();
	            	
	            	ListaActivityFragment pelisFragment = (ListaActivityFragment) fm.findFragmentByTag("lista");
	            	ListaCinesActivityFragment cinesFragment = (ListaCinesActivityFragment) fm.findFragmentByTag("cines");
	            	
	            	
	            	if(v.getId() == R.id.botonCine){
	            		checkTimersOnBack();
	            		
	            		if(cinesFragment != null && cinesFragment.isVisible())
	            			return;
	            		
	            		cambiarFragment("0", "cines", ListaCinesActivityFragment.newInstance());
	            		            		
	            	}else if(v.getId() == R.id.botonPeli){
	            		checkTimersOnBack();
	            		
	            		if(pelisFragment != null && pelisFragment.isVisible())
	            			return;
	            		
//	            		fm.popBackStack("lista", FragmentManager.POP_BACK_STACK_INCLUSIVE);
	            		
	            		cambiarFragment("0", "lista", ListaActivityFragment.getInstance());
//	            		
	            	}else if(v.getId() == R.id.botonHome){
	            		checkTimersOnBack();
	            		isListaVisible = true;
	            		int rootFragment = fm.getBackStackEntryAt(0).getId();
	            	    fm.popBackStack(rootFragment, 0);
	            		
	            		
	            	}
	                 
	            }
	        };
	    }
	
	
	
	
    private void addDynamicFragment() {
        // TODO Auto-generated method stub
//    eliminarBackStacks();     
//  	if(orient){
  		
  		Object viewer = getSupportFragmentManager().findFragmentByTag("lista");
    	Fragment fg = ListaActivityFragment.getInstance();
    	FragmentTransaction  transaction = getSupportFragmentManager().beginTransaction();   
    	
    	
  		if(viewer == null){  			
  			
  			transaction.add(R.id.Layout1, fg, "lista");
  			transaction.addToBackStack("lista");
  			transaction.commit();   			
  		}else{  			
  			transaction.replace(R.id.Layout1, fg, "lista");   			
  			transaction.commit();
  		}
//    		}
//    	else {    		
//    		Object viewer = (Object) getSupportFragmentManager().findFragmentByTag("lista");
//    		DetallesActivityFragment2 viewer2 = (DetallesActivityFragment2) getSupportFragmentManager().findFragmentByTag("detalles");
//        	
//    		Fragment fg = ListaActivityFragment.newInstance();
//        	Fragment fg2 = DetallesActivityFragment2.getInstance(0);
//    		
//    		if(viewer == null){
//    			getSupportFragmentManager().beginTransaction().add(R.id.Layout1, fg, "lista").commit();
//    		}else{
//    			FragmentTransaction  transaction = getSupportFragmentManager().beginTransaction();
//      			transaction.replace(R.id.Layout1, fg, "lista"); 
//      			transaction.commit();
//    		}
//    		
//    		if(viewer2 == null){
//    			getSupportFragmentManager().beginTransaction().add(R.id.Layout2, fg2, "detalles").commit();
//    		}else{
//    			FragmentTransaction  transaction = getSupportFragmentManager().beginTransaction();
//      			transaction.replace(R.id.Layout2, fg2, "detalles");  
//      			transaction.commit();
//    		}
//    			  		
//    	
//    		
//    	}
        
    }


//@Override
//public boolean onCreateOptionsMenu(Menu menu) {
//    MenuInflater menuInflater = getMenuInflater();
//    menuInflater.inflate(R.menu.main, menu);
//
//    // Calling super after populating the menu is necessary here to ensure that the
//    // action bar helpers have a chance to handle this event.
//    return super.onCreateOptionsMenu(menu);
//}

//@Override
//public boolean onOptionsItemSelected(MenuItem item) {
//    switch (item.getItemId()) {
//        case android.R.id.home:
//            Toast.makeText(this, "Tapped home", Toast.LENGTH_SHORT).show();
//            break;
//
//        case R.id.menu_refresh: 	
//        	
//        	final ListaActivityFragment viewer = (ListaActivityFragment) getSupportFragmentManager()
//            .findFragmentByTag("lista");
//        	
//        	if(viewer == null)
//        		return super.onOptionsItemSelected(item);
//        	
////            Toast.makeText(this, "Fake refreshing...", Toast.LENGTH_SHORT).show();
//            getActionBarHelper().setRefreshActionItemState(true);
//            
//            new Thread(new Runnable() {             			
//        		public void run() {
//        			
//        			MovieManager.getInstance().refresh();
//        			getWindow().getDecorView().post(new Runnable() {
//                public void run() {
//                	getActionBarHelper().setRefreshActionItemState(false); 	
//                    viewer.inicializarLista();
//                	
//                }
//              });       			
//        	}}).start(); 
//            
//           
//            
//            break;
//        
//        case R.id.menu_share:
//            Toast.makeText(this, "Tapped share", Toast.LENGTH_SHORT).show();
//            break;
//        
//    }
//    return super.onOptionsItemSelected(item);
//}





@Override
public void onListaSelected(String movieId) {
	// TODO Auto-generated method stub
	MovieManager.getInstance().setIndexSelection(movieId);
	
	//cambiarFragment(movieId, "detalles", DetallesActivityFragment2.getInstance(movieId));
	
	
}



@Override
public void showTimeSelected() {	
	if(PrefManager.getInstance().checkLoginStatus() == false || LoginManager.getInstance().getLoginStatus() == false){
		cambiarFragment("0", "login", LoginActivityFragment.getInstance());
	}else{
		cambiarFragment("0", "comprar", ComprarActivityFragment.getInstance(MovieManager.getInstance().getShowTimeId()));
	}
	
}


@Override
public void PagarSelected() {
	// TODO Auto-generated method stub
	if(DetallesCompra.getInstance().getPremium())
		irAPagar(true);
	else
		irAPagar(false);
		
	
		
	
	
}


private void irAPagar(boolean butacas) {
	if(butacas){
		Contador.getInstance().setTimer(300000);  // 5=300000 minutos para pagar
		cambiarFragment("0", "seleccionbutacas", SeleccionButacasActivityFragment.getInstance());
		}else
			cambiarFragment("0", "pagar", PagarActivityFragment.getInstance());
		
}

@Override
public void irAPagar2(boolean donde) {
	// TODO Auto-generated method stub
	if(donde == true)
		cambiarFragment("0", "pagar", PagarActivityFragment.getInstance());
	else
		cambiarFragment("0", "butacas", ButacasActivityFragment.getInstance());
	
}


@Override
public void irConfirmacion() {
	// TODO Auto-generated method stub
	
	cambiarFragment("0", "confirmar", ConfirmarActivityFragment.getInstance());
	
}

private void cambiarFragment(String index, String tag, Fragment newFragment){
		
	DetallesActivityFragment2 viewer = null;
	
	if(!tag.equals("lista"))
		isListaVisible = false;
	else
		isListaVisible = true;
		
	if(tag.equals("detalles"))
		viewer = (DetallesActivityFragment2) getSupportFragmentManager().findFragmentByTag(tag);

	
    if (viewer == null || !viewer.isInLayout()) {    	
    	
    	FragmentTransaction  transaction = getSupportFragmentManager().beginTransaction();    	
    	    	
//    	if(orient)
    		transaction.replace(R.id.Layout1, newFragment, tag);
//    	else
//    		transaction.replace(R.id.Layout2, newFragment, tag);	    	
    	
    	transaction.addToBackStack(tag);
    	
//    	transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);    	
    	
    	transaction.commit();    	

    } else {
    	viewer.mostrarDetalles(index);    	
      
    }
}

@Override
public void onBackPressed() {
	
    FragmentManager fm = getSupportFragmentManager();
   
    
    if (fm.getBackStackEntryCount() > 1) {
    	
    	checkTimersOnBack();
    	
        fm.popBackStack();
    }else{
    	
    	AlertDialog.Builder alertDialog = new AlertDialog.Builder(this); 
        
        alertDialog.setTitle("CinesUnidos"); 
        
        alertDialog.setMessage("¿Está seguro que desea salir?");
       
        alertDialog.setIcon(R.drawable.icono); 
       
        alertDialog.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
            @Override
			public void onClick(DialogInterface dialog,int which) { 
            	CopyOfMainActivity.this.finish();     
            	android.os.Process.killProcess(android.os.Process.myPid());
            }
        }); 
        
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
			public void onClick(DialogInterface dialog,    int which) {            
            
            dialog.cancel();
            }
        });
 
       
        alertDialog.show();    	

    }
    
    if(indicadordepagina == 1)
    	indicadordepagina = 0;
}


@Override
protected void onDestroy() {
    super.onDestroy();
    
//    MovieManager.getInstance().clean();
}

@Override
protected void onStop(){
	super.onStop();
	
	ListaActivityFragment.delInstance();
	
}

@Override
public void butacasListener() {
	
	
}




@Override
public void onListaCinesSelected(int cineId) {
	
//	MovieManager.getInstance().setIndexSelection(movieId);	
	cambiarFragment(String.valueOf(cineId), "detallescines", DetallesCinesActivityFragment.getInstance(cineId));
	
}



private class RefreshTask extends AsyncTask<String, Void, Integer> {
	 ProgressDialog pd;
	 boolean refresh = false;
	 boolean resultado;	 
	 private Movie pelicula;
	 @Override
	protected void onPreExecute() {
		 
		 setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		 pd = ProgressDialog.show(CopyOfMainActivity.this, "Espere.", "Actualizando listas.", true, false);
		 
		 
   	}
    
	 @Override
	protected Integer doInBackground(String... params) {
		 
		resultado = MovieManager.getInstance().refresh();
		
		if(resultado == false)
			return 0;
		
		if(params[0].equals("refresh"))
			 refresh = true;
		
		MovieManager.getInstance().resetRankingSearch();
		
		 while((pelicula = MovieManager.getInstance().getNextMovieRank(0)) != null){
			 if(pelicula.isInCache())
				 pelicula.setPoster();    			 
		 }
        
        return 0;
    }	    

    @Override
	protected void onPostExecute(Integer result) {
    	
    	
    	if(resultado == false){
    		pd.cancel();
    		Toast.makeText(CopyOfMainActivity.this, "Fallo de conexión.", Toast.LENGTH_SHORT).show();
    		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
    		return;
    		    		
    	}
    	
    	MovieManager.getInstance().needToRefresh = false;	
    	
    	if(refresh == false)
    		addDynamicFragment();
    	else{	
    		FragmentManager fm = getSupportFragmentManager();
    		goHome();
  			
    	}
    	FragmentManager fm = getSupportFragmentManager();    	
    	ListaActivityFragment pelisFragment = (ListaActivityFragment) fm.findFragmentByTag("lista");
    	
    	if(pelisFragment.isVisible())
    		ListaActivityFragment.getInstance().refreshList();
    	else
    		goHome();
    	pd.cancel();
    	setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
    	
        
    }
}

private void generarMenu(){	
	
	ActionItem refreshItem = new ActionItem(ID_REFRESH, "\nActualizar\n", null);
	ActionItem selectCinemaItem = new ActionItem(ID_SELECT, "\nCines Favoritos\n", null);	
	ActionItem logoutItem = new ActionItem(ID_LOGOUT, "\nCerrar Sesión\n", null);
	ActionItem cacheItem = new ActionItem(ID_CACHE, "\nBorrar Caché\n", null);
	ActionItem acercaDe = new ActionItem(ID_ACERCA, "\nAcerca de\n", null);
	
	
	
	
	final QuickAction quickAction = new QuickAction(this, QuickAction.VERTICAL);
	
	quickAction.addActionItem(refreshItem);
	quickAction.addActionItem(selectCinemaItem);
	quickAction.addActionItem(logoutItem);
	quickAction.addActionItem(cacheItem);
	quickAction.addActionItem(acercaDe);
	
	
	quickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {			
		@Override
		public void onItemClick(QuickAction source, int pos, int actionId) {				
			ActionItem actionItem = quickAction.getActionItem(pos);
			Intent myIntent; 
			FragmentManager fm = getSupportFragmentManager();
			//here we can filter which action item was clicked with pos or actionId parameter
			if (actionId == ID_REFRESH) {
				checkTimersOnBack();
				if(PrefManager.getInstance().isAnyCinemaChecked() == false){
					mensajeError("No hay cines favoritos, por favor seleccione al menos un cine favorito.");
			        return;
				}								
				MovieManager.getInstance().needToRefresh = true;
				new RefreshTask().execute("");				
//				myIntent = new Intent(MainActivity.this, DummyActivity.class);
//	            startActivityForResult(myIntent, 0); 
			} else if (actionId == ID_CACHE) {		
				checkTimersOnBack();
				goHome();
				ConnectionManager.getInstance().clearCache(null);
				Toast.makeText(getApplicationContext(), "Caché eliminado", Toast.LENGTH_SHORT).show();
			} else if (actionId == ID_LOGOUT) {
				checkTimersOnBack();
				goHome();
				PrefManager.getInstance().cleanUserInfo();
				LoginManager.getInstance().setLoginStatus(false);				
			} else if (actionId == ID_SELECT) {	
				checkTimersOnBack();
				myIntent = new Intent(CopyOfMainActivity.this, SeleccionCiudadesActivity.class);
				myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
	            startActivityForResult(myIntent, 0);    
	        } else if (actionId == ID_ACERCA) {
	        	cambiarFragment("0", "acercade", AcercaActivityFragment.getInstance());
	        } 
			
		}
	});	
	
	RelativeLayout preferencias = (RelativeLayout) findViewById(R.id.botonMenu); 
	preferencias.setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View v) {
			quickAction.show(v);
		}
	});
	
	
}

@Override
protected void onResume(){
	super.onResume();
	
	
	if(fromCinesFavoritos){
		fromCinesFavoritos = false;
		
		if(MovieManager.getInstance().needToRefresh == true){
    		new RefreshTask().execute("");
    		return;
    		
    	}
	}
		
	
	if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
		orient = true;
	else
		orient = false;
	
		
	
	if(!orient && isListaVisible){		
		Intent myIntent = new Intent(this, CarruselActivity.class);
		myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(myIntent, 0);    
	}else if(orient && isListaVisible || fromCarrusel == true){
		
		addDynamicFragment();
		
		if(fromCarrusel == true && !fromDetails){
			fromCarrusel = false;				
			String movieId = MovieManager.getInstance().getIndexSelection();
			//cambiarFragment(movieId, "detalles", DetallesActivityFragment2.getInstance(movieId));
			}
		else if(fromDetails == false){
			isListaVisible = true;
			
		}
			
	}
	
	
}

private void goHome(){
	
	int rootFragment = getSupportFragmentManager().getBackStackEntryAt(0).getId();
	getSupportFragmentManager().popBackStack(rootFragment, 0);
}

private void mensajeError(String error){
	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setTitle("CinesUnidos");
    builder.setMessage(error);       
    builder.setPositiveButton("OK",null);
    builder.create();
    builder.setIcon(R.drawable.icono);
    builder.show(); 
}

private void checkTimersOnBack(){
	FragmentManager fm = getSupportFragmentManager();
	SeleccionButacasActivityFragment myFragment1 = (SeleccionButacasActivityFragment) fm.findFragmentByTag("seleccionbutacas");
	ButacasActivityFragment myFragment2 = (ButacasActivityFragment) fm.findFragmentByTag("butacas");
	DetallesActivityFragment2 myFragment3 = (DetallesActivityFragment2) fm.findFragmentByTag("detalles");
	PagarActivityFragment myFragment4 = (PagarActivityFragment) fm.findFragmentByTag("pagar");
	
	if (myFragment1 != null && myFragment1.isVisible()) {
		SeleccionButacasActivityFragment.getInstance().cancelTimer();
		ButacasManager.getInstance().cancelOrder(this);    		
	}else if(myFragment2 != null && myFragment2.isVisible()){    		
		ButacasActivityFragment.getInstance().cancelTimer();  
		RelativeLayout actionbar = (RelativeLayout) findViewById(R.id.actionBar);
        actionbar.setVisibility(View.VISIBLE);
//		((TextView) findViewById(R.id.actBarButtonText)).setText("");    		
//		((RelativeLayout)findViewById(R.id.genericButtonActionBar)).setClickable(false);
	}else if(myFragment3 != null && myFragment3.isVisible()){
		isListaVisible = true;
	}else if (myFragment4 != null && myFragment4.isVisible()) {
		PagarActivityFragment.getInstance().cancelTimer();		
	}
}

@Override
public void regisrar() {
	// TODO Auto-generated method stub
	cambiarFragment("0", "registro", RegistroActivityFragment.getInstance());
	
	
}






}