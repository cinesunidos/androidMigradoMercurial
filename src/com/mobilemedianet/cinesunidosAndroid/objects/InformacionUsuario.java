package com.mobilemedianet.cinesunidosAndroid.objects;





public class InformacionUsuario  
{
	private String birthdate = "";
	private String sex = "";
	private String login = "";
	private String name = "";
	private String lastName = "";
	private String phone = "";
	private String secureId = "";
	private String externalId = "";
	private String token = "";
	private String codCiudadFavorita = "";	
	private boolean status;
	private String text = "";
	
	static private InformacionUsuario instance = null;
	
	static public InformacionUsuario getInstance(){
		
		if(instance == null)
			instance = new InformacionUsuario();
		
		return instance;
	}
	
	
	
	
	public void setText(String a){
		text = a;
	}
	
	public String getText(){
		return text;
	}
	
	public void setStatus(boolean a){
		status = a;
	}
	
	public boolean getStatus(){
		return status;
	}
	

	public String getCodigoCiudad() 
	{
		return codCiudadFavorita;
	}

	public void setCodigoCiudad(String codigoCiudad) {
		this.codCiudadFavorita = codigoCiudad;
	}

	

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getSecureId() {
		return secureId;
	}

	public void setSecureId(String secureId) {
		this.secureId = secureId;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	
	
}
