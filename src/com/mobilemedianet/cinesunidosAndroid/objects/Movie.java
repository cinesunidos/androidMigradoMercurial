package com.mobilemedianet.cinesunidosAndroid.objects;

import android.graphics.drawable.Drawable;

import com.mobilemedianet.cinesunidosAndroid.R;
import com.mobilemedianet.cinesunidosAndroid.managers.ConnectionManager;
import com.mobilemedianet.cinesunidosAndroid.managers.MovieManager;
import com.mobilemedianet.cinesunidosAndroid.utilities.Fechas;

import java.io.InputStream;
import java.util.ArrayList;


public class Movie 
{
	private String id;
	private int index;
	private String originalTitle = "";
	private String spanishTitle = "";
	private String language = "";
	private String Runtime = "";	
	private String officialSite = "";
	private String director = "";
	private String producer = "";
	private String starring = "";
	private String sinopsis = "";
	private String company = "";
	private String genere = "";
	private String writer = "";
	private String FirstExhibit = "";
	private String PreSale = "";
	private String HO = "";
	private String premierDate = "";
	private int ranking;
	private int duration;
	private boolean for3D;
	private boolean premier;
	private String durationString = "";
	private Drawable poster;
	private ArrayList <Theaters> salas;
	private boolean detailsLoaded = false;
	private boolean posterIsLoaded = false;
	private String FirstExhibitYear;
	private String FirstExhibitMonth;
	private String FirstExhibitDay;
	
	
	
//	http://ws.cinesunidos.com/AndroidAfiches/afiches_155x171/afiche_

//	private static String posterUrl = "http://ws.cinesunidos.com/BlackberryAfiches/afiches_95x105/afichebb_";
	private static String posterUrlID = "http://" + ConnectionManager.wsCinesUnidos + "/AndroidAfiches/afiches_155x171/afiche_";
	//private static String posterUrlHO = "http://imagenes.cinesunidos.com/ImageService.svc/GetMovieImage/";


	private static String posterUrlHO = "http://az693035.vo.msecnd.net/poster/";

	//private Date datePremiere, dateExhibition;
	//private Bitmap posterSmall, posterLarge;
	
	private void parseFirstExhibit(){
		//String dum[] = FirstExhibit.split("T");
		String fecha[] = FirstExhibit.split("-");
		FirstExhibitYear = fecha[0];
		FirstExhibitMonth = Fechas.meses1[Integer.parseInt(fecha[1]) - 1];
		FirstExhibitDay = fecha[2];		
		
	}
	
	public void setPremier(String a){
		premierDate = a;
	}
	
	public String getPremier(){
		return premierDate;
	}
	
	public String getFirstExhibitYear(){
		return FirstExhibitYear;
	}
	
	public String getFirstExhibitMonth(){
		return FirstExhibitMonth;
	}
	
	public String getFirstExhibitDay(){
		return FirstExhibitDay;
	}
	
	public void setPreSale(String a){
		PreSale = a;
	}
	
	public String getPreSale(){
		return PreSale;
	}
	
	public void setHO(String a){
		HO = a;
	}
	
	public String getHO(){
		return HO;
	}
	
	public void setFirstExhibit(String a){
		FirstExhibit = a;
		parseFirstExhibit();
	}
			
	public String getFirstExhibit(){
		return FirstExhibit;
	}
	
	public boolean detailsLoaded(){
		return detailsLoaded;
	}
	
	public boolean posterlsLoaded(){
		return posterIsLoaded;
	}
	
	public void setPosterLoaded(boolean a){
		posterIsLoaded = a;		
	}
	
	
	public void setDetailsLoaded(boolean a){
		detailsLoaded = a;		
	}

	public void setRuntime(String a){
		this.Runtime = a;
	}
	
	public String getRuntime(){
		return this.Runtime;
	}
	
	public void setOfficialSite(String a){
		this.officialSite = a;
	}
	
	public void setDirector(String a){
		this.director = a;
	}
	
	public void setProducer(String a){
		this.producer = a;
	}
	
	public void setStarring(String a){
		this.starring = a;		
	}
	
	public void setSinopsis(String a){
		this.sinopsis = a;
	}
	
	public void setCompany(String a){
		this.company = a;
	}
	
	public void setGenere(String a){
		this.genere = a;
	}
	
	public void setWriter(String a){
		this.writer = a;
	}
	
	
	
	public String getOfficialSite(){
		return this.officialSite;
	}
	
	public String getDirector(){
		return this.director;
	}
	
	public String getProducer(){
		return this.producer;
	}
	
	public String getStarring(){
		return this.starring;		
	}
	
	public String getSinopsis(){
		return this.sinopsis;
	}
	
	public String getCompany(){
		return this.company;
	}
	
	public String getGenere(){
		return this.genere;
	}
	
	public String getWriter(){
		return this.writer;
	}
	
	
	
	public Movie(String id)
	{
		super();
		this.id = id;
		for3D = false;
		premier = false;
		salas = new ArrayList<Theaters>();
		//index = i;
		
	}
	
	public boolean setPoster(){
		
		if(index != 0)
			return false;
		
		InputStream file;		
		String dum;
		
		if(HO == ""){
			dum = String.format("%07d", id);
			file = ConnectionManager.getInstance().getInputStreamFrom(posterUrlHO + dum, "afiche_" + dum + ".jpg");
		}else{
			file = ConnectionManager.getInstance().getInputStreamFrom(posterUrlHO+HO +".jpg", HO +".jpg");
		}
		
		
					
		if(file == null)
			poster = MovieManager.getInstance().getContext().getResources().getDrawable(R.drawable.image_na);
		else
			poster = Drawable.createFromStream(file, "src");
		
		setPosterLoaded(true);
		
		return true;
	
		
	}
	
	public boolean isInCache(){
		//String dum = String.format("%07d", id);
		if(ConnectionManager.getInstance().testCacheFile("afiche_" + HO + ".jpg") == true)
			return true;
		else 
			return false;
	}
	

	
	public Drawable getPoster(){
		return poster;
	}
	
	
	
	public void addSala(Theaters a){
		salas.add(a);
	}
	
	public ArrayList <Theaters> getSalas(){
		return salas;
	}
	
	public Movie(String originalTitle, String spanishTitle) 
	{
		super();
		this.originalTitle = originalTitle;
		this.spanishTitle = spanishTitle;
	}


	public String getOriginalTitle() {
		return originalTitle;
	}
	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}
	public String getSpanishTitle() {
		return spanishTitle;
	}
	public void setSpanishTitle(String spanishTitle) {
		this.spanishTitle = spanishTitle;
	}
	
	
	public int getDuration()
	{
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}	
	
	public boolean isFor3D() {
		return for3D;
	}

	public void setFor3D(boolean is3d) {
		for3D = is3d;
	}

	public boolean isPremier() {
		return premier;
	}

	public void setPremier(boolean premier) {
		this.premier = premier;
	}	

	public int getRanking() {
		return ranking;
	}

	public void setRanking(int ranking) {
		this.ranking = ranking;
	}

	public String getDurationString() {
		return durationString;
	}

	public void setDurationString(String durationString) {
		this.durationString = durationString;
	}
	
}
