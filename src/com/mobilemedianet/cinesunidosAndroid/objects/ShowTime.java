package com.mobilemedianet.cinesunidosAndroid.objects;

import java.util.Calendar;



public class ShowTime {

	int showTimeId;
	int dia, mes, ano, hora, minutos, segundos;
	String AM_PM;
	String fechacompleta;
	
	static String []meses = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", 
		"Octubre", "Noviembre", "Diciembre"};
	
	static String []semana = {"Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"};
	
	public ShowTime(){
		showTimeId = 0;
	}
	
	public String getFecha(){
		return fechacompleta;
	}
	
	public String getFechaLarga(){		
		
		return String.valueOf(dia) + " de " + meses[mes - 1] + " a las "
		+ getHoraFuncion();		
		
	}
	
	public void setShowTimeId(int i){
		showTimeId = i;
	}
	
	public int getShowTimeId(){
		return showTimeId;
	}
	
	public void setHorario(String a){
		String fecha, hora, meridiano;
		String[] temp;
		
		fechacompleta = a;
		
		temp = a.split(" ");
		
		fecha = temp[0];
		hora = temp[1];
		meridiano = temp[2];
		
		temp = fecha.split("-");
		
		mes = Integer.parseInt(temp[1]);
		dia = Integer.parseInt(temp[2]);
		ano = Integer.parseInt(temp[0]);
		
		temp = hora.split(":");
		
		this.hora = Integer.parseInt(temp[0]);
		minutos = Integer.parseInt(temp[1]);
		segundos = Integer.parseInt(temp[2]);
		AM_PM = meridiano;

	}
	
	public int getDia(){				
		return dia;
	}	
	public int getMes(){
		return mes;
	}
	public int getAno(){
		return ano;
	}
	public int getHora(){
		return hora;
	}
	public int getMinutos(){
		return minutos;
	}
	public int getSegundos(){
		return segundos;
	}
	public String getMeridiano(){
		return AM_PM;
	}
	public String getHoraFuncion(){
		
		return String.format("%02d:%02d", hora, minutos) + AM_PM;		
		
	}
	public String getDiaSemana(){
				
		Calendar calendar = Calendar.getInstance();
		 
		calendar.set(ano, mes - 1, dia);
	     
		int day = calendar.get(Calendar.DAY_OF_WEEK);

		return semana[day - 1];
	}
	
	
	
}
