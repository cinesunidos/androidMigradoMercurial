package com.mobilemedianet.cinesunidosAndroid.objects;

import java.util.Hashtable;
import java.util.Vector;


public  class  Cines_old 
{
	//public static final Hashtable cinesCaracas = new Hashtable();
	public static final Hashtable cinesValencia = new Hashtable();
	public static final Hashtable cinesMaracay = new Hashtable();
	public static final Hashtable cinesMaracaibo = new Hashtable();
	public static final Hashtable cinesBarquisimeto = new Hashtable();
	
	
	
	public static final String[] CINES_CARACAS =  {"1008", "1027", "1001", "1026", "1020", "1009", "1002", "1005"};
	public static final String[] NOMBRES_CARACAS =  {"El Marqués", "Líder", "Los Naranjos", "Millennium", "Galerías Ávila", "Galerías Paraíso", "Metrocenter", "Sambil Caracas"};
	
	public static final String[] CINES_VALENCIA =  {"1013", "1011", "1014"};
	public static final String[] NOMBRES_VALENCIA =  {"La Granja", "Metrópolis", "Sambil Valencia"};
	
	public static final String[] CINES_MARACAY =  {"1015", "1010"};
	public static final String[] NOMBRES_MARACAY =  {"Hyper Jumbo", "Las Américas"};
	
	public static final String[] CINES_MARACAIBO =  {"1016", "1017"};
	public static final String[] NOMBRES_MARACAIBO =  {"Centro Sur", "Sambil Maracaibo"};
	
	public static final String[] CINES_BARQUISIMETO =  {"1006", "1025"};
	public static final String[] NOMBRES_BARQUISIMETO =  {"Las Trinitarias", "Sambil Barquisimeto"};
	
	public static final String[] CINES_GUATIRE = {"1003"};
	public static final String[] NOMBRES_GUATIRE = {"Guatire Plaza"};
	
	public static final String[] CINES_COSTAAZUL = {"1022"};
	public static final String[] NOMBRES_COSTAAZUL = {"Costa Azul"};
	
	public static final String[] CINES_MATURIN = {"1007"};
	public static final String[] NOMBRES_MATURIN = {"Petroriente"};
	
	public static final String[] CINES_PORLAMAR = {"1021"};
	public static final String[] NOMBRES_PORLAMAR = {"Sambil Margarita"};
	
	public static final String[] CINES_PUERTOLACRUZ = {"1019"};
	public static final String[] NOMBRES_PUERTOLACRUZ = {"Regina"};
	
	public static final String[] CINES_PUERTOORDAZ = {"1023"};
	public static final String[] NOMBRES_PUERTOORDAZ = {"Orinokia"};
	
	public static final String[] CINES_SANCRISTOBAL = {"1024"};
	public static final String[] NOMBRES_SANCRISTOBAL = {"San Cristóbal"};
	
	static
	{

	}
	
	
	public static int getCantidadCines(String ciudad)
	{
		if ("CCS".equals(ciudad)){
			return CINES_CARACAS.length;
		}else if ("VAL".equals(ciudad)){
			return CINES_VALENCIA.length;
		}else if ("MCA".equals(ciudad)){
			return CINES_MARACAY.length;
		}else if ("MBO".equals(ciudad)){
			return CINES_MARACAIBO.length;
		}else if ("BQM".equals(ciudad)){
			return CINES_BARQUISIMETO.length;
		}else if ("GUA".equals(ciudad)){
			return CINES_GUATIRE.length;
		}else if ("COR".equals(ciudad)){
			return CINES_COSTAAZUL.length;
		}else if ("MAT".equals(ciudad)){
			return CINES_MATURIN.length;
		}else if ("POR".equals(ciudad)){
			return CINES_PORLAMAR.length;
		}else if ("PLC".equals(ciudad)){
			return CINES_PUERTOLACRUZ.length;
		}else if ("PDZ".equals(ciudad)){
			return CINES_PUERTOORDAZ.length;
		}else if ("SCR".equals(ciudad)){
			return CINES_SANCRISTOBAL.length;
		}
		return 0;
	}
	
	
	public static Vector getCinesRestantes(String ciudad)
	{
		Vector cinesRestantes = new Vector();
//		Vector cinesFavoritos = CacheCines.getInstance().getCinesFavoritos(); 
		String[] codigosCines = new String[0];
		if ("CCS".equals(ciudad)){
			codigosCines = CINES_CARACAS;
		}else if ("VAL".equals(ciudad)){
			codigosCines =  CINES_VALENCIA;
		}else if ("MCA".equals(ciudad)){
			codigosCines =  CINES_MARACAY;
		}else if ("MBO".equals(ciudad)){
			codigosCines =  CINES_MARACAIBO;
		}else if ("BQM".equals(ciudad)){
			codigosCines =  CINES_BARQUISIMETO;
		}else if ("GUA".equals(ciudad)){
			codigosCines =  CINES_GUATIRE;
		}else if ("COR".equals(ciudad)){
			codigosCines =  CINES_COSTAAZUL;
		}else if ("MAT".equals(ciudad)){
			codigosCines =  CINES_MATURIN;
		}else if ("POR".equals(ciudad)){
			codigosCines =  CINES_PORLAMAR;
		}else if ("PLC".equals(ciudad)){
			codigosCines =  CINES_PUERTOLACRUZ;
		}else if ("PDZ".equals(ciudad)){
			codigosCines =  CINES_PUERTOORDAZ;
		}else if ("SCR".equals(ciudad)){
			codigosCines =  CINES_SANCRISTOBAL;
		}
		
		for (int i = 0; i < codigosCines.length; i++)
		{
//			if (!cinesFavoritos.contains(codigosCines[i]))
//			{
				cinesRestantes.addElement(codigosCines[i]);
//			}
		}
		return cinesRestantes;		
	}
	
	
	public static final int CARACAS = 0;
	public static final int VALENCIA = 1;
	public static final int MARACAY = 2;
	public static final int MARACAIBO = 3;
	public static final int BARQUISIMETO = 4;
	
//	public static Enumeration getNombresCines(int opcion)
//	{
//		switch (opcion) 
//		{
//			case CARACAS:
//				return cinesCaracas.keys();
//			case VALENCIA:
//				return cinesValencia.keys();
//			case MARACAY:
//				return cinesMaracay.keys();
//			case MARACAIBO:
//				return cinesMaracaibo.keys();
//			case BARQUISIMETO:
//				return cinesBarquisimeto.keys();
//			default:
//				return null;
//		}
//	}
	
//	public static String getCodigoCine(int opcion, String cine)
//	{
//		switch (opcion) 
//		{
//			case CARACAS:
//				return (String)cinesCaracas.get(cine);
//			case VALENCIA:
//				return (String)cinesValencia.get(cine);
//			case MARACAY:
//				return (String)cinesMaracay.get(cine);
//			case MARACAIBO:
//				return (String)cinesMaracaibo.get(cine);
//			case BARQUISIMETO:
//				return (String)cinesBarquisimeto.get(cine);
//			default:
//				return "";
//		}
//	}
	
	

}
