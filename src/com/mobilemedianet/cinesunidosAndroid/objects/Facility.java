package com.mobilemedianet.cinesunidosAndroid.objects;



public class Facility 
{
	int id;
	String name;
	
	public Facility(int id) {
		super();
		this.id = id;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
