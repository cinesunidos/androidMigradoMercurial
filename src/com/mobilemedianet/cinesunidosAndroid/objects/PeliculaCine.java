package com.mobilemedianet.cinesunidosAndroid.objects;

public class PeliculaCine {
	
	private int cineId;
	private String peliculaId;
	
	private Theaters salas = null;
	
	public PeliculaCine(int cine, String pelicula){
		cineId = cine;
		peliculaId = pelicula;
		
	}
	
	public void addSala(Theaters sala){
		salas = sala;		
	}
	
	public Theaters getSalas(){		
		
		return salas;
	}
	
	public String getPeliculaId(){
		return peliculaId;
	}
	

}
