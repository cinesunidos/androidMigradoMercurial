package com.mobilemedianet.cinesunidosAndroid.objects;

import java.util.ArrayList;
import java.util.List;

public class Theaters {
	
	private int cineId;
	private int sala;
	private String nombreSala;
	private String censura;
	private List<ShowTime> shows;
	
	
	public Theaters(){
		cineId = sala = 0;
		shows = new ArrayList<ShowTime>();
	}
	
	public void addShowTime(ShowTime a){
		shows.add(a);
	}
	
	public List<ShowTime> getShowTimes(){
		return shows;
	}
	
	
	public void setCineId(int i){
		cineId = i;
	}
	
	public int getCineId(){
		return cineId;
	}
	
	public void setSalaId(int i){
		sala = i;
	}
	
	public int getSalaId(){
		return sala;
	}
	
	public void setCensura(String s){
		censura = s;
	}
	
	public String getCensura(){
		return censura;
	}

	public String getNombreSala() {
		return nombreSala;
	}

	public void setNombreSala(String nombreSala) {
		this.nombreSala = nombreSala;
	}
}
