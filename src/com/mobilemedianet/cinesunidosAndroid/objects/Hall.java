package com.mobilemedianet.cinesunidosAndroid.objects;



public class Hall 
{
	int id, seats;
	String name;
	boolean premium;
	boolean seleccion_asiento;
	
	
	
	public Hall(int id) {		
		this.id = id;
		premium = false;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getSeats() {
		return seats;
	}
	public void setSeats(int seats) {
		this.seats = seats;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isPremium() {
		return premium;
	}
	public void setPremium(boolean premium) {
		this.premium = premium;
	}
	public void setSeleccionAsiento(boolean a){
		this.seleccion_asiento = a;
	}
	public boolean getSeleccionAsiento(){
		return this.seleccion_asiento;
	}
	

	
}
