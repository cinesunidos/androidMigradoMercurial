package com.mobilemedianet.cinesunidosAndroid.objects;



public  class  Cines 
{
	
	public static int  nombre = 1;
	public static int  sala = 0;
	
	
	private static  String [][] CINES_CARACAS = {{"1008", "1027", "1001", "1026", "1020", "1009", "1002", "1005", "1028"},
			{"El Marqués", "Líder", "Los Naranjos", "Millennium", "Galerías Ávila", "Galerías Paraíso", "Metrocenter", "Sambil Caracas", "VISTA2PIR"}};
	
	private static  String [][] CINES_VALENCIA = {{"1013", "1011", "1014"},{"La Granja", "Metrópolis", "Sambil Valencia"}};
	
	private static  String [][] CINES_MARACAY = {{"1015", "1010"},{"Hyper Jumbo", "Las Américas"}};
	
	private static  String [][] CINES_MARACAIBO = {{"1016", "1017"}, {"Centro Sur", "Sambil Maracaibo"}};
	
	private static  String [][] CINES_BARQUISIMETO = {{"1006", "1025"}, {"Las Trinitarias", "Sambil Barquisimeto"}};
	
	private static  String [][] CINES_GUATIRE = {{"1003"}, {"Guatire Plaza"}};
	

	
	private static  String [][] CINES_MATURIN = {{"1007"}, {"Petroriente"}};
	
	private static  String [][] CINES_PORLAMAR = {{"1021"}, {"Sambil Margarita"}};
	
	private static  String [][] CINES_PUERTOLACRUZ = {{"1019"}, {"Regina"}};
	
	private static  String [][] CINES_PUERTOORDAZ = {{"1023"}, {"Orinokia"}};
	
	private static  String [][] CINES_SANCRISTOBAL = {{"1024"}, {"San Cristóbal"}};
	
//	public String [][] getCinesCiudad(String ciudad){
//
//	if(ciudad.equalsIgnoreCase("Caracas") || ciudad.equalsIgnoreCase("CCS") || ciudad.equalsIgnoreCase("CCSE") || ciudad.equalsIgnoreCase("CCSO")){
//		return CINES_CARACAS;
//	}else if(ciudad.equalsIgnoreCase("Valencia") || ciudad.equalsIgnoreCase("VAL")){
//		return CINES_VALENCIA;
//	}else if(ciudad.equalsIgnoreCase("Maracay") || ciudad.equalsIgnoreCase("MCA")){
//		return CINES_MARACAY;
//	}else if(ciudad.equalsIgnoreCase("Maracaibo") || ciudad.equalsIgnoreCase("MBO")){
//		return CINES_MARACAIBO;
//	}else if(ciudad.equalsIgnoreCase("Barquisimeto") || ciudad.equalsIgnoreCase("BQM")){
//		return CINES_BARQUISIMETO;
//	}else if(ciudad.equalsIgnoreCase("Guatire") || ciudad.equalsIgnoreCase("GUA")){
//		return CINES_GUATIRE;
//	}else if(ciudad.equalsIgnoreCase("Maturin") || ciudad.equalsIgnoreCase("MAT")){
//		return CINES_MATURIN;
//	}else if(ciudad.equalsIgnoreCase("Porlamar") || ciudad.equalsIgnoreCase("POR")){
//		return CINES_PORLAMAR;
//	}else if(ciudad.equalsIgnoreCase("PuertoLaCruz") || ciudad.equalsIgnoreCase("PLC")){
//		return CINES_PUERTOLACRUZ;
//	}else if(ciudad.equalsIgnoreCase("PuertoOrdaz") || ciudad.equalsIgnoreCase("PDZ")){
//		return CINES_PUERTOORDAZ;
//	}else if(ciudad.equalsIgnoreCase("SanCristobal") || ciudad.equalsIgnoreCase("SCR")){
//		return CINES_SANCRISTOBAL;
//	}
//		return null;
//
//	}
	
	public static String [] getCineById(String id){
		
		String []result = {"",""};
		
		for(int i = 0; i < CINES_CARACAS[0].length; i++){
			
			if(CINES_CARACAS[0][i].equalsIgnoreCase(id)){
				result[sala] = CINES_CARACAS[0][i];
				result[nombre] = CINES_CARACAS[1][i];
				return result;
			}		

			
		}
		for(int i = 0; i < CINES_VALENCIA[0].length; i++){
			
			if(CINES_VALENCIA[0][i].equalsIgnoreCase(id)){
				result[sala] = CINES_VALENCIA[0][i];
				result[nombre] = CINES_VALENCIA[1][i];
				return result;
			}		

			
		}
		for(int i = 0; i < CINES_MARACAY[0].length; i++){
			
			if(CINES_MARACAY[0][i].equalsIgnoreCase(id)){
				result[sala] = CINES_MARACAY[0][i];
				result[nombre] = CINES_MARACAY[1][i];
				return result;
			}	
			
		}
		for(int i = 0; i < CINES_MARACAIBO[0].length; i++){
			
			if(CINES_MARACAIBO[0][i].equalsIgnoreCase(id)){
				result[sala] = CINES_MARACAIBO[0][i];
				result[nombre] = CINES_MARACAIBO[1][i];
				return result;
			}	
	
		}
		for(int i = 0; i < CINES_BARQUISIMETO[0].length; i++){
			
			if(CINES_BARQUISIMETO[0][i].equalsIgnoreCase(id)){
				result[sala] = CINES_BARQUISIMETO[0][i];
				result[nombre] = CINES_BARQUISIMETO[1][i];
				return result;
			}	
	
		}
		for(int i = 0; i < CINES_GUATIRE[0].length; i++){
			
			if(CINES_GUATIRE[0][i].equalsIgnoreCase(id)){
				result[sala] = CINES_GUATIRE[0][i];
				result[nombre] = CINES_GUATIRE[1][i];
				return result;
			}	
	
		}

		for(int i = 0; i < CINES_MATURIN[0].length; i++){
			
			if(CINES_MATURIN[0][i].equalsIgnoreCase(id)){
				result[sala] = CINES_MATURIN[0][i];
				result[nombre] = CINES_MATURIN[1][i];
				return result;
			}	
	
		}
		for(int i = 0; i < CINES_PORLAMAR[0].length; i++){
			
			if(CINES_PORLAMAR[0][i].equalsIgnoreCase(id)){
				result[sala] = CINES_PORLAMAR[0][i];
				result[nombre] = CINES_PORLAMAR[1][i];
				return result;
			}	
	
		}
		for(int i = 0; i < CINES_PUERTOLACRUZ[0].length; i++){
			
			if(CINES_PUERTOLACRUZ[0][i].equalsIgnoreCase(id)){
				result[sala] = CINES_PUERTOLACRUZ[0][i];
				result[nombre] = CINES_PUERTOLACRUZ[1][i];
				return result;
			}	
	
		}
		for(int i = 0; i < CINES_PUERTOORDAZ[0].length; i++){
			
			if(CINES_PUERTOORDAZ[0][i].equalsIgnoreCase(id)){
				result[sala] = CINES_PUERTOORDAZ[0][i];
				result[nombre] = CINES_PUERTOORDAZ[1][i];
				return result;
			}	
	
		}
		for(int i = 0; i < CINES_SANCRISTOBAL[0].length; i++){
			
			if(CINES_SANCRISTOBAL[0][i].equalsIgnoreCase(id)){
				result[sala] = CINES_SANCRISTOBAL[0][i];
				result[nombre] = CINES_SANCRISTOBAL[1][i];
				return result;
			}	
			
		}
	
		return null;
		
	}
	
	public static String getZonByCineId(String id){
		
		for(int i = 0; i < CINES_CARACAS[0].length; i++){
			
			if(CINES_CARACAS[0][i].equalsIgnoreCase(id)){				
				return "CCSE,CCSO";
			}		

			
		}
		for(int i = 0; i < CINES_VALENCIA[0].length; i++){
			
			if(CINES_VALENCIA[0][i].equalsIgnoreCase(id)){
				
				return "VAL";
			}		

			
		}
		for(int i = 0; i < CINES_MARACAY[0].length; i++){
			
			if(CINES_MARACAY[0][i].equalsIgnoreCase(id)){
				
				return "MCA";
			}	
			
		}
		for(int i = 0; i < CINES_MARACAIBO[0].length; i++){
			
			if(CINES_MARACAIBO[0][i].equalsIgnoreCase(id)){				
				return "MBO";
			}	
	
		}
		for(int i = 0; i < CINES_BARQUISIMETO[0].length; i++){
			
			if(CINES_BARQUISIMETO[0][i].equalsIgnoreCase(id)){
				
				return "BQM";
			}	
	
		}
		for(int i = 0; i < CINES_GUATIRE[0].length; i++){
			
			if(CINES_GUATIRE[0][i].equalsIgnoreCase(id)){
				
				return "GUA";
			}	
	
		}

		for(int i = 0; i < CINES_MATURIN[0].length; i++){
			
			if(CINES_MATURIN[0][i].equalsIgnoreCase(id)){
				
				return "MAT";
			}	
	
		}
		for(int i = 0; i < CINES_PORLAMAR[0].length; i++){
			
			if(CINES_PORLAMAR[0][i].equalsIgnoreCase(id)){
				
				return "POR";
			}	
	
		}
		for(int i = 0; i < CINES_PUERTOLACRUZ[0].length; i++){
			
			if(CINES_PUERTOLACRUZ[0][i].equalsIgnoreCase(id)){
				
				return "PLC";
			}	
	
		}
		for(int i = 0; i < CINES_PUERTOORDAZ[0].length; i++){
			
			if(CINES_PUERTOORDAZ[0][i].equalsIgnoreCase(id)){
				
				return "PDZ";
			}	
	
		}
		for(int i = 0; i < CINES_SANCRISTOBAL[0].length; i++){
			
			if(CINES_SANCRISTOBAL[0][i].equalsIgnoreCase(id)){
				
				return "SCR";
			}	
			
		}
	
		
		
		
		return null;
		
	}
	
	

}
