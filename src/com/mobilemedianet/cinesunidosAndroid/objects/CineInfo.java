package com.mobilemedianet.cinesunidosAndroid.objects;

public class CineInfo {
	
	private int cineId;
	private String name;
	private String city;

	private boolean premium = false;
	
	
	public void setCineId(int id){
		cineId = id;
	}
	
	public int getCineId(){
		return cineId;		
	}
	
	public void setPremium(boolean i){
		premium = i;
	}
	
	public boolean getPremium(){
		return premium;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

}
