package com.mobilemedianet.cinesunidosAndroid.objects;

public class Butaca 
{
	int x, y;
	
	public static final int DISPONIBLE = 0;
	public static final int OCUPADA = 1;
	public static final int SELECCIONADA = 2;
	public static final int AMIGO = 8;
	public static final int PASILLO = 9;

	public static final int COLOR_DISPONIBLE = 0x5fbc00;
	public static final int COLOR_SELECCIONADA = 0x5e5ee5;
	public static final int COLOR_OCUPADA = 0xe60000;
	public static final int COLOR_PASILLO = 0x000000;
	public static final int COLOR_AMIGO = 0xe89300;
	
	//private String nombreButaca = "";
	private String letraButaca = "";
	private String numeroButaca = "";
	private String filaButaca = "";
	private String areaCategoryCode = "";
	private String areaNumber = "";
	private String status = "";

	private int type;
	
	public Butaca(int posX, int posY, String letra, String numero) 
	{
		this(letra,numero,"",AMIGO);
		x = posX;
		y = posY;
	}
	
	
	
	public Butaca(String letra, String numero, String fila, int tipo) 
	{
		letraButaca = letra;
		numeroButaca = numero;
		filaButaca = fila;
		
		
		switch (tipo) 
		{
			case 0:
				type = DISPONIBLE;
				break;
			case 5:
				type = SELECCIONADA;
				break;
			case 1:
			case 2:
			case 3:
			case 4:
			case 6:
			case 7:
				type = OCUPADA;
				break;
			case 8:
				type = AMIGO;
				break;
			case 9:
			default:
				type = PASILLO;
				break;
		}
		

	}

	public  String getNombreButaca() 
	{
		return letraButaca + numeroButaca;
	}

	

	public int getType() {
		return type;
	}

	public  void setType(int tipo) {
		type = tipo;
	}
	
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getColor()
	{
		
		
		switch (type) 
		{
			case DISPONIBLE:
				return COLOR_DISPONIBLE;
			case OCUPADA:
				return COLOR_OCUPADA;
			case SELECCIONADA:
				return COLOR_SELECCIONADA;
			case AMIGO:
				return COLOR_AMIGO;
			default:
				return COLOR_PASILLO;
		}
	}

	public String getLetraButaca() {
		return letraButaca;
	}

	public void setLetraButaca(String letraButaca) {
		this.letraButaca = letraButaca;
	}

	public String getNumeroButaca() {
		return numeroButaca;
	}

	public void setNumeroButaca(String numeroButaca) {
		this.numeroButaca = numeroButaca;
	}

	public String getFilaButaca() {
		return filaButaca;
	}

	public void setFilaButaca(String filaButaca) {
		this.filaButaca = filaButaca;
	}

	public String getAreaCategoryCode() {
		return areaCategoryCode;
	}

	public void setAreaCategoryCode(String areaCategoryCode) {
		this.areaCategoryCode = areaCategoryCode;
	}

	public String getAreaNumber() {
		return areaNumber;
	}

	public void setAreaNumber(String areaNumber) {
		this.areaNumber = areaNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
