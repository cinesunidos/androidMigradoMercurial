package com.mobilemedianet.cinesunidosAndroid.objects;

public class Boleto 
{
	private String id;
	private double basePrice, bookingFee, fullPrice, price, serviceFee, serviceFeeWithTax, tax;
	//private float price_in_cents, booking_fee_in_cents, booking_fee_with_tax_in_cents, tax_in_cents;
	private String descripcion;
	private int cantidad;
	
	public Boleto()
	{
		cantidad = 0;
	}

	

	public Boleto(String id, float price, float bookingFee, String descripcion) {
		super();
		this.id = id;
//		this.price_in_cents = price;
//		this.booking_fee_in_cents = bookingFee;
		this.descripcion = descripcion;
	}


	

	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}
	
//	public void setFee(float fee){
//		this.booking_fee_in_cents = fee;
//	}
//
//	public void setBookingFeeWithTaxInCents(float a){
//		this.booking_fee_with_tax_in_cents = a;
//	}
//
//	public float getBookingFeeWithTaxInCents(){
//		return this.booking_fee_with_tax_in_cents;
//	}
//
//	public void setTaxInCents(float a){
//		this.tax_in_cents = a;
//	}
//
//	public float getTaxInCents(){
//		return this.tax_in_cents;
//	}
//
//	public float getFee(){
//		return this.booking_fee_in_cents;
//	}
//
//
//	public float getPriceInCents() {
//		return price_in_cents;
//	}
//
//
//	public void setPriceInCents(float price) {
//		this.price_in_cents = price;
//	}


	
	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}



	public int getCantidad() {
		return cantidad;
	}



	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public double getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(double basePrice) {
		this.basePrice = basePrice;
	}

	public double getBookingFee() {
		return bookingFee;
	}

	public void setBookingFee(double bookingFee) {
		this.bookingFee = bookingFee;
	}

	public double getFullPrice() {
		return fullPrice;
	}

	public void setFullPrice(double fullPrice) {
		this.fullPrice = fullPrice;
	}

	public double getServiceFee() {
		return serviceFee;
	}

	public void setServiceFee(double serviceFee) {
		this.serviceFee = serviceFee;
	}

	public double getServiceFeeWithTax() {
		return serviceFeeWithTax;
	}

	public void setServiceFeeWithTax(double serviceFeeWithTax) {
		this.serviceFeeWithTax = serviceFeeWithTax;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

//	public double getPrice_in_cents() {
//		return price_in_cents;
//	}
//
//	public void setPrice_in_cents(float price_in_cents) {
//		this.price_in_cents = price_in_cents;
//	}
//
//	public double getBooking_fee_in_cents() {
//		return booking_fee_in_cents;
//	}
//
//	public void setBooking_fee_in_cents(float booking_fee_in_cents) {
//		this.booking_fee_in_cents = booking_fee_in_cents;
//	}
//
//	public double getBooking_fee_with_tax_in_cents() {
//		return booking_fee_with_tax_in_cents;
//	}
//
//	public void setBooking_fee_with_tax_in_cents(float booking_fee_with_tax_in_cents) {
//		this.booking_fee_with_tax_in_cents = booking_fee_with_tax_in_cents;
//	}
//
//	public double getTax_in_cents() {
//		return tax_in_cents;
//	}
//
//	public void setTax_in_cents(float tax_in_cents) {
//		this.tax_in_cents = tax_in_cents;
//	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
}
