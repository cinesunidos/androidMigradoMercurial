package com.mobilemedianet.cinesunidosAndroid.objects;



public class CacheControl 
{
	private int diaActualizacion, semanaActualizacionAplicacion;
	private long fechaActualizacionTop10, fechaActualizacionEstrenos, fechaActualizacionFunciones;

	private long fechaBorradoCache;
	
	
	
//	public int getSemanaActualizacionEstrenos() {
//		return semanaActualizacionEstrenos;
//	}
//
//	public void setSemanaActualizacionEstrenos(int semanaActualizacion) 
//	{
//		this.semanaActualizacionEstrenos = semanaActualizacion;
//	}
	
//	public int getSemanaActualizacionTop10() {
//		return semanaActualizacionTop10;
//	}
//
//	public void setSemanaActualizacionTop10(int semanaActualizacion) 
//	{
//		this.semanaActualizacionTop10 = semanaActualizacion;
//	}
	
	
	

	public long getFechaActualizacionTop10() {
		return fechaActualizacionTop10;
	}

	public long getFechaActualizacionEstrenos() {
		return fechaActualizacionEstrenos;
	}

	public void setFechaActualizacionEstrenos(long fechaActualizacionEstrenos) {
		this.fechaActualizacionEstrenos = fechaActualizacionEstrenos;
	}

	public void setFechaActualizacionTop10(long fechaActualizacionTop10) {
		this.fechaActualizacionTop10 = fechaActualizacionTop10;
	}

	public int getDiaActualizacion() {
		return diaActualizacion;
	}

	public void setDiaActualizacion(int diaActualizacion) {
		this.diaActualizacion = diaActualizacion;
	}

	public int getSemanaActualizacionAplicacion() {
		return semanaActualizacionAplicacion;
	}

	public void setSemanaActualizacionAplicacion(int semanaActualizacionAplicacion) {
		this.semanaActualizacionAplicacion = semanaActualizacionAplicacion;
	}

	public long getFechaActualizacionFunciones() {
		return fechaActualizacionFunciones;
	}

	public void setFechaActualizacionFunciones(long fechaActualizacionFunciones) 
	{
		this.fechaActualizacionFunciones = fechaActualizacionFunciones;
	}

	public long getFechaBorradoCache() {
		return fechaBorradoCache;
	}

	public void setFechaBorradoCache(long fechaBorradoCache) {
		this.fechaBorradoCache = fechaBorradoCache;
	}

	
	
	
	
}
