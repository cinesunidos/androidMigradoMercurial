package com.mobilemedianet.cinesunidosAndroid;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.mobilemedianet.cinesunidosAndroid.managers.ApplicationStatus;
import com.mobilemedianet.cinesunidosAndroid.managers.ConnectionManager;
import com.mobilemedianet.cinesunidosAndroid.managers.LoginManager;
import com.mobilemedianet.cinesunidosAndroid.managers.MovieManager;
import com.mobilemedianet.cinesunidosAndroid.managers.PrefManager;
import com.mobilemedianet.cinesunidosAndroid.managers.SalasManager;
import com.mobilemedianet.cinesunidosAndroid.objects.Movie;
import com.mobilemedianet.cinesunidosAndroid.utilities.Zonas;
import com.mobmedianet.adutil.AdView;

import java.util.Timer;
import java.util.TimerTask;

//import com.mobilemedianet.cinesunidosAndroid.managers.LocalyticsManager;

public class SplashActivity extends Activity implements OnClickListener{

	private static final String Flurry_API_Key = "GSW94MXD24S7D546NST6";
	
	ImageView iv;
	ImageView logo;
	AdView ad;
	
	public int i=0;
	Timer timer;
	boolean done = false;
	SplashTask tarea = null;	
	
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate (Bundle savedInstanceState) {
    	requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        //Flurry Start Session
        FlurryAgent.init(this, Flurry_API_Key);
        FlurryAgent.onStartSession(this);
    }
    
    
    @Override
	public void onResume(){
		 // Instantiate the object        
        super.onResume();
        
        ApplicationStatus.setStatus(true);        
        PrefManager.getInstance().setAplicationContext(this);
        ConnectionManager.setContext(this);
        
        /*if(LocalyticsManager.localyticsSession() == null){
	    	LocalyticsManager.newLocalytics(getApplicationContext());
	    	LocalyticsManager.localyticsSession().open();    	
	    }*/
        //LocalyticsManager.localyticsSession().tagEvent("Splash");
		//FlurryAgent.logEvent("Splash");
        
        if(!PrefManager.getInstance().getUser().equals("")){
        	PrefManager.getInstance().getUserInfo();
        	LoginManager.getInstance().setLoginStatus(true);        	
        }
        
        PrefManager.getInstance().setAplicationContext(getApplicationContext());
        
        if(PrefManager.getInstance().isAnyCinemaChecked() == false)
		{

			//SalasManager.getInstance().cargarSalas();

			//if(SalasManager.getInstance().cargarSalas()){
//				Intent myIntent = new Intent(SplashActivity.this, SeleccionCiudadesActivity.class);
//				startActivityForResult(myIntent, 0);
//				this.finish();
//				return;
			new RetrieveCinesTask().execute();

			//}




        }
        else
		{
			setContentView(R.layout.splash);
			MovieManager.getInstance().setContext(this);
			MovieManager.getInstance().clean();

			timer = new Timer();

			timer.scheduleAtFixedRate(new TimerTask() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					if(tarea != null)
						tarea.cancel(true);
					irMainActivity(0);

				}
			}, 25000, 25000);





			iv = (ImageView) findViewById(R.id.preventa);
			iv.setOnClickListener(this);

			logo = (ImageView) findViewById(R.id.logoAnuncio);

			/**TODO*/

			PrefManager pref = new PrefManager();
			pref.setAplicationContext(this);
			String aux = pref.getCityWithMostSelections();
			Log.d("CIUDAD-Splash", "" + aux);

			String key = "";
			if (aux.equals("CCSE,CCSO")) {
				key = "SPLASH_CCS";
			} else {
				key = "SPLASH_" + aux;
			}


			Zonas zona = new Zonas();
			zona.createZones();

    	/*ad = (AdView) findViewById(R.id.adViewSplash);
    	ad.setVisibility(View.INVISIBLE);
    	ad.setZoneId(zona.getZona(key));
    	ad.request();*/


			if(MovieManager.getInstance().isLoaded() == false){

				tarea = new SplashTask();
				tarea.execute();

			}
		}


    	
        
        
    }
    
    private class SplashTask extends AsyncTask<String, String, Integer> {
    	
    	private ProgressBar progressBar;
    	private Movie pelicula;    	
    	private boolean result;
    	
    	
    	@Override
		protected void onPreExecute() {
    		
//    		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);    	
    		progressBar = (ProgressBar) findViewById(R.id.progressBar1); 
    		
    	
    	}

    	
    	@Override
		protected Integer doInBackground(String... params) {
    		int count;
    		PrefManager.getInstance().setAplicationContext(getApplicationContext());
    		String zonas = PrefManager.getInstance().getZonaString();
			//String zonas = "PLC";
    		MovieManager.getInstance().setListSelector(MovieManager.listMovies);
    		publishProgress("0", "Cargando Películas...");

    		/*
			if(MovieManager.getInstance().loadMovies(0) == false){

    			if (ConnectionManager.getInstance().getLastError() == -4) {
    				//irMainActivity(-1);
    				result = false;
    				return -4;
    			}
    			result = false;
    			return -1;
    		}
    		*/

    		publishProgress("20", "Cargando Salas...");
    		
    		if(SalasManager.getInstance().cargarSalas() == false){
    			result = false;
    			return -2;
    		}
    		
    		
    		MovieManager.getInstance().resetRankingSearch();
    		
    		count = 40;
    		
    		 while(count < 100 && (pelicula = MovieManager.getInstance().getNextMovieRank(0)) != null){ 
    			 if(this.isCancelled())
    				 return 0;
    			 publishProgress(String.valueOf(count), "Cargando Posters...");
    			 
    			 if(pelicula.setPoster() == false){
    				 result = false;
    				 return -3;
    			 }
    			 
    			 count += 20;
    			 
    		 }
    		 
    		 while((pelicula = MovieManager.getInstance().getNextMovieRank(0)) != null){
    			 if(pelicula.isInCache())
    				 pelicula.setPoster();    			 
    		 }
    		 
    		 
    		

    		result = true;
    		publishProgress("100", "Listo.");   		
    		
    		
    		/*try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
    		
    		
    		
    		publishProgress("");
    		
    	/*	try {
				Thread.sleep(5000);
//				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			return 0;    	   		
         	
        	
        }
    	
    	@Override
		protected void onProgressUpdate (String... params) {
    		
    		if(!params[0].equals("")){
    			
    			progressBar.setProgress(Integer.parseInt(params[0]));
    			((TextView) findViewById(R.id.textView1)).setText(params[1]);    			
    			
    		}else{
    			
    			progressBar.setVisibility(View.INVISIBLE);
            	((TextView) findViewById(R.id.textView1)).setVisibility(View.INVISIBLE);
            	
//            	iv.setImageResource(R.drawable.splash1);
            	iv.setVisibility(View.INVISIBLE);
            //	ad.setVisibility(View.VISIBLE); //AD SCREEN 
         //  	logo.setVisibility(View.VISIBLE);
    			
    		}
    		
          
    	
    	}

        @Override
		protected void onPostExecute(Integer error) {
        	
        	if(error == -1 || error == -2){
        		timer.cancel();
        		ConnectionManager.getInstance().clearCache(null);
        		errorTerminal("  La aplicación ha detectado un error en los datos descargados, es posible que se deba a una descarga incompleta de estos.\n\n Es necesario reiniciar la aplicación, asegurese de tener una conexión estable a Internet.");
        		return;
        	}
        	
        	if (error == -4) {
        		timer.cancel();
        		ConnectionManager.getInstance().clearCache(null);
        		irMainActivity(-1);
        		return;
        	}
        	
        	if(this.isCancelled() == false){
        		if(result == false) {
        			Toast.makeText(SplashActivity.this, "Fallo de conexión.", Toast.LENGTH_LONG).show();
            		irMainActivity(-1);
        		} else {
        			irMainActivity(0);
        		}
        	}

        }
    }


	class RetrieveCinesTask extends AsyncTask<String, Void, Boolean> {

		private Exception exception;

		protected Boolean doInBackground(String... urls) {
			return SalasManager.getInstance().cargarSalas();
		}

		protected void onPostExecute(Boolean cargo) {
			if (cargo)
			{
				Intent myIntent = new Intent(SplashActivity.this, SeleccionCiudadesActivity.class);
				startActivityForResult(myIntent, 0);
				//this.finish();
				SplashActivity.this.finish();
				return;
			}
			else
			{
				errorTerminal("Error al obtener la información de los cines.\n\n Es necesario reiniciar la aplicación, asegurese de tener una conexión estable a Internet.");
				return;
			}
		}
	}

    

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub		
//		irMainActivity();
		
	}
	
	private void irMainActivity(int status){
		if(done == true)
			return;		
		done = true;
		if(timer != null)
			timer.cancel();
		
		Intent myIntent = new Intent(this, ListaActivity.class);
		if (status == -1) { //Corregir esto!
			myIntent.putExtra("timeout", -1);		
		} else if (status == 0) {
			myIntent.putExtra("timeout", 0);
		}
		myIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP); 
		myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);		
        startActivityForResult(myIntent, 0);         
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        this.finish();
        
		
	}
	
	
	private void errorTerminal(String mensaje){
		 AlertDialog.Builder alertDialog = new AlertDialog.Builder(this); 
	      
	      alertDialog.setTitle("CinesUnidos"); 
	      
	      alertDialog.setMessage(mensaje); 
	     
	      alertDialog.setIcon(R.drawable.icono); 
	     
	      alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
	          @Override
			public void onClick(DialogInterface dialog,int which) {  
	        	SplashActivity.this.finish();     
	          	android.os.Process.killProcess(android.os.Process.myPid());
	          }
	      }); 
	 
	     
	      try{
	    	  alertDialog.show();    
	      }catch(Exception e){
	    	  return;
	      }
		}
	
	
	/*@Override
	public void onPause()
	{		
		overridePendingTransition(R.anim.incoming, R.anim.outgoin);
		LocalyticsManager.localyticsSession().upload();		
	    super.onPause();
	}*/

	@Override
	protected void onPause() {
		super.onPause();

		FlurryAgent.onEndSession(this);
	}

	@Override
	protected void onStop() {
		super.onStop();

		FlurryAgent.onEndSession(this);
	}

}