package com.mobilemedianet.cinesunidosAndroid.managers;

import android.util.Log;

import com.mobilemedianet.cinesunidosAndroid.objects.Boleto;
import com.mobilemedianet.cinesunidosAndroid.parsers.TicketsParserJson;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

public class DetallesCompra {
	
	static private DetallesCompra instance;
	
	private int salaId;
	private String salaName;
	private String cineId;
	private String movieId;
	private String showTimeId;
	private int index; //Dia de compra
	
	private int ReservedSeats;
	private int BookingPerTrans;
	private int SalesPerTrans;
	private int TransPerCard;
	private int MaxTimeForBooking;
	private int ReservedPerSection;
	private int hora;
	private int timerMin;
	private int timerSec;
	//private int availableSeats;
	
	private float valorEntrada;
	private float costoTotal;
	private float bookingFee;
	
		
	private String censura;
	private String fecha;
	private String bookingNumber;
	private String titulo;
	private String date;
	private String time;
	
	private String diaSemana;
	private String tiempoRestantes;
	private String origen;

	private String TAG = this.getClass().getSimpleName();
	
	public boolean fail = false;	
	private boolean premium = false;
	private boolean seleccion_asientos = false;
	private boolean cambiobutacas = false;
	
	private List<Boleto> boletos;
	
	
	static public DetallesCompra getInstance(){
		
		if(instance == null)
			instance = new DetallesCompra();			
			
		return instance;
			
	}
	
	public void setSeleccionAsientos(boolean a){
		seleccion_asientos = a;
	}
	
	public boolean getSeleccionAsientos(){
		return seleccion_asientos;
	}
	
	public void cambioButacas(boolean a){
		cambiobutacas = a;
	}
	
	public boolean getCambioButacas(){
		return cambiobutacas;
	}
	
	public boolean getPremium(){
		return premium;
	}
	
	public void setDia(int i){
		index = i;
	}
	
	public int getDia(){
		return index;
	}
	
	public void setTittle(String a){
		titulo = a;
	}
	
	public String getTittle(){
		return titulo;
	}
	
	public void setPremium(boolean i){
		premium = i;
	}
	
	public void setBookingNumber(String a){
		bookingNumber = a;
	}
	
	public String getBookingNumber(){
		return bookingNumber;
	}
	
	public void setNumeroEntradas(String tag, int cantidad){
		Boleto dum;
		
		if(boletos == null)
			return;
		
		Iterator<Boleto> a = this.boletos.iterator();
		
		while(a.hasNext()){
			dum = a.next();
			
			if(dum.getDescripcion().equals(tag)){
				dum.setCantidad(cantidad);
				return;
			}
		}
		
	}
	
	public int getNumeroTotalEntradas(){
		Boleto dum;
		
		if(boletos == null)
			return 0;
		
		Iterator<Boleto> a = this.boletos.iterator();
		int count = 0;
		
		
		
		while(a.hasNext()){
			dum = a.next();
			count += dum.getCantidad();			
			}
		
		return count;
	}
	
	public String getSeleccionVoletos(){		
		Boleto dum;
		Iterator<Boleto> a = this.boletos.iterator();
		String tmp = "";
		
		while(a.hasNext()){
			dum = a.next();
			if(dum.getCantidad() > 0){
				tmp +=	dum.getId() + ">" + String.valueOf(dum.getCantidad());
				tmp += ";";
			}
				
			}		
			
		return tmp.substring(0, tmp.length() - 1);	
		
	}

	public List<Boleto> getSeleccionBoletosNew(){
		return this.boletos;

	}
	
	public int getNumeroEntradasByTag(String tag){
		Boleto dum;
		
		if(boletos == null)
			return 0;
		
		Iterator<Boleto> a = this.boletos.iterator();
		
		while(a.hasNext()){
			dum = a.next();
			
			if(dum.getDescripcion().equals(tag)){				
				return dum.getCantidad();
			}
		}
		return 0;
		
	}
	
	public float getTotalEntradasValor(){
		Boleto dum;
		
		if(boletos == null)
			return 0;
		
		Iterator<Boleto> a = this.boletos.iterator();
		float count = 0;
		
		while(a.hasNext()){
			dum = a.next();
			count += dum.getCantidad() * dum.getBasePrice();
			}
		
		return count;
		
	}
	
	public void setReservedSeats(int i){
		ReservedSeats = i;
	}
	
	public void setBookingPerTrans(int i){
		BookingPerTrans = i;
	}
	
	public void setSalesPerTrans(int i){
		SalesPerTrans = i;
	}
	
	public void setTransPerCard(int i){
		TransPerCard = i;
	}
	
	public void setMaxTimeForBooking(int i){
		MaxTimeForBooking = i;
	}
	
	public void setReservedPerSection(int i){
		ReservedPerSection = i;
	}
	
	
	
	public int getReservedSeats(){
		return ReservedSeats;
	}
	
	public int getBookingPerTrans(){
		return BookingPerTrans;
	}
	
	public int getSalesPerTrans(){
		return SalesPerTrans;
	}
	
	public int getTransPerCard(){
		return TransPerCard;
	}
	
	public int getMaxTimeForBooking(){
		return MaxTimeForBooking;
	}
	
	public int getReservedPerSection(){
		return ReservedPerSection;
	}
	
	
	public List<Boleto> getBoletos(){
		return boletos;
	}
	
	public void loadBoletos()
	{
		//Cambio a Json
		HttpHandler sh = new HttpHandler();

		//String url = "http://cu-dev-wcf-services.cloudapp.net/Premier.svc/";
		// Making a request to url and getting response
		String jsonStr = sh.makeServiceCall(ConnectionManager.CU_WS_NEW + ConnectionManager.TICKETS_WS + String.valueOf(cineId) + "/" + String.valueOf(showTimeId));

		Log.e(TAG, "Response from url: " + jsonStr);

		if (jsonStr != null) {

			TicketsParserJson parserjson = new TicketsParserJson();
			boletos = parserjson.parse(jsonStr);
			salaName = parserjson.getHallName();
			BookingPerTrans = parserjson.getMaxSeatsPerTransaction();
			ButacasManager.getInstance().setSeatsAvailability(parserjson.getSeatsAvailable());

			//availableSeats = ;

			if(boletos != null)
			{
				fail = false;
			}
			else
			{
				fail = true;
			}
		}

//		BoletosParserSax mpar;
//
//        mpar = new BoletosParserSax("http://" + ConnectionManager.wsCinesUnidos + "/Blackberry.asmx/GetTickets?cinemaid=" +
//        String.valueOf(cineId) + "&id=" + String.valueOf(showTimeId));
//
//
//        boletos = mpar.parse();
//        this.isLoaded = true;
	}
	
	public float getBookingFeeInCents() {
		Boleto dum;

		if(boletos == null)
			return 0;
		
		Iterator<Boleto> a = this.boletos.iterator();
		
		
		float count = 0;
		
		while(a.hasNext()){
			dum = a.next();
			count += dum.getCantidad() * dum.getServiceFee();
//			count += dum.getCantidad() * dum.getFee();
			}
		
		return count;		
	}

	public float getCargosWeb() {
		Boleto dum;

		if(boletos == null)
			return 0;

		Iterator<Boleto> a = this.boletos.iterator();


		float count = 0;

		while(a.hasNext()){
			dum = a.next();
			count += dum.getCantidad() * dum.getBookingFee();
//			count += dum.getCantidad() * dum.getFee();
		}

		return count;
	}
	
	public float getBookingFeeWithTaxInCents() {
		Boleto dum;

		if(boletos == null)
			return 0;
		
		Iterator<Boleto> a = this.boletos.iterator();
		
		
		float count = 0;
		
		while(a.hasNext()){
			dum = a.next();
			count += dum.getCantidad() * dum.getServiceFee();
			//count += dum.getCantidad() * dum.getBookingFeeWithTaxInCents();
			}
		
		return count;		
	}
	
	public float getTaxInCents() {
		Boleto dum;

		if(boletos == null)
			return 0;
		
		Iterator<Boleto> a = this.boletos.iterator();
		
		
		float count = 0;
		
		while(a.hasNext()){
			dum = a.next();
			count += dum.getCantidad() * dum.getTax();
			//count += dum.getCantidad() * dum.getTaxInCents();
			}
		
		return count;		
	}


	public void setBookingFee(float a) {
		bookingFee = a;
	}


	
	
	public void setCostoTotal(float a){
		costoTotal = a;
	}
	
	public float getCostoTotal(){
		return costoTotal;
	}
	
	public void setValorEntrada(float a){
		valorEntrada = a;
	}
	
	public float getVAlorEntrada(){
		return valorEntrada;
	}
	
	public void setSalaId(int a){
		salaId = a;
	}
	
	public void setCineId(String a){
		cineId = a;
	}
	
	public void setShowTimeId(String a){
		showTimeId = a;		
	}
	
	
		
	
	public void setCensura(String a){
		censura = a;
	}
	
	public String getCensura(){
		return censura;
	}
	
	public void setFecha(String a){
		fecha = a;				
	}
	
	public String getFecha(){
		return fecha;
	}
	
	
	public int getSalaId(){
		return salaId;
	}
	
	public String getCineId(){
		return cineId;
	}
	
	public String getShowTimeId(){
		return showTimeId;		
	}
	
	public void setDate(int year, int month, int day){
		date = "" + day + "-" + month + "-" + year;
	}
	
	public void setTime(int hour, int minutes, String meridiano){
		this.hora = hour;
		time = "" + hour + ":" + minutes + ":" +  meridiano;
	}
	
	public String getDate(){
		return date;
	}
	
	public String getTime(){
		return time;
	}
	
	public void  setDiaSemana(String a){
		this.diaSemana = a;
	}
	
	public String getDiaSemana(){
		return this.diaSemana;
	}
	
	public void setTiempoRestantes(int min, int sec){
		
		timerMin = min;
		timerSec = sec;
		
	}
	
	public String getTiempoRestante(){
		
		if(timerSec > 30){		
			timerSec = 30;
		}else if(timerSec <= 30){
			timerSec = 0;			
		}
		
		return String.format("%d:%02d", timerMin, timerSec);
	}
	
	public String getHorasAntes(){
		
		if(index > 0)
			return "+24";
		
		Calendar calendar = Calendar.getInstance();			
		
		return String.valueOf(this.hora - calendar.get(Calendar.HOUR));
	}
	
	
	public void setOrigen(String a){
		this.origen = a;
	}
	
	public String getOringen(){
		return this.origen;
	}
	
	//Solo el primer tipo que encuentre
	public String nombreBoletos(){	
		
		Boleto bolobj;
				
		if(boletos == null)
			return "";
		
		for(int i = 0; i < boletos.size(); i++){
			
			bolobj = boletos.get(i);
			
			if(bolobj.getCantidad() > 0)
				return bolobj.getDescripcion();
			
		}
		
		return "";
		
	}

	public String getSalaName() {
		return salaName;
	}

	public String getMovieId() {
		return movieId;
	}

	public void setMovieId(String movieId) {
		this.movieId = movieId;
	}
}
