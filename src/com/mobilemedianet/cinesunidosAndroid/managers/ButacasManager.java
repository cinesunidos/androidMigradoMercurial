package com.mobilemedianet.cinesunidosAndroid.managers;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.mobilemedianet.cinesunidosAndroid.objects.Butaca;
import com.mobilemedianet.cinesunidosAndroid.objects.InformacionUsuario;
import com.mobilemedianet.cinesunidosAndroid.parsers.AsientosParserSax;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

public class ButacasManager {
	
	static private ButacasManager instance = null;
	
	private int SeatsAvailable;
	private Vector<Butaca> butacasSeleccionadas;
	private Vector<Butaca> butacasSeleccionadas2;
	private ArrayList<Butaca> matriz;
	private int reservedcount;
	private boolean backFromMap = false;	
	private String orderId;
	private Context apcontext;
	private int cantidadColumnas, cantidadFilas;
	private ArrayList<Integer> pasillosFil, pasillosCol;
	private String dimensionSala;
	private String areaCategoryCode;
	private String areaNumber;
	
	static public ButacasManager getInstance(){
		
		if(instance == null)
			instance = new ButacasManager();
		
		return instance;
		
	}
	
		
	public boolean LoadSeatsAvailability(String cinemaId, String performanceId){
		
		AsientosParserSax apar;
		
		apar = new AsientosParserSax("http://" + ConnectionManager.wsCinesUnidos + "/blackBerry.asmx/GetSeatsAvailability?cinemaId=" 
		+ cinemaId +"&performanceId=" + performanceId);
		
		if(apar.parse() == -1)
			return false;
		
		return true;
		
	}
	
	public Butaca getButacaByName(String name){		
		Butaca butobj;
		
		for(int i = 0; i < matriz.size(); i++){
			butobj = matriz.get(i);
			if(butobj.getNombreButaca().equals(name))
				return butobj;
		}
		
		return null;
		
	}
	
	
	public int getCol(){
		return cantidadColumnas;
	}
	
	public int getFil(){
		return cantidadFilas;
	}
	
	public Butaca getAt(int x, int y){
		
		Butaca butobj;
		
		for(int i = 0; i < matriz.size(); i++){
			butobj = matriz.get(i);
			if(butobj.getX() == x && butobj.getY() == y)
				return butobj;
		}
		
		return null;
		
	}
	
	
	public boolean LoadSeats(){
		
		DetallesCompra comprar = DetallesCompra.getInstance();
		InformacionUsuario user = LoginManager.getInstance().getUserInfo();
		NewSeats asientos = NewSeats.getInstance();
		
		asientos.setcinemaId(String.valueOf(comprar.getCineId()));
		asientos.setperformanceId(String.valueOf(comprar.getShowTimeId()));
		asientos.setUserId(user.getSecureId());
		asientos.setUserFirstName(user.getName());
		asientos.setUserLastName(user.getLastName());
		asientos.setUserEmail(user.getLogin());
		asientos.setUserNationalId(user.getExternalId());
		asientos.setUserSelectsSeats("false");
		asientos.setUserSeatSelection("");
		asientos.setUserTicketSelection(DetallesCompra.getInstance().getSeleccionVoletos());
		asientos.setUserTicketSelectionNew(DetallesCompra.getInstance().getSeleccionBoletosNew());
		asientos.setMovieId(DetallesCompra.getInstance().getMovieId());
		
		//asientos.generateBodyRequest();
		asientos.sendRequest();		
		
		if(asientos.getResult(false))
		{
			matriz = parserSala(asientos.getSalaString());
			if(matriz == null){
				return false;				
			}
			orderId = asientos.getOrderId();
			detectarPasillosFil();
			detectarPasillosCol();
			
		}
		else{
			matriz = null;
			return false;
		}
		
		return true;
				
		
				
	}	
	
	public void checkForNewSeats(){
		NewSeats asientos = NewSeats.getInstance();
		asientos.checkSeatSelection();
		if(asientos.getResult(true)){
			matriz = parserSala(asientos.getSalaString());
			orderId = asientos.getOrderId();
			detectarPasillosFil();
			detectarPasillosCol();
			
		}
		else
			matriz = null;
	}
	
	
	
		
	public String getOrderId(){
		return orderId;
	}
	
	
	
	public void setSeatsAvailability(int i){
		SeatsAvailable = i;
	}
	
	public int getSeatsAvailability(){
		return SeatsAvailable;
	}
	
	public void setNewVectorSelecction(Vector<Butaca> sel){
		this.butacasSeleccionadas2 = sel;
		
	}
	
	public Iterator<Butaca> getNewVectorSelectionIterator()
	{
		return this.butacasSeleccionadas2.iterator();
	}
	private ArrayList<Butaca> parserSala(String salaString)
	{
		butacasSeleccionadas = new Vector<Butaca>();
		reservedcount = 0;
		
		String filas[] = salaString.split("\\|");
			
		cantidadFilas = Integer.parseInt(filas[1]) - 2;
		cantidadColumnas = Integer.parseInt(filas[2]) - 2;
		dimensionSala = filas[5];

		areaCategoryCode = filas[5];
		areaNumber = filas[4];
		
		ArrayList<Butaca> salaCine = new ArrayList<Butaca>();
		
		int posicionY = 0;
		String letra = "";
		String fila = "";
		String butacas = "";
		int j = 0;
		
		
		for (int i = 12; i < filas.length; i++)
		{
			if (j == 0)
			{
//				if (i==12){
//					numFilaActual = [filas[i] integerValue];
//				}else{
//					while (numFilaActual > [filas[i] integerValue]){
//						[cantidadPorFila addObject:[NSNumber numberWithInt:0]];
//						numFilaActual --;
//					}
//				}
				posicionY = Integer.parseInt(filas[i]) - 1;
				fila = filas[i];
				j++;
			}
			else if (j == 1)
			{
				letra = filas[i];
				j++;
			}
			else if (j == 2)
			{
				butacas = filas[i];

				String butacasArray[] = butacas.split(" ");;

				for (int l = 0; l < butacasArray.length; l++)
				{
					String butacaRaw = butacasArray[l];

					if (butacaRaw.length() > 6)
					{


						char posicionChar = butacaRaw.toCharArray()[butacaRaw.length() - 6];
						int posicionX = getPosicionX(posicionChar) - 1;
						int status = Integer.parseInt(String.valueOf(butacaRaw.toCharArray()[butacaRaw.length()-1]));

						Butaca butaca = new Butaca(letra, (posicionX+1) + "", fila, status);
						butaca.setX(posicionX);
						butaca.setY(posicionY);
						butaca.setAreaNumber(areaNumber);
						butaca.setAreaCategoryCode(areaCategoryCode);
						butaca.setStatus(String.valueOf(1));


						if (butaca.getType() == Butaca.SELECCIONADA)
						{
							butacasSeleccionadas.addElement(butaca);
							reservedcount++;
						}
						salaCine.add(butaca);


					}
				}
				j = 0;
			}
		}
		return salaCine;
	}
	
	public ArrayList<Butaca> getSalaMapa(){
		return matriz;
		
	}
	
	public String getDimensionSala(){
		return dimensionSala;
	}
	
	private int getPosicionX(char variable)
	{
		switch (variable) 
		{
			case 'A':
				return 10;
			case 'B':
				return 11;
			case 'C':
				return 12;
			case 'D':
				return 13;
			case 'E':
				return 14;
			case 'F':
				return 15;
			case 'G':
				return 16;
			case 'H':
				return 17;
			case 'I':
				return 18;
			case 'J':
				return 19;
			case 'K':
				return 20;
			case 'L':
				return 21;
			case 'M':
				return 22;
			case 'N':
				return 23;
			case 'O':
				return 24;
			case 'P':
				return 25;
			case 'Q':
				return 26;
			case 'R':
				return 27;
			case 'S':
				return 28;
			case 'T':
				return 29;
			case 'U':
				return 30;
			case 'V':
				return 31;
			case 'W':
				return 32;
			case 'X':
				return 33;
			case 'Y':
				return 34;
			case 'Z':
				return 35;
			case 'a':
				return 36;
			case 'b':
				return 37;
			case 'c':
				return 38;
			case 'd':
				return 39;
			case 'e':
				return 40;
			case 'f':
				return 41;
			case 'g':
				return 42;
			case 'h':
				return 43;
			case 'i':
				return 44;
			case 'j':
				return 45;
			case 'k':
				return 46;
			case 'l':
				return 47;
			case 'm':
				return 48;
			case 'n':
				return 49;
			case 'o':
				return 50;
			default:
				return Integer.parseInt(String.valueOf(variable));
		}
	}
	
	public void addSelection(Butaca butaca){
		butacasSeleccionadas.addElement(butaca);
		reservedcount++;
		if(reservedcount > DetallesCompra.getInstance().getNumeroTotalEntradas()){
			removeFirstSelected();
		}
	}
	
	public void removeFirstSelected(){		
		butacasSeleccionadas.remove(0);
		reservedcount--;
	}
	
	
	public void removeSelection(Butaca butaca){
		Iterator<Butaca> iterator = butacasSeleccionadas.iterator();
		Butaca dum;
		
		while(iterator.hasNext()){
			dum = iterator.next();
			if(dum.getNombreButaca().equals(butaca.getNombreButaca())){
				iterator.remove();
				reservedcount--;
				return;
			}
				
		}
		
	}
	

	public Butaca getFirstReservedSeat(){
		return butacasSeleccionadas.firstElement();
	}
	
	public Iterator<Butaca> getReserverdSeatsIterator(){
		if(butacasSeleccionadas != null)
			return butacasSeleccionadas.iterator();
		else
			return null;
	}
	
	public int getReserverdCount(){
		return reservedcount;
	}
	
	public void setBackFromMap(boolean a){
		backFromMap = a;
	}
	
	public boolean getBackFromMap(){
		return backFromMap;
	}
	
	public void cancelOrder(Context c){
		apcontext = c;
		
		new cancelOrderTask().execute("");
	}
	
	
	private class cancelOrderTask extends AsyncTask<String, Void, Integer> {
		
		ProgressDialog pd;
		 
		 @Override
		protected void onPreExecute() {
			 
			 pd = ProgressDialog.show(apcontext, "Espere.", "Cancelando Orden.", true, false);				
			 
			 
	    	}
	     
		 @Override
		protected Integer doInBackground(String... params) {
			 
			 	String userAgent = ConnectionManager.getInstance().getUserAgent();

				HttpClient httpclient = new DefaultHttpClient();
				HttpGet httpget = new HttpGet("http://" + ConnectionManager.wsCinesUnidos + "/blackBerry.asmx/GetOrderCancel?orderId=" + orderId);
				httpget.setHeader("User-Agent", userAgent);
				
				 try {
					 httpclient.execute(httpget);
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 
				 return 0;
	     }	    

	     @Override
		protected void onPostExecute(Integer result) {
	     try {
	    	 pd.cancel();
	         pd = null;
	        } catch (Exception e) { }
	     }
	 }
	
private	void detectarPasillosFil(){

		Butaca butobj = null;

		pasillosFil = new ArrayList<Integer>();

		boolean pasillofila;

		int yoffset = 0;
	
		for(int y = 0; y < cantidadFilas; y++){

			pasillofila = true;

			for(int x = 0; x < cantidadColumnas; x++){

				butobj = getAt(x, y);

				if(butobj != null){


					if(butobj.PASILLO != butobj.getType()){
						pasillofila = false;
					}

				}

			}

			if(pasillofila == true){
				pasillosFil.add(Integer.valueOf(y));
				yoffset++;
			}

		}

		//cantidadFilas += yoffset;

	}

private void detectarPasillosCol(){

	Butaca butobj = null;

	pasillosCol = new ArrayList<Integer>();

	boolean pasillocolumna;

	int xoffset = 0;

	for(int x = 0; x < cantidadColumnas; x++){

		pasillocolumna = true;

		for(int y = 0; y < cantidadFilas; y++){

			butobj = getAt(x, y);

			if(butobj != null){


				if(butobj.PASILLO != butobj.getType()){
					pasillocolumna = false;
				}

			}

		}

		if(pasillocolumna == true){
			pasillosCol.add(Integer.valueOf(x));
			xoffset++;
		}

	}

}

public ArrayList<Integer> getPasillosFil(){
	return pasillosFil;
}

public ArrayList<Integer> getPasillosCol(){
	return pasillosCol;
}

}
