package com.mobilemedianet.cinesunidosAndroid.managers;

import android.util.Log;

import com.mobilemedianet.cinesunidosAndroid.objects.InformacionUsuario;
import com.mobilemedianet.cinesunidosAndroid.parsers.LoginParserJson;

import org.json.JSONException;
import org.json.JSONObject;

import static org.acra.ACRA.log;

public class LoginManager {

	public static LoginManager instance = null;
	private InformacionUsuario userInfo;
	private boolean loged = false;

	private String TAG = this.getClass().getSimpleName();

	
	public static LoginManager getInstance(){
		if(instance == null)
			instance = new LoginManager();
		
		return instance;
	}
	
	public LoginManager(){
		userInfo = InformacionUsuario.getInstance();
	}
	
	public boolean login(String user, String pass){


		HttpHandler sh = new HttpHandler();


		JSONObject payload = new JSONObject();
		try
		{
			payload.put("Email", user);
			payload.put("Password",pass);
			payload.put("Platform","android");
			payload.put("URL","");
		}
		catch (JSONException e)
		{
			log.e(TAG, e.getMessage());
		}


		String jsonStr = sh.makeServiceCall(ConnectionManager.CU_WS_NEW + ConnectionManager.LOGIN_WS, payload);

		Log.e(TAG, "Response from url LOGIN: " + jsonStr);

		if (jsonStr != null && !jsonStr.equals(""))
		{
			LoginParserJson parserJson = new LoginParserJson(userInfo);
			userInfo = parserJson.parse(jsonStr);

			if(userInfo != null && userInfo.getStatus() == true){
				InformacionUsuario.getInstance().setLogin(user);
				loged = true;
				return true;
			}

		}

		return false;



//		LoginParserSax mpar;
//		String u = null;
//		String p = null;
//
//		try {
//			u = URLEncoder.encode(user, "utf-8");
//			p = URLEncoder.encode(pass, "utf-8");
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		String dum = "login=" + u + "&password=" + p;
//
//
//		mpar = new LoginParserSax("http://" + ConnectionManager.wsCinesUnidos + "/blackBerry.asmx/Login?" + dum);
//
//		if(mpar.parse(userInfo) == -1){
//			return false;
//		}
//
//		if(userInfo.getStatus() == true){
//			loged = true;
//			return true;
//		}else
//			return false;
//			return false;
	}
	
	public boolean getLoginStatus(){
		return loged;
	}
	
	public void setLoginStatus(boolean status){
		loged = status;
	}
	
	public InformacionUsuario getUserInfo(){
		return userInfo;
	}
	
}
