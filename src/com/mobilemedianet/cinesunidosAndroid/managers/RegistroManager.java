package com.mobilemedianet.cinesunidosAndroid.managers;

import android.util.Log;

import com.mobilemedianet.cinesunidosAndroid.parsers.RegistroParserJson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.UUID;

import static org.acra.ACRA.log;

public class RegistroManager {

	private String TAG = this.getClass().getSimpleName();
	static private RegistroManager instance = null;
	
	private int dia;
	private int mes;
	private int ano;
	private String sexo;
	private String estado;
	private String ciudad;
	private String correo;
	private String contrasena;
	private String preguntasecreta;
	private String respuesta;
	private String nombre;
	private String apellido;
	private String cedula;
	private String celular;
	private String direccion;
	private String telefono;
	private String fecha;
	private String bodyRequest;
	private boolean result;
	
	private boolean recibirCorreos = true;
	private String tipocedula;
	
	static public RegistroManager getInstance(){
		if(instance == null)
			instance = new RegistroManager();
		
		return instance;
	}
	
	
	
	public void generateBodyRequest(){
		
		bodyRequest = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
		bodyRequest += "<v:Envelope xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:d=\"http://www.w3.org/2001/XMLSchema\" xmlns:c=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:v=\"http://schemas.xmlsoap.org/soap/envelope/\">";
		bodyRequest += 		"<v:Header />";
		bodyRequest += 				"<v:Body>";		
		bodyRequest += 					"<RegisterUser xmlns=\"http://tempuri.org/\" id=\"o0\" c:root=\"1\">";
		bodyRequest += 							"<login i:type=\"d:string\">" + correo + "</login>";
		bodyRequest +=							"<password i:type=\"d:string\">" + contrasena +"</password>";		
		bodyRequest += 							"<answer i:type=\"d:string\">" + respuesta + "</answer>";
		bodyRequest += 							"<question i:type=\"d:string\">" + preguntasecreta + "</question>";
		bodyRequest += 							"<subscribed i:type=\"d:boolean\">"+ recibirCorreos + "</subscribed>";
		bodyRequest += 							"<birthDate i:type=\"d:string\">" + fecha + "</birthDate>";
		bodyRequest += 							"<email i:type=\"d:string\">" + correo + "</email>";
		bodyRequest += 							"<sex i:type=\"d:string\">" + sexo + "</sex>";
		bodyRequest += 							"<cedula i:type=\"d:string\">" + tipocedula + cedula + "</cedula>";
		bodyRequest += 							"<nombre i:type=\"d:string\">" + nombre + "</nombre>";
		bodyRequest += 							"<apellido i:type=\"d:string\">" + apellido + "</apellido>";		
		bodyRequest += 							"<telefono i:type=\"d:string\">" + telefono + "</telefono>";		
		bodyRequest += 							"<celular i:type=\"d:string\">" + celular + "</celular>";		
		bodyRequest += 							"<direccion i:type=\"d:string\">" + direccion + "</direccion>";
		bodyRequest += 							"<ciudad i:type=\"d:string\">" + ciudad + "</ciudad>";
		bodyRequest +=							"<estado i:type=\"d:string\">" + estado + "</estado>";
		bodyRequest += 					"</RegisterUser>";
		bodyRequest += 		"</v:Body>";
		bodyRequest += "</v:Envelope>";
		
	}
	
	public  void sendRequest(){



		HttpHandler sh = new HttpHandler();


		JSONObject payload = new JSONObject();
		try
		{
			payload.put("Id", UUID.randomUUID().toString());
			payload.put("Name", nombre);
			payload.put("LastName",apellido);
			payload.put("Password",contrasena);
			payload.put("PasswordComparation",contrasena);
			payload.put("SecretQuestion",preguntasecreta);
			payload.put("SecretAnswer",respuesta);
			payload.put("ReceiveMassMail",recibirCorreos);
			payload.put("BirthDate","/Date("+fecha+")/");
			payload.put("Email",correo);
			payload.put("EmailComparation",correo);
			payload.put("IdCard", tipocedula + cedula);
			payload.put("IdCardType",tipocedula);
			payload.put("IdCardNumber",cedula);
			payload.put("Phone",telefono);
			payload.put("MobilePhone",celular);
			payload.put("Sex",sexo);  //Todo: Revisar valores
			payload.put("City",ciudad);
			payload.put("State",estado);
			payload.put("Country","VE");
			payload.put("Address",direccion);
			payload.put("CreationDate","/Date(1970-01-01 00:00:00)/");
			payload.put("ActivationDate","/Date(1970-01-01 00:00:00)/");
			payload.put("Active",true);
			payload.put("DateOut","/Date(1970-01-01 00:00:00)/");
			payload.put("DateLastVisit","/Date(1970-01-01 00:00:00)/");
			payload.put("DateCurrentVisit","/Date(1970-01-01 00:00:00)/");
			payload.put("DateLastUpdate","/Date(1970-01-01 00:00:00)/");
			payload.put("Zipcode","0");
			payload.put("Twitter","");
			payload.put("Facebook","");
			payload.put("Instagram","");
		}
		catch (JSONException e)
		{
			log.e(TAG, e.getMessage());
		}




		String jsonStr = sh.makeServiceCall(ConnectionManager.CU_WS_NEW + ConnectionManager.REGISTRO_WS, payload);

		Log.e(TAG, "Response from url: " + jsonStr);


		if (jsonStr != null)
		{
			RegistroParserJson parserJson = new RegistroParserJson();
			result = parserJson.parse(jsonStr);
		}




//		StringEntity tmp = null;
//		HttpResponse response = null;
//		StringBuilder tmp2 = null;
//
//		generateBodyRequest();
//
//		 try {
//			 tmp = new StringEntity(bodyRequest,"UTF-8");
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		 HttpClient httpclient = new DefaultHttpClient();
//		 HttpPost httppost = new HttpPost("http://" + ConnectionManager.wsCinesUnidos + "/blackberry.asmx");
//
//		 httppost.setHeader("Content-Type", "text/xml");
//		 httppost.setHeader("SOAPAction", "http://tempuri.org/RegisterUser");
//
//		 httppost.setEntity(tmp);
//
//		 try {
//			 response = httpclient.execute(httppost);
//		} catch (ClientProtocolException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		 try {
//			tmp2 = inputStreamToString(response.getEntity().getContent());
//		} catch (IllegalStateException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		 result = tmp2.toString();
		 		 

	}
	
	public boolean getResult(){

		return result;

//		String resultado = getValue(result, "RegisterUser Successful");
//
//		if(resultado.equals("True"))
//			return true;
//
//		return false;
		
		
	}
	
	
	String getValue(String str, String tag){
		int index = str.indexOf(tag);
		
		if(index == -1)
			return "-1";
		
		index = str.indexOf("\"", index) + 1;
		String result = str.substring(index, str.indexOf("\"", index + 1));
		return result;
		
	}
	
	
	private StringBuilder inputStreamToString(InputStream is) {
	    String line = "";
	    StringBuilder total = new StringBuilder();
	    
	    // Wrap a BufferedReader around the InputStream
	    BufferedReader rd = new BufferedReader(new InputStreamReader(is));

	    // Read response until the end
	    try {
			while ((line = rd.readLine()) != null) { 
			    total.append(line); 
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    // Return full string
	    return total;
	}
	
	
	public void setTelefono(String a){
		telefono = a;
//		try {
//			telefono = URLEncoder.encode(a,"utf-8");
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	
	public void setDireccion(String a){
		direccion = a;
//		try {
//			direccion = URLEncoder.encode(a,"utf-8");
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	
	public void setCelular(String a){
		celular = a;
//		try {
//			celular = URLEncoder.encode(a,"utf-8");
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	
	public void setCedula(String a){
		cedula = a;
//		try {
//			cedula = URLEncoder.encode(a,"utf-8");
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	
	public void setApellido(String a){
		apellido = a;
//		try {
//			apellido = URLEncoder.encode(a,"utf-8");
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	
	public void setNombre(String a){
		nombre = a;
//		try {
//			nombre = URLEncoder.encode(a,"utf-8");
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	
	public void setRespuestaPregunta(String a){
		respuesta = a;
//		try {
//			respuesta = URLEncoder.encode(a,"utf-8");
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
	}
	
	public void setPreguntaSecreta(String a){
		preguntasecreta = a;
//		try {
//			preguntasecreta = URLEncoder.encode(a,"utf-8");
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	
	public void setCorreo(String a){
		correo = a;
//		try {
//			correo = URLEncoder.encode(a,"utf-8");
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	
	public void setContrasena(String a){
		contrasena = a;
//		try {
//			contrasena = URLEncoder.encode(a,"utf-8");
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	
	public void setFecha(int d, int m, int a){
		dia = d;
		mes = m;
		ano = a;
		fecha = String.format("%02d-%02d-%d 00:00:00", ano, mes, dia);
	}
	
	public void setSexo(String a){
		if(a.equals("Masculino"))
			sexo = "0";
		else 
			sexo = "1";
	}
	
	public void setEstado(String a){
		estado = a;
	}
	
	public void setCiudadCod(String a){
		ciudad = a;
	}
	
	public void setRecibirCorreos(boolean a){
		recibirCorreos = a;
	}
	
	public void setTipoCedula(String a){
		tipocedula = a;
//		try {
//			tipocedula = URLEncoder.encode(a,"utf-8");
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	
	
	

}
