package com.mobilemedianet.cinesunidosAndroid.managers;

import android.util.Log;

import com.mobilemedianet.cinesunidosAndroid.objects.Movie;
import com.mobilemedianet.cinesunidosAndroid.parsers.EstrenosParserJson;
import com.mobilemedianet.cinesunidosAndroid.parsers.MovieDetailsParserSax;

import java.util.Iterator;
import java.util.List;

public class EstrenosManager {

	private String TAG = this.getClass().getSimpleName();

	private static EstrenosManager instance = null;
	private List<Movie> pelis;
	private boolean isloaded = false;
	
	static public EstrenosManager getInstance(){
		if(instance == null)
			instance = new EstrenosManager();
		return instance;
	}
	
	
	public boolean loadMovies()
	{
		//Cambio a Json
		HttpHandler sh = new HttpHandler();



		//String url = "http://cu-dev-wcf-services.cloudapp.net/Premier.svc/";
		// Making a request to url and getting response
		String jsonStr = sh.makeServiceCall(ConnectionManager.CU_WS_NEW + ConnectionManager.ESTRENOS_WS);

		Log.e(TAG, "Response from url: " + jsonStr);


		if (jsonStr != null) {

			EstrenosParserJson parserjson = new EstrenosParserJson();
			pelis = parserjson.parse(jsonStr);

			if(pelis != null)
			{
				isloaded = true;
				return true;
			}
		}
		return false;


//		EstrenosParserSax mpar;
//
//        mpar = new EstrenosParserSax("http://" + ConnectionManager.wsCinesUnidos + "/Blackberry.asmx/GetPremier", 0);
//
//        //GetCinemaMoviesDay?cines=string&date=string
//
//        pelis = mpar.parse();
//        if(pelis != null){
//        	isloaded = true;
//        	return true;
//        }
//        return false;
        	
	}
	
	public boolean isLoaded(){
		return isloaded;
	}
	
	public List<Movie> getMovies(){
		return pelis;
	}
	
	public Movie getMovieById(String id){

		if(pelis == null)
			return null;
		
		Iterator<Movie> peliterator = pelis.iterator();
		
		Movie dumpeli = null;	
			
		while(peliterator.hasNext()){
			dumpeli = (Movie) peliterator.next();
			if(dumpeli.getId() == id)
				return dumpeli;
		}
		
		return null;
		
		
	}
	
	public boolean loadMoviesDetail(String id){
		MovieDetailsParserSax mpar = null;		
		String dum = String.format("%07d", id);		
		Movie a = getMovieById(id);
		
		if(a.getPreSale().equals("P")){
			mpar = new MovieDetailsParserSax("http://" + ConnectionManager.wsCinesUnidos + "/Blackberry.asmx/GetMovieExt?id=" + dum, "");
	
			
			if(mpar.parse(a)){
				a.setDetailsLoaded(true);			
			}else{
				return false;
			}
			
		}else{
			mpar = new MovieDetailsParserSax("http://" + ConnectionManager.wsCinesUnidos + "/Blackberry.asmx/GetPremierExt?id=" + dum, "");
			

			if(mpar.parse(a)){
				a.setDetailsLoaded(true);			
			}else{
				return false;
			}
			
		}
			
		

		return true;
		
		
		
	}

}
