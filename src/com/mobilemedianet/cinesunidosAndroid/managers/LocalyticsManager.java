package com.mobilemedianet.cinesunidosAndroid.managers;

import com.localytics.android.LocalyticsSession;

import android.content.Context;

public class LocalyticsManager {
	
	static LocalyticsManager instance = null;
	
	static String localyticsKey = "db82b8a8a3d68a4149863bd-ad0d7d64-e234-11e1-4bed-00ef75f32667";
	static private LocalyticsSession localyticsSession;
	
	static public LocalyticsSession localyticsSession(){
		return localyticsSession;		
	}
	
	static public LocalyticsSession newLocalytics(Context appContext){
				
		localyticsSession = new LocalyticsSession(appContext,  localyticsKey); 
		localyticsSession.open();                // open the session
		return localyticsSession;
	}
	
	static public void endSession(){
		if(localyticsSession != null){
			localyticsSession.close();
			localyticsSession.upload();
			localyticsSession = null;
		}
	}		
	

}
