  package com.mobilemedianet.cinesunidosAndroid.managers;


  import android.util.Log;

import com.mobilemedianet.cinesunidosAndroid.objects.Boleto;
import com.mobilemedianet.cinesunidosAndroid.objects.Butaca;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.List;

import static org.acra.ACRA.log;

  public class NewSeats {
		
//		vistaUserData
		private  String cinemaId;
		private  String performanceId;
	  	private  String movieId;
		private  String UserId;
		private  String UserFirstName;
		private  String UserLastName;
		private  String UserEmail;
		private  String UserPhone;
		private  String UserNationalId;
		private  String UserSelectsSeats;
		private  String UserSeatSelection;
		private  String UserTicketSelection;
	  	private List<Boleto> userTicketSelectionNew;
		private boolean seatChanged = false;
		
		
		private String bodyRequest;

		private String TAG = this.getClass().getSimpleName();
				
		private String xmlns = "xmlns=\"http://webservice.cinesunidos.net\">";
		
		public static NewSeats instance = null;
		
		private String result;
		private String orderid;
		private String salaString;
		
		public static NewSeats getInstance(){
			if(instance == null){
				instance = new NewSeats();
			}
			return instance;
		}
		
		public boolean getSeatChanged(){
			return seatChanged;
		}
		
		public void setcinemaId(String a){
			cinemaId = a;
		}
		
		public void setperformanceId(String a){
			performanceId = a;
		}
		
		public void setUserId(String a){
			UserId = a;
		}
		
		public void setUserFirstName(String a){
			UserFirstName = a;
		}
		
		public void setUserLastName(String a){
			UserLastName = a;
		}
		
		public void setUserEmail(String a){
			UserEmail = a; 
		}
		
		public void setUserPhone(String a){
			UserPhone = a;
		}
		
		public void setUserNationalId(String a){
			UserNationalId = a;
		}
		
		public void setUserSelectsSeats(String a){
			UserSelectsSeats = a;
		}
		
		public void setUserSeatSelection(String a){
			UserSeatSelection = a;
		}
		
		public void setUserTicketSelection(String a){
			UserTicketSelection = a;
		}

	  public List<Boleto> getUserTicketSelectionNew() {
		  return userTicketSelectionNew;
	  }

	  public void setUserTicketSelectionNew(List<Boleto> userTicketSelectionNew) {
		  this.userTicketSelectionNew = userTicketSelectionNew;
	  }

	  public String getMovieId() {
		  return movieId;
	  }

	  public void setMovieId(String movieId) {
		  this.movieId = movieId;
	  }

	  public void generateBodyRequest(){
			
			bodyRequest = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
			bodyRequest += "<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">";
			bodyRequest += "<soap12:Body>";
			bodyRequest += "<GetOrderNewSeats xmlns=\"http://tempuri.org/\">";
			
			
			bodyRequest += "<cinemaId>" + cinemaId + "</cinemaId>";
			bodyRequest += "<performanceId>" + performanceId + "</performanceId>";
			
			bodyRequest += "<userData>";
			
			bodyRequest += "<UserId " + xmlns + UserId + "</UserId>";
			bodyRequest += "<UserFirstName " + xmlns + UserFirstName + "</UserFirstName>";
			bodyRequest += "<UserLastName " + xmlns + UserLastName + "</UserLastName>";
			bodyRequest += "<UserEmail " + xmlns + UserEmail + "</UserEmail>";
			bodyRequest += "<UserPhone " + xmlns + UserPhone + "</UserPhone>";
			bodyRequest += "<UserNationalId " + xmlns + UserNationalId + "</UserNationalId>";
			bodyRequest += "<UserSelectsSeats " + xmlns + UserSelectsSeats + "</UserSelectsSeats>";
			bodyRequest += "<UserSeatSelection " + xmlns + UserSeatSelection + "</UserSeatSelection>";
			bodyRequest += "<UserTicketSelection " + xmlns + UserTicketSelection + "</UserTicketSelection>";
			
			bodyRequest += "</userData>";
			
						
			bodyRequest += "</GetOrderNewSeats>";
			bodyRequest += "</soap12:Body>";
			bodyRequest += "</soap12:Envelope>";
		}
		
		public  void sendRequest()
		{
			//Cambio a Json
			HttpHandler sh = new HttpHandler();

			JSONArray ticketsSelected = new JSONArray();


			Iterator<Boleto> a = this.userTicketSelectionNew.iterator();

			JSONObject payload = new JSONObject();
			Boleto dum;
			try
			{
				while(a.hasNext())
				{
					dum = a.next();

					JSONObject boletoJson = new JSONObject();

					boletoJson.put("BookingFee", dum.getBookingFee());
					boletoJson.put("ServiceFee", dum.getServiceFee());
					boletoJson.put("ServiceFeeWithTax", dum.getServiceFeeWithTax());
					boletoJson.put("Name", dum.getDescripcion());
					boletoJson.put("Price", dum.getPrice());
					boletoJson.put("Tax", dum.getTax());
					boletoJson.put("Code", dum.getId());
					boletoJson.put("Quantity", dum.getCantidad());

					ticketsSelected.put(boletoJson);
				}



				payload.put("TheaterId", cinemaId);
				payload.put("SessionId", performanceId);
				payload.put("MovieId", movieId);
				payload.put("Tickets", ticketsSelected);
			}
			catch (JSONException e)
			{
				log.e(TAG, e.getMessage());
			}


			result = sh.makeServiceCall(ConnectionManager.CU_WS_NEW + ConnectionManager.BUTACAS_WS, payload);

			Log.e(TAG, "Response from url: " + result);






//			StringEntity tmp = null;
//			HttpResponse response = null;
//			StringBuilder tmp2 = null;
//			String userAgent = ConnectionManager.getInstance().getUserAgent();
//
//			generateBodyRequest();
//
//			 try {
//				 tmp = new StringEntity(bodyRequest,"UTF-8");
//			} catch (UnsupportedEncodingException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//
//			 HttpClient httpclient = new DefaultHttpClient();
//			 HttpPost httppost = new HttpPost("http://" + ConnectionManager.wsCinesUnidos + "/Blackberry.asmx");
//
//			 httppost.setHeader("Content-Type", "application/soap+xml; charset=utf-8");
//			 httppost.setHeader("User-Agent", userAgent);
//
//			 httppost.setEntity(tmp);
//
//			 try {
//				 response = httpclient.execute(httppost);
//			} catch (ClientProtocolException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//
//			 try {
//				tmp2 = inputStreamToString(response.getEntity().getContent());
//			} catch (IllegalStateException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}

//			 result = tmp2.toString();

		}
		
		public boolean getResult(boolean checkSeats)
		{
			if (result == null || result.length() == 0)
			{
				return false;
			}

			if (checkSeats)
			{
				confirmSeats();
				refreshMap();

				try {
					JSONObject jsonObj = new JSONObject(result);

					if (jsonObj.getBoolean("WithError"))
					{
						return false;
					}

					orderid = jsonObj.getString("Id");
					salaString = jsonObj.getString("Seats");
					seatChanged = true;
					return true;

				}
				catch (final JSONException e)
				{
					Log.e(TAG, "Json parsing error: " + e.getMessage());
					return false;
				}

//				if (confirmSeats())
//				{
//					refreshMap();
//					return true;
//				}
//				return false;
			}
			else
			{
				try {
					JSONObject jsonObj = new JSONObject(result);

					if (jsonObj.getBoolean("WithError"))
					{
						return false;
					}

					orderid = jsonObj.getString("Id");
					salaString = jsonObj.getString("Seats");
					seatChanged = true;
					return true;

				}
				catch (final JSONException e)
				{
					Log.e(TAG, "Json parsing error: " + e.getMessage());
					return false;
				}
			}






//			int index;
//
//			String resultwitherrors = getValue(result, "Result WithErrors");
//			String ordersuccessful = getValue(result, "Order Successful");
//			String seatschanged = null;
//
//
//
//			if(!resultwitherrors.equals("False") || !ordersuccessful.equals("True"))
//				return false;
//
//			orderid = getValue(result, "OrderId");
//
//			if(checkSeats){
//
//				seatschanged = getValue(result, "SeatsChanged");
//
//				index = result.indexOf("<SessionSeatData><![CDATA[");
//
//				index = result.indexOf("CDATA[", index) + 6;
//
//				String tmp = result.substring(index);
//
//				index = tmp.indexOf("]");
//
//				salaString = tmp.substring(0, index);
//
//				if(seatschanged.equals("True"))
//					seatChanged = true;
//				else
//					seatChanged = false;
//
//
//
//			}else{
//
//				index = result.indexOf("SessionSeatData");
//
//				index = result.indexOf(';', index) + 1;
//
//				String tmp = result.substring(index);
//
//				index = tmp.indexOf("&");
//
//				salaString = tmp.substring(0, index);
//
//			}
//
//			return true;
			
		}


	  public  void refreshMap()
	  {
		  HttpHandler sh = new HttpHandler();

		  result = sh.makeServiceCall(ConnectionManager.CU_WS_NEW + ConnectionManager.REFRESH_MAP_WS + orderid );
		  Log.e(TAG, "URL: " + ConnectionManager.CU_WS_NEW + ConnectionManager.REFRESH_MAP_WS + orderid );
		  Log.e(TAG, "Response from url - refresmap: " + result);


	  }


		
		public void checkSeatSelection(){			

			//confirmSeats();
			//sendRequest();
//				String seleccion = getSelectedSeats();
//				String request = "http://" + ConnectionManager.wsCinesUnidos + "/blackBerry.asmx/GetOrderSeats?orderId=" + orderid + "&selectedSeatData=" + seleccion;
//				String userAgent = ConnectionManager.getInstance().getUserAgent();
//
//				HttpResponse response = null;
//				StringBuilder tmp2 = null;
//
//				HttpClient httpclient = new DefaultHttpClient();
//				HttpGet httpget = new HttpGet(request);
//				httpget.setHeader("Content-Type", "application/soap+xml; charset=utf-8");
//				httpget.setHeader("User-Agent", userAgent);
//
//				try {
//					 response = httpclient.execute(httpget);
//				} catch (ClientProtocolException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//
//				 try {
//					tmp2 = inputStreamToString(response.getEntity().getContent());
//				} catch (IllegalStateException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//
//				 result = tmp2.toString();
			
			
		}

	  public boolean confirmSeats(){



		  //Cambio a Json
		  HttpHandler sh = new HttpHandler();

		  JSONArray seatsSelected = new JSONArray();


		  Iterator<Butaca> butacas = ButacasManager.getInstance().getReserverdSeatsIterator();


		  JSONObject payload = new JSONObject();
		  Butaca dum;
		  try
		  {
			  while(butacas.hasNext())
			  {
				  dum = butacas.next();

				  JSONObject butacaJson = new JSONObject();

				  butacaJson.put("AreaCategoryCode", dum.getAreaCategoryCode());
				  butacaJson.put("AreaNumber", dum.getAreaNumber());
				  butacaJson.put("ColumnIndex", dum.getX());
				  butacaJson.put("Name", dum.getNombreButaca());
				  butacaJson.put("RowIndex", dum.getY());
				  butacaJson.put("Status", "0");
				  butacaJson.put("Type", 1);


				  seatsSelected.put(butacaJson);
			  }

			  payload.put("UserSessionId", orderid);
			  payload.put("Seats", seatsSelected);
		  }
		  catch (JSONException e)
		  {
			  log.e(TAG, e.getMessage());
		  }

		  log.e(TAG, "JSON CONFIRMACION " + payload);


		  String confirmResult = sh.makeServiceCall(ConnectionManager.CU_WS_NEW + ConnectionManager.BUTACAS_CONFIRM_WS, payload);

		  Log.e(TAG, "Response from url: " + confirmResult);


		  try {
			  JSONObject jsonObj = new JSONObject(confirmResult);

			  boolean isValid = jsonObj.getBoolean("IsValid");


			  seatChanged = isValid;
			  return isValid;


		  }
		  catch (final JSONException e)
		  {
			  Log.e(TAG, "Json parsing error: " + e.getMessage());
			  return false;
		  }


	  }



		private String getSelectedSeats(){
			
			String seleccion = "";
	        String tmp = "";
			int count = 0;
	        
			Iterator<Butaca> butacas = ButacasManager.getInstance().getReserverdSeatsIterator();
			
			if(butacas == null)
				return null;
			
			Butaca dum;
			
			
			
			while(butacas.hasNext()){
				count++;
				dum = butacas.next();
				tmp += ButacasManager.getInstance().getDimensionSala() + "%7C1%7C" + (dum.getY() + 1) + "%7C" + (dum.getX() + 1) + "%7C";
			}		
			
			seleccion += "%7C" + count + "%7C" + tmp; 
			
			return seleccion;
			
		}
		
		public String getSalaString(){
			return salaString;
		}
		
		public String getOrderId(){
			return orderid;
		}
		
		String getValue(String str, String tag){
			int index = str.indexOf(tag);
			index = str.indexOf("\"", index) + 1;
			String result = str.substring(index, str.indexOf("\"", index + 1));
			return result;
			
		}
		
		private StringBuilder inputStreamToString(InputStream is) {
		    String line = "";
		    StringBuilder total = new StringBuilder();
		    
		    // Wrap a BufferedReader around the InputStream
		    BufferedReader rd = new BufferedReader(new InputStreamReader(is));

		    // Read response until the end
		    try {
				while ((line = rd.readLine()) != null) { 
				    total.append(line); 
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
		    // Return full string
		    return total;
		}
		
		

	

}
