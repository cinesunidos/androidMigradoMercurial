package com.mobilemedianet.cinesunidosAndroid.managers;

import android.app.AlertDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;

import com.mobilemedianet.cinesunidosAndroid.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;

public class ConnectionManager {
	
	static ConnectionManager instance = null;
	static Context context;
	public static String wsCinesUnidos = "ws.cinesunidos.com";//ws.cinesunidos.com - bbservice.cinesunidos.net/Blackberry
	//public static String wsCinesUnidos = "200.1.119.40:7080";
	//public static String wsCinesUnidos = "localhost:52185";


	public static String newWS_dev = "http://cu-dev-wcf-services.cloudapp.net/";
	public static String newWS_prod = "";

	public static String CU_WS_NEW = newWS_dev;


	public static String ESTRENOS_WS = "Premier.svc/";
	public static String PELICULAS_WS = "Movie.svc/ListFavorite";
	public static String CINES_WS = "AppMobile.svc/GetTheaters";
	public static String LOGIN_WS = "Security.svc/SignIn/";
	public static String REGISTRO_WS = "Security.svc/UserMobile/";
	public static String TICKETS_WS = "Theater.svc/Session/";
	public static String BUTACAS_WS = "UserSessionServices.svc/Mobile";
	public static String BUTACAS_CONFIRM_WS = "UserSessionServices.svc/ConfirmationMobile/";
	public static String REFRESH_MAP_WS = "UserSessionServices.svc/";
	public static String PAGO_WS = "UserSessionServices.svc/PaymentMobile/";


	public static String GetCinemaMoviesDay = "GetCinemaMoviesDay.xml";
	public static String GetTheaters = "Theaters.xml";



	static String cinemaTimeTag = "CinemaTimeCache";
	static String movieTimeTag = "MovieDetailTimeCache";
	static String posterTimeTag = "PostersTimeCache";
	static String theatersTimeTag = "TheatersTimeCache";
	
	static int cinematime = 5; //horas
	static int movietime = 5;
	static int postertime = 2; //dias
	static int theaterstime = 5;
	static int lasterror;
	
	String UserAgent;
	boolean internet = true;
	
	public static ConnectionManager  getInstance(){
		
		if(instance == null)
			instance = new ConnectionManager();		
		
		return instance;
		
	}
	
	public String getUserAgent(){
		return UserAgent;
	}
	
	public ConnectionManager(){
		UserAgent = "Plataform:Android;SOVersion:" + Build.VERSION.RELEASE + ";Brand:" + 
		        Build.MANUFACTURER + ";Model:" + Build.MODEL;
	}
	
	static public void setContext(Context ctx){
		context = ctx;		
	}
	
		
	public InputStream getFileStream(String name) {
		
		File f = new File(context.getFilesDir(), name);
		
		FileInputStream fis = null;
		try {
		fis = new FileInputStream(f);
		} catch (Exception e) {
			return null;		
		}
		return fis;
		}
	
	
	public boolean testCacheFile(String file){
		
		if(getFileStream(file) == null)
			return false;
		
		return true;
		
	}
	
	public int getLastError(){
		return lasterror;
	}
		
	public InputStream getInputStreamFrom(String url, String filename){
		
		InputStream file = null;		
		
		
		file = getFileStream(filename);
		
		if(filename.contains(GetCinemaMoviesDay)){
			
			if(compareTime(cinemaTimeTag) == false){
				try {
					if(file != null)
						file.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				file = null;
				clearCache(GetCinemaMoviesDay);				   
				PrefManager.getInstance().setCacheTime(cinemaTimeTag);				
				
			}
			
		}else if(filename.contains("movie_")){
			
			if(compareTime(movieTimeTag) == false){
				try {
					if(file != null)
						file.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				file = null;
				clearCache("movie_");
				PrefManager.getInstance().setCacheTime(movieTimeTag);
				
			}
			
		}else if(filename.contains("HO")){
			
			if(compareTime(posterTimeTag) == false){
				try {
					if(file != null)
						file.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				file = null;			
				clearCache("afiche_");
				PrefManager.getInstance().setCacheTime(posterTimeTag);
				
			}
			
		}else if(filename.contains(GetTheaters)){
			
			if(compareTime(posterTimeTag) == false){
				try {
					if(file != null)
						file.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				file = null;
				clearCache(GetTheaters);
				PrefManager.getInstance().setCacheTime(GetTheaters);
				
			}
			
		}
		
		if(file == null){
			try {			
			downloadFile(url, filename);
			file = getFileStream(filename);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				lasterror = -1;
				return null;
				}
			}	
		
		
		lasterror = 0;
		return file;		
				
	}
	
	public void downloadFile(String link, String filename) throws Exception{		    
		  	
		  	try {
		  		URL url = new URL (link);
		  		URLConnection conexion = url.openConnection(); //URLConnection conexion = url.openConnection();  //HttpURLConnection conexion = (HttpsURLConnection) url.openConnection();
			    conexion.setConnectTimeout(20000);
				conexion.setRequestProperty("User-Agent", UserAgent);
				InputStream input = conexion.getInputStream();//openConnection().getInputStream();
				
				try {    
			    	OutputStream output = context.openFileOutput(filename, Context.MODE_PRIVATE);			    	
			        try {
			            byte[] buffer = new byte[1024];
			            int bytesRead = 0;
			            while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) {
			                output.write(buffer, 0, bytesRead);
			            }
			        } finally {
			            output.close();
			        }
			    } finally {
			        input.close();
			    }
		  	} catch (java.net.SocketTimeoutException e) { //SOCKET EXCEPTION TIMEOUT
		  		e.printStackTrace();
		  		lasterror = -4;
		  		return;
		  	} catch (java.net.SocketException e) {
		  		lasterror = -5;
		  		return;
		  	} 
		  				
			/*if(conexion.getResponseCode() >= 400){   //Manejo de errores en el servidor
			    lasterror = -3;
			    conexion.disconnect();
			    return;
		    }*/
		    
		    
		
	}
	
		
	
	public void clearCache(String tag){		
		File file = new File(context.getFilesDir(), "");
	    if (file != null && file.isDirectory()) {
	        File[] files = file.listFiles();
	        if(files != null) {
	            for(File f : files) {  
	            	if(tag != null && f.getName().contains(tag)){	            		
	            		f.delete();	            		
	            	}
	            	else if(tag == null)
	            		f.delete();
	            }
	        }
	    }
	    
	    
	}
	
	
	
	
	private boolean compareTime(String tag){
		
		Calendar todaydate = Calendar.getInstance();
		Calendar cachedate = Calendar.getInstance();
		
		String tmp = PrefManager.getInstance().getCacheTime(tag);
		
		if(tmp == null)
			return false;
		
		String time2[] = tmp.split("-");
		
		
		int horacache = Integer.parseInt(time2[1]);
		
		String fechacache[] = time2[0].split("/");

		cachedate.set(Integer.parseInt(fechacache[2]), Integer.parseInt(fechacache[1]), Integer.parseInt(fechacache[0]), horacache, 0);
		cachedate.set(Calendar.HOUR_OF_DAY, horacache);		

		if(tag.equals(cinemaTimeTag)){
			
			cachedate.add(Calendar.HOUR, cinematime);

			if(cachedate.compareTo(todaydate) < 0)
				return false;
			
		}else if(tag.equals(movieTimeTag)){
			
			cachedate.add(Calendar.HOUR, movietime);
			
			if(cachedate.compareTo(todaydate) < 0)
				return false;
			
		}else if(tag.equals(theatersTimeTag)){
			
			cachedate.add(Calendar.HOUR, theaterstime);
			
			if(cachedate.compareTo(todaydate) < 0)
				return false;
			
		}else if(tag.equals(posterTimeTag)){

			
			cachedate.add(Calendar.DATE, postertime);
			
			if(cachedate.compareTo(todaydate) < 0)
				return false;

		}
		
		return true;
				
		
	}
	
	public InputStream getConnection(String url){
		URL rssUrl;
		URLConnection conexion;
		InputStream stream;
		
		if(checkConn() == false)
			return null;
		
		try {
			rssUrl = new URL(url);			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			lasterror = -1;
			return null;
		}
		
		try {
			conexion = rssUrl.openConnection();
			conexion.setRequestProperty("User-Agent", UserAgent);	
			conexion.setConnectTimeout(10000);
	//		conexion.setReadTimeout(7000);
			stream = conexion.getInputStream();//openConnection().getInputStream();
		} catch (java.net.SocketTimeoutException e) { //SOCKET EXCEPTION TIMEOUT
			e.printStackTrace();
			lasterror = -4;
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			lasterror = -1;
			return null;
		} 
		
		lasterror = 0;
		
		return stream;
		
	}
	
	public void mensajeError(String error){
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("CinesUnidos");
        builder.setMessage(error);       
        builder.setPositiveButton("OK",null);
        builder.create();
        builder.setIcon(R.drawable.icono);
        builder.show(); 
	}
	
	public static boolean checkConn(){
		ConnectivityManager conMgr =  (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		boolean ret = true;
		
		if (conMgr != null) {
		    NetworkInfo i = conMgr.getActiveNetworkInfo();
		    if (i != null) {
		        if (!i.isConnected())
		            ret = false;
		        if (!i.isAvailable())
		            ret = false;                
		    }

		    if (i == null)
		        ret = false;

		} else
		    ret = false;
		
		return ret;
		
	}
}
