package com.mobilemedianet.cinesunidosAndroid.managers;


	import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static org.acra.ACRA.log;

public class Pagar {
		
//		vistaUserData
		private  String cinemaId;
		private  String performanceId;
		private  String UserId;
		private  String UserFirstName;
		private  String UserLastName;
		private  String UserEmail;
		private  String UserPhone;
		private  String UserNationalId;
		private  String UserSelectsSeats;
		private  String UserSeatSelection;
		private  String UserTicketSelection;
		private  String OrderId; 
		
//		Payment data
		
		private  String CardExpiryMonth;
		private  String CardExpiryYear;
		private  String CardHolder;
		private  String CardNumber;
		private  String CardType;
		private  String ClubNationalId;
	private  String TypeNationalId;
		private  String PaymentType;
		
		private boolean seleccion_butacas = false;
		
		private int error;
		
		private  String verificationCode;
		
		private String bodyRequest;
		
		private String xmlns = "xmlns=\"http://webservice.cinesunidos.net\">";
		
		public static Pagar instance = null;

		private String TAG = this.getClass().getSimpleName();

		private String result;
		
		public String msgerr1;
		public String msgerr2;
		
		public int getLastError(){
			return error;
		}
		
		public static Pagar getInstance(){
			if(instance == null){
				instance = new Pagar();
			}
			return instance;
		}
		
		public void setSeleccionButacas(boolean a){
			seleccion_butacas = a;
		}
		
		public void setOrderId(String a){
			OrderId = a;
		}
		
		public Pagar(){
			
		}
		
		public void setcinemaId(String a){
			cinemaId = a;
		}
		
		public void setperformanceId(String a){
			performanceId = a;
		}

	public String getTypeNationalId() {
		return TypeNationalId;
	}

	public void setTypeNationalId(String typeNationalId) {
		TypeNationalId = typeNationalId;
	}

	public void setUserId(String a){
			UserId = a;
		}
		
		public void setUserFirstName(String a){
			UserFirstName = a;
		}
		
		public void setUserLastName(String a){
			UserLastName = a;
		}
		
		public void setUserEmail(String a){
			UserEmail = a; 
		}
		
		public void setUserPhone(String a){
			UserPhone = a;
		}
		
		public void setUserNationalId(String a){
			UserNationalId = a;
		}
		
		public void setUserSelectsSeats(String a){
			UserSelectsSeats = a;
		}
		
		public void setUserSeatSelection(String a){
			UserSeatSelection = a;
		}
		
		public void setUserTicketSelection(String a){
			UserTicketSelection = a;
		}
		
		public void setCardExpiryMonth(String a){
			CardExpiryMonth = a;
		}
		
		public void setCardExpiryYear(String a){
			CardExpiryYear = a;
		}
		
		public void setCardHolder(String a){
			CardHolder = a;
		}
		
		public void setCardNumber(String a){
			CardNumber = a;
		}
		
		public void setCardType(String a){
			CardType = a;
		}
		
		public void setClubNationalId(String a){
			ClubNationalId = a;
		}
		
		public void setPaymentType(String a){
			PaymentType = a;
		}
		
		public void setverificationCode(String a){
			verificationCode = a;
		}
		
		public void generateBodyRequest(){
			
			bodyRequest = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
			bodyRequest += "<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">";
			bodyRequest += "<soap12:Body>";
			
			if(seleccion_butacas == true)
				bodyRequest += "<GetOrderPaymentByInvitation xmlns=\"http://tempuri.org/\">";
			else
				bodyRequest += "<GetBookingByInvitation xmlns=\"http://tempuri.org/\">";
			
			
			bodyRequest += "<cinemaId>" + cinemaId + "</cinemaId>";
			
			if(seleccion_butacas == true)
				bodyRequest += "<orderId>" + OrderId + "</orderId>";
			else
				bodyRequest += "<performanceId>" + performanceId + "</performanceId>";
			
			bodyRequest += "<vistaUserData>";
			
			bodyRequest += "<UserId " + xmlns + UserId + "</UserId>";
			bodyRequest += "<UserFirstName " + xmlns + UserFirstName + "</UserFirstName>";
			bodyRequest += "<UserLastName " + xmlns + UserLastName + "</UserLastName>";
			bodyRequest += "<UserEmail " + xmlns + UserEmail + "</UserEmail>";
			bodyRequest += "<UserPhone " + xmlns + UserPhone + "</UserPhone>";
			bodyRequest += "<UserNationalId " + xmlns + UserNationalId + "</UserNationalId>";
			bodyRequest += "<UserSelectsSeats " + xmlns + UserSelectsSeats + "</UserSelectsSeats>";
			bodyRequest += "<UserSeatSelection " + xmlns + UserSeatSelection + "</UserSeatSelection>";
			bodyRequest += "<UserTicketSelection " + xmlns + UserTicketSelection + "</UserTicketSelection>";
			
			bodyRequest += "</vistaUserData>";
			
			bodyRequest += "<paymentData>";
			
			bodyRequest += "<CardExpiryMonth>" + CardExpiryMonth + "</CardExpiryMonth>";
			bodyRequest += "<CardExpiryYear>" + CardExpiryYear + "</CardExpiryYear>";
			bodyRequest += "<CardHolder>" + CardHolder + "</CardHolder>";
			bodyRequest += "<CardNumber>" + CardNumber + "</CardNumber>";
			bodyRequest += "<CardType>" + CardType + "</CardType>";
			bodyRequest += "<ClubNationalId>" + ClubNationalId + "</ClubNationalId>";
			bodyRequest += "<PaymentType>" + PaymentType + "</PaymentType>";
			
			bodyRequest += "</paymentData>";
			
			bodyRequest += "<verificationCode>" + verificationCode + "</verificationCode>";
			bodyRequest += "<userThatInvite></userThatInvite>";
			
			if(seleccion_butacas == true)
				bodyRequest += "</GetOrderPaymentByInvitation>";
			else
				bodyRequest += "</GetBookingByInvitation>";
			
			bodyRequest += "</soap12:Body>";
			bodyRequest += "</soap12:Envelope>";
		}
		
		public  void sendRequest(){

			error = 0;
			//Cambio a Json
			HttpHandler sh = new HttpHandler();

			JSONObject payload = new JSONObject();
			try
			{
//				int type = 0;
//				int month = 2;
//				int year = 2018;
//				String cardNumber = "4110970045642462";
//				int secCod =  230;
//				String name = "Pedro M";
//				String certificateType = "V";
//				int certificate = 4095730;
//				String phone = "04127354692";
//				String email = "example@domain.com";
//
//
//				payload.put("UserSessionId", OrderId );
//				payload.put("Type", type);
//				payload.put("Month", month);
//				payload.put("Year", year);
//				payload.put("Number", cardNumber);
//				payload.put("SecurityCode", secCod);
//
////				payload.put("Type", "0");
////				payload.put("Month", "2");
////				payload.put("Year", "2018");
////				payload.put("Number", "4110970045642462");
////				payload.put("SecurityCode", "230");
//				payload.put("Name", name);
//				payload.put("CertificateType", certificateType);
//				payload.put("Certificate", certificate);
//				payload.put("Phone", phone);
//				payload.put("Email", email);

				payload.put("UserSessionId", OrderId );
				payload.put("Type", CardType);
				payload.put("Month", CardExpiryMonth);
				payload.put("Year", CardExpiryYear);
				payload.put("Number", CardNumber);
				payload.put("SecurityCode", verificationCode);

				payload.put("Name", CardHolder);
				payload.put("CertificateType", TypeNationalId);
				payload.put("Certificate", ClubNationalId);
				payload.put("Phone", UserPhone);
				payload.put("Email", UserEmail);

			}
			catch (JSONException e)
			{
				log.e(TAG, e.getMessage());
				error = -1;
				return;
			}


			result = sh.makeServiceCall(ConnectionManager.CU_WS_NEW + ConnectionManager.PAGO_WS, payload);

			Log.e(TAG, "Response from url: " + result);










//			error = 0;
//			//StringEntity tmp = null;
//			//HttpResponse response = null;
//			//StringBuilder tmp2 = null;
//			//String userAgent = ConnectionManager.getInstance().getUserAgent();
//			generateBodyRequest();
//
//			 try {
//				 tmp = new StringEntity(bodyRequest,"UTF-8");
//			} catch (UnsupportedEncodingException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//
//			 HttpClient httpclient = new DefaultHttpClient();
//			 HttpPost httppost = new HttpPost("http://" + ConnectionManager.wsCinesUnidos + "/Blackberry.asmx");
//
//			 httppost.setHeader("Content-Type", "application/soap+xml; charset=utf-8");
//			 httppost.setHeader("User-Agent", userAgent);
//
//			 httppost.setEntity(tmp);
//
//			 try {
//				 response = httpclient.execute(httppost);
//			} catch (ClientProtocolException e) {
//				// TODO Auto-generated catch block
//				error = -1;
//				return;
//			} catch (IOException e) {
//				error = -2;
//				return;
//			}
//
//			 try {
//				tmp2 = inputStreamToString(response.getEntity().getContent());
//			} catch (IllegalStateException e) {
//				// TODO Auto-generated catch block
//				error = -3;
//				return;
//			} catch (IOException e) {
//				error = -4;
//				return;
//			}
//
//			 result = tmp2.toString();
			 		 

		}
		
		public String getResult()
		{
			String bookingNumber = "";

			if (result == null || result.length() == 0)
			{
				return null;
			}

			try
			{
				JSONObject jsonObj = new JSONObject(result);

				JSONObject jsonConfirmation = jsonObj.getJSONObject("Confirmation");

				if (!jsonConfirmation.getBoolean("IsValid"))
				{
					msgerr1 = jsonObj.getString("CodeResult");
					msgerr2 = jsonObj.getString("DescriptionResult");

				}

				bookingNumber = jsonObj.getString("BookingNumber");

				if (msgerr1.equals("00"))
				{
					return bookingNumber;
				}
				return null;



			}
			catch (final JSONException e)
			{
				Log.e(TAG, "Json parsing error: " + e.getMessage());
				return null;
			}











//			int index, index1;
//			String BookingNumber;
//			String pars1 = "Message&gt;&lt;![CDATA[";
//			String parse2 = "ErrorMessage&gt;";
//
//			index = result.indexOf("BookingNumber");
//
//			if(index == -1){
//
//				msgerr1 = msgerr2 = null;
//
//				index = result.indexOf(pars1) + pars1.length();
//				if(index == -1)
//					return null;
//				index1 = result.indexOf("]]&gt", index);
//				if(index1 == -1)
//					return null;
//				msgerr1 = result.substring(index, index1);
//
//				index = result.indexOf(parse2) + parse2.length();
//				if(index == -1)
//					return null;
//				index1 = result.indexOf("&lt;/ErrorMessage", index);
//				if(index1 == -1)
//					return null;
//				msgerr2 = result.substring(index, index1);
//
//
//				return null;
//			}
//			index = result.indexOf("&gt;", index) + 4;
//
//			BookingNumber = result.substring(index, result.indexOf("&lt", index));
//
//			return BookingNumber;
			
		}
		
		private StringBuilder inputStreamToString(InputStream is) {
		    String line = "";
		    StringBuilder total = new StringBuilder();
		    
		    // Wrap a BufferedReader around the InputStream
		    BufferedReader rd = new BufferedReader(new InputStreamReader(is));

		    // Read response until the end
		    try {
				while ((line = rd.readLine()) != null) { 
				    total.append(line); 
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
		    // Return full string
		    return total;
		}
		
		

	

}
