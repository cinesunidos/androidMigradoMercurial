package com.mobilemedianet.cinesunidosAndroid.managers;

import java.io.IOException;
import java.util.Iterator;
import java.util.Vector;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.mobilemedianet.cinesunidosAndroid.objects.Butaca;
import com.mobilemedianet.cinesunidosAndroid.objects.InformacionUsuario;
import com.mobilemedianet.cinesunidosAndroid.parsers.AsientosParserSax;

public class AsientosManager {
	
	static private AsientosManager instance = null;
	
	private int SeatsAvailable;
	private Vector<Butaca> butacasSeleccionadas;
	private Butaca matriz[][];
	private int reservedcount;
	private boolean backFromMap = false;	
	private String orderId;
	private Context apcontext;
	
	static public AsientosManager getInstance(){
		
		if(instance == null)
			instance = new AsientosManager();
		
		return instance;
		
	}
	
		
	public void LoadSeatsAvailability(String cinemaId, String performanceId){
		
		AsientosParserSax apar;
		
		apar = new AsientosParserSax("http://" + ConnectionManager.wsCinesUnidos + "/blackBerry.asmx/GetSeatsAvailability?cinemaId=" 
		+ cinemaId +"&performanceId=" + performanceId);
		
		apar.parse();			
		
	}
	
	public Butaca getButacaByName(String name){
		
		for(int i = 0; i < matriz.length; i++)
			for(int j = 0; j < matriz[0].length; j++)				
				if(matriz[i][j] != null && matriz[i][j].getNombreButaca().equals(name))
					return matriz[i][j];
		
		return null;
		
	}
	
	
	
	public void LoadSeats(){
		
		DetallesCompra comprar = DetallesCompra.getInstance();
		InformacionUsuario user = LoginManager.getInstance().getUserInfo();
		NewSeats asientos = NewSeats.getInstance();
		
		asientos.setcinemaId(String.valueOf(comprar.getCineId()));
		asientos.setperformanceId(String.valueOf(comprar.getShowTimeId()));
		asientos.setUserId(user.getSecureId());
		asientos.setUserFirstName(user.getName());
		asientos.setUserLastName(user.getLastName());
		asientos.setUserEmail(user.getLogin());
		asientos.setUserNationalId(user.getExternalId());
		asientos.setUserSelectsSeats("false");
		asientos.setUserSeatSelection("");
		asientos.setUserTicketSelection(DetallesCompra.getInstance().getSeleccionVoletos());
		
		asientos.generateBodyRequest();
		asientos.sendRequest();		
		
		if(asientos.getResult(false)){
			matriz = parserSala(asientos.getSalaString());
			orderId = asientos.getOrderId();
			
		}
		else
			matriz = null;
		//Mostrar algun mensaje de error.			
		
				
	}	
	
	public void checkForNewSeats(){
		NewSeats asientos = NewSeats.getInstance();
		asientos.checkSeatSelection();
		if(asientos.getResult(true)){
			matriz = parserSala(asientos.getSalaString());
			orderId = asientos.getOrderId();			
			
		}
		else
			matriz = null;
	}
	
	
	
		
	public String getOrderId(){
		return orderId;
	}
	
	
	
	public void setSeatsAvailability(int i){
		SeatsAvailable = i;
	}
	
	public int getSeatsAvailability(){
		return SeatsAvailable;
	}
	
	private Butaca[][] parserSala(String salaString)
	{
		butacasSeleccionadas = new Vector<Butaca>();
		reservedcount = 0;
		
		String filas[] = salaString.split("\\|");
			
		int cantidadFilas = Integer.parseInt(filas[1]) - 2;
		int cantidadColumnas = Integer.parseInt(filas[2]) - 2;
		
		Butaca[][] salaCine = new Butaca[cantidadColumnas][cantidadFilas];
		
		int posicionY = 0;
		String letra = "";
		String fila = "";
		String butacas = "";
		int j = 0;
		
		
		for (int i = 12; i < filas.length; i++)
		{
			if (j == 0)
			{
				posicionY = Integer.parseInt(filas[i]) - 1;
				fila = filas[i];
				j++;
			}
			else if (j == 1)
			{
				letra = filas[i];
				j++;
			}
			else if (j == 2)
			{
				butacas = filas[i];
				
				for (int k = 0; k < butacas.length(); k += 7)
				{
					
					String butacaString = butacas.substring(k, k+7);					
					char posicionChar = butacaString.toCharArray()[1];
					int posicionX = getPosicionX(posicionChar) - 1;
					int status = Integer.parseInt(String.valueOf(butacaString.toCharArray()[6]));					
					Butaca butaca = new Butaca(letra, (posicionX+1) + "", fila, status);				
					butaca.setX(posicionX);
					butaca.setY(posicionY);
					
					if (butaca.getType() == Butaca.SELECCIONADA)
					{
						butacasSeleccionadas.addElement(butaca);
						reservedcount++;
					}
					salaCine[posicionX][posicionY] = butaca;
				}
				
				j = 0;
			}
		}
				
		return salaCine;
	}
	
	public Butaca[][] getSalaMapa(){
		return matriz;
		
	}
	
	private int getPosicionX(char variable)
	{
		switch (variable) 
		{
			case 'A':
				return 10;
			case 'B':
				return 11;
			case 'C':
				return 12;
			case 'D':
				return 13;
			case 'E':
				return 14;
			case 'F':
				return 15;
			case 'G':
				return 16;
			case 'H':
				return 17;
			case 'I':
				return 18;
			case 'J':
				return 19;
			case 'K':
				return 20;
			case 'L':
				return 21;
			case 'M':
				return 22;
			case 'N':
				return 23;
			case 'O':
				return 24;
			case 'P':
				return 25;
			case 'Q':
				return 26;
			case 'R':
				return 27;
			case 'S':
				return 28;
			case 'T':
				return 29;
			case 'U':
				return 30;
			case 'V':
				return 31;
			case 'W':
				return 32;
			case 'X':
				return 33;
			case 'Y':
				return 34;
			case 'Z':
				return 35;
			default:
				return Integer.parseInt(String.valueOf(variable));
		}
	}
	
	public void addSelection(Butaca butaca){
		butacasSeleccionadas.addElement(butaca);
		reservedcount++;
		if(reservedcount > DetallesCompra.getInstance().getNumeroTotalEntradas()){
			removeFirstSelected();
		}
	}
	
	public void removeFirstSelected(){		
		butacasSeleccionadas.remove(0);
		reservedcount--;
	}
	
	
	public void removeSelection(Butaca butaca){
		Iterator<Butaca> iterator = butacasSeleccionadas.iterator();
		Butaca dum;
		
		while(iterator.hasNext()){
			dum = iterator.next();
			if(dum.getNombreButaca().equals(butaca.getNombreButaca())){
				iterator.remove();
				reservedcount--;
				return;
			}
				
		}
		
	}
	

	public Butaca getFirstReservedSeat(){
		return butacasSeleccionadas.firstElement();
	}
	
	public Iterator<Butaca> getReserverdSeatsIterator(){
		if(butacasSeleccionadas != null)
			return butacasSeleccionadas.iterator();
		else
			return null;
	}
	
	public int getReserverdCount(){
		return reservedcount;
	}
	
	public void setBackFromMap(boolean a){
		backFromMap = a;
	}
	
	public boolean getBackFromMap(){
		return backFromMap;
	}
	
	public void cancelOrder(Context c){
		apcontext = c;
		
		new cancelOrderTask().execute("");
	}
	
	
	private class cancelOrderTask extends AsyncTask<String, Void, Integer> {
		
		ProgressDialog pd;
		 
		 @Override
		protected void onPreExecute() {
			 
			 pd = ProgressDialog.show(apcontext, "Espere.", "Cancelando Orden.", true, false);				
			 
			 
	    	}
	     
		 @Override
		protected Integer doInBackground(String... params) {
			 

				HttpClient httpclient = new DefaultHttpClient();
				HttpGet httpget = new HttpGet("http://" + ConnectionManager.wsCinesUnidos + "/blackBerry.asmx/GetOrderCancel?orderId=" + orderId);
				
				 try {
					 httpclient.execute(httpget);
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 
				 return 0;
	     }	    

	     @Override
		protected void onPostExecute(Integer result) {
	     try {
	    	 pd.cancel();
	         pd = null;
	        } catch (Exception e) { }
	     }
	 }

}
