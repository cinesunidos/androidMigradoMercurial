package com.mobilemedianet.cinesunidosAndroid.managers;

public class ApplicationStatus {
	
	static boolean loaded = false;
	
	static public void setStatus(boolean a){
		loaded = a;
	}
	
	static public boolean getStatus(){
		return loaded;
	}

}
