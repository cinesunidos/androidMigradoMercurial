package com.mobilemedianet.cinesunidosAndroid.managers;

import android.content.Context;
import android.util.Log;

import com.mobilemedianet.cinesunidosAndroid.objects.Movie;
import com.mobilemedianet.cinesunidosAndroid.objects.PeliculaCine;
import com.mobilemedianet.cinesunidosAndroid.objects.Theaters;
import com.mobilemedianet.cinesunidosAndroid.parsers.MovieDetailsParserSax;
import com.mobilemedianet.cinesunidosAndroid.parsers.MovieParserJson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import static org.acra.ACRA.log;

public class MovieManager {

	private String TAG = this.getClass().getSimpleName();

	private static MovieManager instance = null;
	private List<Movie> []pelis;
	private List<Movie> []pelisEstrenos;
	private List<Movie> []listSelected;
	private boolean isLoaded = false;
	private String indexSelected = "1";
	private int rank = -1;	
	public boolean needToRefresh = false;
	private String showTimeId = null;
	
	private int listSelector = 0;	
	public static int listMovies = 0;
	public static int listPremiere = 1;
	
	private String premierDate = "";
	
	private Context context;
	
	public MovieManager(){
		pelis = new List[7];
		pelisEstrenos = new List[7];
		
		for(int i = 0; i < 7; i++){
			pelis[i] = null;
			pelisEstrenos[i] = null;
		}
		
	}

	public List<Movie>[] getPelis() {
		return pelis;
	}

	public void setPelis(List<Movie>[] pelis) {
		this.pelis = pelis;
	}

	public boolean refresh(){
		
		clean();
		if(loadMovies(0) == false)
			return false;
		
		if(SalasManager.getInstance().refresh() == false)
			return false;
		
				
		return true;
		
	}
	
	public void setShowTimeId(String id){
		showTimeId = id;
	}
	
	public String getShowTimeId(){
		return showTimeId;
	}
	
	public void resetRankingSearch(){
		rank = -1;		
	}
	
	public Movie getNextMovieRank(int dia){
		
		Iterator peliterator = getIterator(dia);
		
		if(peliterator == null){
			return null;
		}
		
		
		Movie dumpeli = null;	
		Movie selected = null;
		
		int localrank = -1;


		if(rank == -1) {
			rank = listSelected[0].size();
		}
			while(peliterator.hasNext()){
				dumpeli = (Movie) peliterator.next();
				if(dumpeli.getRanking() == rank-1 ){
					rank = dumpeli.getRanking();
					selected = dumpeli;
					return selected;
				}

			}
		return null;

//		if(rank == -1)
//		{
//			rank = listSelected[0].size();
//			while(peliterator.hasNext()){
//				dumpeli = (Movie) peliterator.next();
//				if(dumpeli.getRanking() == rank-1 ){
//					rank = dumpeli.getRanking();
//					selected = dumpeli;
//				}
//
//			}
//
//			return selected;
//		}else{
//			while(peliterator.hasNext()){
//				dumpeli = (Movie) peliterator.next();
//
//				if(dumpeli.getRanking() > localrank && dumpeli.getRanking() < rank){
//					localrank = dumpeli.getRanking();
//					selected = dumpeli;
//				}
//
//			}
//			rank = localrank;
//			return selected;
//		}

	}
	
	public void setPremierDate(String date){
		
		String []tmp = date.split("T")[0].split("-");
		
		premierDate = tmp[1] + "/" + tmp[2] + "/" + tmp[0];
		
	}
	
	public void setListSelector(int a){
		
		listSelector = a;
		
		if(listSelector == listMovies)
			listSelected = pelis;
		else
			listSelected = pelisEstrenos;	
		
	}
	
	public void clean(){
		
		isLoaded = false;
		
		for(int i = 0; i < 7; i++){
			pelis[i] = null;
			pelisEstrenos [i] = null;
		}
	}
	
	public void setContext(Context a){
		context = a;
	}
	
	public Context getContext(){
		return context;
	}
	
	public static MovieManager getInstance(){
		if(instance == null)
			instance = new MovieManager();
		return instance;
	}
	
	public boolean isLoaded(){
		return this.isLoaded;
	}
	
	public void setIndexSelection(String i){
		indexSelected = i;
	}
	
	public String getIndexSelection(){
		return indexSelected;
	}
	
	public boolean isSelectedMovieLoaded(int index){
		
		if(listSelected[index] == null)
			return false;
		
		return true;
		
	}
	
	public Movie getSelectedMovie(int index){
	
		if(listSelected[index] == null){
			if(loadMovies(index) == false)
				return null;
		}
		
		try{
			
			Movie dumy = getMovieById(indexSelected, index); //pelis[index].get(indexSelected);
			return dumy;			
		}catch(IndexOutOfBoundsException  e){
			return null;
		}
	}
	
	public boolean loadMovies(int index)
	{
		//Cambio a Json
		HttpHandler sh = new HttpHandler();

		ArrayList<String> cines = PrefManager.getInstance().getCinemaArray();

		//String url = "http://cu-dev-wcf-services.cloudapp.net/Premier.svc/";
		// Making a request to url and getting response

		JSONObject payload = new JSONObject();
		try
		{

			if (index == 0)
			{
				payload.put("day","");
				payload.put("month","");
				payload.put("cines",new JSONArray(cines));
			}
			else
			{
				payload.put("day",getDayByIndex(index));
				payload.put("month",getMonthByIndex(index));
				payload.put("cines",new JSONArray(cines));
			}

		}
		catch (JSONException e)
		{
			log.e(TAG, e.getMessage());
		}



		String jsonStr = sh.makeServiceCall(ConnectionManager.CU_WS_NEW + ConnectionManager.PELICULAS_WS, payload);

		Log.e(TAG, "Response from url: " + jsonStr);

		if (jsonStr != null) {

			MovieParserJson parserjson = new MovieParserJson();
			listSelected[index]  = parserjson.parse(jsonStr);

			if(listSelected[index]  != null && listSelected[index].size() > 0)
			{
				isLoaded = true;
				//return true;
			}
		}
		if (isLoaded == true)
		{
			return true;
		}
		else
		{
			return false;
		}


//
//
//		if(listSelector == listMovies){
//	        mpar = new MovieParserSax("http://" + ConnectionManager.wsCinesUnidos + "/Blackberry.asmx/GetCinemaMoviesDay?cines="
//	                + cines + "&date=" + getDatebyIndex(index), index);
//		}else if(listSelector == listPremiere){
//	        mpar = new MovieParserSax("http://" + ConnectionManager.wsCinesUnidos + "/Blackberry.asmx/GetCinemaMoviesDay?cines="
//	                + cines + "&date=" + getDatebyIndex(index), -1);
//		}
//			//mpar = new MovieDetailsParserSax("http://" + ConnectionManager.wsCinesUnidos + "/Blackberry.asmx/GetMovieExt?id=" + dum, "");
//
//
//        listSelected[index] = mpar.parse(index);
//        if(listSelected[index] != null){
//        	isLoaded = true;
//        	return true;
//        }
//        return false;
        	
	}
	
	public boolean loadMoviesDetail(String id){
		MovieDetailsParserSax mpar = null;
		//String dum = String.format("%07d", id);
		Movie a = getMovieById(id, 0);
		a.setDetailsLoaded(true);
		if(a == null)
			return false;
		
//		if(listSelector == listMovies)
//			mpar = new MovieDetailsParserSax("http://" + ConnectionManager.wsCinesUnidos + "/Blackberry.asmx/GetMovieExt?id=" + dum, "movie_" + dum + ".xml");
//		else if(listSelector == listPremiere)
//			mpar = new MovieDetailsParserSax("http://" + ConnectionManager.wsCinesUnidos + "/Blackberry.asmx/GetMovieExt?id=" + dum, "");
//
//		if(mpar == null)
//			return false;
//
//		if(mpar.parse(a)){
//			a.setDetailsLoaded(true);
//		}else{
//			return false;
//		}
		return true;
		
		
		
	}
	
	public String getDatebyIndex(int index){
		String date = null;		
		Calendar calendar = Calendar.getInstance();
		int day = 0;
		int month = 0;
		int year = 0;
		String []tmp;
		
		if(listSelector == listMovies){			
			calendar.add(Calendar.DAY_OF_MONTH, index);
			day = calendar.get(Calendar.DAY_OF_MONTH);
			month = calendar.get(Calendar.MONTH) + 1;
			year = calendar.get(Calendar.YEAR);			
			date = String.valueOf(month) + "/" + String.valueOf(day) + "/" + String.valueOf(year);
		}else if(listSelector == listPremiere){
			
			if(index == 0)
				return premierDate;
			
			tmp = premierDate.split("/");		
			
			date = tmp[0] + "/" + String.valueOf(Integer.parseInt(tmp[1]) + index) + "/" + tmp[2];
		}				
				
		return date;
		
	}


	public String getDayByIndex(int index)
	{
		String date = null;
		Calendar calendar = Calendar.getInstance();
		int day = 0;
		int month = 0;
		int year = 0;
		String []tmp;

		if(listSelector == listMovies)
		{
			calendar.add(Calendar.DAY_OF_MONTH, index);
			day = calendar.get(Calendar.DAY_OF_MONTH);
			month = calendar.get(Calendar.MONTH) + 1;
			year = calendar.get(Calendar.YEAR);
			date = String.valueOf(month) + "/" + String.valueOf(day) + "/" + String.valueOf(year);


			date = String.valueOf(day);
		}
		else if(listSelector == listPremiere)
		{

			if(index == 0)
				return premierDate;

			tmp = premierDate.split("/");

			date = tmp[0] + "/" + String.valueOf(Integer.parseInt(tmp[1]) + index) + "/" + tmp[2];

			date = String.valueOf(Integer.parseInt(tmp[1]) + index);
		}

		return date;

	}


	public String getMonthByIndex(int index)
	{
		String date = null;
		Calendar calendar = Calendar.getInstance();
		int day = 0;
		int month = 0;
		int year = 0;
		String []tmp;

		if(listSelector == listMovies)
		{
			calendar.add(Calendar.DAY_OF_MONTH, index);
			day = calendar.get(Calendar.DAY_OF_MONTH);
			month = calendar.get(Calendar.MONTH) + 1;
			year = calendar.get(Calendar.YEAR);
			date = String.valueOf(month) + "/" + String.valueOf(day) + "/" + String.valueOf(year);


			date = String.valueOf(month);
		}
		else if(listSelector == listPremiere)
		{

			if(index == 0)
				return premierDate;

			tmp = premierDate.split("/");

			date = tmp[0] + "/" + String.valueOf(Integer.parseInt(tmp[1]) + index) + "/" + tmp[2];

			date = tmp[0];
		}

		return date;

	}
	
	
	public Iterator<Movie> getIterator(int index){
		
		if(listSelected == null)
			return null;
		
		if(listSelected[index] == null){
			if(loadMovies(index) == false)
				return null;
		}
		
		return listSelected[index].iterator();
	}
	
	public Movie getMovie(int index, int dia){
		return listSelected[dia].listIterator(index).next();
	}
	
	public Movie getMovieById(String id, int index){
		
//		if(id == 0){
//			resetRankingSearch();
//			return getNextMovieRank();
//		}


		if(listSelected[index] == null)
			return null;

		Iterator peliterator = listSelected[index].iterator();

		if (index == 0 && listSelected[0].size() == 0)
		{
			for(int i = 1; i < listSelected.length; i++ )
			{
				if (listSelected[i].size() > 0)
				{
					peliterator = listSelected[i].iterator();
					break;
				}

			}
		}


		Movie dumpeli = null;	
		
		while(peliterator.hasNext()){
			dumpeli = (Movie) peliterator.next();
			if(dumpeli.getId().compareTo(id) == 0)
				return dumpeli;
		}
		
		return null;
		
		
	}
	
	public ArrayList<PeliculaCine> getMoviesByCinema(int id, int day){
		
		Movie peli = null;
		Theaters sala;
		ArrayList<PeliculaCine> cine = new ArrayList<PeliculaCine>();
		
		Iterator<Theaters> iteraSalas = null;
		
		PeliculaCine entry = null;
		
		resetRankingSearch();
		
		while((peli = getNextMovieRank(day)) != null){
			
			
			if(peli.getSalas() != null){
				
				iteraSalas = peli.getSalas().iterator();
				
				while(iteraSalas.hasNext()){
					
					sala = iteraSalas.next();
					
					if(sala.getCineId() == id){
						
						entry = new PeliculaCine(id, peli.getId());
						
						entry.addSala(sala);
						
						cine.add(entry);
						
					}
					
				}
			
			}
			
			
		}	
		
		return cine;	
		
	}

}
