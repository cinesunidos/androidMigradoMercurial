package com.mobilemedianet.cinesunidosAndroid.managers;

import com.mobilemedianet.cinesunidosAndroid.objects.CineInfo;

import java.util.ArrayList;
import java.util.Iterator;

public class CinesManager {


	private String[] ciudades = {"Barquisimeto",	"Caracas", "Guatire", "Maracaibo", "Maracay", "Margarita",
			"Maturín", "Puerto La Cruz","Puerto Ordaz", "San Cristóbal", "Valencia"};



	private static CinesManager instance = null;
	
	private ArrayList<CineInfo> cines = null;
	private ArrayList<CineInfo> cines_caracas = null;
	private ArrayList<CineInfo> cines_valencia = null;
	private ArrayList<CineInfo> cines_maracay = null;
	private ArrayList<CineInfo> cines_maracaibo = null;
	private ArrayList<CineInfo> cines_barquisimeto = null;
	private ArrayList<CineInfo> cines_guatire = null;
	private ArrayList<CineInfo> cines_maturin = null;
	private ArrayList<CineInfo> cines_porlamar = null;
	private ArrayList<CineInfo> cines_ptolacruz = null;
	private ArrayList<CineInfo> cines_ptoordaz = null;
	private ArrayList<CineInfo> cines_sancristobal = null;





	public static CinesManager getInstance(){
		
		if(instance == null)
			instance = new CinesManager();
		
		return instance;			
	}
	
	public CinesManager(){
		cines = new ArrayList<CineInfo>();
		cines_caracas =  new ArrayList<CineInfo>();
		cines_valencia =  new ArrayList<CineInfo>();
		cines_maracay =  new ArrayList<CineInfo>();
		cines_maracaibo =  new ArrayList<CineInfo>();
		cines_barquisimeto =  new ArrayList<CineInfo>();
		cines_guatire =  new ArrayList<CineInfo>();
		cines_maturin =  new ArrayList<CineInfo>();
		cines_porlamar =  new ArrayList<CineInfo>();
		cines_ptolacruz =  new ArrayList<CineInfo>();
		cines_ptoordaz =  new ArrayList<CineInfo>();
		cines_sancristobal =  new ArrayList<CineInfo>();
	}
	
	public void addCine(CineInfo c){
		cines.add(c);

		if (c.getCity().equals("Caracas")) {
			cines_caracas.add(c);
		}else if (c.getCity().equals("Valencia")) {
			cines_valencia.add(c);
		}else if (c.getCity().equals("Maracay")) {
			cines_maracay.add(c);
		}else if (c.getCity().equals("Maracaibo")) {
			cines_maracaibo.add(c);
		}else if (c.getCity().equals("Puerto La Cruz")) {
			cines_ptolacruz.add(c);
		}else if (c.getCity().equals("Margarita")) {
			cines_porlamar.add(c);
		}else if (c.getCity().equals("Puerto Ordaz")) {
			cines_ptoordaz.add(c);
		}else if (c.getCity().equals("San Cristobal")) {
			cines_sancristobal.add(c);
		}else if (c.getCity().equals("Barquisimeto")) {
			cines_barquisimeto.add(c);
		}else if (c.getCity().equals("Maturin")) {
			cines_maturin.add(c);
		}else if (c.getCity().equals("Guatire")) {
			cines_guatire.add(c);
		}
	}
	
	public CineInfo getCineInfo(int id){
		
		Iterator<CineInfo> dum = cines.iterator();
		CineInfo cine;
		
		while(dum.hasNext()){
			cine = dum.next();
			if(cine.getCineId() == id)
				return cine;
		}
		
		return null;

	}


	public String getCine(int posCiudad, int posCine)
	{
		String ciudad = ciudades[posCiudad];

		if (ciudad.equals("Caracas")) {
			return ((CineInfo)cines_caracas.toArray()[posCine]).getName();
		}else if (ciudad.equals("Valencia")) {
			return ((CineInfo)cines_valencia.toArray()[posCine]).getName();
		}else if (ciudad.equals("Maracay")) {
			return ((CineInfo)cines_maracay.toArray()[posCine]).getName();
		}else if (ciudad.equals("Maracaibo")) {
			return ((CineInfo)cines_maracaibo.toArray()[posCine]).getName();
		}else if (ciudad.equals("Puerto La Cruz")) {
			return ((CineInfo)cines_ptolacruz.toArray()[posCine]).getName();
		}else if (ciudad.equals("Margarita")) {
			return ((CineInfo)cines_porlamar.toArray()[posCine]).getName();
		}else if (ciudad.equals("Puerto Ordaz")) {
			return ((CineInfo)cines_ptoordaz.toArray()[posCine]).getName();
		}else if (ciudad.equals("San Cristóbal")) {
			return ((CineInfo)cines_sancristobal.toArray()[posCine]).getName();
		}else if (ciudad.equals("Barquisimeto")) {
			return ((CineInfo)cines_barquisimeto.toArray()[posCine]).getName();
		}else if (ciudad.equals("Maturín")) {
			return ((CineInfo)cines_maturin.toArray()[posCine]).getName();
		}else if (ciudad.equals("Guatire")) {
			return ((CineInfo)cines_guatire.toArray()[posCine]).getName();
		}
		return "";
	}


	public String getCineId(int posCiudad, int posCine)
	{
		String ciudad = ciudades[posCiudad];
		int idCine = 0;

		if (ciudad.equals("Caracas")) {
			idCine = ((CineInfo)cines_caracas.toArray()[posCine]).getCineId();
		}else if (ciudad.equals("Valencia")) {
			idCine = ((CineInfo)cines_valencia.toArray()[posCine]).getCineId();
		}else if (ciudad.equals("Maracay")) {
			idCine = ((CineInfo)cines_maracay.toArray()[posCine]).getCineId();
		}else if (ciudad.equals("Maracaibo")) {
			idCine = ((CineInfo)cines_maracaibo.toArray()[posCine]).getCineId();
		}else if (ciudad.equals("Puerto La Cruz")) {
			idCine = ((CineInfo)cines_ptolacruz.toArray()[posCine]).getCineId();
		}else if (ciudad.equals("Margarita")) {
			idCine = ((CineInfo)cines_porlamar.toArray()[posCine]).getCineId();
		}else if (ciudad.equals("Puerto Ordaz")) {
			idCine = ((CineInfo)cines_ptoordaz.toArray()[posCine]).getCineId();
		}else if (ciudad.equals("San Cristóbal")) {
			idCine = ((CineInfo)cines_sancristobal.toArray()[posCine]).getCineId();
		}else if (ciudad.equals("Barquisimeto")) {
			idCine = ((CineInfo)cines_barquisimeto.toArray()[posCine]).getCineId();
		}else if (ciudad.equals("Maturín")) {
			idCine = ((CineInfo)cines_maturin.toArray()[posCine]).getCineId();
		}else if (ciudad.equals("Guatire")) {
			idCine = ((CineInfo)cines_guatire.toArray()[posCine]).getCineId();
		}
		return String.valueOf(idCine);
	}


	public int getCantidadCinesCiudad(int posCiudad)
	{
		String ciudad = ciudades[posCiudad];

		if (ciudad.equals("Caracas")) {
			return cines_caracas.size();
		}else if (ciudad.equals("Valencia")) {
			return cines_valencia.size();
		}else if (ciudad.equals("Maracay")) {
			return cines_maracay.size();
		}else if (ciudad.equals("Maracaibo")) {
			return cines_maracaibo.size();
		}else if (ciudad.equals("Puerto La Cruz")) {
			return cines_ptolacruz.size();
		}else if (ciudad.equals("Margarita")) {
			return cines_porlamar.size();
		}else if (ciudad.equals("Puerto Ordaz")) {
			return cines_ptoordaz.size();
		}else if (ciudad.equals("San Cristóbal")) {
			return cines_sancristobal.size();
		}else if (ciudad.equals("Barquisimeto")) {
			return cines_barquisimeto.size();
		}else if (ciudad.equals("Maturín")) {
			return cines_maturin.size();
		}else if (ciudad.equals("Guatire")) {
			return cines_guatire.size();
		}

		return 0;
	}


	public String[] getCiudades() {
		return ciudades;
	}
}
