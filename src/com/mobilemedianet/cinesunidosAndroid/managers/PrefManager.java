package com.mobilemedianet.cinesunidosAndroid.managers;

import android.content.Context;
import android.content.SharedPreferences;

import com.mobilemedianet.cinesunidosAndroid.objects.Cines;
import com.mobilemedianet.cinesunidosAndroid.objects.InformacionUsuario;
import com.mobilemedianet.cinesunidosAndroid.utilities.Fechas;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PrefManager {
	
	public static final String preferencesFileCines = "cines";
	public static final String preferencesFileZona = "zonas";
	public static final String preferencesFileOpciones = "opciones";
	
	
	static private PrefManager instance = null;
	SharedPreferences prefsCines; 
	SharedPreferences.Editor editorCines;
	SharedPreferences prefsZonas; 
	SharedPreferences.Editor editorZonas;
	SharedPreferences prefsOpciones; 
	SharedPreferences.Editor editorOpciones;
	Context aplicationContext;
	
	
	static public PrefManager getInstance(){
		if(instance == null)
			instance = new PrefManager();
		
		return instance;
	}
	
//	public PreferenceManager(){
//		prefs = getSharedPreferences(preferencesFile, android.content.Context.MODE_PRIVATE);
//		
//	}
	
	public void setAplicationContext(Context cont){
		aplicationContext = cont;
		
		prefsCines = aplicationContext.getSharedPreferences(preferencesFileCines, android.content.Context.MODE_PRIVATE);
		editorCines = prefsCines.edit();
		
		prefsZonas = aplicationContext.getSharedPreferences(preferencesFileZona, android.content.Context.MODE_PRIVATE);
		editorZonas = prefsZonas.edit();
		
		prefsOpciones = aplicationContext.getSharedPreferences(preferencesFileOpciones, android.content.Context.MODE_PRIVATE);
		editorOpciones = prefsOpciones.edit();
	}
	
	public boolean isAnyCinemaChecked(){
		
		Map<String,?> prefsMap = prefsCines.getAll();

        for(Map.Entry<String,?> entry : prefsMap.entrySet())
        {      
 
        	if(entry.getValue().equals(true)){
        		return true;
        	}
        }
        
		return false;
		
	}
	
	public int getCinemaChecked(){
		
		Map<String,?> prefsMap = prefsCines.getAll();
		
		int count = 0;

        for(Map.Entry<String,?> entry : prefsMap.entrySet())
        {      
 
        	if(entry.getValue().equals(true))
        		count++;
        	
        }
        
		return count;
		
	}
	
	public String getCinemaString(){
		
		String cines = "";
		
		Map<String,?> prefsMap = prefsCines.getAll();

		
		
        for(Map.Entry<String,?> entry : prefsMap.entrySet())
        {       
        	if(entry.getValue().equals(true)){
        		cines += entry.getKey() + ",";
        	}
        }
        
        if(cines.length() != 0)
        	cines = cines.substring(0, cines.length()-1);
        		
		return cines;
		
	}

	public ArrayList<String> getCinemaArray(){



		Map<String,?> prefsMap = prefsCines.getAll();

		ArrayList<String> cines = new ArrayList<String>();


		for(Map.Entry<String,?> entry : prefsMap.entrySet())
		{
			if(entry.getValue().equals(true))
			{
				cines.add(entry.getKey());
			}
		}



		return cines;

	}

	
	public void setCinemaStatus(String cineid, boolean i){
		String zonaid = Cines.getZonByCineId(cineid);
		editorCines.putBoolean(cineid, i);
		editorCines.commit();
		
		if(i == true){		
		
			if(!checkZoneStatus(zonaid))
				setZonaStatus(zonaid, true);
		
		}else{
			
			if(checkZoneStatus(zonaid))
				setZonaStatus(zonaid, false);
			
		}
		
		
	}
	
	public void setZonaStatus(String zonaid, boolean i){
		
		editorZonas.putBoolean(zonaid, i);
		editorZonas.commit();
		
	}
	
	
	
public String getZonaString(){
		
		String zonas = "";
		
		Map<String,?> prefsMap = prefsZonas.getAll();

        for(Map.Entry<String,?> entry : prefsMap.entrySet())
        {       
        	if (entry.getValue() != null){
	        	if(entry.getValue().equals(true)){
	        		zonas += entry.getKey() + ",";
	        	}
        	}
        }

		if(zonas.length() > 0)
        zonas = zonas.substring(0, zonas.length()-1);
        		
		return zonas;
		
	}	
	
	
	
	private boolean checkZoneStatus(String zona){
		
		Map<String,?> prefsMap = prefsCines.getAll();

		
        for(Map.Entry<String,?> entry : prefsMap.entrySet())
        {       
        	if(entry.getValue().equals(true)){
        		if(entry.getKey().equals(Cines.getZonByCineId(entry.getKey()))){
        			return true;
        		}
        	}
        }
		
		return false;
		
	}
	
	public void saveUser(String user){
		
		editorOpciones.putString("user", user);		
		editorOpciones.commit();
		
	}
	
	public String getUser(){
				
		return prefsOpciones.getString("user", "");
		
	}
	
	public String getCityWithMostSelections(){
		
		Map<String, Integer> ciudades = new HashMap<String, Integer>();
		
		ciudades.put("CCSE,CCSO", 0);
		ciudades.put("VAL", 0);
		ciudades.put("MCA", 0);
		ciudades.put("MBO", 0);
		ciudades.put("BQM", 0);
		ciudades.put("GUA", 0);
		ciudades.put("COR", 0);
		ciudades.put("MAT", 0);
		ciudades.put("POR", 0);
		ciudades.put("PLC", 0);
		ciudades.put("PDZ", 0);
		ciudades.put("SCR", 0);
		
		
		Map<String,?> prefsMap = prefsCines.getAll();
	
				
        for(Map.Entry<String,?> entry : prefsMap.entrySet())
        {       
        	if(entry.getValue().equals(true)){
        		
        		String nombre = Cines.getZonByCineId(entry.getKey());
        		
        		for(Map.Entry<String,?> ciudad : ciudades.entrySet()){
        			
        			if(ciudad.getKey().equals(nombre)){
        				int valor = (Integer) ciudad.getValue();
        				valor++;
        				ciudades.put(nombre, valor);        				
        			}
        			
        		}
        			        		
        	}
        }
        
        String keywinner = null;
        int count = 0;
        
        for(Map.Entry<String,?> ciudad : ciudades.entrySet()){
        	
        	if(((Integer)ciudad.getValue()) >= count){
        		count = (Integer) ciudad.getValue();
        		keywinner = ciudad.getKey();
        	}
			
        				
		}
		
		return keywinner;
		
	}
	
	public void saveUserInfo(){
		
		InformacionUsuario userInfo = InformacionUsuario.getInstance();
		
			
		editorOpciones.putString("birthdate", userInfo.getBirthdate());
		editorOpciones.putString("sex", userInfo.getSex());
		editorOpciones.putString("login", userInfo.getLogin());
		editorOpciones.putString("name", userInfo.getName());
		editorOpciones.putString("phone", userInfo.getPhone());
		editorOpciones.putString("secureId", userInfo.getSecureId());
		editorOpciones.putString("externalId", userInfo.getExternalId());
		editorOpciones.putString("token", userInfo.getToken());
		editorOpciones.putString("loged", "true");
		editorOpciones.commit();		
	}
	
	public void getUserInfo(){
		
		InformacionUsuario userInfo = InformacionUsuario.getInstance();
		
		userInfo.setBirthdate(prefsOpciones.getString("birthdate", ""));
		userInfo.setSex(prefsOpciones.getString("sex", ""));
		userInfo.setLogin(prefsOpciones.getString("login", ""));
		userInfo.setName(prefsOpciones.getString("name", ""));
		userInfo.setPhone(prefsOpciones.getString("phone", ""));
		userInfo.setSecureId(prefsOpciones.getString("secureId", ""));
		userInfo.setExternalId(prefsOpciones.getString("externalId", ""));
		userInfo.setToken(prefsOpciones.getString("token", ""));
		
	}
	
	public void putUserPaymentName(String name){
		editorOpciones.putString("paymentName", name);
		editorOpciones.commit();
	}
	
	public void putUserPaymentCI(String ci){
		editorOpciones.putString("paymentCI", ci);
		editorOpciones.commit();
		
	}
	
	public void putUserPaymentCIType(String tipo){
		editorOpciones.putString("paymentCIType", tipo);
		editorOpciones.commit();
		
	}
	
	public void putUserPaymentTel(String tel){
		editorOpciones.putString("paymentTel", tel);
		editorOpciones.commit();
		
	}
	
	public void putUserPaymentTelCode(String cod){
		editorOpciones.putString("paymentTelCod", cod);
		editorOpciones.commit();
		
	}
	
	public void putUserCheckBoxStatus(boolean a){
		editorOpciones.putBoolean("checkBoxStatus", a);
		editorOpciones.commit();
	}
		
	public void cleanUserPaymentInfo(){
		editorOpciones.putString("paymentName", "");
		editorOpciones.putString("paymentCI", "");
		editorOpciones.putString("paymentCIType", "");
		editorOpciones.putString("paymentTel", "");
		editorOpciones.putString("paymentTelCod", "");
		editorOpciones.putBoolean("checkBoxStatus", false);
		editorOpciones.commit();
	}
	
	
	public String getUserPaymentName(){
		return prefsOpciones.getString("paymentName", "");
	}
	
	public String getUserPaymentCI(){
		return prefsOpciones.getString("paymentCI", "");
	}
	
	public String getUserPaymentCIType(){
		return prefsOpciones.getString("paymentCIType", "");
	}
	
	public String getUserPaymentTel(){
		return prefsOpciones.getString("paymentTel", "");
	}
	
	public String getUserPaymentTelCode(){
		return prefsOpciones.getString("paymentTelCod", "");
	}
	
	public boolean getUserCheckBoxStatus(){
		return prefsOpciones.getBoolean("checkBoxStatus", false);
		
	}
	
	
	
	
	
	
	public boolean checkLoginStatus(){
		String status = prefsOpciones.getString("loged", "false");
		
		if(status.equals("false"))
			return false;
		else 
			return true;
		
		
	}
	
	public void cleanUserInfo(){
				
		editorOpciones.putString("birthdate", "");
		editorOpciones.putString("sex", "");
		editorOpciones.putString("login", "");
		editorOpciones.putString("name", "");
		editorOpciones.putString("phone", "");
		editorOpciones.putString("secureId", "");
		editorOpciones.putString("externalId", "");
		editorOpciones.putString("token", "");
		editorOpciones.putString("loged", "false");
		editorOpciones.commit();		
	}

	public void setCacheTime(String tag){
		String time = Fechas.getFecha();
	
		editorOpciones.putString(tag, time);
		editorOpciones.commit();
		
	}
	
	

	public String getCacheTime(String tag){	 
	
		return prefsOpciones.getString(tag, null);
		
	}	
	

}
