package com.mobilemedianet.cinesunidosAndroid.managers;

import android.util.Log;

import com.mobilemedianet.cinesunidosAndroid.objects.Hall;
import com.mobilemedianet.cinesunidosAndroid.parsers.SalasParserJson;

import java.util.Iterator;
import java.util.List;

public class SalasManager {

	private static SalasManager instance = null;
	
	private List<Hall> salas;
	private String TAG = this.getClass().getSimpleName();
	
	public static SalasManager getInstance(){
		
		if(instance == null)			
			instance = new SalasManager();
		
		
		return instance;
	}
	
	public boolean refresh(){
		salas = null;
		if(cargarSalas() == false)
			return false;
		
		return true;
			
		
	}
	
	public boolean cargarSalas()
	{
		//Cambio a Json
		HttpHandler sh = new HttpHandler();

		// Making a request to url and getting response
		String jsonStr = sh.makeServiceCall(ConnectionManager.CU_WS_NEW + ConnectionManager.CINES_WS);
		Log.e(TAG, "Url: " + ConnectionManager.CU_WS_NEW + ConnectionManager.CINES_WS);
		Log.e(TAG, "Response from url: " + jsonStr);


		if (jsonStr != null) {

			SalasParserJson parserjson = new SalasParserJson();
			salas = parserjson.parse(jsonStr);

			if(salas != null)
			{
				return true;
			}
		}
		return false;


//		 SalasParserSax mpar;
//
//	        mpar = new SalasParserSax("http://" + ConnectionManager.wsCinesUnidos + "/Blackberry.asmx/GetTheater?zone=" + zona);
//
//	        salas = mpar.parse();
//
//	        if(salas == null)
//	        	return false;
//
//	        return true;

		
	}
	
	public String getSalaNameById(int id){		
		if(getSalaById(id) != null)
			return getSalaById(id).getName();
		return null;
		
	}
	
	public Hall getSalaById(int id){
		
		if(salas == null)
			return null;
		
		Iterator<Hall> a = salas.iterator();
		Hall dum;
		
		while(a.hasNext()){
			dum = a.next();
			if(dum.getId() == id)
				return dum;
		}
		
		return null;
	}

}
