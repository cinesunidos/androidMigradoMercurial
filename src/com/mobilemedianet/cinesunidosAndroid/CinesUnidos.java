package com.mobilemedianet.cinesunidosAndroid;

import android.app.Application;

import org.acra.*;
import org.acra.annotation.*;
import org.acra.ReportField;

import com.github.anrwatchdog.ANRWatchDog;



@ReportsCrashes(
		mailTo="soporte_cu@mobmedianet.com",
		formKey = "", // will not be used
        customReportContent = { ReportField.APP_VERSION_CODE, ReportField.ANDROID_VERSION, ReportField.PHONE_MODEL, ReportField.STACK_TRACE, ReportField.LOGCAT, ReportField.EVENTSLOG},   
        mode = ReportingInteractionMode.TOAST,
        socketTimeout = 10000,
        resToastText = R.string.reporteCrash
)

public class CinesUnidos extends Application {
	@Override
	public void onCreate() {
		super.onCreate();
		
		ACRA.init(this);
		
		if(BuildConfig.DEBUG == false) {
			new ANRWatchDog().start();
		}
	}
}
