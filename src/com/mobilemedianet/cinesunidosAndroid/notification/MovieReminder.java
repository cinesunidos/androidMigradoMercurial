package com.mobilemedianet.cinesunidosAndroid.notification;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mobilemedianet.cinesunidosAndroid.R;
import com.mobilemedianet.cinesunidosAndroid.managers.ConnectionManager;
import com.mobilemedianet.cinesunidosAndroid.managers.LocalyticsManager;
import com.mobilemedianet.cinesunidosAndroid.managers.MovieManager;
import com.mobilemedianet.cinesunidosAndroid.managers.PrefManager;
import com.mobilemedianet.cinesunidosAndroid.utilities.Metrics;
import com.mobilemedianet.cinesunidosAndroid.utilities.Zonas;
import com.mobmedianet.adutil.AdView;

import java.io.InputStream;

public class MovieReminder extends Activity {
	
	private AdView ad;
	
	String localizador;
	String titulo;
	String lugar;
	String horario;
	String censura;
	String id;
	String entradas;
	String asientos;
	String sala;
	String valor;
	String cargos;
	String cargosWeb;
	String iva;
	
	
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
    	 super.onCreate(savedInstanceState);
         setContentView(R.layout.notificacion);
         ConnectionManager.setContext(this);
         PrefManager.getInstance().setAplicationContext(this);
         Metrics.setContext(this);
         
         
         if(LocalyticsManager.localyticsSession() == null)
        	 LocalyticsManager.newLocalytics(getApplicationContext());
         
 	    LocalyticsManager.localyticsSession().open();
 	    LocalyticsManager.localyticsSession().tagEvent("Notificación de Función");
 	                
         
         Bundle bundle = getIntent().getExtras();
         
         
         if(bundle.get("localizador") != null)
        	  localizador = bundle.get("localizador").toString();
         
         titulo = bundle.get("titulo").toString();
         lugar = bundle.get("lugar").toString();
         horario = bundle.get("horario").toString();
         censura = bundle.get("censura").toString();
         id = bundle.get("id").toString();
         entradas = bundle.get("entradas").toString();    
         valor = bundle.get("valor").toString();   
         cargos = bundle.get("cargos").toString();
		cargosWeb = bundle.get("cargosWeb").toString();
         iva = bundle.get("iva").toString();
         
         
         if(bundle.get("asientos") != null)
        	 asientos = bundle.get("asientos").toString();
         else
        	 asientos = null;
        	 
         sala = bundle.get("sala").toString();
             
         
      
        
        ((ImageView)findViewById(R.id.icon)).setImageDrawable(getPoster(id)); 	
    	
        ((TextView)findViewById(R.id.TituloPeliDetalles)).setText(Html.fromHtml("<big><b>" + titulo + "</b></big>" +  "<br />" + 
                  "<font color=#000000>" + censura + "<br />" + horario + "<br />" + lugar +  " - " + sala + "</font>"));
	    
        
        
        RelativeLayout informacion = (RelativeLayout) findViewById(R.id.informacion);
        informacion.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW, 
					       Uri.parse("http://pda.cinesunidos.com/infoticketcollection_bb.asp"));			
					startActivity(i);
				
			}
		});
        
        mostrarDetalles();         
        
//	    String detalles = "<b>" + lugar;	  
//	    detalles += " - " +  sala + "</b>";
//	    detalles += "<br/>" + horario;
//	    detalles += "<br/><b>Localizador: </b>" + localizador;
//	    if(asientos != null)
//	    	detalles += "<br/><b>Asientos: </b>" + asientos;
//	    detalles += "<br/><b>Entradas:</b><br/>" + entradas;
//	   
//	    ((TextView)findViewById(R.id.textView2)).setText(Html.fromHtml(detalles));
	    
	    PrefManager pref = PrefManager.getInstance();
        pref.setAplicationContext(this);
        String aux = pref.getCityWithMostSelections();
        Log.d("CIUDAD", "" + aux);
        
        String key = "";
        if (aux.equals("CCSE,CCSO")) {
        	key = "POST_CCS";
        } else {
        	key = "POST_" + aux;
        }
        Zonas zona = new Zonas();
        zona.createZones();
        
    	ad = (AdView) findViewById(R.id.adViewPago);
    	ad.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, Metrics.dpToPixel(50)));
    	ad.setZoneId(zona.getZona(key));
         
        
	 }	
	
	private void mostrarDetalles(){
		TextView localizadortv = (TextView) findViewById(R.id.textView4);
		TextView entradastv = (TextView) findViewById(R.id.textView5);
		
		
//		TextView total = (TextView)findViewById(R.id.total);		
		
		localizadortv.setText(Html.fromHtml("Localizador: " + "<font color=#c72a10>" + localizador + "</font>"));
		
		if(asientos != null)
			entradastv.setText(Html.fromHtml("Butacas: " + "<font color=#c72a10>" + asientos +  "</font>"));
		else
			entradastv.setVisibility(View.GONE);
					
		((TextView)findViewById(R.id.totalEntradas_text)).setText(entradas);
	    ((TextView)findViewById(R.id.valorentradas_text)).setText(this.valor);
	    ((TextView)findViewById(R.id.cargosServicios_text)).setText(this.cargos);
	    ((TextView)findViewById(R.id.iva_text)).setText(iva);
	    
	    Float total = Float.parseFloat(this.valor.replace(" Bs", "").replace(",", ".")) + Float.parseFloat(this.cargos.replace(" Bs", "").replace(",", ".")) + Float.parseFloat(this.iva.replace(" Bs", "").replace(",", "."));
	    
	    ((TextView)findViewById(R.id.total)).setText(String.format("Total: Bs. %.2f", total));
		
	    
		
		
		
	}
	
	private Drawable getPoster(String id){
		
		Drawable poster;
		
		InputStream file;
		
		String posterUrl = "http://" + ConnectionManager.wsCinesUnidos + "/AndroidAfiches/afiches_155x171/afiche_";		
		
		file = ConnectionManager.getInstance().getInputStreamFrom(posterUrl + id + ".jpg", "afiche_" + id + ".jpg");
		
		if(file == null)
			return null;
		
		poster = Drawable.createFromStream(file, "src");		
					
		if(poster == null)
			poster = MovieManager.getInstance().getContext().getResources().getDrawable(R.drawable.image_na);
		
				
		return poster;
	}
	
	public void onPause()
	{
	    
	    LocalyticsManager.localyticsSession().upload();
	    super.onPause();
	}
}
