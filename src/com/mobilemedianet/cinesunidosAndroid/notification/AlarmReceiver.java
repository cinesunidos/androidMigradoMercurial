package com.mobilemedianet.cinesunidosAndroid.notification;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.mobilemedianet.cinesunidosAndroid.R;


public class AlarmReceiver extends BroadcastReceiver {
	private NotificationManager notificationManager;
    private Notification notification;
    private final int STATUS_BAR_NOTIFICATION = 1;
    
	 @Override
	 public void onReceive(Context context, Intent intent) {
		 
		 notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
         
         int icon = R.drawable.icono;
         CharSequence tickerText = "La película está por comenzar";
         long when = System.currentTimeMillis();
         
         notification = new Notification(icon, tickerText, when);
         notification.flags |= Notification.FLAG_AUTO_CANCEL;
         CharSequence contentTitle = "Cines Unidos";
         CharSequence contentText = "La película está por comenzar";
         
         String localizador = null;
         
       
         
         String titulo = intent.getExtras().get("titulo").toString();
         String lugar = intent.getExtras().get("lugar").toString();
         String horario = intent.getExtras().get("horario").toString();
         String sala = intent.getExtras().get("sala").toString();
         String censura = intent.getExtras().get("censura").toString();
         String id = intent.getExtras().get("id").toString();
         String entradas = intent.getExtras().get("entradas").toString();
         String valor = intent.getExtras().get("valor").toString();
         String cargos = intent.getExtras().get("cargos").toString();
          String cargosWeb = intent.getExtras().get("cargosWeb").toString();
         String iva = intent.getExtras().get("iva").toString();
         
         String asientos = null;
          
         
         
                  
         Intent notificationIntent = new Intent(context, MovieReminder.class);
         
         
         if(intent.getExtras().get("localizador") != null){
        	 localizador = intent.getExtras().get("localizador").toString();
        	 notificationIntent.putExtra("localizador", localizador);
         }         
         
         notificationIntent.putExtra("titulo", titulo);
         notificationIntent.putExtra("lugar", lugar);
         notificationIntent.putExtra("horario", horario);
         notificationIntent.putExtra("sala", sala);
         notificationIntent.putExtra("censura", censura);
         notificationIntent.putExtra("id", id);
         notificationIntent.putExtra("entradas", entradas);
         notificationIntent.putExtra("valor", valor);
         notificationIntent.putExtra("cargos", cargos);
          notificationIntent.putExtra("cargosWeb", cargosWeb);
         notificationIntent.putExtra("iva", iva);
         
         if(intent.getExtras().get("asientos") != null){
        	 asientos = intent.getExtras().get("asientos").toString();
        	 notificationIntent.putExtra("asientos", asientos);
         }
         
         
         
         
         notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_SINGLE_TOP);
         
         PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
         
         notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);
         
         notificationManager.notify(STATUS_BAR_NOTIFICATION, notification);
	 }	 	
	
}
