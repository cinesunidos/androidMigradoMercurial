package com.mobilemedianet.cinesunidosAndroid;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobilemedianet.cinesunidosAndroid.managers.ConnectionManager;
import com.mobilemedianet.cinesunidosAndroid.managers.MovieManager;
import com.mobilemedianet.cinesunidosAndroid.objects.Movie;

@SuppressWarnings("deprecation")
public class CarruselActivity extends Activity {
	
	private ArrayList<Movie> pelis;
	Gallery myGallery;
	
	int posterHeight;
	int posterWidth;
	
	
	@Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);  
        
        ConnectionManager.setContext(this); 
        
        
       
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
        	Intent myIntent = new Intent(this, ListaActivity.class);
        	myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        	startActivity(myIntent);    
        	this.finish();
    	}  
        
        if(MovieManager.getInstance().isLoaded() == false){
       	 AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("CinesUnidos");
            builder.setMessage("No tenemos información para mostrar, intente refrescar.");
            builder.setPositiveButton("OK",null);
            builder.create();
            builder.setIcon(R.drawable.icono);
            builder.show();   
       	 return;
        }
        
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        posterHeight = (int) (metrics.heightPixels * 0.6);
        posterWidth =  (int) (posterHeight * 0.8);
        
        
        
        
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
                   
        setContentView(R.layout.carrusel);
        
        MovieManager.getInstance().resetRankingSearch();
        
        Movie pelicula;
        pelis = new ArrayList<Movie>();
        while((pelicula = MovieManager.getInstance().getNextMovieRank(0)) != null){        	
        	pelis.add(pelicula);
        }
        
        myGallery = (Gallery) findViewById(R.id.gallery1);
    	myGallery.setAdapter(new ImageAdapter(this));
    	
    	
    	
    	myGallery.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View v,
					int position, long id) {
//				i.seLayoutParams(posterWidth, posterHeight);
//				v.setLayoutParams(new Gallery.LayoutParams(posterWidth*(1 + 1/3), posterHeight*(1 + 1/3)));
				((TextView)findViewById(R.id.textView1)).setText(pelis.get(position).getSpanishTitle());
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
								
			}
		});
    	
    	myGallery.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position,
					long id) {
				Log.i("Clicked", String.valueOf(position));
				
				MovieManager.getInstance().setIndexSelection(pelis.get(position).getId());
				Intent myIntent = new Intent(CarruselActivity.this, MainActivity.class);
		    	myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		        startActivityForResult(myIntent, 0);   
		        
		        								
			}
    		
		});
        
	}
	
	
	
	public class ImageAdapter extends BaseAdapter {
        int mGalleryItemBackground;
        private Context mContext;
        
        public ImageAdapter(Context c) {
            mContext = c;
            TypedArray a = obtainStyledAttributes(R.styleable.HelloGallery);
//            TypedArray a = c.obtainStyledAttributes(R.styleable.Gallery1);
            mGalleryItemBackground = a.getResourceId(
                    R.styleable.HelloGallery_android_galleryItemBackground, 0);
            a.recycle();
        }
        @Override
		public int getCount() {
            return pelis.size();
        }
        @Override
		public Object getItem(int position) {        	
            return position;
        }
        @Override
		public long getItemId(int position) {
            return position;
        }
        @Override
		public View getView(int position, View convertView, ViewGroup parent) {
            ImageView i = new ImageView(mContext);
            if(pelis.get(position).posterlsLoaded() == true){
            	i.setImageDrawable( pelis.get(position).getPoster());            	
            }else{
            	i.setImageResource(R.drawable.image_na);
            	new DownloadImageTask().execute(String.valueOf(position));
            }
                       
            i.setLayoutParams(new Gallery.LayoutParams(posterWidth, posterHeight));
            i.setScaleType(ImageView.ScaleType.FIT_XY);
            i.setBackgroundResource(mGalleryItemBackground);
//            i.setTag(position);
//            i.setBackgroundColor(0x0000);
            return i;
        }
    }
	
	private class DownloadImageTask extends AsyncTask<String, Void, Drawable> {
    	int index;
    	
    	
    	@Override
		protected Drawable doInBackground(String... position) {
        	index = Integer.parseInt(position[0]);
        	        	
        	pelis.get(index).setPoster();
        	        	            	
        	return pelis.get(index).getPoster();
        	
        }

        @Override
		protected void onPostExecute(Drawable poster) {  
        	ImageView imagen;
        	
        	
        	        	
        	imagen = (ImageView) myGallery.getChildAt(index - myGallery.getFirstVisiblePosition());        	
//        	imagen = (ImageView) myGallery.getItemAtPosition(index);        	
        	if(imagen!= null){
        		imagen.setImageDrawable(poster);
        		imagen.invalidate();
        	}
        	        	

        }
    }
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();		
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
	    //write your logic here	   
	    super.onConfigurationChanged(newConfig);
	    Log.i("cambio", "cambio");
	}
	
	

}
