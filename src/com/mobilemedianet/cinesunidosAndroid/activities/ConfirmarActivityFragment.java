package com.mobilemedianet.cinesunidosAndroid.activities;



import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.mobilemedianet.cinesunidosAndroid.R;
import com.mobilemedianet.cinesunidosAndroid.managers.ButacasManager;
import com.mobilemedianet.cinesunidosAndroid.managers.DetallesCompra;
import com.mobilemedianet.cinesunidosAndroid.managers.MovieManager;
import com.mobilemedianet.cinesunidosAndroid.managers.PrefManager;
import com.mobilemedianet.cinesunidosAndroid.notification.AlarmReceiver;
import com.mobilemedianet.cinesunidosAndroid.objects.Butaca;
import com.mobilemedianet.cinesunidosAndroid.objects.Cines;
import com.mobilemedianet.cinesunidosAndroid.objects.Movie;
import com.mobilemedianet.cinesunidosAndroid.utilities.Metrics;
import com.mobilemedianet.cinesunidosAndroid.utilities.Zonas;
import com.mobmedianet.adutil.AdView;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static com.mobilemedianet.cinesunidosAndroid.R.id.iva_text;
import static com.mobilemedianet.cinesunidosAndroid.R.id.total;

//import com.mobilemedianet.cinesunidosAndroid.managers.LocalyticsManager;


public class ConfirmarActivityFragment extends Fragment{

	
static private ConfirmarActivityFragment instance;
	private static final String Flurry_API_Key = "GSW94MXD24S7D546NST6";
	
	private AdView ad;
	

	static public ConfirmarActivityFragment getInstance(){
		
		if(instance == null)
			instance = new ConfirmarActivityFragment();
		return instance;
	}
	
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
         View viewer = inflater.inflate(R.layout.pago_recibido, container, false);
         
        return viewer;
    }   
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
	    super.onCreate(savedInstanceState);
		//Flurry Start Session
		FlurryAgent.init(getActivity(), Flurry_API_Key);
		FlurryAgent.onStartSession(getActivity());
	    
	}
	
	@Override
	public void onStart(){
		super.onStart();
		
		ViewTreeObserver vto = getView().getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
            	if(getView() == null)
            		return;            	
            	mostrar();
                ViewTreeObserver obs = getView().getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);

            }
        });
        
        
        
	}
	
	public void onResume(){
		super.onResume();	
		
	}
	
	public void mostrar(){
		
		if(getView() == null)
    		return;
		
		String booking = DetallesCompra.getInstance().getBookingNumber();
//	    String detalles;
		
		
	    
	    Movie peli = MovieManager.getInstance().getSelectedMovie(0);
	    
		String nombreCine = Cines.getCineById(DetallesCompra.getInstance().getCineId())[1];
    	//String salaNombre = SalasManager.getInstance().getSalaNameById(DetallesCompra.getInstance().getSalaId());
		String salaNombre = DetallesCompra.getInstance().getSalaName();
		String censura = DetallesCompra.getInstance().getCensura();
    	String hora = DetallesCompra.getInstance().getFecha();
    	
//    	((ImageView)getView().findViewById(R.id.ImagenPeli)).setImageDrawable(peli.getPoster());    	
//    	DetallesCompra.getInstance().setPremium(SalasManager.getInstance().getSalaById(DetallesCompra.getInstance().getSalaId()).isPremium());
//        ((TextView)getView().findViewById(R.id.TituloPeliDetalles)).setText(Html.fromHtml("<big><b>" + peli.getSpanishTitle() + "</b></big>" +  "<br />" + 
//                  "<font color=#000000>" + censura + "<br />" + hora + "<br />" + nombreCine + " - " + salaNombre + "</font>"));
        
    	
    	 ((TextView)getView().findViewById(R.id.textViewPeliculaNombre)).setText(peli.getSpanishTitle());
	     ((TextView)getView().findViewById(R.id.textViewCineNombre)).setText(nombreCine + " - " + salaNombre);
	     ((TextView)getView().findViewById(R.id.textViewFecha)).setText(hora);
	     
       
        
        
        String localizador = "Localizador: " + "<font color=#c72a10>" + booking + "</font>";
        ((TextView)getView().findViewById(R.id.textView4)).setText(Html.fromHtml(localizador));
        
        if(DetallesCompra.getInstance().getSeleccionAsientos()){
        	String butacas = "Butacas: " + "<font color=#c72a10>" + butacas() +  "</font>";
        	((TextView)getView().findViewById(R.id.textView5)).setText(Html.fromHtml(butacas));        	
        }else{
        	((TextView)getView().findViewById(R.id.textView5)).setVisibility(View.GONE);
        }
        
        mostrarEntradas();
        
        Button boton = (Button) getView().findViewById(R.id.button1);
        
        boton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				getActivity().finish();
				
			}
		});
        
        RelativeLayout informacion = (RelativeLayout) getView().findViewById(R.id.informacion);
        informacion.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW, 
					       Uri.parse("http://pda.cinesunidos.com/infoticketcollection_bb.asp"));			
					startActivity(i);
				
			}
		});
        
	  
//	    ((TextView)getView().findViewById(R.id.textView2)).setText(Html.fromHtml(detalles)); 	    
	    
        PrefManager pref = PrefManager.getInstance();
        pref.setAplicationContext(getActivity());
        String aux = pref.getCityWithMostSelections();
        Log.d("CIUDAD", "" + aux);
        
        String key = "";
        if (aux.equals("CCSE,CCSO")) {
        	key = "POST_CCS";
        } else {
        	key = "POST_" + aux;
        }
        Zonas zona = new Zonas();
        zona.createZones();
        
    	ad = (AdView) getView().findViewById(R.id.adViewPago);
    	ad.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, Metrics.dpToPixel(50)));
    	ad.setZoneId(zona.getZona(key));
//    	ad.request();
    	setAlarm();
    	
		/*if(LocalyticsManager.localyticsSession() == null){
    		LocalyticsManager.newLocalytics(getActivity().getApplicationContext());
        	LocalyticsManager.localyticsSession().open();
    	}*/
     		
    	Map <String,String> values = new HashMap<String,String> ();    	
    
    	values.put("Pelicula", peli.getSpanishTitle());
    	values.put("Cine", nombreCine);
    	values.put("Cambio Butacas", String.valueOf(DetallesCompra.getInstance().getCambioButacas()));
    	values.put("Dias Antes", "" + DetallesCompra.getInstance().getDia());
    	values.put("Horas Antes", DetallesCompra.getInstance().getHorasAntes());
    	values.put("Dia Semana", DetallesCompra.getInstance().getDiaSemana());
    	values.put("Hora de la Pelicula", DetallesCompra.getInstance().getTime());
    	values.put("Origen", DetallesCompra.getInstance().getOringen());
    	values.put("Tiempo Restante", DetallesCompra.getInstance().getTiempoRestante());
    	values.put("Tipo de Boleto", DetallesCompra.getInstance().nombreBoletos()); 
    	values.put("Numero de Entradas", String.valueOf(DetallesCompra.getInstance().getNumeroTotalEntradas()));  
            	
    	//LocalyticsManager.localyticsSession().tagEvent("Compra Realizada", values);
		FlurryAgent.logEvent("Compra Realizada");
    	
    	
	    
	}
	
	private void mostrarEntradas(){
				
		((TextView)getView().findViewById(R.id.totalEntradas_text)).setText(String.valueOf(DetallesCompra.getInstance().getNumeroTotalEntradas()));
	    ((TextView)getView().findViewById(R.id.valorentradas_text)).setText(String.format("Bs. %.2f", DetallesCompra.getInstance().getTotalEntradasValor()));
	    ((TextView)getView().findViewById(R.id.cargosServicios_text)).setText(String.format("Bs. %.2f", DetallesCompra.getInstance().getBookingFeeInCents()));
		((TextView)getView().findViewById(R.id.cargosServiciosWeb_text)).setText(String.format("Bs. %.2f", DetallesCompra.getInstance().getCargosWeb()));

		((TextView)getView().findViewById(iva_text)).setText(String.format("Bs. %.2f", DetallesCompra.getInstance().getTaxInCents()));
	    
		float totalPago = DetallesCompra.getInstance().getTotalEntradasValor() + DetallesCompra.getInstance().getBookingFeeInCents() + DetallesCompra.getInstance().getCargosWeb() +  DetallesCompra.getInstance().getTaxInCents();

	    ((TextView)getView().findViewById(total)).setText(String.format("Total: Bs. %.2f", totalPago));
		
		
		  
		
			 
	 }
	
	private String butacas(){
		
		String seleccion = "";
		
		Iterator<Butaca> butacas = ButacasManager.getInstance().getNewVectorSelectionIterator();
		
		
		if(butacas == null)
			return null;
		
		Butaca dum;
		
		while(butacas.hasNext()){
			dum = butacas.next();
			seleccion += " " + dum.getNombreButaca();
			if(butacas.hasNext())
				seleccion += ",";
		}
		
		return seleccion;
	}
	
	public void setAlarm(){
		String hora[] = DetallesCompra.getInstance().getTime().split(":");
		String fecha[] = DetallesCompra.getInstance().getDate().split("-");
		
		int hour = Integer.parseInt(hora[0]);
		int minutos = Integer.parseInt(hora[1]);
		int postm;
		if(hora[2].equals("AM"))
			postm = Calendar.AM;
		else
			postm = Calendar.PM;
		
		int day = Integer.parseInt(fecha[0]);
		int month = Integer.parseInt(fecha[1]) - 1;
		int year = Integer.parseInt(fecha[2]);
		
	    
	    Calendar cal = Calendar.getInstance();
	    
        cal.clear();
	    	    
	    cal.set(Calendar.YEAR, year);
	    cal.set(Calendar.MONTH, month);
	    cal.set(Calendar.DAY_OF_MONTH, day);
	    cal.set(Calendar.HOUR, hour);
	    cal.set(Calendar.MINUTE, minutos);	
	    cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
	    cal.set(Calendar.AM_PM, postm); 
	        	    
	    Movie peli = MovieManager.getInstance().getSelectedMovie(0);
	    
	    // add -15 minutes to the calendar object
	    cal.add(Calendar.MINUTE, -15);	    
	    	    
	    AlarmManager am = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
	    
	    String localizador = DetallesCompra.getInstance().getBookingNumber();
	    String titulo = DetallesCompra.getInstance().getTittle();
	    String lugar = Cines.getCineById(DetallesCompra.getInstance().getCineId())[1];
	    String horario = DetallesCompra.getInstance().getFecha();//hora[0] + ":" + hora[1] + " " + hora[2];
	    //String sala = SalasManager.getInstance().getSalaNameById(DetallesCompra.getInstance().getSalaId());
		String sala = DetallesCompra.getInstance().getSalaName();
	    String censura = DetallesCompra.getInstance().getCensura();
	    //String id = String.format("%07d", peli.getId());
		String id = peli.getId();
	    String entradas = "" + DetallesCompra.getInstance().getNumeroTotalEntradas();
	    String valor = String.format("Bs. %.2f", DetallesCompra.getInstance().getTotalEntradasValor());
	    String cargos = String.format("Bs. %.2f", DetallesCompra.getInstance().getBookingFeeInCents());
		String cargosWeb = String.format("Bs. %.2f", DetallesCompra.getInstance().getCargosWeb());
		String cargos_sin_iva = String.format("Bs. %.2f", DetallesCompra.getInstance().getBookingFeeWithTaxInCents());
	    String iva = String.format("Bs. %.2f", DetallesCompra.getInstance().getTaxInCents());






		Intent intent = new Intent(getActivity(), AlarmReceiver.class);
	    
	    
	    intent.putExtra("localizador", localizador);
	    intent.putExtra("titulo", titulo);
	    intent.putExtra("lugar", lugar);
	    intent.putExtra("horario", horario);
	    intent.putExtra("sala", sala);
	    intent.putExtra("censura", censura);
	    intent.putExtra("id", id);
	    intent.putExtra("entradas", entradas);
	    intent.putExtra("valor", valor);
	    intent.putExtra("cargos", cargos);
		intent.putExtra("cargosWeb", cargosWeb);
	    intent.putExtra("iva", iva);
	    
	    
	    
	    
	    if(DetallesCompra.getInstance().getSeleccionAsientos()){
	    	intent.putExtra("asientos", butacas());	  
	    }
	  
	    
	 
	    // In reality, you would want to have a static variable for the request code instead of 192837
	    PendingIntent sender = PendingIntent.getBroadcast(getActivity() , (int) System.currentTimeMillis() , intent, PendingIntent.FLAG_UPDATE_CURRENT);
	    
	    // Get the AlarmManager service
	    
	    am.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), sender);  
	}
	
	/*public void onPause()
	{
		
	    LocalyticsManager.localyticsSession().upload();
	    super.onPause();
	}*/

	@Override
	public void onPause() {
		super.onPause();

		FlurryAgent.onEndSession(getActivity());
	}

	@Override
	public void onStop() {
		super.onStop();

		FlurryAgent.onEndSession(getActivity());
	}
	
	
	
}
