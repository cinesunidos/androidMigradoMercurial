package com.mobilemedianet.cinesunidosAndroid.activities;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.mobilemedianet.cinesunidosAndroid.R;
import com.mobilemedianet.cinesunidosAndroid.managers.MovieManager;
import com.mobilemedianet.cinesunidosAndroid.managers.PrefManager;
import com.mobilemedianet.cinesunidosAndroid.objects.Movie;
import com.mobilemedianet.cinesunidosAndroid.utilities.Zonas;
import com.mobmedianet.adutil.AdView;

import java.util.ArrayList;

public class ListaActivityFragment extends Fragment {
	
	private ArrayList<MovieEntry> pelis = null;
	private IconListViewAdapter m_adapter;
	private Zonas zona;	
	private ListView lv = null;
	
	View selected = null;
	int colortmp;
	
	OnListaSelectedListener onListaSelected;
	
	static ListaActivityFragment instance;
	
	
	static public ListaActivityFragment getInstance(){
		
		if(instance == null)
			instance = new ListaActivityFragment();
		return instance;
	}
	
	static public void delInstance(){
		instance = null;
	}
	
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);   
        
//        setRetainInstance(true);        
//        MainActivity.isListaVisible = true;
             
        
        
    }
	
	 @Override
	    public void onStart (){
	    	super.onStart();    
	    	
	    	
	    	ViewTreeObserver vto = getView().getViewTreeObserver();
	        vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
	            @Override
	            public void onGlobalLayout() {
	            	if(getView() == null)
	            		return;	    
	            	
	            	inicializarLista();
	                ViewTreeObserver obs = getView().getViewTreeObserver();
	                obs.removeGlobalOnLayoutListener(this);

	            }
	        });
	    	
	  }
	
		
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	
  	 View viewer = inflater.inflate(R.layout.listview, container, false); 
  	   
  	 
  	 
  	 return viewer;
    }	

	
	public static class MovieEntry {
        public MovieEntry(Movie a) {
        	peli = a;
       
        		
        }
      
        public Movie getPeli() {
            return peli;
        }       
        private Movie peli;      
        
       
    }

	
	public void inicializarLista(){
		 MovieEntry entry;
         Movie pelicula;
         
                        
                  
         pelis = new ArrayList<MovieEntry>();
         this.m_adapter = new IconListViewAdapter(getActivity(), R.layout.list_item_icon_text, pelis);
        
         // Create corresponding array of entries and load their labels.                  
         
//         if(MovieManager.getInstance().isLoaded() == false && MovieManager.getInstance().needToRefresh == false){
//        	 AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//             builder.setTitle("CinesUnidos");
//             builder.setMessage("No tenemos información para mostrar, intente actualizar.");
//             builder.setPositiveButton("OK",null);
//             builder.create();
//             builder.setIcon(R.drawable.icono);
//             builder.show();   
//        	 return;
//         }
         
                
        MovieManager.getInstance().resetRankingSearch();
         
         
        while((pelicula = MovieManager.getInstance().getNextMovieRank(0)) != null){        	
      	      	            	
      	entry = new MovieEntry(pelicula);
      	            	            	
      	m_adapter.add(entry);      	
      	
      	
       }        
        
        
       FrameLayout fl = (FrameLayout) getView().findViewById(R.id.ListContainer);
   	   
       lv = new ListView(getActivity());
       fl.addView(lv);          
               
       lv.setAdapter(this.m_adapter);
       lv.setOnItemClickListener(new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
			// TODO Auto-generated method stub
			onListaSelected.onListaSelected((m_adapter.getItem(position)).getPeli().getId());
			
		}
    	   
	});
//       lv.onListItemClick((OnClickListener) onListaSelected);
       
       
	   	PrefManager pref = PrefManager.getInstance();
	    pref.setAplicationContext(getActivity());
	    String aux = pref.getCityWithMostSelections();
	    Log.d("CIUDAD - LAF", "" + aux);
	    
//	    String key = "";
	    
	    
//	    Zonas zona = new Zonas();
	    zona = new Zonas();
	    zona.createZones();
        
	}
	
	public void refreshList(){
		MovieEntry entry;
        Movie pelicula;
        
        if(m_adapter == null)
        	return;
        
		m_adapter.clear();		
		
		 
	     MovieManager.getInstance().resetRankingSearch();
	     
	     while((pelicula = MovieManager.getInstance().getNextMovieRank(0)) != null){
	    	 entry = new MovieEntry(pelicula);
	    	 m_adapter.add(entry); 
	      	
	       }   
	     
	     m_adapter.notifyDataSetChanged();
		
		
	}
	
	public class IconListViewAdapter extends ArrayAdapter<MovieEntry> {

        private ArrayList<MovieEntry> items;
        

        public IconListViewAdapter(Context context, int textViewResourceId, ArrayList<MovieEntry> items) {
                super(context, textViewResourceId, items);
                this.items = items;
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
                
        	View v = lv.getChildAt(position - lv.getFirstVisiblePosition());//convertView;
        	LinearLayout contenedor;    
                if (v == null) {
                	
                    LayoutInflater vi = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    v = vi.inflate(R.layout.list_item_icon_text, null);  
                    
                    }
                
                contenedor = (LinearLayout) v.findViewById(R.id.contenedor);
                
                if(position%2 == 0){                	
                	contenedor.setBackgroundResource(R.drawable.custom_button_dos);
                }
                else{
                	contenedor.setBackgroundResource(R.drawable.custom_button_tres);
                }
                   
                    
                    Movie pelicula = items.get(position).getPeli();
                    
                    if (pelicula != null) {
                    	
                    	//poblamos la lista de elementos
                    	
                    		ViewHolder holder = null;                    		
                    		TextView tt = null;
                            ImageView im = null;
                    		
                    		if(v.getTag() == null){
                    			holder = new ViewHolder();
                    			holder.icon = (ImageView) v.findViewById(R.id.icon);
                    			holder.text = (TextView) v.findViewById(R.id.text);
                    			tt = holder.text;
                    			im = holder.icon;
                    			v.setTag(holder);
                    		}else{
                    			holder = (ViewHolder) v.getTag();
                    			tt = holder.text;
                    			im = holder.icon;
                    		}
                    	
                            
                                                                           
                            
                            if (tt!= null) {
                            	tt.setText(Html.fromHtml("<b>" + pelicula.getSpanishTitle() + "</b>" + "<br/><br/>" + 
                                        "<font color=#707070>" + pelicula.getOriginalTitle() + "</font>" + "<br/>"));
                            }
                            
                            if (im!= null) {                        	
                            	if(pelicula.posterlsLoaded() == true){                        		
                            		im.setImageDrawable(pelicula.getPoster());
                            	}else{
                            		new DownloadImageTask().execute(String.valueOf(position));
                            		
                            	}                 	
                            	                     	
                            }        
                            
                            if(position == 2){
                            	PrefManager pref = new PrefManager();
                                pref.setAplicationContext(getActivity());
                                String aux = pref.getCityWithMostSelections();
                                Log.d("-CIUDAD-", "" + aux);
                                
                                String key = "";
                                if (aux.equals("CCSE,CCSO")) {
                                	key = "ALL_CCS";
                                } else {
                                	key = "ALL_" + aux;
                                }
                                Zonas zona = new Zonas();
                                zona.createZones();
                            	
                                AdView ad = (AdView) v.findViewById(R.id.adViewAll);
//                               ad.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, Metrics.dpToPixel(60)));
                                ad.setZoneId(zona.getZona(key));
//                                ad.request();
//                                ad.setVisibility(View.VISIBLE);

                                	
                               
                            	
//                            	((ImageView) v.findViewById(R.id.adds)).setImageResource(R.drawable.add360);//               	   	
//                            	((View) v.findViewById(R.id.separador)).setVisibility(v.VISIBLE);
                            }else{
                            	AdView ad = (AdView) v.findViewById(R.id.adViewAll);
                            	ad.setVisibility(View.INVISIBLE);
                            	
//                            	((ImageView) v.findViewById(R.id.adds)).setImageResource(0);                	
//                            	((View) v.findViewById(R.id.separador)).setVisibility(v.INVISIBLE);
                            }
                                                                
                    }
//                    v.setTag(String.valueOf(position));
                               
                                
                return v;
        }
        
        private class ViewHolder {
        	  TextView text;        	  
        	  ImageView icon;        	  
        	}
                 
        
	}
	
	private class DownloadImageTask extends AsyncTask<String, Void, Drawable> {
    	int index;
    	
    	
    	@Override
		protected Drawable doInBackground(String... position) {
        	index = Integer.parseInt(position[0]);        	
        	
        	(m_adapter.getItem(index)).getPeli().setPoster();
        	            	
        	return (m_adapter.getItem(index)).getPeli().getPoster();
        	
        }

        @Override
		protected void onPostExecute(Drawable poster) {  
        	View itemView;        		
        	
        	
        	if(lv != null)
        		itemView = lv.getChildAt(index - lv.getFirstVisiblePosition());
        	else
        		return;
            
        	if (itemView != null) {
                ImageView itemImageView = (ImageView) itemView.findViewById(R.id.icon);
                itemImageView.setImageDrawable(poster);
            }          	
        	

        }
    }
	
	
    
	
	public interface OnListaSelectedListener {
        public void onListaSelected(String movieId);
    }
	
	 @Override
	    public void onAttach(Activity activity) {
	        super.onAttach(activity);
	        try {
	        	onListaSelected = (OnListaSelectedListener) activity;
	        } catch (ClassCastException e) {
	            throw new ClassCastException(activity.toString()
	                    + " must implement a Listener");
	        }
	    }
	


}
