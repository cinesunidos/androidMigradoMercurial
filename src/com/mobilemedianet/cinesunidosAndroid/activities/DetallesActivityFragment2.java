/*
 * Copyright (C) 2011 Jake Wharton
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mobilemedianet.cinesunidosAndroid.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.TextUtils.TruncateAt;
import android.text.format.Time;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.mobilemedianet.cinesunidosAndroid.MainActivity;
import com.mobilemedianet.cinesunidosAndroid.R;
import com.mobilemedianet.cinesunidosAndroid.SplashActivity;
import com.mobilemedianet.cinesunidosAndroid.managers.ConnectionManager;
import com.mobilemedianet.cinesunidosAndroid.managers.DetallesCompra;
import com.mobilemedianet.cinesunidosAndroid.managers.EstrenosManager;
import com.mobilemedianet.cinesunidosAndroid.managers.MovieManager;
import com.mobilemedianet.cinesunidosAndroid.objects.Cines;
import com.mobilemedianet.cinesunidosAndroid.objects.Movie;
import com.mobilemedianet.cinesunidosAndroid.objects.ShowTime;
import com.mobilemedianet.cinesunidosAndroid.objects.Theaters;
import com.mobilemedianet.cinesunidosAndroid.utilities.Fechas;
import com.mobilemedianet.cinesunidosAndroid.utilities.Metrics;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

//import com.mobilemedianet.cinesunidosAndroid.managers.LocalyticsManager;


public class DetallesActivityFragment2 extends Fragment implements OnClickListener{
	
	static String [] semana = {"D", "L", "M", "M", "J", "V", "S"};
	
	static String []meses = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", 
		"Octubre", "Noviembre", "Diciembre"};

	private static final String Flurry_API_Key = "GSW94MXD24S7D546NST6";
		
	LinearLayout ly, ll;
	
	OnShowTimeSelectedListener showTimeSelected;
	
	private int botonesHorizontalesMax = 2;	
	private int fagmentWidth = 0;
	private int activetab = 0;
	
	Movie peli, pelideldia;  
	Handler uiHandler;
	private List <Theaters> salas;
	
	static private String selectedMovieId = "0";
	static private boolean premier = false;
	static private DetallesActivityFragment2 instance = null;
	
	private String []premierDate;
	 

	static public DetallesActivityFragment2 getInstance(String movId, boolean prem){
		selectedMovieId = movId;
		premier = prem;
		if(instance == null)
			instance = new DetallesActivityFragment2();
		return instance;
	}
	
	static public DetallesActivityFragment2 getInstance(){
		
		if(instance == null)
			instance = new DetallesActivityFragment2();
		return instance;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
	    super.onCreate(savedInstanceState);
		//Flurry Start Session
		FlurryAgent.init(getActivity(), Flurry_API_Key);
		FlurryAgent.onStartSession(getActivity());

//	    Log.i("cambiando", "cambiando orientacion");
	    
//	    setRetainInstance(true);
	}
	
    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	
  	    View viewer = inflater.inflate(R.layout.detalles2, container, false);    
  	    
  	   return viewer;
    }
    
    
  
    @Override
    public void onResume(){
    	super.onResume();
    	ConnectionManager.setContext(getActivity());
    	if(ConnectionManager.checkConn() == false){
    		
    		
    		AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
    		alertDialog.setTitle("CinesUnidos"); 
    		alertDialog.setMessage("No hay conexión a internet");
    		alertDialog.setIcon(R.drawable.icono); 
    		alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
    			
    			@Override
    			public void onClick(DialogInterface dialog,int which) {
    				dialog.cancel();
    				((MainActivity) getActivity()).goHome();
    				return;
    			}
  	      }); 
  	      
  	    
  	      alertDialog.show(); 
  		
  	      return;
    	}
    	

    	ViewTreeObserver vto = getView().getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
            	if(getView() == null)
            		return;
            	
            	Log.i("Selected", String.valueOf(selectedMovieId));
            	mostrarDetalles(selectedMovieId);
                ViewTreeObserver obs = getView().getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);

            }
        });
    	
  }
    
    
    public void mostrarDetalles(String id){
    	
    String textinfo = "";
    	
	/*if(LocalyticsManager.localyticsSession() == null){
    		LocalyticsManager.newLocalytics(getActivity().getApplicationContext());
        	LocalyticsManager.localyticsSession().open();
    }*/
		
	Map <String,String> values = new HashMap<String,String> ();
		
   if(premier == false){
    	
    	MovieManager.getInstance().setListSelector(MovieManager.listMovies);
    	
    	peli = MovieManager.getInstance().getMovieById(id, 0);
    	
    	if(peli == null){
    		Log.i("mostrarDetalles", "pelicula es null");
    		return;
    	}
    	
    	values.put("Título Película", peli.getSpanishTitle());
    	
    	//LocalyticsManager.localyticsSession().tagEvent("Detalles de Película");
	    FlurryAgent.logEvent("Detalles de Película");
    	
        if(peli == null){	
        	Intent reLaunchMain=new Intent(getActivity(), SplashActivity.class);
     		reLaunchMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
     		startActivity(reLaunchMain);
         	getActivity().finish();
         }
    	
        salas = peli.getSalas();
        
        if(peli.detailsLoaded() == false){
       	 new GetDetailsTask().execute();
        
        }else{
       	    	 
       	 ImageView post = (ImageView) getView().findViewById(R.id.icon);
       	 TextView text = (TextView) getView().findViewById(R.id.TituloPeliDetalles);
       	 
       	 if(post == null || text == null)
       		 return;
       	 
       	 post.setImageDrawable(peli.getPoster());
       	 
       	 if(peli.getDirector() != "")
       		 textinfo += "<b>Director: </b><font color=#707070>" + peli.getDirector() + "</font>";
       	 
       	 text.setText(Html.fromHtml("<big><b>" + peli.getSpanishTitle() + "</b></big>" +  "<br />" + 
                     "<font color=#707070>" + peli.getOriginalTitle() + "</font><br />" + "<b>Género: </b><font color=#707070>"
                     + peli.getGenere() + "</font><br />" + textinfo));
            
        }
    	MovieManager.getInstance().loadMoviesDetail(peli.getId());
        
        inflarFunciones();
        
        getView().findViewById(R.id.frameLayout1).setOnClickListener(this);
   	 	getView().findViewById(R.id.frameLayout2).setOnClickListener(this);
   	   	         
   	 	setActiveTab(0);
   	 	
    }else{
    	
    	MovieManager.getInstance().setListSelector(MovieManager.listPremiere);
    	    	   	
    	peli = EstrenosManager.getInstance().getMovieById(id);    
    	
    	values.put("Título Película", peli.getSpanishTitle());
    	
    	//LocalyticsManager.localyticsSession().tagEvent("Detalle Estreno");
	    FlurryAgent.logEvent("Detalle Estreno");
    	
    	premierDate = peli.getFirstExhibit().split("T")[0].split("-");
    	
        if(peli == null){	
        	Intent reLaunchMain=new Intent(getActivity(), SplashActivity.class);
     		reLaunchMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
     		startActivity(reLaunchMain);
         	getActivity().finish();
         }      

        
        if(peli.detailsLoaded() == false){
        	
        		new GetDetailsTask().execute();
           
        }else{
          	    	 
          	 ImageView post = (ImageView) getView().findViewById(R.id.icon);
          	 TextView text = (TextView) getView().findViewById(R.id.TituloPeliDetalles);
          	 
          	 if(post == null || text == null)
          		 return;
          	 
           	 if(peli.getDirector() != "")
           		 textinfo += "<b>Director: </b><font color=#707070>" + peli.getDirector() + "</font>";
          	 
          	 post.setImageDrawable(peli.getPoster());
          	 
           	 if(peli.getDirector() != "")
           		 textinfo += "<b>Director: </b><font color=#707070>" + peli.getDirector() + "</font>";
           	 
           	 text.setText(Html.fromHtml("<big><b>" + peli.getSpanishTitle() + "</b></big>" +  "<br />" + 
                         "<font color=#707070>" + peli.getOriginalTitle() + "</font><br />" + "<b>Género: </b><font color=#707070>"
                         + peli.getGenere() + "</font><br />" + textinfo));
          	 

               
           }
        
     	 if(peli.getPreSale().equals("P")){       
     		 
     		
            
            getView().findViewById(R.id.frameLayout1).setOnClickListener(this);
       	 	getView().findViewById(R.id.frameLayout2).setOnClickListener(this);
       	 	
  		 	MovieManager.getInstance().setPremierDate(peli.getFirstExhibit());
  		 	setActiveTab(0);
  		 	inflarFunciones(); 

     	 }else{
     		setActiveTab(1);     		
     		inflarSinopsis();
     		
     		FrameLayout frame1 = (FrameLayout) getView().findViewById(R.id.frameLayout1);
     		FrameLayout frame2 = (FrameLayout) getView().findViewById(R.id.frameLayout2);
     		
     		TextView textfecha = (TextView) getView().findViewById(R.id.textView2);
     		
     		textfecha.setText(premierDate[2] + " de " + meses[Integer.parseInt(premierDate[1]) - 1] + " del " + premierDate[0]);

     		frame1.setVisibility(FrameLayout.INVISIBLE);   	
     		frame2.setBackgroundResource(R.drawable.cinta);	

     	 }
     	 

    }
    
}
    
        
    private void setActiveTab(int i){
    
    	switch(i){
    	
    case 0 :
    	
		getView().findViewById(R.id.frameLayout1).setBackgroundResource(R.drawable.tabrojocentro);   	
		getView().findViewById(R.id.frameLayout2).setBackgroundResource(R.drawable.tabcentral);		
		((TextView)getView().findViewById(R.id.textView1)).setTextColor(Color.WHITE);
		((TextView)getView().findViewById(R.id.textView2)).setTextColor(Color.BLACK);		
		break;
		
	case 1 :
		getView().findViewById(R.id.frameLayout2).setBackgroundResource(R.drawable.tabrojocentro);
		getView().findViewById(R.id.frameLayout1).setBackgroundResource(R.drawable.tabcentral);		
		((TextView)getView().findViewById(R.id.textView2)).setTextColor(Color.WHITE);
		((TextView)getView().findViewById(R.id.textView1)).setTextColor(Color.BLACK);		
		
		break;	
	
    	}
    }
    
    private void inflarFunciones(){
    	FrameLayout ll = (FrameLayout) getView().findViewById(R.id.contenedor);
    	
    	ll.removeAllViewsInLayout();		 	

    	View vv = View.inflate(getActivity(), R.layout.funciones_contenedor, null);

    	ll.addView(vv, new FrameLayout.LayoutParams( ll.getLayoutParams().width,
    			ll.getLayoutParams().height));
    	
    	crearBarraFechas(); 
    	
    	dia(0);
    	
    }
    
    private void inflarSinopsis(){
    	FrameLayout ll = (FrameLayout) getView().findViewById(R.id.contenedor);
    	
    	String infotext = "";
    	
    	ll.removeAllViewsInLayout();		 	

    	View vv = View.inflate(getActivity(), R.layout.sinopsis_contenedor, null);

    	ll.addView(vv, new FrameLayout.LayoutParams( ll.getLayoutParams().width,
    			ll.getLayoutParams().height));
    	
    	    	
    	ll.invalidate();
    	
    	    	
    	((TextView)getView().findViewById(R.id.sinopsis)).setText("  " + peli.getSinopsis());
    	
    	if(peli.getStarring() != "")
    		infotext += "<b>Actores: </b>" + peli.getStarring() + "<br>";
    	
    	if(peli.getRuntime() != "")
    		infotext += "<b>Duración: </b>" + peli.getRuntime() + " minutos.<br>";
    	
    	if(peli.getWriter() != "")
    		infotext += "<b>Escritor: </b>" + peli.getWriter() + "<br>";
    		
    	if(peli.getProducer() != "")
    		infotext += "<b>Productor: </b>" + peli.getProducer() + "<br>";
    	
    	if(peli.getCompany() != "")
    		infotext += "<b>Compañia: </b>" + peli.getCompany() + "<br>";
    	
    	((TextView)getView().findViewById(R.id.Actores)).setText(Html.fromHtml(infotext));
    
    	
    }
    
//    private void inflarTrailer(){
//    	FrameLayout ll = (FrameLayout) getView().findViewById(R.id.contenedor);
//    	
//    	ll.removeAllViewsInLayout();		 	
//
//    	View vv = View.inflate(getActivity(), R.layout.trailer_contenedor, null);
//
//    	ll.addView(vv, new FrameLayout.LayoutParams( ll.getLayoutParams().width,
//    			ll.getLayoutParams().height));
//    	
//    	    	
//    	ll.invalidate();
//    	
//    }
    
 
    private void agregarShowTimeText(String cine, String sala){
    	
    	LinearLayout ly;
    	TextView tv = new TextView(getActivity());
    	
    	ly = (LinearLayout) getView().findViewById(R.id.showTimesLayout);
    	ly.addView(tv);
        	  	   
//    	ViewGroup.MarginLayoutParams mlp1 = (ViewGroup.MarginLayoutParams) getView().findViewById(R.id.FechaFunciones).getLayoutParams();
//    	ViewGroup.MarginLayoutParams mlp2 = (ViewGroup.MarginLayoutParams) tv.getLayoutParams();
    	
//    	mlp2.setMargins(0, 15, 0, 5);    	
    	tv.setText("   " + cine + " - " + sala); 
    	tv.setGravity(Gravity.CENTER_VERTICAL);
    	tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
    	tv.setMinimumHeight(Metrics.dpToPixel(30));
    	tv.setTypeface(null, Typeface.BOLD);
    	tv.setEllipsize(TruncateAt.MARQUEE);
    	tv.setSingleLine();
    	tv.setSelected(true);
    	
//    	mlp2.height = 50;
    	
    	tv.setBackgroundResource(R.drawable.main_header_selector);
    	
    }
    
    private int agregarShowTimeButton(String horario, LinearLayout ly, String tag, boolean unico, int index){
    	
    	int weigth, width;
    	
    	
    	Button b = new Button(getActivity());
    	
    	b.setText(Html.fromHtml("<big>" + horario + "</big>"));
    	
    	
    	b.setTag(tag);
    	
    	b.setBackgroundResource(R.drawable.custom_button_dos);
//    	b.setTextColor(R.color.button_text_color);  //Esto no sirve
    	
    	
    	
    	//TODO aqui    	
    	if(unico){
    		width = getView().getWidth()/botonesHorizontalesMax;
    		weigth = 0;   		
    	}
    	else{
    		width = getView().getWidth()/botonesHorizontalesMax;
//    		width = 0;
//    		weigth = 1;
    		weigth = 0;
    	}
    	
    	LayoutParams lp = new LinearLayout.LayoutParams(width, LayoutParams.WRAP_CONTENT, weigth); 
    	
    	b.setLayoutParams(lp);
    	
    	
    	ly.addView(b);
    	
    	int a = b.getWidth();
    	
    	if(index == 0 && CompareTimes(horario) == false && !(peli.getPreSale().equals("P"))){
    		b.setTextColor(Color.parseColor("#cccccc"));    	
    		b.setEnabled(false);
    	}else
    		b.setOnClickListener(OnClickDoSomething(b));
    	
    	return b.getMeasuredHeight();
    	
    	
    	
    }
    
    private boolean CompareTimes(String a){
    	
    	boolean pmLocal, pmCine;
    	int horaCine, horaLocal, minutosCine;
    	String []parse;
    	
    	Time today = new Time(Time.getCurrentTimezone());		
		today.setToNow();
		
		horaLocal = today.hour;		
		
		parse = a.split(":");
		
		horaCine = Integer.parseInt(parse[0]);
		minutosCine = Integer.parseInt(parse[1].substring(0, 2));
		if(parse[1].substring(2, 4).equals("PM"))
			pmCine = true;
		else 
			pmCine = false;
		
		if(horaCine == 12 && pmCine == false)	
			horaCine = 0;
		else if(horaCine < 12 && pmCine == true)
			horaCine += 12;
		
		if(horaLocal <= horaCine){
			
			if(horaLocal == horaCine)
				if(today.minute + 15 < minutosCine)
					return true;
				else
					return false;
			
			return true;
		}		
		
		return false;
    	
    	
    }
    
    View.OnClickListener OnClickDoSomething(final Button button)  {
        return new View.OnClickListener() {
            @Override
			public void onClick(View v) {  
               MovieManager.getInstance().setShowTimeId(v.getTag().toString());
               
               
               showTimeSelected.showTimeSelected();
                 
            }
        };
    }
	
    /*
    private void irAComprar(short id){
    	Bundle bundle = new Bundle();
    	bundle.putShort("ShowTimeId", id);    	
    	Intent intent = new Intent(getActivity(), ComprarActivityFragment.class);
    	intent.putExtras(bundle);
        startActivity(intent);     	
    }*/
   
    
    
   public interface OnShowTimeSelectedListener{
       public void showTimeSelected();
   } 
    
  
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
        	showTimeSelected = (OnShowTimeSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement a Listener");
        }
    }
	
    
private void crearBarraFechas(){
    	
    	TextView textEditDia = null;     	
    	
    	View viewer = getView();    	

        
       ViewTreeObserver vto = getView().findViewById(R.id.separador1).getViewTreeObserver();        
        vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
            	
            	View viewer = getView();
            	
            	View separador = getView().findViewById(R.id.separador1);
            	
            	ImageView barra = (ImageView) viewer.findViewById(R.id.selector); 
            	
            	LayoutParams Barraframe = barra.getLayoutParams(); 
            	
            	Barraframe.width =  separador.getLeft() - separador.getWidth();
            	
            	barra.setLayoutParams(Barraframe);    	 
            	
            	ViewTreeObserver obs = getView().getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
            	

            }
        });        
        
        
        ly = (LinearLayout) viewer.findViewById(R.id.showTimesLayout);      
        
        Calendar calendar = Calendar.getInstance();
        
        if(premier == true)      	
        	calendar.set(Integer.valueOf(premierDate[0]), Integer.valueOf(premierDate[1]) - 1, Integer.valueOf(premierDate[2]));
        
        
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        
        day--;
        //calendar.ad
        for(int i = 0; i < 7; i++){
        	if(day == 7)
        		day = 0;        	
        	
        	switch(i){
        	case 0 :
        		textEditDia = (TextView) viewer.findViewById(R.id.dia0);
        		break;
        	case 1 :
        		textEditDia = (TextView) viewer.findViewById(R.id.dia1);		
        		break;
        	case 2 :
        		textEditDia = (TextView) viewer.findViewById(R.id.dia2);
        		break;
        	case 3 :
        		textEditDia = (TextView) viewer.findViewById(R.id.dia3);
        		break;
        	case 4 :
        		textEditDia = (TextView) viewer.findViewById(R.id.dia4);
        		break;
        	case 5 :
        		textEditDia = (TextView) viewer.findViewById(R.id.dia5);
        		break;
        	case 6 :
        		textEditDia = (TextView) viewer.findViewById(R.id.dia6);
        		break;
        	}
        	if(day == 0 || day == 6)
        		textEditDia.setText(Html.fromHtml("<font size=\"0\" color=\"#c71f1f\">" + semana[day]+ "</font> <br> " + String.valueOf(calendar.get(Calendar.DAY_OF_MONTH))));
        	else
        		textEditDia.setText(Html.fromHtml("<font size=\"0\" color=\"#000000\">" + semana[day]+ "</font> <br> " + String.valueOf(calendar.get(Calendar.DAY_OF_MONTH))));
        	
        	textEditDia.setOnClickListener(this);	
//        	textEditDia.setTag(calendar.get)
        	
        	//setText(Html.fromHtml("<font size=\"0\" color=\"#ff0000\">" + semana[day]+ "</font> " + String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
        	day++;
        	calendar.add(Calendar.DAY_OF_MONTH, 1);
        }
    }

	@Override
	public void onClick(View v) {
		
		ImageView barra = null;		
		ViewGroup.MarginLayoutParams lpt = null;
		LayoutParams Barraframe = null;
		View separadorA = null, separadorB = null;
		
		
		if(getView().findViewById(R.id.selector) != null){
			barra = (ImageView) getView().findViewById(R.id.selector);
			lpt =(MarginLayoutParams)barra.getLayoutParams();
			Barraframe = barra.getLayoutParams();
		}
	
	    		
		switch(v.getId()){
		
		case R.id.dia0 :
			
			lpt.setMargins(v.getLeft(),lpt.topMargin,lpt.rightMargin,lpt.bottomMargin);
			barra.setLayoutParams(lpt);
			
			separadorA = getView().findViewById(R.id.separador1);
			
			Barraframe.width = separadorA.getLeft();
			barra.setLayoutParams(Barraframe);
			
			ly.removeAllViewsInLayout();
			ly.invalidate();
			
			dia(0);
			
			break;
			
		case R.id.dia1 :	
			
			separadorA = getView().findViewById(R.id.separador1);
			separadorB = getView().findViewById(R.id.separador2);
	        
			
			lpt.setMargins(v.getLeft(),lpt.topMargin,lpt.rightMargin,lpt.bottomMargin);
			barra.setLayoutParams(lpt);
			
					
			
			Barraframe.width = separadorB.getLeft() - separadorA.getLeft() - separadorA.getWidth();
			barra.setLayoutParams(Barraframe);
	
			
			ly.removeAllViewsInLayout();
			ly.invalidate();
			
			dia(1);
			
			break;
			
		case R.id.dia2 :
			
			separadorA = getView().findViewById(R.id.separador2);
			separadorB = getView().findViewById(R.id.separador3);
			
			lpt.setMargins(v.getLeft(),lpt.topMargin,lpt.rightMargin,lpt.bottomMargin);
			barra.setLayoutParams(lpt);
			
			Barraframe.width = separadorB.getLeft() - separadorA.getLeft() - separadorA.getWidth();
			barra.setLayoutParams(Barraframe);
			
			ly.removeAllViewsInLayout();
			ly.invalidate();
			
			dia(2);
			
			break;
			
		case R.id.dia3 :
			
			separadorA = getView().findViewById(R.id.separador3);
			separadorB = getView().findViewById(R.id.separador4);
			
			lpt.setMargins(v.getLeft(),lpt.topMargin,lpt.rightMargin,lpt.bottomMargin);
			barra.setLayoutParams(lpt);
			
			Barraframe.width = separadorB.getLeft() - separadorA.getLeft() - separadorA.getWidth();
			barra.setLayoutParams(Barraframe);
			
			ly.removeAllViewsInLayout();
			ly.invalidate();
			
			dia(3);
			
			break;
			
		case R.id.dia4 :
			
			separadorA = getView().findViewById(R.id.separador4);
			separadorB = getView().findViewById(R.id.separador5);
		
			lpt.setMargins(v.getLeft(),lpt.topMargin,lpt.rightMargin,lpt.bottomMargin);
			barra.setLayoutParams(lpt);
			
			Barraframe.width = separadorB.getLeft() - separadorA.getLeft() - separadorA.getWidth();
			barra.setLayoutParams(Barraframe);
			
			ly.removeAllViewsInLayout();
			ly.invalidate();
			
			dia(4);
			
			break;
			
		case R.id.dia5 :
			
			separadorA = getView().findViewById(R.id.separador5);
			separadorB = getView().findViewById(R.id.separador6);
			
			lpt.setMargins(v.getLeft(),lpt.topMargin,lpt.rightMargin,lpt.bottomMargin);
			barra.setLayoutParams(lpt);
			
			Barraframe.width = separadorB.getLeft() - separadorA.getLeft() - separadorA.getWidth();
			barra.setLayoutParams(Barraframe);
			
			ly.removeAllViewsInLayout();
			ly.invalidate();
			
			dia(5);
			
			break;
			
		case R.id.dia6 :
			
			separadorA = getView().findViewById(R.id.separador6);
			
		
			lpt.setMargins(v.getLeft(),lpt.topMargin,lpt.rightMargin,lpt.bottomMargin);
			barra.setLayoutParams(lpt);
			
			Barraframe.width = getView().getWidth() - separadorA.getLeft();
			barra.setLayoutParams(Barraframe);
			
			
			
			ly.removeAllViewsInLayout();
			ly.invalidate();
			
			dia(6);
			
			break;
			
		case R.id.frameLayout1 :
			activetab = 0;
			setActiveTab(0);
			inflarFunciones();
			break;
			
		case R.id.frameLayout2 :
			activetab = 1;
			setActiveTab(1);
			inflarSinopsis();			
			break;
		
				
			
		}
		
	}
	
	private void llenarFuncion(Iterator iterSalas, int index){
		
		Theaters dumSala;
		ShowTime dumShowTime;
		Iterator iterShows;
		String [] cineInfo;
		String salaNombre;
	
	
		
		int counter;
		List<ShowTime> shows;
		
		if(iterSalas.hasNext() == false){
			Log.i("No hay funciones", "nada");
		}
		
		((TextView)getView().findViewById(R.id.nodisponible)).setVisibility(View.GONE);	
		
//		((TextView) getView().findViewById(R.id.FechaFunciones)).setText(text);
		
		while(iterSalas.hasNext()){
			dumSala = (Theaters) iterSalas.next();
			shows = dumSala.getShowTimes();
			iterShows = shows.iterator();
			String tag = "";
			Movie dumy = MovieManager.getInstance().getSelectedMovie(0);
			
			cineInfo = Cines.getCineById(String.valueOf(dumSala.getCineId()));
			
						
			ll = new LinearLayout(getActivity());   
			
	        ll.setOrientation(LinearLayout.HORIZONTAL);    
	        
			
	        //salaNombre = SalasManager.getInstance().getSalaNameById(dumSala.getSalaId());
			salaNombre = dumSala.getNombreSala();
	        
	        if(salaNombre == null){
	        	ConnectionManager.getInstance().clearCache("Theaters");
	        	Intent reLaunchMain= new Intent(getActivity(), SplashActivity.class);
	    		reLaunchMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
	    		startActivity(reLaunchMain);
	    		getActivity().finish();
	        	return;
	        }
	        
	        agregarShowTimeText(cineInfo[Cines.nombre], salaNombre);
	        
	        ly.addView(ll);
	        
	        counter = 0;	        
			while(iterShows.hasNext()){
				dumShowTime = (ShowTime) iterShows.next();
				
				tag = String.valueOf(dumSala.getCineId()) + "-" + String.valueOf(dumShowTime.getShowTimeId()) + "-" + String.valueOf(dumy.getId() 
						+ "-" + String.valueOf(dumSala.getSalaId()));
								
				if(counter < botonesHorizontalesMax){			
					
										
					if(iterShows.hasNext() || counter > 0)
			        	agregarShowTimeButton(dumShowTime.getHoraFuncion(), ll, tag, false, index);
					else{
						int height = agregarShowTimeButton(dumShowTime.getHoraFuncion(), ll, tag, true, index);
//						TODO aqui fondo
				        ImageView fondo = new ImageView(getActivity());
				        fondo.setBackgroundResource(R.drawable.bg);
				        int width = getView().getWidth()/botonesHorizontalesMax;
				        
			    		int weigth = 0;   
				        LayoutParams lp = new LinearLayout.LayoutParams(width, LayoutParams.MATCH_PARENT, weigth); 
				        fondo.setLayoutParams(lp);
				        ll.addView(fondo);
					}
					
					counter++;		
			    			        	
			       
				}else{					
						ll = new LinearLayout(getActivity());   						
				        ll.setOrientation(LinearLayout.HORIZONTAL);  
				        ly.addView(ll);
				        int height = agregarShowTimeButton(dumShowTime.getHoraFuncion(), ll, tag, true, index);
				        counter = 1;
				        //TODO aqui fondo
				        if(!iterShows.hasNext()){
				        	ImageView fondo = new ImageView(getActivity());
				        	fondo.setBackgroundResource(R.drawable.bg);
				        	int width = getView().getWidth()/botonesHorizontalesMax;
				        	int weigth = 0;
				        	LayoutParams lp = new LinearLayout.LayoutParams(width, LayoutParams.MATCH_PARENT, weigth);
				        	fondo.setLayoutParams(lp);
				        	ll.addView(fondo);
				        }
				        
				        
				}	
				
				
				
			}
			
		}
		
	}
	
	
	private void dia(final int index){		
		
		ly.removeAllViewsInLayout();
		ly.invalidate();
		
		final ProgressDialog pd;
		
		String date = "";
		
		if(premier == true)
			date =  premierDate[0] + "/" + String.valueOf(Integer.parseInt(premierDate[1]) + index) + "/" + premierDate[2];
		else
			date = Fechas.fechaNormal(index);
		
		
		if(MovieManager.getInstance().isSelectedMovieLoaded(index) == false){
					
			new GetMovieTask().execute(String.valueOf(index));			
			
		}else{
			 
			pelideldia = MovieManager.getInstance().getSelectedMovie(index);
			
			
			if(pelideldia == null){				
			  ((TextView)getView().findViewById(R.id.nodisponible)).setVisibility(View.VISIBLE);				
				//				((TextView)getView().findViewById(R.id.FechaFunciones)).setText("No hay funciones disponibles");
				return;
			}
			
//			((TextView)getView().findViewById(R.id.FechaFunciones)).setText("Funciones: " + date);
			
			List <Theaters> salasdiaX = pelideldia.getSalas();		
			
			llenarFuncion(salasdiaX.iterator(), index);			
			
			DetallesCompra.getInstance().setDia(index);
			
		}
	
	
	}
	
	
	private class GetDetailsTask extends AsyncTask<String, Void, Integer> {
		 
		ProgressDialog pd;
		boolean loadresult;
		 		 
		 @Override
		protected void onPreExecute() {
		    
			 
			 pd = ProgressDialog.show(getActivity(), "Espere.", "Cargando detalles.", true, true);				
			 
	    	}
	     
		 @Override
		protected Integer doInBackground(String... params) {
			 
			 //if(premier == false)
				 //loadresult = MovieManager.getInstance().loadMoviesDetail(peli.getId());
			 //else
				 //loadresult = EstrenosManager.getInstance().loadMoviesDetail(peli.getId());
	         
	         return 0;
	     }	    

	     @Override
		protected void onPostExecute(Integer result) {
	    	 String textinfo = "";
	    	 if(getView() == null)
	    		 return;
	    	 	    	 
	    	 try {
		    	 pd.cancel();
		         pd = null;
		        } catch (Exception e) { }
	    	 
	    	 if(loadresult){
	    		 ((ImageView)getView().findViewById(R.id.icon)).setImageDrawable(peli.getPoster());
	    		 
	           	 if(peli.getDirector() != "")
	           		 textinfo += "<b>Director: </b><font color=#707070>" + peli.getDirector() + "</font>";
	           	 
	           	((TextView)getView().findViewById(R.id.TituloPeliDetalles)).setText(Html.fromHtml("<big><b>" + peli.getSpanishTitle() + "</b></big>" +  "<br />" + 
	                         "<font color=#707070>" + peli.getOriginalTitle() + "</font><br />" + "<b>Género: </b><font color=#707070>"
	                         + peli.getGenere() + "</font><br />" + textinfo));
	          	 
	    		 if(peli.getPreSale().equals("NP")){		          		
		          		inflarSinopsis();	          		 
	          	 }else if(peli.getPreSale().equals("P")){

	          		 	
	          	 }
	    		 
	    	 }else{
	    		 Toast.makeText(getActivity(), "Fallo de conexión.", Toast.LENGTH_LONG).show();
	    	 }
	    	 	    	 
	    	 	    	 
	         
	     }
	 }

	
	 private class GetMovieTask extends AsyncTask<String, Void, Integer> {
		 ProgressDialog pd;
		 int index;
		 boolean result;
		 @Override
		protected void onPreExecute() {

			 pd = ProgressDialog.show(getActivity(), "Espere.", "Consultando funciones disponibles.", true, false);				
			 pd.setCancelable(true);
	    	}
	     
		 @Override
		protected Integer doInBackground(String... params) {
			 
			 index = Integer.parseInt(params[0]); 		 
			 pelideldia = MovieManager.getInstance().getSelectedMovie(index);
			 
			 Fechas.fechaNormal(index);
	         
			 if(pelideldia == null)
				result = false;
			 else
				 result = true;			 
			 
	         return 0;
	     }	    

	     @Override
		protected void onPostExecute(Integer result) {
	    	 
	    	 
	    	 
	    	 if(getView() == null)
          		return;
	    	 
	    	 if(pelideldia == null){
	    		 pd.cancel();
	    		 
	             if(ConnectionManager.getInstance().getLastError() != -1){	            	
	            	 ((TextView)getView().findViewById(R.id.nodisponible)).setVisibility(View.VISIBLE);	
	             }else
	            	 ConnectionManager.getInstance().mensajeError("Fallo de conexión.");
				
 				return;
 			}	    	 
	    	
	    	
 			
 			List <Theaters> salasdiaX = pelideldia.getSalas();		
 			
 			llenarFuncion(salasdiaX.iterator(), index);			
 			
 			DetallesCompra.getInstance().setDia(index);
 			
 			try {
 		    	 pd.cancel();
 		         pd = null;
 		        } catch (Exception e) { }
	         
	     }
	 }
	 
	 public void configurationChanged(){
		 
		  ViewTreeObserver vto = getView().getViewTreeObserver();        
	        vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
	            @Override
	            public void onGlobalLayout() {
	            	
	      
	           		setActiveTab(activetab);
			 		 
	        		 if(activetab == 0)
	        			   inflarFunciones();
	        		  else
	        			   inflarSinopsis();        	
	            		 
	            	
	            	ViewTreeObserver obs = getView().getViewTreeObserver();
	                obs.removeGlobalOnLayoutListener(this);
	            	

	            }
	        });        
		
		 
	 }
	 
	 /*public void onPause()
		{
		
		    LocalyticsManager.localyticsSession().upload();
		    super.onPause();
		}*/

	@Override
	public void onPause() {
		super.onPause();

		FlurryAgent.onEndSession(getActivity());
	}

	@Override
	public void onStop() {
		super.onStop();

		FlurryAgent.onEndSession(getActivity());
	}
	 
	
	
}
