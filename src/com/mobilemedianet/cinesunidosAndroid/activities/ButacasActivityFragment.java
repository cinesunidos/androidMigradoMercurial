package com.mobilemedianet.cinesunidosAndroid.activities;



import java.io.IOException;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.FloatMath;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.ZoomControls;

import com.flurry.android.FlurryAgent;
import com.mobilemedianet.cinesunidosAndroid.R;
import com.mobilemedianet.cinesunidosAndroid.managers.ButacasManager;
import com.mobilemedianet.cinesunidosAndroid.managers.DetallesCompra;
//import com.mobilemedianet.cinesunidosAndroid.managers.LocalyticsManager;
import com.mobilemedianet.cinesunidosAndroid.managers.MovieManager;
import com.mobilemedianet.cinesunidosAndroid.objects.Butaca;
import com.mobilemedianet.cinesunidosAndroid.objects.Movie;
import com.mobilemedianet.cinesunidosAndroid.utilities.Contador;
import com.mobilemedianet.cinesunidosAndroid.utilities.Metrics;



public class ButacasActivityFragment extends Fragment implements OnTouchListener{

	OnButacasListener irAPagar2;

	
static private ButacasActivityFragment instance;

/******************************************/
private static final String Flurry_API_Key = "GSW94MXD24S7D546NST6";
private static final String TAG = "Touch";
// We can be in one of these 3 states
static final int NONE = 0;
static final int DRAG = 1;
static final int ZOOM = 2;
private int mode = NONE;
private boolean moved = false;
private boolean zoomstatus = false;
// Remember some things for zooming
PointF start = new PointF();
PointF mid = new PointF();
PointF rmid = new PointF();	
float oldDist = 1f;
float oldSize = 0;
long timeClick = 0;
private  MyCount contador = null;
boolean fingerup = true;

private ArrayList<Butaca> matriz;

private TableLayout contenedor;
private LinearLayout mainly;
private String tag;
private int width;
private int nwidth;
private int relativewidth;
private int mRows;
private int mCols;
private int currentX;
private int currentY;


private int  oldcontainerheight;
private int  oldcontainerwidth; 
private int  oldercontainerheight;
private int  oldercontainerwidth;
private int  mincontainerwidth;
private int  mincontainerheight;


private int epsilon;
private int areah;
private int areaw;
private ButacasManager butman;

//private RelativeLayout actBarButton; 
//private TextView actBarButtonText;



private boolean zoomed = false;

/******************************************/
	
	

	static public ButacasActivityFragment getInstance(){
		
		if(instance == null)
			instance = new ButacasActivityFragment();
		return instance;
	}
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
	    super.onCreate(savedInstanceState);
	    butman = ButacasManager.getInstance();

		//Flurry Start Session
		FlurryAgent.init(getActivity(), Flurry_API_Key);
		FlurryAgent.onStartSession(getActivity());

	}
	
	public void onResume(){
		super.onResume();
		/*if(LocalyticsManager.localyticsSession() == null){
    		LocalyticsManager.newLocalytics(getActivity().getApplicationContext());
        	LocalyticsManager.localyticsSession().open();
    	}*/
		//LocalyticsManager.localyticsSession().tagEvent("Butacas Selección");
		FlurryAgent.logEvent("Butacas Selección");
		
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
         Bundle savedInstanceState) {
		
	    	
	    	
	   
         View viewer = inflater.inflate(R.layout.butacas, container, false);   
        
         ButacasManager.getInstance().setBackFromMap(true);
         
//         actBarButton = (RelativeLayout) getActivity().findViewById(R.id.genericButtonActionBar);
//         actBarButtonText = (TextView) getActivity().findViewById(R.id.actBarButtonText);
//         actBarButtonText.setText("Listo");
//         actBarButton.setBackgroundResource(R.drawable.action_buttons_centro);
         
         RelativeLayout actionbar = (RelativeLayout) getActivity().findViewById(R.id.actionBar);
         actionbar.setVisibility(View.GONE);
         
         
         
         
         
         
         ViewTreeObserver vto = ((LinearLayout) viewer.findViewById(R.id.mainlayout)).getViewTreeObserver();
         vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
			 @Override
			 public void onGlobalLayout() {
				 if (getView() == null)
					 return;

				 contador = new MyCount(Contador.getInstance().getTimer(), 500);
				 contador.start();
				 String hora = DetallesCompra.getInstance().getFecha();
				 Movie peli = MovieManager.getInstance().getSelectedMovie(0);
				 ((TextView) getView().findViewById(R.id.TituloPeliDetalles)).setText(Html.fromHtml(peli.getSpanishTitle() + "<br /><small>" +
						 hora + "</small><br />"));


				 RelativeLayout listo = (RelativeLayout) getView().findViewById(R.id.botonlisto);

				 listo.setOnClickListener(new OnClickListener() {

					 @Override
					 public void onClick(View v) {
						 int selected = ButacasManager.getInstance().getReserverdCount();
						 int tickets = DetallesCompra.getInstance().getNumeroTotalEntradas();
						 // TODO Auto-generated method stub
						 if (selected == tickets) {
							 cancelTimer();
							 RelativeLayout actionbar = (RelativeLayout) getActivity().findViewById(R.id.actionBar);
							 actionbar.setVisibility(View.VISIBLE);
							 getFragmentManager().popBackStack();
						 } else {
							 AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
							 alertDialog.setTitle("CinesUnidos");
							 alertDialog.setMessage("Faltan butacas por seleccionar.");
							 alertDialog.setIcon(R.drawable.icono);
							 alertDialog.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
								 @Override
								 public void onClick(DialogInterface dialog, int which) {
									 dialog.cancel();
								 }
							 });

							 alertDialog.show();
						 }

					 }
				 });

				 ZoomControls zoomControls = (ZoomControls) getView().findViewById(R.id.zoomControls1);

				 zoomControls.setOnZoomInClickListener(new OnClickListener() {

					 @Override
					 public void onClick(View v) {
						 // TODO Auto-generated method stub
						 resize(0, 1);

					 }
				 });

				 zoomControls.setOnZoomOutClickListener(new OnClickListener() {

					 @Override
					 public void onClick(View v) {
						 // TODO Auto-generated method stub
						 resize(0, -1);
						 contenedor.scrollTo(0, 0);

					 }
				 });
				 generar();
				 ViewTreeObserver obs = ((LinearLayout) getView().findViewById(R.id.mainlayout)).getViewTreeObserver();
				 obs.removeGlobalOnLayoutListener(this);

			 }
		 });
         
        return viewer;
    }   
	
	public void cancelTimer(){
		if(contador == null)
			return;
		contador.cancel();
		contador = null;
		
	}
	
	/***************************************/
	
	  private void generar(){
	    	
		  	epsilon = Metrics.dpToPixel(15);	    			  
	    		    	
	    	contenedor = (TableLayout) getView().findViewById(R.id.tablelayout);
	    	contenedor.setTag("contenedor");
	    	
	    	mainly = (LinearLayout) getView().findViewById(R.id.mainlayout);
	    	mainly.setOnTouchListener(this);
	    	
	    	areah = mainly.getHeight() - ((LinearLayout) getView().findViewById(R.id.lytope)).getHeight();
	    	areaw = mainly.getWidth();
	    	
	    	
	    		    	
//	    	tb.setOnTouchListener(this);
	    	
	    	try {
				generarButacas(contenedor);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	
	    	
	    	
	    }
		    
	 private void generarButacas(TableLayout ly) throws IOException{
		 	boolean espasillo = false;
		 	int county = 0;
		 	int countx = 0;
		 
		 	ArrayList<Integer> pasillosFil = butman.getPasillosFil();
		 	ArrayList<Integer> pasillosCol = butman.getPasillosCol();
		 	
		 	int ycount = pasillosFil.size();
		 			 			 	
		 	if(ycount != 0)
		 		mRows = butman.getFil() + ycount + 2;
		 	else
		 		mRows = butman.getFil() + 2;
		 	
	    	mCols = butman.getCol() + 2;

	    	width = (getView().getWidth()-Metrics.dpToPixel(6))/mCols;
	    	
	    	Butaca butobj;
	    	
	    	TableLayout table = ly;
	        for (int y = 0; y < mRows; y++) {

	            TableRow tr = new TableRow(getActivity());
	            tr.setLayoutParams(new TableRow.LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT));

	            for (int x = 0; x < mCols; x++) {

	                ImageView view = new ImageView(getActivity());                
	  
	                if(y == 0 || y == mRows - 1){
	                	if(x == 0 || x == mCols - 1 ){
	                		view.setImageDrawable(Drawable.createFromStream(getActivity().getAssets().open("butacas/asientopasillotouch.png"), null));
	                	if(y == mRows - 1 && x == 0)
	                		countx = 0;
	                	}else{
	                		/**********/
	                		
	                		for(int i = 0; i < pasillosCol.size(); i++){
	                			
	                			if(pasillosCol.get(i) == x - 1)
	                				espasillo = true;
	                			
	                		}
	                		
	                		if(espasillo == true){
	                			
	                			espasillo = false;
	                			
	                			countx++;
	             
	                			view.setImageDrawable(Drawable.createFromStream(getActivity().getAssets().open("butacas/asientopasillotouch.png"), null));
	                			
	                		}else{
	                			view.setImageDrawable(getSquareByNumer(x  - countx));       			
	                		}
	                		               		
	                		
	                		/**********/
	                		
	                		//view.setImageDrawable(getSquareByNumer(x));
	                	
	                	}
	                }else if(x == 0 || x == mCols - 1){
	                	if(y == 0)
	                		view.setImageDrawable(Drawable.createFromStream(getActivity().getAssets().open("butacas/asientopasillotouch.png"), null));
	                	else{
	                		
	                		for(int i = 0; i < pasillosFil.size(); i++){
	                			
	                			if(pasillosFil.get(i) == y - 1)
	                				espasillo = true;
	                			
	                		}
	                		
	                		if(espasillo == true){
	                			
	                			espasillo = false;
	                			
	                			if(x == 0)
	                				county++;
	                			
	                			view.setImageDrawable(Drawable.createFromStream(getActivity().getAssets().open("butacas/asientopasillotouch.png"), null));
	                			
	                		}else{
	                			view.setImageDrawable(getSquareLetter(y - county + ycount));	                			
	                		}
	                		
	                	}	                		                	             	
	                	
	                }else{
	                	
	                	butobj = butman.getAt(x-1, y-1);
	                	
	                	 if(butobj != null){
	                		 switch(butobj.getType()){
	                		 case Butaca.DISPONIBLE :
	                			 view.setImageDrawable(Drawable.createFromStream(getActivity().getAssets().open("butacas/asientodisponibletouch.png"), null));
	                			break;
	                			case Butaca.OCUPADA :
	                				view.setImageDrawable(Drawable.createFromStream(getActivity().getAssets().open("butacas/asientoocupadotouch.png"), null));
	                			break;
		 	                	case Butaca.SELECCIONADA :
		 	                		view.setImageDrawable(Drawable.createFromStream(getActivity().getAssets().open("butacas/asientoseleccionadotouch.png"), null));
		 	                	break;	                		
		                		default :
		                		view.setImageDrawable(Drawable.createFromStream(getActivity().getAssets().open("butacas/asientopasillotouch.png"), null));
		                		break;
		                		} 	 
	                		 view.setTag(butobj.getNombreButaca());	 
	                		 view.setOnTouchListener(this);
//	                		 view.setOnTouchListener(new OnTouchListener() {
//								
//								@Override
//								public boolean onTouch(View v, MotionEvent event) {
//									// TODO Auto-generated method stub
//									Log.i("Touched", v.getTag().toString());
//									return false;
//								}
//							});
	 	                }else{
	 	                	view.setImageResource(R.drawable.asientopasillotouch);	 	                	
	                	}
	                }
	                view.setId(mCols*y + x);
	                view.setScaleType(ImageView.ScaleType.CENTER_CROP);
	                view.setLayoutParams(new LayoutParams(width, width));
	                view.setOnTouchListener(this);
	                tr.addView(view);
	                
	            }
	            table.addView(tr);
	        }
	        
	        TableRow tr = new TableRow(getActivity());
	        tr.setLayoutParams(new TableRow.LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
	        
	        int inicio = (mCols - 8)/2;

	        for(int x = 0; x < mCols; x++){
	        	ImageView view = new ImageView(getActivity()); 
	        	if(x >= inicio && x < inicio + 8){
	        		switch(x - inicio){
	        		case 0 :
	        			view.setImageDrawable(getSquareLetterInOrder(15));//P
	        			break;
	        		case 1 :
	        			view.setImageDrawable(getSquareLetterInOrder(0));//A
	        			break;
	        		case 2 :
	        			view.setImageDrawable(getSquareLetterInOrder(13));//N
	        			break;
	        		case 3 :
	        			view.setImageDrawable(getSquareLetterInOrder(19));//T
	        			break;
	        		case 4 :
	        			view.setImageDrawable(getSquareLetterInOrder(0));//A
	        			break;
	        		case 5 :
	        			view.setImageDrawable(getSquareLetterInOrder(11));//L
	        			break;
	        		case 6 :
	        			view.setImageDrawable(getSquareLetterInOrder(11));//L
	        			break;
	        		case 7 :
	        			view.setImageDrawable(getSquareLetterInOrder(0));//A
	        			break;        			
	        		}
	        	}else
	        	view.setImageDrawable(Drawable.createFromStream(getActivity().getAssets().open("butacas/asientopasillotouch.png"), null));
				view.setId(mCols*(mRows)  + x);
	            view.setScaleType(ImageView.ScaleType.CENTER_CROP);
	            view.setLayoutParams(new LayoutParams(width, width));
//	            view.setOnTouchListener(this);
	            tr.addView(view);
	        	
	        }
	        table.addView(tr);
	        table.setOnTouchListener(this);
	        table.invalidate();  
	        mRows++;
	        
	        relativewidth = nwidth = width;
	        mincontainerheight = oldercontainerheight = oldcontainerheight = width*mRows;
	        mincontainerwidth = oldercontainerwidth = oldcontainerwidth = width*mCols;
	    	
	    } 
	    
	    private void resize(float size, int type){
	    	
//	    	float newSize = size - oldSize;	
//	    	int newWidth = (int) (nwidth +  newSize*30);   	    	
	    	
	    	int newWidth = (int) (relativewidth*(size));  
	    	
	    		    	    		
	    	if(type == 1){
	    		relativewidth = newWidth = relativewidth*2;
	    	}else if(type == -1 || newWidth < width){
	    		if(relativewidth >= width*2)
	    			relativewidth = newWidth = relativewidth/2;
	    		else
	    			relativewidth = newWidth = width;	    			
	    	}
	    	else if(type == 0){
	    		if(newWidth < width){	    			
	    			relativewidth = newWidth = width;    			
	    		}
//	    		
	    	}
	    	
	    	
	    	    	
	    	int containerheight = (int) (newWidth*mRows + newWidth/1.5);
	    	int containerwidth =  (int) (newWidth*mCols + newWidth/1.5);
	    	
	    	
	    	contenedor.setLayoutParams(new TableRow.LayoutParams(containerwidth, containerheight));    	
	    
	    	for (int i = 0; i < mRows; i++) {
	    		 for (int j = 0; j < mCols; j++) {   
	    			 ImageView view = (ImageView) getView().findViewById(mCols*i + j);    
	    			 view.getLayoutParams().height = newWidth;
	    			 view.getLayoutParams().width = newWidth;    			 
	   			 
	    			 }
	    		
	    	}
	    	
	    	if(type == 0 && newWidth == width)
	    		contenedor.scrollTo(0, 0);  		   			
	    	
	     	nwidth = newWidth;
	    	oldSize = size;    	
	    	
	    	oldcontainerheight = containerheight;
	    	oldcontainerwidth = containerwidth;
	    	
	    	
	    }
	    
	       
	    
		
		@Override
		public boolean onTouch(View v, MotionEvent event) {
		      

		      // Dump touch event to log
//		      Log.i("Tag", v.getTag().toString());
		      if(v.getTag() != null && !v.getTag().toString().equals("contenedor")){
		    	  tag = v.getTag().toString();
		    	  return false;		    	  
		      }
		    	  

		      // Handle touch events here...
		      switch (event.getAction() & MotionEvent.ACTION_MASK) {
		      case MotionEvent.ACTION_DOWN:
		    	 start.set(event.getX(), event.getY());
		         currentX = (int) event.getRawX();
		         currentY = (int) event.getRawY();
//		         Log.i("Mode", "ACTION_DOWN");		        
		         break;
		      case MotionEvent.ACTION_POINTER_DOWN:
		    	 if(event.getPointerCount() < 2)
		    		 break;
		         oldDist = spacing(event);
		         if (oldDist > 10f) {
		        	if(event.getPointerCount() < 2)
		        		break;
		            midPoint(mid, event);
		            mode = ZOOM;		            
		         }
//		         Log.i("Mode", "ACTION_POINTER_DOWN");
		         if(fingerup == true){
		        	 rmid = relativeMidPoint(v);
		        	 fingerup = false;
		         }
		         break;
		      case MotionEvent.ACTION_UP:
		    	  
//		    	  Log.i("Mode", "ACTION_UP");		    	 
		    	  if(mode == NONE && moved == false){
		    		  try {
		    			  butacaClick();
		    			  }catch (IOException e) {
		    				  // TODO Auto-generated catch block
		    				  e.printStackTrace();
		    				  }
		    		  }
		    	  mode = NONE;	  	    	
		    	  moved = false;
		    	  break;
		      case MotionEvent.ACTION_POINTER_UP:
//		    	  Log.i("Mode", "ACTION_POINTER_UP");
		    	  fingerup = true;
		    	  mode = NONE;
		    	  relativewidth = nwidth;		                
		         break;
		      case MotionEvent.ACTION_MOVE:	       
//		    	 Log.i("Mode", "ACTION_MOVE");		    	 
		         if (mode == ZOOM) {
		        	moved = true;
		            float newDist = spacing(event);		           
		            if (newDist > 10f) {	               
		               float scale = newDist / oldDist;
		               resize(scale, 0);
		               scrollToRelativeCenter();
		            }
		         }else{
			        	 int x2 = (int) event.getRawX();
			             int y2 = (int) event.getRawY();
			             int xsc = contenedor.getScrollX();
			             int ysc = contenedor.getScrollY();
			             
			             int rightlimit = nwidth*mCols;
			             int leftlimit = 0;
			             int uplimit = 0;
			             int downlimit = nwidth*mRows;
			             boolean limite = false;
			             
			             
			             
			             if(Math.abs(currentX - x2) > 10 || Math.abs(currentY - y2) > 10)
			            	 moved = true;
			             
		            
			             if(xsc + currentX - x2 < leftlimit || ysc + currentY - y2 < uplimit){
			            	 			            	 
			            	 if(xsc < leftlimit  && rightlimit > mincontainerwidth)
			            		 limite = false;
			            	 else if(ysc < uplimit && downlimit > mincontainerheight)
			            		 limite = false;			            	
			            	 else			            
			            		 limite = true;            	 
		       	 
			            	 
			             }
			             
			             if(xsc + (currentX - x2) + areaw > rightlimit + epsilon || ysc + (currentY - y2) + areah > downlimit + epsilon){
			            	 
			            	 if(xsc + areaw > rightlimit + epsilon && rightlimit > mincontainerwidth)
			            		 limite = false;
			            	 else if(ysc + areah > downlimit + epsilon && downlimit > mincontainerheight)
			            		 limite = false;
			            	 else
			            		 limite = true;	
			            	
			             }
			             
			             if(limite == false)
			            	 contenedor.scrollBy(currentX - x2 , currentY - y2);  
			             
			                     
			             currentX = x2;
			             currentY = y2;
			             break;
			         }
		        			         
		         break;
		      }

		      return true; // indicate event was handled
		   }
		
		 /** Determine the space between the first two fingers */
		   private float spacing(MotionEvent event) {
			   float x = 0;
			   float y = 0;
			  if(event.getPointerCount() >= 2){
				   x = event.getX(0) - event.getX(1);
				   y = event.getY(0) - event.getY(1);				  
			  }
			  return FloatMath.sqrt(x * x + y * y);
		      
		   }

		   /** Calculate the mid point of the first two fingers */
		   private void midPoint(PointF point, MotionEvent event) {
			   float x = 0;
			   float y = 0;
			   if(event.getPointerCount() >= 2){
				    x = event.getX(0) + event.getX(1);
				    y = event.getY(0) + event.getY(1);
			   }
		      point.set(x / 2, y / 2);
		      
		   }
	
	/***************************************/
	
	
	
	
	
	
	public interface OnButacasListener{
	       public void butacasListener();
	   }
	
	@Override
    public void onAttach(Activity activity) {
     super.onAttach(activity);
     try {
    	 irAPagar2 = (OnButacasListener) activity;
     } catch (ClassCastException e) {
         throw new ClassCastException(activity.toString()
                 + " must implement a Listener");
     }
 }
	
	
	 private Drawable getSquareByNumer(int number){
		 String file = "cuadro" + String.valueOf(number) + ".png";
		 Drawable image = null;
		 
		 try {
			image = Drawable.createFromStream(getActivity().getAssets().open("cuadros/" + file), null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		 
		   return image;
		   
	   }
	   
	   private Drawable getSquareLetter(int number){
		   int i = (mRows - 2) - number;
		   char a = (char) ('A' + i);
		   
		   String file = "cuadro" + String.valueOf(a) + ".png";
		   Drawable image = null;
			 
			 try {
				image = Drawable.createFromStream(getActivity().getAssets().open("cuadros/" + file), null);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
			 
			   return image;
			   
		   }
	   
	   private Drawable getSquareLetterInOrder(int number){		   
		   char a = (char) ('A' + number);
		   
		   String file = "cuadro" + String.valueOf(a) + ".png";
		   Drawable image = null;
			 
			 try {
				image = Drawable.createFromStream(getActivity().getAssets().open("cuadros/" + file), null);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
			 
			   return image;
			   
		   }
	   
	   private void butacaClick() throws IOException{		  
		   View v = getView().findViewWithTag(tag);
		   Butaca butaca = null;
		   Butaca dum;
		   if(v == null || v.getTag() == null){
			   
			   return;
		   }
		   
		   
		   
		   butaca = ButacasManager.getInstance().getButacaByName(v.getTag().toString());
		   
		   if(butaca == null)
			   return;
		   
		   if(butaca.getType() != Butaca.DISPONIBLE && butaca.getType() != Butaca.SELECCIONADA)
			   return;
		   
		   else if(butaca.getType() == Butaca.DISPONIBLE){
			   DetallesCompra.getInstance().cambioButacas(true);
			   if(ButacasManager.getInstance().getReserverdCount() == DetallesCompra.getInstance().getNumeroTotalEntradas()){
				   dum = ButacasManager.getInstance().getFirstReservedSeat();
				   int x = dum.getX() + 1;
				   int y = dum.getY() + 1;
				   
				   ((ImageView)getView().findViewById(mCols*y + x)).setImageDrawable(Drawable.createFromStream(getActivity().getAssets().open("butacas/asientodisponibletouch.png"), null));
				   dum.setType(Butaca.DISPONIBLE);
				   ButacasManager.getInstance().removeFirstSelected();
			   }
			   ((ImageView) v).setImageDrawable(Drawable.createFromStream(getActivity().getAssets().open("butacas/asientoseleccionadotouch.png"), null));
			   butaca.setType(Butaca.SELECCIONADA);
			   ButacasManager.getInstance().addSelection(butaca);
            	
		   }else if(butaca.getType() == Butaca.SELECCIONADA){
			   ((ImageView) v).setImageDrawable(Drawable.createFromStream(getActivity().getAssets().open("butacas/asientodisponibletouch.png"), null));
			   butaca.setType(Butaca.DISPONIBLE);
			   ButacasManager.getInstance().removeSelection(butaca);
		   }
		   
		   
		   
	   }
	   
	   public class MyCount extends CountDownTimer{
			
			public MyCount(long millisInFuture, long countDownInterval) {			
				super(millisInFuture, countDownInterval);
				
			}
			
			@Override
			public void onTick(long millisUntilFinished) {
				int seconds = (int) (millisUntilFinished / 1000);
				int minutes = seconds / 60;
				seconds     = seconds % 60;
				
				TextView text;
				
				try {
					 text = (TextView) getView().findViewById(R.id.Timer);
					 }catch (Exception e){
					    return;
					    }
				
				
				
				text.setText(String.format("Tiempo Restante: %d:%02d", minutes, seconds));
				Contador.getInstance().setTimer(millisUntilFinished);
				
			}
			
			@Override
			public void onFinish() {
				
				TextView text;
				
				try {
					 text = (TextView) getView().findViewById(R.id.Timer);
					 }catch (Exception e){
					    return;
					    }
				
				//LocalyticsManager.localyticsSession().tagEvent("Tiempo de Compra Agotado");
				//FlurryAgent.logEvent("Tiempo de Compra Agotado");
				ButacasManager.getInstance().setBackFromMap(false);
				text.setText("Agotado.");
				AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());			        
		        alertDialog.setTitle("CinesUnidos"); 			        
		        alertDialog.setMessage("Se ha agotado el tiempo de reserva, vuelva a empezar."); 			       
		        alertDialog.setIcon(R.drawable.icono);
		        alertDialog.setCancelable(false);
		        alertDialog.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
		            @Override
					public void onClick(DialogInterface dialog,    int which) {
		            	dialog.cancel();
		            	RelativeLayout actionbar = (RelativeLayout) getActivity().findViewById(R.id.actionBar);
		                actionbar.setVisibility(View.VISIBLE);
		            	getActivity().getSupportFragmentManager().popBackStack("comprar", 0);
		            }
		        });
		        alertDialog.show();
			}
			}
	   
	   private PointF relativeMidPoint(View v){		    
		   PointF point = new PointF();
		   point.set((mid.x)/oldercontainerwidth, (mid.y)/oldercontainerheight);

		   return point;
	   }
	   
	   private void scrollToRelativeCenter(){
		   int x = 0, y = 0, scx, scy;
		   
		   scx = contenedor.getScrollX();
		   scy = contenedor.getScrollY();
		   
//		  Log.i("scx scy", String.valueOf(scx) + " " + String.valueOf(scy));
		   
		  

		  
		  
		  x = (int) ((oldcontainerwidth - oldercontainerwidth)*(rmid.x));
		  y = (int) ((oldcontainerheight - oldercontainerheight)*(rmid.y));
		
		  
//		  Log.i("x y", String.valueOf(x) + " " + String.valueOf(y)); 

		   
		   contenedor.scrollBy(x, y);
		   oldercontainerwidth = oldcontainerwidth;
		   oldercontainerheight = oldcontainerheight;
	   }
	   
	   
	   public void actualizar(){
		   
		   ViewTreeObserver vto = getView().getViewTreeObserver();
	         vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
				 @Override
				 public void onGlobalLayout() {
					 if (getView() == null)
						 return;
					 unbindDrawables(getView().findViewById(R.id.tablelayout));
					 generar();
					 ViewTreeObserver obs = getView().getViewTreeObserver();
					 obs.removeGlobalOnLayoutListener(this);

				 }
			 });
	   }
	   
	   private void unbindDrawables(View view) {
			if(view == null)
				return;
		    if (view.getBackground() != null) {
		        view.getBackground().setCallback(null);
		    }
		    if (view instanceof ViewGroup) {
		        for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
		            unbindDrawables(((ViewGroup) view).getChildAt(i));
		        }
		        try{
		        	((ViewGroup) view).removeAllViews();
		        }catch(UnsupportedOperationException e){
		        	
		        }
		    }
		}
	   
	   /*public void onPause()
		{
		    
		    LocalyticsManager.localyticsSession().upload();
		    super.onPause();
		}*/

	@Override
	public void onPause() {
		super.onPause();

		FlurryAgent.onEndSession(getActivity());
	}

	@Override
	public void onStop() {
		super.onStop();

		FlurryAgent.onEndSession(getActivity());
	}

}
