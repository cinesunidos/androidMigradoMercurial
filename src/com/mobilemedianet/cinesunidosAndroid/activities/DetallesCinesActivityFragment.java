package com.mobilemedianet.cinesunidosAndroid.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.TextUtils.TruncateAt;
import android.text.format.Time;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.mobilemedianet.cinesunidosAndroid.MainActivity;
import com.mobilemedianet.cinesunidosAndroid.R;
import com.mobilemedianet.cinesunidosAndroid.SplashActivity;
import com.mobilemedianet.cinesunidosAndroid.managers.CinesManager;
import com.mobilemedianet.cinesunidosAndroid.managers.ConnectionManager;
import com.mobilemedianet.cinesunidosAndroid.managers.DetallesCompra;
import com.mobilemedianet.cinesunidosAndroid.managers.MovieManager;
import com.mobilemedianet.cinesunidosAndroid.objects.CineInfo;
import com.mobilemedianet.cinesunidosAndroid.objects.Cines;
import com.mobilemedianet.cinesunidosAndroid.objects.Movie;
import com.mobilemedianet.cinesunidosAndroid.objects.PeliculaCine;
import com.mobilemedianet.cinesunidosAndroid.objects.ShowTime;
import com.mobilemedianet.cinesunidosAndroid.objects.Theaters;
import com.mobilemedianet.cinesunidosAndroid.utilities.Fechas;
import com.mobilemedianet.cinesunidosAndroid.utilities.Metrics;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

//import com.mobilemedianet.cinesunidosAndroid.managers.LocalyticsManager;

public class DetallesCinesActivityFragment extends Fragment implements OnClickListener{
	
String [] semana = {"D", "L", "M", "M", "J", "V", "S"};
	
	LinearLayout ly, ll;
	
	OnShowTimeSelectedListener showTimeSelected;

	private static final String Flurry_API_Key = "GSW94MXD24S7D546NST6";

	private int botonesHorizontalesMax = 2;	
	private int fagmentWidth = 0;
	Movie peli, pelideldia;  
	Handler uiHandler;
	ArrayList<PeliculaCine> cine;
	private List <Theaters> salas;
	String cineInfo[];
	
	static private int selectedCineId = 0;
	static private DetallesCinesActivityFragment instance = null;
	 

	static public DetallesCinesActivityFragment getInstance(int i){
		selectedCineId = i;
		if(instance == null)
			instance = new DetallesCinesActivityFragment();
		return instance;
	}
	
	static public DetallesCinesActivityFragment getInstance(){
		
		if(instance == null)
			instance = new DetallesCinesActivityFragment();
		return instance;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
	    super.onCreate(savedInstanceState);

		//Flurry Start Session
		FlurryAgent.init(getActivity(), Flurry_API_Key);
		FlurryAgent.onStartSession(getActivity());
	    
//	    Log.i("cambiando", "cambiando orientacion");
	    
//	    setRetainInstance(true);
	}
	
    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	
  	    View viewer = inflater.inflate(R.layout.detalles_cine, container, false);    
  	    
  	   return viewer;
    }
    
    
  
    @Override
    public void onResume(){
    	super.onResume(); 
    	
    	ConnectionManager.setContext(getActivity());
    	
    	cineInfo = Cines.getCineById(String.valueOf(selectedCineId));
    	
    	if(ConnectionManager.checkConn() == false){
    		
    		
    		AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
    		alertDialog.setTitle("CinesUnidos"); 
    		alertDialog.setMessage("No hay conexión a internet");
    		alertDialog.setIcon(R.drawable.icono); 
    		alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
    			
    			@Override
    			public void onClick(DialogInterface dialog,int which) {
    				dialog.cancel();
    				((MainActivity) getActivity()).goHome();
    				return;
    			}
  	      }); 
  	      
  	    
  	      alertDialog.show(); 
  		
  	      return;
    	}
    	
    		/*if(LocalyticsManager.localyticsSession() == null){
    			LocalyticsManager.newLocalytics(getActivity().getApplicationContext());
    			LocalyticsManager.localyticsSession().open();
    		}*/
    	
    		Map <String,String> values = new HashMap<String,String> ();
    		
    		values.put("Nombre Cine", cineInfo[Cines.nombre]);
    		
 	    	//LocalyticsManager.localyticsSession().tagEvent("Detalle Cine", values);
			FlurryAgent.logEvent("Detalle Cine");
 	    	
    	
    	ViewTreeObserver vto = getView().getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
            	if(getView() == null)
            		return;            	
            	mostrarDetalles(selectedCineId);
                ViewTreeObserver obs = getView().getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);

            }
        });
    	
  }
    
    
    
    
    
    public void mostrarDetalles(int id){    	
    	
         ((TextView)getView().findViewById(R.id.TituloPeliDetalles)).setText(Html.fromHtml("<small>CinesUnidos</small>" +  "<br />" + 
                  "<big>" + cineInfo[Cines.nombre] + "</big>"));
         
         CineInfo info = CinesManager.getInstance().getCineInfo(selectedCineId);
         
         if(info == null){
        	Intent reLaunchMain=new Intent(getActivity(), SplashActivity.class);
      		reLaunchMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
      		startActivity(reLaunchMain);
          	getActivity().finish();
         }
		 else
		 {
			 if(info.getPremium())
				 ((ImageView)getView().findViewById(R.id.premiumImg)).setVisibility(View.VISIBLE);
		 }
         

         
         

     
     inflarFunciones(); 
          
    }
    
        
    private void inflarFunciones(){
    	FrameLayout ll = (FrameLayout) getView().findViewById(R.id.contenedor);
    	
    	ll.removeAllViewsInLayout();		 	

    	View vv = View.inflate(getActivity(), R.layout.funciones_contenedor, null);

    	ll.addView(vv, new FrameLayout.LayoutParams( ll.getLayoutParams().width,
    			ll.getLayoutParams().height));
    	
    	crearBarraFechas(); 
    	
    	dia(0);
    	
    }
       
    
 
    private void agregarShowTimeText(String cine, String sala){
    	
    	LinearLayout ly;
    	TextView tv = new TextView(getActivity());
    	
    	ly = (LinearLayout) getView().findViewById(R.id.showTimesLayout);
    	ly.addView(tv);
        	  	   

    	ViewGroup.MarginLayoutParams mlp2 = (ViewGroup.MarginLayoutParams) tv.getLayoutParams();
    	

    	tv.setText("   " + cine + " - " + sala); 
    	tv.setTypeface(null, Typeface.BOLD);
    	tv.setGravity(Gravity.CENTER_VERTICAL);
    	tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
    	tv.setMinimumHeight(Metrics.dpToPixel(30));
    	tv.setEllipsize(TruncateAt.MARQUEE);
    	tv.setSingleLine();
    	tv.setSelected(true);
    	tv.setBackgroundResource(R.drawable.main_header_selector);
    	
    }
    
    private int agregarShowTimeButton(String horario, LinearLayout ly, String tag, boolean unico, int index){
    	
    	int weigth, width;
    	
    	
    	Button b = new Button(getActivity());
    	
    	b.setText(Html.fromHtml("<big>" + horario + "</big>"));
    	
    	b.setTag(tag);
    	
    	b.setBackgroundResource(R.drawable.custom_button_dos);

    	if(unico){
    		width = getView().getWidth()/botonesHorizontalesMax;
    		weigth = 0;   		
    	}
    	else{
    		width = getView().getWidth()/botonesHorizontalesMax;
    		weigth = 0;  

    	}
    	
    	LayoutParams lp = new LinearLayout.LayoutParams(width, LayoutParams.WRAP_CONTENT, weigth); 
    	
    	b.setLayoutParams(lp);
    	
    	ly.addView(b);
    	
    	int a = b.getWidth();
    	
    	if(index == 0 && CompareTimes(horario) == false){
    		b.setTextColor(Color.parseColor("#cccccc"));    	
    		b.setEnabled(false);
    	}else
    		b.setOnClickListener(OnClickDoSomething(b));
    	
    	return b.getMeasuredHeight();
    	
    }
    
    private boolean CompareTimes(String a){
    	
    	boolean pmLocal, pmCine;
    	int horaCine, horaLocal, minutosCine;
    	String []parse;
    	
    	Time today = new Time(Time.getCurrentTimezone());		
		today.setToNow();
		
		horaLocal = today.hour;		
		
		parse = a.split(":");
		
		horaCine = Integer.parseInt(parse[0]);
		minutosCine = Integer.parseInt(parse[1].substring(0, 2));
		if(parse[1].substring(2, 4).equals("PM"))
			pmCine = true;
		else 
			pmCine = false;
		
		if(horaCine == 12 && pmCine == false)	
			horaCine = 0;
		else if(horaCine < 12 && pmCine == true)
			horaCine += 12;
		
		if(horaLocal <= horaCine){
			
			if(horaLocal == horaCine)
				if(today.minute + 15 < minutosCine)
					return true;
				else
					return false;
			
			return true;
		}		
		
		return false;
	
    }
    
    View.OnClickListener OnClickDoSomething(final Button button)  {
        return new View.OnClickListener() {
            @Override
			public void onClick(View v) {          
            
            	String []values = v.getTag().toString().split("-");
            	//MovieManager.getInstance().setIndexSelection(Integer.parseInt(values[2]));
				MovieManager.getInstance().setIndexSelection(values[2]);
            	MovieManager.getInstance().setShowTimeId(v.getTag().toString());
            	showTimeSelected.showTimeSelected();
               
                 
            }
        };
    }
   
   public interface OnShowTimeSelectedListener{	   
       public void showTimeSelected();
   } 
    
  
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
        	showTimeSelected = (OnShowTimeSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement a Listener");
        }
    }
	
    
private void crearBarraFechas(){
    	
    	TextView textEditDia = null;     	
    	
    	View viewer = getView();    	   	
    	
        
       ViewTreeObserver vto = getView().findViewById(R.id.separador1).getViewTreeObserver();        
        vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
            	
            	View viewer = getView();
            	
            	View separador = getView().findViewById(R.id.separador1);
            	
            	ImageView barra = (ImageView) viewer.findViewById(R.id.selector); 
            	
            	LayoutParams Barraframe = barra.getLayoutParams(); 
            	
            	Barraframe.width =  separador.getLeft() - separador.getWidth();
            	
            	barra.setLayoutParams(Barraframe);    	 
            	
            	ViewTreeObserver obs = getView().getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
            	

            }
        });        
        
        
        ly = (LinearLayout) viewer.findViewById(R.id.showTimesLayout);      
        
        Calendar calendar = Calendar.getInstance();        
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        
        day--;
        //calendar.ad
        for(int i = 0; i < 7; i++){
        	if(day == 7)
        		day = 0;        	
        	
        	switch(i){
        	case 0 :
        		textEditDia = (TextView) viewer.findViewById(R.id.dia0);
        		break;
        	case 1 :
        		textEditDia = (TextView) viewer.findViewById(R.id.dia1);		
        		break;
        	case 2 :
        		textEditDia = (TextView) viewer.findViewById(R.id.dia2);
        		break;
        	case 3 :
        		textEditDia = (TextView) viewer.findViewById(R.id.dia3);
        		break;
        	case 4 :
        		textEditDia = (TextView) viewer.findViewById(R.id.dia4);
        		break;
        	case 5 :
        		textEditDia = (TextView) viewer.findViewById(R.id.dia5);
        		break;
        	case 6 :
        		textEditDia = (TextView) viewer.findViewById(R.id.dia6);
        		break;
        	}
        	if(day == 0 || day == 6)
        		textEditDia.setText(Html.fromHtml("<font size=\"0\" color=\"#FF0000\">" + semana[day]+ "</font> <br> " + String.valueOf(calendar.get(Calendar.DAY_OF_MONTH))));
        	else
        		textEditDia.setText(Html.fromHtml("<font size=\"0\" color=\"#000000\">" + semana[day]+ "</font> <br> " + String.valueOf(calendar.get(Calendar.DAY_OF_MONTH))));
        	
        	textEditDia.setOnClickListener(this);	
//        	textEditDia.setTag(calendar.get)
        	
        	//setText(Html.fromHtml("<font size=\"0\" color=\"#ff0000\">" + semana[day]+ "</font> " + String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
        	day++;
        	calendar.add(Calendar.DAY_OF_MONTH, 1);
        }
    }

	@Override
	public void onClick(View v) {
		
		ImageView barra = null;		
		ViewGroup.MarginLayoutParams lpt = null;
		LayoutParams Barraframe = null;
		View separadorA = null, separadorB = null;
		
		
		if(getView().findViewById(R.id.selector) != null){
			barra = (ImageView) getView().findViewById(R.id.selector);
			lpt =(MarginLayoutParams)barra.getLayoutParams();
			Barraframe = barra.getLayoutParams();
		}
	
	    		
		switch(v.getId()){
		
		case R.id.dia0 :
			
			lpt.setMargins(v.getLeft(),lpt.topMargin,lpt.rightMargin,lpt.bottomMargin);
			barra.setLayoutParams(lpt);
			
			separadorA = getView().findViewById(R.id.separador1);
			
			Barraframe.width = separadorA.getLeft();
			barra.setLayoutParams(Barraframe);
			
			ly.removeAllViewsInLayout();
			ly.invalidate();
			
			dia(0);
			
			break;
			
		case R.id.dia1 :	
			
			separadorA = getView().findViewById(R.id.separador1);
			separadorB = getView().findViewById(R.id.separador2);
	        
			
			lpt.setMargins(v.getLeft(),lpt.topMargin,lpt.rightMargin,lpt.bottomMargin);
			barra.setLayoutParams(lpt);
			
					
			
			Barraframe.width = separadorB.getLeft() - separadorA.getLeft() - separadorA.getWidth();
			barra.setLayoutParams(Barraframe);
	
			
			ly.removeAllViewsInLayout();
			ly.invalidate();
			
			dia(1);
			
			break;
			
		case R.id.dia2 :
			
			separadorA = getView().findViewById(R.id.separador2);
			separadorB = getView().findViewById(R.id.separador3);
			
			lpt.setMargins(v.getLeft(),lpt.topMargin,lpt.rightMargin,lpt.bottomMargin);
			barra.setLayoutParams(lpt);
			
			Barraframe.width = separadorB.getLeft() - separadorA.getLeft() - separadorA.getWidth();
			barra.setLayoutParams(Barraframe);
			
			ly.removeAllViewsInLayout();
			ly.invalidate();
			
			dia(2);
			
			break;
			
		case R.id.dia3 :
			
			separadorA = getView().findViewById(R.id.separador3);
			separadorB = getView().findViewById(R.id.separador4);
			
			lpt.setMargins(v.getLeft(),lpt.topMargin,lpt.rightMargin,lpt.bottomMargin);
			barra.setLayoutParams(lpt);
			
			Barraframe.width = separadorB.getLeft() - separadorA.getLeft() - separadorA.getWidth();
			barra.setLayoutParams(Barraframe);
			
			ly.removeAllViewsInLayout();
			ly.invalidate();
			
			dia(3);
			
			break;
			
		case R.id.dia4 :
			
			separadorA = getView().findViewById(R.id.separador4);
			separadorB = getView().findViewById(R.id.separador5);
		
			lpt.setMargins(v.getLeft(),lpt.topMargin,lpt.rightMargin,lpt.bottomMargin);
			barra.setLayoutParams(lpt);
			
			Barraframe.width = separadorB.getLeft() - separadorA.getLeft() - separadorA.getWidth();
			barra.setLayoutParams(Barraframe);
			
			ly.removeAllViewsInLayout();
			ly.invalidate();
			
			dia(4);
			
			break;
			
		case R.id.dia5 :
			
			separadorA = getView().findViewById(R.id.separador5);
			separadorB = getView().findViewById(R.id.separador6);
			
			lpt.setMargins(v.getLeft(),lpt.topMargin,lpt.rightMargin,lpt.bottomMargin);
			barra.setLayoutParams(lpt);
			
			Barraframe.width = separadorB.getLeft() - separadorA.getLeft() - separadorA.getWidth();
			barra.setLayoutParams(Barraframe);
			
			ly.removeAllViewsInLayout();
			ly.invalidate();
			
			dia(5);
			
			break;
			
		case R.id.dia6 :
			
			separadorA = getView().findViewById(R.id.separador6);
			
		
			lpt.setMargins(v.getLeft(),lpt.topMargin,lpt.rightMargin,lpt.bottomMargin);
			barra.setLayoutParams(lpt);
			
			Barraframe.width = getView().getWidth() - separadorA.getLeft();
			barra.setLayoutParams(Barraframe);
			
			
			
			ly.removeAllViewsInLayout();
			ly.invalidate();
			
			dia(6);
			
			break;
		}
		
	}
	
	private void llenarFuncion(Iterator<PeliculaCine> iterCine, int index){
		
		Theaters dumSala;
		ShowTime dumShowTime;
		Iterator<ShowTime> iterShows;
		String [] cineInfo;
		String salaNombre;
		Movie pelicula;
		PeliculaCine cinedummy;

		int counter;
		List<ShowTime> shows;
		
		if((TextView)getView().findViewById(R.id.nodisponible) == null)
			return;
		
		((TextView)getView().findViewById(R.id.nodisponible)).setVisibility(View.GONE);	
//		((TextView) getView().findViewById(R.id.FechaFunciones)).setText(text);
		
		while(iterCine.hasNext()){
			cinedummy = (PeliculaCine) iterCine.next();
			dumSala = cinedummy.getSalas();
			shows = dumSala.getShowTimes();
			iterShows = shows.iterator();
			String tag = "";
//			Movie dumy = MovieManager.getInstance().getSelectedMovie(0);
			
			cineInfo = Cines.getCineById(String.valueOf(dumSala.getCineId()));
			
						
			ll = new LinearLayout(getActivity());   
			
	        ll.setOrientation(LinearLayout.HORIZONTAL);    
			
	        //salaNombre = SalasManager.getInstance().getSalaNameById(dumSala.getSalaId());
			salaNombre = dumSala.getNombreSala();
	        
	        pelicula = MovieManager.getInstance().getMovieById(cinedummy.getPeliculaId(), index);
	        
	        agregarShowTimeText(pelicula.getSpanishTitle(), salaNombre);
	        
	        ly.addView(ll);
	        
	        counter = 0;	        
			while(iterShows.hasNext()){
				dumShowTime = (ShowTime) iterShows.next();
				
				tag = String.valueOf(dumSala.getCineId()) + "-" + String.valueOf(dumShowTime.getShowTimeId()) + "-" + String.valueOf(cinedummy.getPeliculaId()) 
						+ "-" + String.valueOf(dumSala.getSalaId());
								
				if(counter < botonesHorizontalesMax){			
					
										
					if(iterShows.hasNext() || counter > 0)
			        	agregarShowTimeButton(dumShowTime.getHoraFuncion(), ll, tag, false, index);
					else{
						agregarShowTimeButton(dumShowTime.getHoraFuncion(), ll, tag, true, index);
						 //TODO aqui fondo
				        ImageView fondo = new ImageView(getActivity());
				        fondo.setBackgroundResource(R.drawable.bg);
				        int width = getView().getWidth()/botonesHorizontalesMax;
			    		int weigth = 0;   
				        LayoutParams lp = new LinearLayout.LayoutParams(width, LayoutParams.MATCH_PARENT, weigth); 
				        fondo.setLayoutParams(lp);
				        ll.addView(fondo);
					}				
			      
			        	
			        counter++;
				}else{					
						ll = new LinearLayout(getActivity());   						
				        ll.setOrientation(LinearLayout.HORIZONTAL);  
				        ly.addView(ll);
				        agregarShowTimeButton(dumShowTime.getHoraFuncion(), ll, tag, true, index);
				        counter = 1;
				        //TODO aqui fondo
				        if(!iterShows.hasNext()){
				        	ImageView fondo = new ImageView(getActivity());
				        	fondo.setBackgroundResource(R.drawable.bg);
				        	int width = getView().getWidth()/botonesHorizontalesMax;
				        	int weigth = 0;
				        	LayoutParams lp = new LinearLayout.LayoutParams(width, LayoutParams.MATCH_PARENT, weigth);
				        	fondo.setLayoutParams(lp);
				        	ll.addView(fondo);
				        }
				        
				}	
			
			}
			
		}
		
	}
	
	
	private void dia(final int index){		
		
		ly.removeAllViewsInLayout();
		ly.invalidate();
		
		final ProgressDialog pd;
		
		String date = Fechas.fechaNormal(index);
		
		DetallesCompra.getInstance().setDia(index);
					
		new GetMovieTask().execute(String.valueOf(index));			

	}
	
	
	private class GetDetailsTask extends AsyncTask<String, Void, Integer> {
		 
		ProgressDialog pd;
		 		 
		 @Override
		protected void onPreExecute() {
		    	
			 pd = ProgressDialog.show(getActivity(), "Espere.", "Cargando detalles.", true, true);				
			 
	    	}
	     
		 @Override
		protected Integer doInBackground(String... params) {
			 
//			 MovieManager.getInstance().loadMoviesDetail(peli.getId());		 
	         
	         return 0;
	     }	    

	     @Override
		protected void onPostExecute(Integer result) {
	    	
	    	 
	    	 try {
		    	 pd.cancel();
		         pd = null;
		        } catch (Exception e) { }	    	 
	         
	     }
	 }

	
	 private class GetMovieTask extends AsyncTask<String, Void, Integer> {
		 ProgressDialog pd;
		 int index;
		 String date;
		 
		 @Override
		protected void onPreExecute() {
			
			 pd = ProgressDialog.show(getActivity(), "Espere.", "Consultando funciones disponibles.", true, true);				
			 
	    	}
	     
		 @Override
		protected Integer doInBackground(String... params) {
			 
			 index = Integer.parseInt(params[0]); 		
			 cine = MovieManager.getInstance().getMoviesByCinema(selectedCineId, index);

			 date = Fechas.fechaNormal(index);
	         
	         return 0;
	     }	    

	     @Override
		protected void onPostExecute(Integer result) {
	    	 

 			
	    	if(cine.isEmpty()){
				try {
					((TextView) getView().findViewById(R.id.nodisponible)).setVisibility(View.VISIBLE);
//	    		((TextView)getView().findViewById(R.id.FechaFunciones)).setText("No hay funciones disponibles");
				}catch(Exception e){
					Log.d("Vista no encontrada", "No se encontro la vista de no disponible");
				}
	    	}else
	    		llenarFuncion(cine.iterator(), index);			
 			
 			
	    	try {
		    	 pd.cancel();
		         pd = null;
		        } catch (Exception e) { }
         	
	         
	     }
	 }
	 
	 public void configurationChanged(){
		 
		  ViewTreeObserver vto = getView().getViewTreeObserver();        
	        vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
	            @Override
	            public void onGlobalLayout() {
	            	
	            	
	        			   inflarFunciones();
	        		
	            	
	            	ViewTreeObserver obs = getView().getViewTreeObserver();
	                obs.removeGlobalOnLayoutListener(this);
	            	

	            }
	        });        
		
		 
	 }
	 
	/* public void onPause()
		{
		 	
		    LocalyticsManager.localyticsSession().upload();
		    super.onPause();
		}*/

	@Override
	public void onPause() {
		super.onPause();

		FlurryAgent.onEndSession(getActivity());
	}

	@Override
	public void onStop() {
		super.onStop();

		FlurryAgent.onEndSession(getActivity());
	}



}
