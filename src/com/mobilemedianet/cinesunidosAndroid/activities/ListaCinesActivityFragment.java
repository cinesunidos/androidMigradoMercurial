package com.mobilemedianet.cinesunidosAndroid.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.mobilemedianet.cinesunidosAndroid.R;
import com.mobilemedianet.cinesunidosAndroid.managers.DetallesCompra;
import com.mobilemedianet.cinesunidosAndroid.managers.PrefManager;
import com.mobilemedianet.cinesunidosAndroid.objects.Cines;

import java.util.ArrayList;

//import com.mobilemedianet.cinesunidosAndroid.managers.LocalyticsManager;

public class ListaCinesActivityFragment extends Fragment {
	
	private IconListViewAdapter m_adapter;
	private ArrayList<CineEntry> cinesList = null;
	View selected = null;
	int colortmp;
	private ListView lv = null;
	
	OnListaCinesSelectedListener onListaSelected;
	
	static ListaCinesActivityFragment instance;

	private static final String Flurry_API_Key = "GSW94MXD24S7D546NST6";
	
	static public ListaCinesActivityFragment newInstance(){
		
		if(instance == null)
			instance = new ListaCinesActivityFragment();
		return instance;
	}
	
	
	
	static public void delInstance(){
		instance = null;
	}
	
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
		
        super.onCreate(savedInstanceState);

		//Flurry Start Session
		FlurryAgent.init(getActivity(), Flurry_API_Key);
		FlurryAgent.onStartSession(getActivity());
    }
	
	public void onResume(){
		super.onResume();
		/*if(LocalyticsManager.localyticsSession() == null){
    		LocalyticsManager.newLocalytics(getActivity().getApplicationContext());
        	LocalyticsManager.localyticsSession().open();
    	}*/
		//LocalyticsManager.localyticsSession().tagEvent("Lista de Cines Favoritos");
		FlurryAgent.logEvent("Lista de Cines Favoritos");
		
		DetallesCompra.getInstance().setOrigen("Cines");
		
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	
  	   View viewer = inflater.inflate(R.layout.listview, container, false);    
  	   
  	 	
  	    
  	   return viewer;
    }	
	
	@Override
    public void onStart (){
    	super.onStart();    
    	
    	
    	ViewTreeObserver vto = getView().getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
            	if(getView() == null)
            		return;	    
            	
            	inicializarLista();
                ViewTreeObserver obs = getView().getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);

            }
        });
    	
  }

	
	public static class CineEntry {
        public CineEntry(String id, String nombre) {
        	cineId = Integer.parseInt(id);    
        	cineNombre = nombre;        	
        		
        }
      
        public int getCineId() {
            return cineId;
        }      
        
        public String getCineNombre(){
        	return cineNombre;
        }
        
        private String cineNombre;
        private int cineId;
       
    }

	
	public void inicializarLista(){
		CineEntry entry;
		//Esto es temporal, se supone que leeremos los codigos de los cines de la informacion seleccionada 
		//por el usuario.
		
//		String cines[] = {"1008", "1027", "1001", "1026", "1020", "1009", "1002", "1005"};
		String cines[] = PrefManager.getInstance().getCinemaString().split(",");
		String [] cineInfo;
		
		cinesList = new ArrayList<CineEntry>();
		
		this.m_adapter = new IconListViewAdapter(getActivity(), R.layout.list_item, cinesList);
		
		
		for(int i = 0; i < cines.length; i++){
			
			cineInfo = Cines.getCineById(cines[i]);				
			
			entry = new CineEntry(cines[i], cineInfo[Cines.nombre]);
			//Log.d("MMMM",""+entry.getCineNombre());
			m_adapter.add(entry);

			
		}			
		
		FrameLayout fl = (FrameLayout) getView().findViewById(R.id.ListContainer);
	   	   
	    lv = new ListView(getActivity());
	    fl.addView(lv);          
	               
	    lv.setAdapter(this.m_adapter);
	    lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
				// TODO Auto-generated method stub
				onListaSelected.onListaCinesSelected(m_adapter.getItem(position).getCineId());
				
			}
	    	   
		});
		
		
        
	}
	
	public class IconListViewAdapter extends ArrayAdapter<CineEntry> {

        private ArrayList<CineEntry> items;
        

        public IconListViewAdapter(Context context, int textViewResourceId, ArrayList<CineEntry> items) {
                super(context, textViewResourceId, items);
                this.items = items;
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
                
        	View v = lv.getChildAt(position - lv.getFirstVisiblePosition());//convertView;
                
                if (v == null) {
                    LayoutInflater vi = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    v = vi.inflate(R.layout.list_item, null); 
                    
                    if(position%2 == 0)
                        v.setBackgroundResource(R.drawable.custom_button_dos);        
                        else
                        	v.setBackgroundResource(R.drawable.custom_button_tres);
                        
                      
                        
                        String cinenombre = items.get(position).getCineNombre();
                        
                        if (cinenombre != null) {
                        	
                        	//poblamos la lista de elementos
                        	
                                TextView tt = (TextView) v.findViewById(R.id.text);  
                                
                                if (tt!= null) {
                                	tt.setText(cinenombre);
                                }                        
                                          
                                                	                        
                        }
                        
                    v.setTag(String.valueOf(position));
                    
                    
                                                            
                }        
                
                return v;
        }
        
                 
        
	}
	
		
	
	public interface OnListaCinesSelectedListener {
        public void onListaCinesSelected(int cineId);
    }
	
	 @Override
	    public void onAttach(Activity activity) {
	        super.onAttach(activity);
	        try {
	        	onListaSelected = (OnListaCinesSelectedListener) activity;
	        } catch (ClassCastException e) {
	            throw new ClassCastException(activity.toString()
	                    + " must implement a Listener");
	        }
	    }
	 
	 /*public void onPause()
		{
		 	
		    LocalyticsManager.localyticsSession().upload();
		    super.onPause();
		}*/

	@Override
	public void onPause() {
		super.onPause();

		FlurryAgent.onEndSession(getActivity());
	}

	@Override
	public void onStop() {
		super.onStop();

		FlurryAgent.onEndSession(getActivity());
	}
		

}
