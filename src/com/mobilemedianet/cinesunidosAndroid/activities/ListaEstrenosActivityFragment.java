package com.mobilemedianet.cinesunidosAndroid.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.mobilemedianet.cinesunidosAndroid.MainActivity;
import com.mobilemedianet.cinesunidosAndroid.R;
import com.mobilemedianet.cinesunidosAndroid.managers.ConnectionManager;
import com.mobilemedianet.cinesunidosAndroid.managers.EstrenosManager;
import com.mobilemedianet.cinesunidosAndroid.objects.Movie;
import com.mobilemedianet.cinesunidosAndroid.utilities.Zonas;
import com.mobmedianet.adutil.AdView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

//import com.mobilemedianet.cinesunidosAndroid.managers.LocalyticsManager;

public class ListaEstrenosActivityFragment extends Fragment {

	private String TAG = ListaEstrenosActivityFragment.class.getSimpleName();

	private ArrayList<MovieEntry> pelisentry = null;
	private IconListViewAdapter m_adapter;
	private Zonas zona;
	private String key = "";
	private ListView lv = null;

	private static final String Flurry_API_Key = "GSW94MXD24S7D546NST6";

	View selected = null;
	int colortmp;
	
	OnListaSelectedListener onListaSelected;
	
	
	static ListaEstrenosActivityFragment instance;
	
	
	static public ListaEstrenosActivityFragment getInstance(){
		
		if(instance == null)
			instance = new ListaEstrenosActivityFragment();
		return instance;
	}
	
	static public void delInstance(){
		instance = null;
	}
	
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
		
        super.onCreate(savedInstanceState);

		//Flurry Start Session
		FlurryAgent.init(getActivity(), Flurry_API_Key);
		FlurryAgent.onStartSession(getActivity());
	    	
//        setRetainInstance(true);        
//        MainActivity.isListaVisible = true;

    }
	
	 @Override
	    public void onResume (){
	    	super.onResume();
	    	
	    	/*if(LocalyticsManager.localyticsSession() == null){
	    		LocalyticsManager.newLocalytics(getActivity().getApplicationContext());
	        	LocalyticsManager.localyticsSession().open();
	    	}*/
	    	//LocalyticsManager.localyticsSession().tagEvent("Lista de Estrenos");
		 	//FlurryAgent.logEvent("Lista de Estrenos");

	    	ConnectionManager.setContext(getActivity());
	    	if(ConnectionManager.checkConn() == false){
	    		
	    		
	    		AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
	    		alertDialog.setTitle("CinesUnidos"); 
	    		alertDialog.setMessage("No hay conexión a internet");
	    		alertDialog.setIcon(R.drawable.icono); 
	    		alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
	    			
	    			@Override
	    			public void onClick(DialogInterface dialog,int which) {
	    				dialog.cancel();
	    				((MainActivity) getActivity()).goHome();
	    				return;
	    			}
	  	      }); 
	  	      
	  	    
	  	      alertDialog.show(); 
	  		
	  	      return;
	    	}
	    	
	    	
	    	ViewTreeObserver vto = getView().getViewTreeObserver();
	        vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
	            @Override
	            public void onGlobalLayout() {
	            	if(getView() == null)
	            		return;	    
	            	
	            	new GetEstrenosTask().execute();
	                ViewTreeObserver obs = getView().getViewTreeObserver();
	                obs.removeGlobalOnLayoutListener(this);

	            }
	        });
	    	
	  }
	
		
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	
  	 View viewer = inflater.inflate(R.layout.listview, container, false); 
  	 
  	 return viewer;
    }	

	
	public static class MovieEntry {
        public MovieEntry(Movie a) {
        	peli = a;
       
        		
        }
      
        public Movie getPeli() {
            return peli;
        }       
        private Movie peli;      
        
       
    }

	
	public void inicializarLista(){
		 MovieEntry entry;
         Movie pelicula;
         
                        
                 
         pelisentry = new ArrayList<MovieEntry>();
         List<Movie> pelis = EstrenosManager.getInstance().getMovies();
         Iterator dum = pelis.iterator();
         
         this.m_adapter = new IconListViewAdapter(getActivity(), R.layout.list_item_icon_text, pelisentry);
        
         // Create corresponding array of entries and load their labels.                  
         
//         if(MovieManager.getInstance().isLoaded() == false && MovieManager.getInstance().needToRefresh == false){
//        	 AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//             builder.setTitle("CinesUnidos");
//             builder.setMessage("No tenemos información para mostrar, intente actualizar.");
//             builder.setPositiveButton("OK",null);
//             builder.create();
//             builder.setIcon(R.drawable.icono);
//             builder.show();   
//        	 return;
//         }
        
        
        while(dum.hasNext()){
           	entry = new MovieEntry((Movie) dum.next());
        	m_adapter.add(entry);        	
        }
         
               
        
       FrameLayout fl = (FrameLayout) getView().findViewById(R.id.ListContainer);
   	   
       lv = new ListView(getActivity());
       fl.addView(lv);          
               
       lv.setAdapter(this.m_adapter);
       
       lv.setOnItemClickListener(new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
			// TODO Auto-generated method stub
			Movie mov = (m_adapter.getItem(position)).getPeli();
			
			onListaSelected.onListaSelected(mov.getId(), true);
			
		}
    	   
	});
       

       


	}
	
//	public void refreshList(){
//		MovieEntry entry;
//        Movie pelicula;
//        
//        if(m_adapter == null)
//        	return;
//        
//		m_adapter.clear();		
//		
//		 
//	     MovieManager.getInstance().resetRankingSearch();
//	     
//	     while((pelicula = MovieManager.getInstance().getNextMovieRank(0)) != null){
//	    	 entry = new MovieEntry(pelicula);
//	    	 m_adapter.add(entry); 
//	      	
//	       }   
//	     
//	     m_adapter.notifyDataSetChanged();
//		
//		
//	}
	
	public class IconListViewAdapter extends ArrayAdapter<MovieEntry> {

        private ArrayList<MovieEntry> items;
        

        public IconListViewAdapter(Context context, int textViewResourceId, ArrayList<MovieEntry> items) {
                super(context, textViewResourceId, items);
                this.items = items;
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
                
        	View v = lv.getChildAt(position - lv.getFirstVisiblePosition());//convertView;
        	LinearLayout contenedor;    
                if (v == null) {
                	
                    LayoutInflater vi = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    v = vi.inflate(R.layout.list_item_icon_text_estrenos, null);  
                    
                    }
                
                contenedor = (LinearLayout) v.findViewById(R.id.contenedor);
                
                if(position%2 == 0){                	
                	contenedor.setBackgroundResource(R.drawable.custom_button_dos);
                }
                else{
                	contenedor.setBackgroundResource(R.drawable.custom_button_tres);
                }
                   
                    
                    Movie pelicula = items.get(position).getPeli();
                    
                    if (pelicula != null) {
                    	
                    	//poblamos la lista de elementos
                    	
                    		ViewHolder holder = null;                    		
                    		TextView tesp = null;
                    		TextView torg = null;                    		
                    		TextView td = null;
                    		TextView tm = null;
                    		TextView ty = null;
                            ImageView im = null;
                            ImageView pre = null;
                    		
                    		if(v.getTag() == null){
                    			holder = new ViewHolder();
                    			holder.icon = (ImageView) v.findViewById(R.id.icon);
                    			holder.tituloesp = (TextView) v.findViewById(R.id.titulo_espanol);
                    			holder.tituloorg = (TextView) v.findViewById(R.id.titulo_original);
                    			holder.day = (TextView) v.findViewById(R.id.day);
                    			holder.month = (TextView) v.findViewById(R.id.month);
                    			holder.year = (TextView) v.findViewById(R.id.year);
                    			holder.pre = (ImageView) v.findViewById(R.id.preventa);
                    			
                    			tesp = holder.tituloesp;
                    			torg = holder.tituloorg;
                    			td = holder.day;
                    			tm = holder.month;
                    			ty = holder.year;
                    			im = holder.icon;
                    			pre = holder.pre;
                    			v.setTag(holder);
                    		}else{
                    			holder = (ViewHolder) v.getTag();
                    			tesp = holder.tituloesp;
                    			torg = holder.tituloorg;
                    			td = holder.day;
                    			tm = holder.month;
                    			ty = holder.year;
                    			im = holder.icon;
                    			pre = holder.pre;
                    		}
                    	
                            
                                                                           
                            if(tesp != null){
                            	tesp.setText(pelicula.getSpanishTitle());
                            }
                            
                            if(torg != null){
                            	torg.setText(pelicula.getOriginalTitle());
                            }
                                                       
                            if(td != null){
                            	td.setText(pelicula.getFirstExhibitDay());
                            }
                            
                            if(tm != null){
                            	tm.setText(pelicula.getFirstExhibitMonth());
                            }
                            
                            if(ty != null){
                            	ty.setText(pelicula.getFirstExhibitYear());
                            }
                                                        
                            
                            if (im!= null) {                        	
                            	if(pelicula.posterlsLoaded() == true){                        		
                            		im.setImageDrawable(pelicula.getPoster());
                            	}else{
                            		new DownloadImageTask().execute(String.valueOf(position));
                            		
                            	}                 	
                            	                     	
                            }        
                            
                            if(pre != null && pelicula.getPreSale().equals("P"))
                            	pre.setVisibility(View.VISIBLE);
                            
                            if(position == 2){
//                            	PrefManager pref = new PrefManager();
//                                pref.setAplicationContext(getActivity());
//                                String aux = pref.getCityWithMostSelections();
//                                Log.d("CIUDAD", "" + aux);
//                                
//                                String key = "";
//                                if (aux.equals("CCSE,CCSO")) {
//                                	key = "ALL_CCS";
//                                } else {
//                                	key = "ALL_" + aux;
//                                }
//                                Zonas zona = new Zonas();
//                                zona.createZones();
                            	
//                                AdView ad = (AdView) v.findViewById(R.id.adViewAll); 
//                                ad.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, Metrics.dpToPixel(60)));
//                                ad.setZoneId(zona.getZona(key));
//                                ad.setVisibility(View.VISIBLE);

                                	
                               
                            	
//                            	((ImageView) v.findViewById(R.id.adds)).setImageResource(R.drawable.add360);//               	   	
//                            	((View) v.findViewById(R.id.separador)).setVisibility(v.VISIBLE);
                            }else{
                            	AdView ad = (AdView) v.findViewById(R.id.adViewAll);
                            	ad.setVisibility(View.INVISIBLE);
                            	
//                            	((ImageView) v.findViewById(R.id.adds)).setImageResource(0);                	
//                            	((View) v.findViewById(R.id.separador)).setVisibility(v.INVISIBLE);
                            }
                                                                
                    }
//                    v.setTag(String.valueOf(position));
                               
                                
                return v;
        }
        
        private class ViewHolder {
        	  TextView day;
        	  TextView month;
        	  TextView year;
        	  TextView tituloesp;
        	  TextView tituloorg;
        	  ImageView icon;    
        	  ImageView pre;
        	}
                 
        
	}
	
	
	
	
	 private class GetEstrenosTask extends AsyncTask<String, Void, Integer> {
		 ProgressDialog pd;
		
		 @Override
		protected void onPreExecute() {
			 
			 			 
			 pd = ProgressDialog.show(getActivity(), "Espere.", "Consultando funciones disponibles.", true, false);				
			 
	    	}
	     
		 @Override
		protected Integer doInBackground(String... params) {



			 if(EstrenosManager.getInstance().isLoaded() == false){
				 
				 if(EstrenosManager.getInstance().loadMovies() == false)
					 return -1;
			 }
			 
			 			 
			 return 0;
			 
			
	     }	    

	     @Override
		protected void onPostExecute(Integer result) {   
	    	 
	    	 try {
 		    	 pd.cancel();
 		         pd = null;
 		        } catch (Exception e) { }
	    	 
 		     if(result == -1){
 		    	 ConnectionManager.getInstance().mensajeError("No hemos podido acceder a la información sobre estrenos");
 		    	 return;
 		     }
	    	 inicializarLista();   	 
	    	
	         
	     }
	 }
	
	
	
	
	private class DownloadImageTask extends AsyncTask<String, Void, Drawable> {
    	int index;
    	
    	
    	@Override
		protected Drawable doInBackground(String... position) {
        	index = Integer.parseInt(position[0]);        	
        	
        	(m_adapter.getItem(index)).getPeli().setPoster();
        	            	
        	return (m_adapter.getItem(index)).getPeli().getPoster();
        	
        }

        @Override
		protected void onPostExecute(Drawable poster) {  
        	View itemView;        		
        	
        	
        	if(lv != null)
        		itemView = lv.getChildAt(index - lv.getFirstVisiblePosition());
        	else
        		return;
            
        	if (itemView != null) {
                ImageView itemImageView = (ImageView) itemView.findViewById(R.id.icon);
                itemImageView.setImageDrawable(poster);
            }          	
        	

        }
    }
	
	
    
	/*public void onPause()
	{
		
	    LocalyticsManager.localyticsSession().upload();
	    super.onPause();
	}*/

	@Override
	public void onPause() {
		super.onPause();

		FlurryAgent.onEndSession(getActivity());
	}

	@Override
	public void onStop() {
		super.onStop();

		FlurryAgent.onEndSession(getActivity());
	}
	
	public interface OnListaSelectedListener {
        public void onListaSelected(String movieId, boolean premier);
    }
	
	 @Override
	    public void onAttach(Activity activity) {
	        super.onAttach(activity);
	        try {
	        	onListaSelected = (OnListaSelectedListener) activity;
	        } catch (ClassCastException e) {
	            throw new ClassCastException(activity.toString()
	                    + " must implement a Listener");
	        }
	    }
	
	
	 



}
