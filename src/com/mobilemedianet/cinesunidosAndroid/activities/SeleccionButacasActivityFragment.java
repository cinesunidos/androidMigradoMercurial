package com.mobilemedianet.cinesunidosAndroid.activities;



import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.mobilemedianet.cinesunidosAndroid.MainActivity;
import com.mobilemedianet.cinesunidosAndroid.R;
import com.mobilemedianet.cinesunidosAndroid.managers.ButacasManager;
import com.mobilemedianet.cinesunidosAndroid.managers.ConnectionManager;
//import com.mobilemedianet.cinesunidosAndroid.managers.LocalyticsManager;
import com.mobilemedianet.cinesunidosAndroid.managers.NewSeats;
import com.mobilemedianet.cinesunidosAndroid.objects.Butaca;
import com.mobilemedianet.cinesunidosAndroid.utilities.Contador;
import com.mobilemedianet.cinesunidosAndroid.utilities.Metrics;



public class SeleccionButacasActivityFragment extends Fragment{

	OnSeleccionButacasListener irAPagar2;	
	TableLayout container;
	static private SeleccionButacasActivityFragment instance;
	
	ArrayList <Butaca> matriz;
	private int width;	
	private int mRows;
	private int mCols;
	private  MyCount contador = null;
	ButacasManager butman;

	private static final String Flurry_API_Key = "GSW94MXD24S7D546NST6";

	static public SeleccionButacasActivityFragment getInstance(){
		
		if(instance == null)
			instance = new SeleccionButacasActivityFragment();
		return instance;
	}
	
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
         View viewer = inflater.inflate(R.layout.seleccion_butacas_fragment, container, false);
         
         ViewTreeObserver vto = viewer.getViewTreeObserver();
         vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
             @Override
             public void onGlobalLayout() { 
            	 if(getView() == null)
              		return;
             	
             	iniciar();
                 ViewTreeObserver obs = getView().getViewTreeObserver();
                 obs.removeGlobalOnLayoutListener(this);

             }
         });
         
        return viewer;
    }   
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
	    super.onCreate(savedInstanceState);

        //Flurry Start Session
        FlurryAgent.init(getActivity(), Flurry_API_Key);
        FlurryAgent.onStartSession(getActivity());

	    butman = ButacasManager.getInstance();
	    
	}
	
	public void onResume(){
		super.onResume();
		
		/*if(LocalyticsManager.localyticsSession() == null){
    		LocalyticsManager.newLocalytics(getActivity().getApplicationContext());
        	LocalyticsManager.localyticsSession().open();
    	}*/
		//LocalyticsManager.localyticsSession().tagEvent("Butacas Selección");
		FlurryAgent.logEvent("Butacas Selección");
	}
	
		
	public void iniciar(){
		
		ConnectionManager.setContext(getActivity());
    	if(ConnectionManager.checkConn() == false){
    		
    		
    		AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
    		alertDialog.setTitle("CinesUnidos"); 
    		alertDialog.setMessage("No hay conexión a internet");
    		alertDialog.setIcon(R.drawable.icono); 
    		alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
    			
    			@Override
    			public void onClick(DialogInterface dialog,int which) {
    				dialog.cancel();
    				((MainActivity) getActivity()).goHome();
    				return;
    			}
  	      }); 
  	      
  	    
  	      alertDialog.show(); 
  		
  	      return;
    	}
				
		getView().findViewById(R.id.button1).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				cancelTimer();
				irAPagar2.irAPagar2(true);
				
			}
		});

		new LoadSeatsTask().execute();
	    
	    getView().findViewById(R.id.tablelayout).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				cancelTimer();
				irAPagar2.irAPagar2(false);
				
				
			}
		});
	    
	    contador = new MyCount(Contador.getInstance().getTimer(), 500);
	    contador.start();	    
	  
	}
	
	public void cancelTimer(){
		if(contador == null)
			return;
		
		contador.cancel();
		contador = null;
		
	}
	
	public class MyCount extends CountDownTimer{
		
		public MyCount(long millisInFuture, long countDownInterval) {			
			super(millisInFuture, countDownInterval);
			
		}
		
		@Override
		public void onTick(long millisUntilFinished) {
			int seconds = (int) (millisUntilFinished / 1000);
			int minutes = seconds / 60;
			seconds     = seconds % 60;
			TextView text;
			try {
			 text = (TextView) getView().findViewById(R.id.Timer);
			} catch (Exception e){
			    return;
			  }
			
						
			text.setText(String.format("Selección de butacas - Tiempo Restante: %d:%02d", minutes, seconds));
			Contador.getInstance().setTimer(millisUntilFinished);
			
		}
		
		@Override
		public void onFinish() {
			TextView text;
			try {
				 text = (TextView) getView().findViewById(R.id.Timer);
				} catch (Exception e){
				    return;
				  }			
			//LocalyticsManager.localyticsSession().tagEvent("Tiempo de Compra Agotado");
			FlurryAgent.logEvent("Tiempo de Compra Agotado");
			text.setText("Selección de butacas - Tiempo Agotado.");
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());			        
	        alertDialog.setTitle("CinesUnidos"); 			        
	        alertDialog.setMessage("Se ha agotado el tiempo de reserva, vuelva a empezar."); 			       
	        alertDialog.setIcon(R.drawable.icono);
	        alertDialog.setCancelable(false);
	        alertDialog.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
	            @Override
				public void onClick(DialogInterface dialog,    int which) {
	            	dialog.cancel();
	            	getActivity().getSupportFragmentManager().popBackStack("comprar", 0);
	            }
	        });
	        alertDialog.show();	        
	        
	        
		}
		}
	
	//TODO actualizarAsientosSeleccionados
	private void actualizarAsientosSeleccionados(){
		String seleccion = "Butacas:";
		
		ajustarColumnasButacas();
		
		Iterator<Butaca> butacas = ButacasManager.getInstance().getNewVectorSelectionIterator();
		
		if(butacas == null)
			return;
		
		
		
		Butaca dum;
		
		while(butacas.hasNext()){
			dum = butacas.next();
			seleccion += " " + dum.getNombreButaca();
		}
		
		((TextView)getView().findViewById(R.id.textView3)).setText(seleccion);
		
		
	}
	
	private void ajustarColumnasButacas(){
		
		Vector<Butaca> butacasnuevas = new Vector<Butaca>();		
		
		Iterator<Butaca> butacas = ButacasManager.getInstance().getReserverdSeatsIterator();	
		
		if(butacas == null)
			return;
		
		ArrayList<Integer> pasillosCol = butman.getPasillosCol();
		

		Butaca butobj, butobjadd;
		int count = 0;
		
		while(butacas.hasNext()){
			butobj = butacas.next();
			
			count = 0;
				
			for(int i = 0; i < pasillosCol.size(); i++){

				if(butobj.getX() + 1 > pasillosCol.get(i))
					count++;
			}
			
			butobjadd = new Butaca(butobj.getX() - count, butobj.getY(), butobj.getLetraButaca(), String.valueOf(butobj.getX() - count + 1));
			
			butobjadd.setFilaButaca(butobj.getFilaButaca());
			
			butobjadd.setType(butobj.getType());
			
			butacasnuevas.add(butobjadd);

			
		}		
		
		ButacasManager.getInstance().setNewVectorSelecction(butacasnuevas);
		
		
	}
	
	public interface OnSeleccionButacasListener{		
	       public void irAPagar2(boolean donde);
	   }
	
	@Override
    public void onAttach(Activity activity) {
     super.onAttach(activity);
     try {
    	 irAPagar2 = (OnSeleccionButacasListener) activity;
     } catch (ClassCastException e) {
         throw new ClassCastException(activity.toString()
                 + " must implement a Listener");
     }
 }
	
	private class LoadSeatsTask extends AsyncTask<String, Void, Boolean> {		
				
		ProgressDialog pd;
		 
		 
		 @Override
		protected void onPreExecute() {		 
			
			 if(getView() == null)
				 return;
			 

			 pd = ProgressDialog.show(getActivity(), "Espere.", "Consultando Butacas.", true, false);				
			 
			 
	    	}
	     
		 @Override
		protected Boolean doInBackground(String... params) {	 
			
			if(ButacasManager.getInstance().getBackFromMap() == false){
				if(ButacasManager.getInstance().LoadSeats() == false){
					return false;
				}
					
			}else{
				ButacasManager.getInstance().checkForNewSeats();				
			}
			
			if(ButacasManager.getInstance().getSalaMapa() != null)
				return true;
			else 
				return false;			
			
	     }	    

	     @Override
		protected void onPostExecute(Boolean result) {	  
	    	 
	    	 if(getView() == null)
	    		 return;
	    	 
	    	 if(result == false){
	    		 try {
	    	    	 pd.cancel();
	    	         pd = null;
	    	        } catch (Exception e) { }	    		 
	    		 
	    		AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());			        
	 	        alertDialog.setTitle("CinesUnidos"); 			        
	 	        alertDialog.setMessage("Ocurrio un error al consultar la sala, es posible que se encuentre llena."); 			       
	 	        alertDialog.setIcon(R.drawable.icono);
	 	        alertDialog.setCancelable(false);
	 	        alertDialog.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
	 	            @Override
					public void onClick(DialogInterface dialog,    int which) {
	 	            	cancelTimer();
	 	            	dialog.cancel();
	 	            	getActivity().getSupportFragmentManager().popBackStack();
	 	            }
	 	        });
	 	        alertDialog.show();	
	    	 }
	    	 else{
	    		 actualizarAsientosSeleccionados();
	    		 generar();	 
	    		 try {
	    	    	 pd.cancel();
	    	         pd = null;
	    	        } catch (Exception e) { }
	    	 }
	    	 
	    	 if(NewSeats.getInstance().getSeatChanged() == false && ButacasManager.getInstance().getBackFromMap() == true){
				 AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	             builder.setTitle("CinesUnidos");
	             builder.setMessage("Selección rechazada, otra persona reservó antes.");
	             builder.setPositiveButton("OK",null);
	             builder.create();
	             builder.setIcon(R.drawable.icono);
	             builder.show();   
			}
	    	 
	    	 ButacasManager.getInstance().setBackFromMap(false);	    		
	    	 return;
	     }	
	}
	
	
	 private boolean generar(){	 
	    	
	    	
	    		    	
	    	container = (TableLayout) getView().findViewById(R.id.tablelayout);
	    	
	    	
	    	try {
				generarButacas(container);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true; 	
	    		    	
	    }
	 
	 private void generarButacas(TableLayout ly) throws IOException{		 	
		 	boolean espasillo = false;
		 	int county = 0;
		 	int countx = 0;		 	
		 	
		 	ArrayList<Integer> pasillosFil = butman.getPasillosFil();
		 	ArrayList<Integer> pasillosCol = butman.getPasillosCol();
		 	
		 	int ycount = pasillosFil.size();
		 			 			 	
		 	if(ycount != 0)
		 		mRows = butman.getFil() + ycount + 2;
		 	else
		 		mRows = butman.getFil() + 2;
		 	
	    	mCols = butman.getCol() + 2;
	    	
	    	width = (getView().getWidth()-Metrics.dpToPixel(6))/mCols;
	    	
	    	Butaca butobj;	    	
	    		    	
	    	TableLayout table = ly;
	        for (int y = 0; y < mRows; y++) {

	            TableRow tr = new TableRow(getActivity());
	            tr.setLayoutParams(new TableRow.LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT));

	            for (int x = 0; x < mCols; x++) {

	                ImageView view = new ImageView(getActivity());                
	  
	                if(y == 0 || y == mRows - 1){
	                	if(x == 0 || x == mCols - 1 ){
	                		view.setImageDrawable(Drawable.createFromStream(getActivity().getAssets().open("butacas/asientopasillotouch.png"), null));
	                	if(y == mRows - 1 && x == 0)
		                	countx = 0;
	                	}else{	                		
	                		/**********/
	                		
	                		for(int i = 0; i < pasillosCol.size(); i++){
	                			
	                			if(pasillosCol.get(i) == x - 1)
	                				espasillo = true;
	                			
	                		}
	                		
	                		if(espasillo == true){
	                			
	                			espasillo = false;
	                	        
	                			countx++;
	                		                	
	                			view.setImageDrawable(Drawable.createFromStream(getActivity().getAssets().open("butacas/asientopasillotouch.png"), null));
	                			
	                		}else{
	                			view.setImageDrawable(getSquareByNumer(x - countx));       			
	                		}
	                		
	                		
	                		
	                		/**********/
	                		
	                		//view.setImageDrawable(getSquareByNumer(x));
	                	}
	                }else if(x == 0 || x == mCols - 1){
	                	if(y == 0)
	                		view.setImageDrawable(Drawable.createFromStream(getActivity().getAssets().open("butacas/asientopasillotouch.png"), null));
	                	else{
	                		
	                		for(int i = 0; i < pasillosFil.size(); i++){
	                			
	                			if(pasillosFil.get(i) == y - 1)
	                				espasillo = true;
	                			
	                		}
	                		
	                		if(espasillo == true){
	                			
	                			espasillo = false;
	                			
	                			if(x == 0)
	                				county++;
	                			
	                			view.setImageDrawable(Drawable.createFromStream(getActivity().getAssets().open("butacas/asientopasillotouch.png"), null));
	                			
	                		}else{
	                			view.setImageDrawable(getSquareLetter(y - county + ycount));	                			
	                		}
	                		
	                	}
	                	
	                }else{
	                	
	                	butobj = butman.getAt(x-1, y-1);
	                	
	                	 if(butobj != null){
	                		 switch(butobj.getType()){
	                		 case Butaca.DISPONIBLE :
	                			 view.setImageDrawable(Drawable.createFromStream(getActivity().getAssets().open("butacas/asientodisponibletouch.png"), null));
	                			break;
	                			case Butaca.OCUPADA :
	                				view.setImageDrawable(Drawable.createFromStream(getActivity().getAssets().open("butacas/asientoocupadotouch.png"), null));
	                			break;
		 	                	case Butaca.SELECCIONADA :
		 	                		view.setImageDrawable(Drawable.createFromStream(getActivity().getAssets().open("butacas/asientoseleccionadotouch.png"), null));
		 	                	break;	                		
		                		default :
		                		view.setImageDrawable(Drawable.createFromStream(getActivity().getAssets().open("butacas/asientopasillotouch.png"), null));
		                		break;
		                		} 	 
	                		 view.setTag(butobj.getNombreButaca());	                		 
	 	                }else{
	 	                	view.setImageResource(R.drawable.asientopasillotouch);	 	                	
	                	}
	                }
	                view.setId(mCols*y + x);
	                view.setScaleType(ImageView.ScaleType.CENTER_CROP);
	                view.setLayoutParams(new LayoutParams(width, width));	                
	                tr.addView(view);
	                
	            }
	            table.addView(tr);
	        }
	        
	        TableRow tr = new TableRow(getActivity());
	        tr.setLayoutParams(new TableRow.LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
	        
	        int inicio = (mCols - 8)/2;

	        for(int x = 0; x < mCols; x++){
	        	ImageView view = new ImageView(getActivity()); 
	        	if(x >= inicio && x < inicio + 8){
	        		switch(x - inicio){
	        		case 0 :
	        			view.setImageDrawable(getSquareLetterInOrder(15));//P
	        			break;
	        		case 1 :
	        			view.setImageDrawable(getSquareLetterInOrder(0));//A
	        			break;
	        		case 2 :
	        			view.setImageDrawable(getSquareLetterInOrder(13));//N
	        			break;
	        		case 3 :
	        			view.setImageDrawable(getSquareLetterInOrder(19));//T
	        			break;
	        		case 4 :
	        			view.setImageDrawable(getSquareLetterInOrder(0));//A
	        			break;
	        		case 5 :
	        			view.setImageDrawable(getSquareLetterInOrder(11));//L
	        			break;
	        		case 6 :
	        			view.setImageDrawable(getSquareLetterInOrder(11));//L
	        			break;
	        		case 7 :
	        			view.setImageDrawable(getSquareLetterInOrder(0));//A
	        			break;        			
	        		}
	        	}else	        	
	        	view.setImageDrawable(Drawable.createFromStream(getActivity().getAssets().open("butacas/asientopasillotouch.png"), null));				
	        	view.setId(mCols*(mRows)  + x);
	        	view.setScaleType(ImageView.ScaleType.CENTER_CROP);
	            view.setLayoutParams(new LayoutParams(width, width));	           
	            tr.addView(view);
	        	
	        }
	        table.addView(tr);

	        table.invalidate();  
	        mRows++;
	       
	    	
	    } 
	 
	 
	 
	 private Drawable getSquareByNumer(int number){
		 String file = "cuadro" + String.valueOf(number) + ".png";
		 Drawable image = null;
		 
		 try {
			image = Drawable.createFromStream(getActivity().getAssets().open("cuadros/" + file), null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		 
		   return image;
		   
     }
	   
	   private Drawable getSquareLetter(int number){
		   int i = (mRows - 2) - number;
		   char a = (char) ('A' + i);
		   
		   String file = "cuadro" + String.valueOf(a) + ".png";
		   Drawable image = null;
			 
			 try {
				image = Drawable.createFromStream(getActivity().getAssets().open("cuadros/" + file), null);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
			 
			   return image;
			   
		   }
	   
	   
	   private Drawable getSquareLetterInOrder(int number){		   
		   char a = (char) ('A' + number);
		   
		   String file = "cuadro" + String.valueOf(a) + ".png";
		   Drawable image = null;
			 
			 try {
				image = Drawable.createFromStream(getActivity().getAssets().open("cuadros/" + file), null);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
			 
			   return image;
			   
		   }
	   
	   
	  
	   public void actualizar(){
		   
		   ViewTreeObserver vto = getView().getViewTreeObserver();
	         vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
	             @Override
	             public void onGlobalLayout() { 
	            	 if(getView() == null)
	              		return;	     
	            	 unbindDrawables(getView().findViewById(R.id.tablelayout));
	            	 generar();	            	
	                 ViewTreeObserver obs = getView().getViewTreeObserver();
	                 obs.removeGlobalOnLayoutListener(this);

	             }
	         });		   
	   }
	   
	   private void unbindDrawables(View view) {
			if(view == null)
				return;
		    if (view.getBackground() != null) {
		        view.getBackground().setCallback(null);
		    }
		    if (view instanceof ViewGroup) {
		        for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
		            unbindDrawables(((ViewGroup) view).getChildAt(i));
		        }
		        try{
		        	((ViewGroup) view).removeAllViews();
		        }catch(UnsupportedOperationException e){
		        	
		        }
		    }
		}
	   
	   /*public void onPause()
		{
		   	
		    LocalyticsManager.localyticsSession().upload();
		    super.onPause();
		}*/

    @Override
    public void onPause() {
        super.onPause();

        FlurryAgent.onEndSession(getActivity());
    }

    @Override
    public void onStop() {
        super.onStop();

        FlurryAgent.onEndSession(getActivity());
    }
	
}
	


