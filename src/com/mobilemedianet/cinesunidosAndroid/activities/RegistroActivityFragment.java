package com.mobilemedianet.cinesunidosAndroid.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.mobilemedianet.cinesunidosAndroid.R;
import com.mobilemedianet.cinesunidosAndroid.managers.ConnectionManager;
import com.mobilemedianet.cinesunidosAndroid.managers.RegistroManager;
import com.mobilemedianet.cinesunidosAndroid.utilities.Fechas;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

//import com.mobilemedianet.cinesunidosAndroid.managers.LocalyticsManager;

public class RegistroActivityFragment extends Fragment implements OnClickListener, OnItemSelectedListener{
	
	String[] estados = {"Amazonas", "Anzoátegui", "Apure", "Aragua", "Area Metropolitana", "Barinas", "Bolívar", "Carabobo", "Cojedes", "Delta Amacuro", "Falcon", "Guárico", "Lara", "Mérida", "Miranda", "Monagas", "Nueva Esparta", "Portuguesa", "Sucre", "Táchira", "Trujillo", "Vargas", "Yaracuy", "Zulia"};
	String[] ciudades = {"Barquisimeto", "Caracas", "Coro", "Guatire", "Maracaibo", "Maracay", "Margarita", "Maturín", "Puerto La Cruz", "Puerto Ordaz", "San Cristobal", "Valencia"};
	String[] codigoCiudades = {"910", "954", "914", "932", "900", "928", "2035", "942", "938", "14263", "17216", "920"};
	String[] sexos = {"Masculino", "Femenino"};
	String[] nacionalidades = {"V", "E", "P"};
	
static public RegistroActivityFragment instance;

	private static final String Flurry_API_Key = "GSW94MXD24S7D546NST6";

	static public RegistroActivityFragment getInstance(){
		
		if(instance == null)
			instance = new RegistroActivityFragment();
		return instance;
	}
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
	    super.onCreate(savedInstanceState);
		//Flurry Start Session
		FlurryAgent.init(getActivity(), Flurry_API_Key);
		FlurryAgent.onStartSession(getActivity());

	}
	
	public void onResume(){
		super.onResume();
		
		/*if(LocalyticsManager.localyticsSession() == null){
    		LocalyticsManager.newLocalytics(getActivity().getApplicationContext());
        	LocalyticsManager.localyticsSession().open();
    	}*/
		//LocalyticsManager.localyticsSession().tagEvent("Registro de Usuario");
		FlurryAgent.logEvent("Registro de Usuario");
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		
       View viewer = inflater.inflate(R.layout.registro, container, false);       
               
       return viewer;
    }   
	
	@Override
    public void onStart (){
    	super.onStart();    	    	
    	
    	ViewTreeObserver vto = getView().getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {
				if (getView() == null)
					return;
				iniciar();
				ViewTreeObserver obs = getView().getViewTreeObserver();
				obs.removeGlobalOnLayoutListener(this);

			}
		});
    	
  }
	
	public void iniciar(){
		getView().findViewById(R.id.fechaEdit).setOnClickListener(this);
		getView().findViewById(R.id.Enviar).setOnClickListener(this);
		getView().findViewById(R.id.editText7).setOnClickListener(this);
		addItemsOnSpinner1();
		addItemsOnSpinner2();
		addItemsOnSpinner3();
		RegistroManager.getInstance().setTipoCedula("V");  
		
		
	}
	
	
	
	private boolean checkParams(){
		
		if(( (EditText) getView().findViewById(R.id.editText1) ).getText().toString().equals("")){
			 Toast.makeText(getActivity(), "Debe introducir un e-mail", Toast.LENGTH_SHORT).show();
				return false;					
		 }else if(( (EditText) getView().findViewById(R.id.editText2) ).getText().toString().equals("")){
			 Toast.makeText(getActivity(), "Debe introducir una contraseña", Toast.LENGTH_SHORT).show();
				return false;					
		 }else if(( (EditText) getView().findViewById(R.id.editText3) ).getText().toString().equals("")){
			 Toast.makeText(getActivity(), "Debe introducir la contraseña de confirmación", Toast.LENGTH_SHORT).show();
				return false;					
		 }else if(( (EditText) getView().findViewById(R.id.editText4) ).getText().toString().equals("")){
			 Toast.makeText(getActivity(), "Debe introducir su pregunta secreta", Toast.LENGTH_SHORT).show();
				return false;					
		 }else if(( (EditText) getView().findViewById(R.id.editText5) ).getText().toString().equals("")){
			 Toast.makeText(getActivity(), "Debe introducir la respuesta a su pregunta secreta", Toast.LENGTH_SHORT).show();
				return false;					
		 }else if(( (EditText) getView().findViewById(R.id.editText6) ).getText().toString().equals("")){
			 Toast.makeText(getActivity(), "Debe introducir su nombre", Toast.LENGTH_SHORT).show();
				return false;					
		 }else if(( (EditText) getView().findViewById(R.id.editText9) ).getText().toString().equals("")){
			 Toast.makeText(getActivity(), "Debe introducir su apellido", Toast.LENGTH_SHORT).show();
				return false;					
		 }else if(( (EditText) getView().findViewById(R.id.editText10) ).getText().toString().equals("")){
			 Toast.makeText(getActivity(), "Debe introducir cédula o pasaporte", Toast.LENGTH_SHORT).show();
				return false;					
		 }else if(( (EditText) getView().findViewById(R.id.fechaEdit) ).getText().toString().equals("")){
			 Toast.makeText(getActivity(), "Debe introducir su fecha de nacimiento", Toast.LENGTH_SHORT).show();
				return false;					
		 }else if(( (EditText) getView().findViewById(R.id.editText12) ).getText().toString().equals("")){
			 Toast.makeText(getActivity(), "Debe introducir un número celular", Toast.LENGTH_SHORT).show();
				return false;					
		 }else if(( (EditText) getView().findViewById(R.id.editText16) ).getText().toString().equals("")){
			 Toast.makeText(getActivity(), "Debe introducir una dirección", Toast.LENGTH_SHORT).show();
				return false;					
		 }else if(( (EditText) getView().findViewById(R.id.editText17) ).getText().toString().equals("")){
			 Toast.makeText(getActivity(), "Debe introducir un número de teléfono", Toast.LENGTH_SHORT).show();
				return false;					
		 }else if(( (EditText) getView().findViewById(R.id.editText1) ).getText().toString().indexOf("@") == -1){
			 Toast.makeText(getActivity(), "Dirección de correo invalida", Toast.LENGTH_SHORT).show();
				return false;
		 }
		
		String pass1 = ((EditText) getView().findViewById(R.id.editText2) ).getText().toString();
		String pass2 = ((EditText) getView().findViewById(R.id.editText3) ).getText().toString();
		
		if(!pass1.equals(pass2)){
			Toast.makeText(getActivity(), "Las contraseñas no coinciden.", Toast.LENGTH_SHORT).show();
			return false;
			
		}
		
	
		
		return true;
		
	}
	
	private void getParams(){
		EditText parametro;
		
		//Correo
		parametro = (EditText) getView().findViewById(R.id.editText1);
		RegistroManager.getInstance().setCorreo(parametro.getText().toString());
		//Contraseña
		parametro = (EditText) getView().findViewById(R.id.editText2);
		RegistroManager.getInstance().setContrasena(parametro.getText().toString());
		//Pregunta Secreta
		parametro = (EditText) getView().findViewById(R.id.editText4);
		RegistroManager.getInstance().setPreguntaSecreta(parametro.getText().toString());
		//Restpuesta
		parametro = (EditText) getView().findViewById(R.id.editText5);
		RegistroManager.getInstance().setRespuestaPregunta(parametro.getText().toString());
		//Nombre
		parametro = (EditText) getView().findViewById(R.id.editText6);
		RegistroManager.getInstance().setNombre(parametro.getText().toString());
		//Apellido
		parametro = (EditText) getView().findViewById(R.id.editText9);
		RegistroManager.getInstance().setApellido(parametro.getText().toString());
		//Cedula
		parametro = (EditText) getView().findViewById(R.id.editText10);
		RegistroManager.getInstance().setCedula(parametro.getText().toString());
		//Celular
		parametro = (EditText) getView().findViewById(R.id.editText12);
		RegistroManager.getInstance().setCelular(parametro.getText().toString());
		//Direccion
		parametro = (EditText) getView().findViewById(R.id.editText16);
		RegistroManager.getInstance().setDireccion(parametro.getText().toString());
		//Telefono
		parametro = (EditText) getView().findViewById(R.id.editText17);
		RegistroManager.getInstance().setTelefono(parametro.getText().toString());
		
		
		
		
		CheckBox check = (CheckBox) getView().findViewById(R.id.checkBox1);
		if(check.isChecked())
			RegistroManager.getInstance().setRecibirCorreos(true);
		else
			RegistroManager.getInstance().setRecibirCorreos(false);
		
	}
	
	
	
private void addItemsOnSpinner1(){
		
		Spinner spinner = (Spinner) getView().findViewById(R.id.spinner1);
		List<String> list = new ArrayList<String>();
		list.add(sexos[0]);
		list.add(sexos[1]);
		
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(dataAdapter);
		spinner.setOnItemSelectedListener(this);
				
	}

private void addItemsOnSpinner2(){
	
	Spinner spinner = (Spinner) getView().findViewById(R.id.spinner2);
	List<String> list = new ArrayList<String>();
	
	for(int i = 0; i < estados.length; i++)
		list.add(estados[i]);
			
	ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list);
	dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	spinner.setAdapter(dataAdapter);
	spinner.setOnItemSelectedListener(this);
			
}

private void addItemsOnSpinner3(){
	
	Spinner spinner = (Spinner) getView().findViewById(R.id.spinner3);
	List<String> list = new ArrayList<String>();
	
	for(int i = 0; i < ciudades.length; i++)
		list.add(ciudades[i]);
			
	ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list);
	dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	spinner.setAdapter(dataAdapter);
	spinner.setOnItemSelectedListener(this);
			
}
	
	
	public static class DatePickerFragment extends DialogFragment
	
	implements DatePickerDialog.OnDateSetListener {
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current date as the default date in the picker
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);
			
			// Create a new instance of DatePickerDialog and return it
			return new DatePickerDialog(getActivity(), this, year, month, day);
			}		
		

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub			
			
			
			String fecha =  Fechas.getStringDate(dayOfMonth, monthOfYear, year);			
			((EditText) instance.getView().findViewById(R.id.fechaEdit)).setText(fecha);
			RegistroManager.getInstance().setFecha(dayOfMonth, monthOfYear+1, year);			
			
		}
	}
	
	public void seleccionarCedula(){
		
		
		//Prepare the list dialog box

    	AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

    	//Set its title

    	builder.setTitle("Seleccione el tipo de cédula.");
    	builder.setIcon(R.drawable.cedula);
   
    	builder.setSingleChoiceItems(nacionalidades, -1, new DialogInterface.OnClickListener() {

			// Click listener

			@Override
			public void onClick(DialogInterface dialog, int item) {

				switch (item) {
					case 0:
						RegistroManager.getInstance().setTipoCedula("V");
						break;
					case 1:
						RegistroManager.getInstance().setTipoCedula("E");
						break;
					case 2:
						RegistroManager.getInstance().setTipoCedula("P");
						break;

				}

				((EditText) getView().findViewById(R.id.editText7)).setText("  " + nacionalidades[item]);
				RegistroManager.getInstance().setTipoCedula(nacionalidades[item]);

				dialog.dismiss();

			}

		});

    	AlertDialog alert = builder.create();
    	alert.show();

    }


	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
		Log.i("Id", "" + parent.getId());		
		if(parent.getId() == R.id.spinner1)
			RegistroManager.getInstance().setSexo(parent.getItemAtPosition(pos).toString());
		else if(parent.getId() == R.id.spinner2)
			RegistroManager.getInstance().setEstado(parent.getItemAtPosition(pos).toString());
		else if(parent.getId() == R.id.spinner3)
			RegistroManager.getInstance().setCiudadCod(codigoCiudades[pos]);
		
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		ConnectionManager.setContext(getActivity());
    	if(ConnectionManager.checkConn() == false){
    		
    		
    		AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
    		alertDialog.setTitle("CinesUnidos"); 
    		alertDialog.setMessage("No hay conexión a internet");
    		alertDialog.setIcon(R.drawable.icono); 
    		alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
    			
    			@Override
    			public void onClick(DialogInterface dialog,int which) {
    				dialog.cancel();    				
    				return;
    			}
  	      }); 
  	      
  	    
  	      alertDialog.show(); 
  		
  	      return;
    	}
		
		if(R.id.fechaEdit == v.getId()){
			DialogFragment newFragment = new DatePickerFragment();
		    newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
		}else if(R.id.Enviar == v.getId()){
			
			if(checkParams()){
				if(ConnectionManager.checkConn() == false){
					mensajeError("No hay conexión a internet.");
					return;
				}
				getParams();				
				new RegistrarTask().execute();
			}
		}else if(R.id.editText7 == v.getId()){
			seleccionarCedula();
		}
		
	}


	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}
	
private class RegistrarTask extends AsyncTask<String, Void, Boolean> {
		
		
		ProgressDialog pd;
		 
		 
		 @Override
		protected void onPreExecute() {
			 
			pd = ProgressDialog.show(getActivity(), "Espere.", "Realizando Registro.", true, false);				
			 
			 
	    	}
	     
		 @Override
		protected Boolean doInBackground(String... params) {	 			 
			 RegistroManager.getInstance().sendRequest();			
			 
			return RegistroManager.getInstance().getResult();
			//return true;
				
			 
					
	     }	    

	     @Override
		protected void onPostExecute(Boolean result) {
//	    	boolean result = RegistroManager.getInstance().getResult();
	    	 if(getView() == null)
	    		 return;
	    	 
	    	 try {
		    	 pd.cancel();
		         pd = null;
		        } catch (Exception e) { }
	    	 
	    	 if(result == false){										
					mensajeError("Ocurrió un error durante el registro, verifique que los datos sean válidos.");
					return;
				}else{					
					mensajeError("Su cuenta ha sido creada");					
					getActivity().getSupportFragmentManager().popBackStack();
					return;
					
				}
				
				
	    	
	    	 }
	     }	

private void mensajeError(String error){
	AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    builder.setTitle("CinesUnidos");
    builder.setMessage(error);       
    builder.setPositiveButton("OK",null);
    builder.create();
    builder.setIcon(R.drawable.icono);
    builder.show(); 
}

/*public void onPause()

{
	
    LocalyticsManager.localyticsSession().upload();
    super.onPause();
}*/
	@Override
	public void onPause() {
		super.onPause();

		FlurryAgent.onEndSession(getActivity());
	}

	@Override
	public void onStop() {
		super.onStop();

		FlurryAgent.onEndSession(getActivity());
	}
	
}
