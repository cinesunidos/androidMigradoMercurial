package com.mobilemedianet.cinesunidosAndroid.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.text.format.Time;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.mobilemedianet.cinesunidosAndroid.MainActivity;
import com.mobilemedianet.cinesunidosAndroid.R;
import com.mobilemedianet.cinesunidosAndroid.managers.ButacasManager;
import com.mobilemedianet.cinesunidosAndroid.managers.ConnectionManager;
import com.mobilemedianet.cinesunidosAndroid.managers.DetallesCompra;
import com.mobilemedianet.cinesunidosAndroid.managers.LoginManager;
import com.mobilemedianet.cinesunidosAndroid.managers.MovieManager;
import com.mobilemedianet.cinesunidosAndroid.managers.NewSeats;
import com.mobilemedianet.cinesunidosAndroid.managers.Pagar;
import com.mobilemedianet.cinesunidosAndroid.managers.PrefManager;
import com.mobilemedianet.cinesunidosAndroid.objects.Butaca;
import com.mobilemedianet.cinesunidosAndroid.objects.Cines;
import com.mobilemedianet.cinesunidosAndroid.objects.InformacionUsuario;
import com.mobilemedianet.cinesunidosAndroid.objects.Movie;
import com.mobilemedianet.cinesunidosAndroid.utilities.Contador;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static com.mobilemedianet.cinesunidosAndroid.R.id.iva_text;

//import com.mobilemedianet.cinesunidosAndroid.managers.LocalyticsManager;

public class PagarActivityFragment extends Fragment implements OnClickListener{

	static boolean visa, master, american;
	Movie peli;  
	OnPagarListener pagado;
	String tipoCedula = "V";
	String [] cedulas = {"Venezolana", "Extranjera", "Jurídica", "Gubernamental"};
	Pagar paga;
	DetallesCompra comprar;
	private  MyCount contador = null;
	EditText numero_tarjeta;
	
	static final int MASTERCARD = 0, VISA = 1, AMEX = 2;	   
	
static private PagarActivityFragment instance;

	private static final String Flurry_API_Key = "GSW94MXD24S7D546NST6";
	
	

	static public PagarActivityFragment getInstance(){
		
		if(instance == null)
			instance = new PagarActivityFragment();
		return instance;
	}
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);

        //Flurry Start Session
        FlurryAgent.init(getActivity(), Flurry_API_Key);
        FlurryAgent.onStartSession(getActivity());

		ConnectionManager.setContext(getActivity());
		if(ConnectionManager.checkConn() == false){
    		
    		
    		AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
    		alertDialog.setTitle("CinesUnidos"); 
    		alertDialog.setMessage("No hay conexión a internet");
    		alertDialog.setIcon(R.drawable.icono); 
    		alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
    			
    			@Override
    			public void onClick(DialogInterface dialog,int which) {
    				dialog.cancel();
    				((MainActivity) getActivity()).goHome();
    				return;
    			}
  	      }); 
  	      
  	    
  	      alertDialog.show(); 
  		
  	      return;
    	}

		
		    	
		    	
//		setRetainInstance(true);
		peli = MovieManager.getInstance().getSelectedMovie(0);
        
	}
	
	
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View viewer = inflater.inflate(R.layout.pagar, container, false);
        
       return viewer;
    }   
	
	@Override
	public void onResume(){
		super.onResume();
		
		/*if(LocalyticsManager.localyticsSession() == null){
    		LocalyticsManager.newLocalytics(getActivity().getApplicationContext());
        	LocalyticsManager.localyticsSession().open();
    	}*/
		//LocalyticsManager.localyticsSession().tagEvent("Pagar Entradas");
		FlurryAgent.logEvent("Pagar Entradas");
				
		ViewTreeObserver vto = getView().getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
            	if(getView() == null)
            		return;
            	if(DetallesCompra.getInstance().getSeleccionAsientos()){
            		contador = new MyCount(Contador.getInstance().getTimer(), 500);
            		contador.start();
					new ConfirmTask().execute();
            	}
            	llenarCampos();
            	
                ViewTreeObserver obs = getView().getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);

            }
        });		
		
		
		 
	    
	}
	
	public void llenarCampos(){
		String nombreCine = Cines.getCineById(DetallesCompra.getInstance().getCineId())[1];
		String salaNombre = DetallesCompra.getInstance().getSalaName();
    	//String salaNombre = SalasManager.getInstance().getSalaNameById(DetallesCompra.getInstance().getSalaId());
		
//		((ImageView)getView().findViewById(R.id.ImagenPeli)).setImageDrawable(peli.getPoster());
//        
//	     ((TextView)getView().findViewById(R.id.TituloPeliDetalles)).setText(Html.fromHtml("<b><big>" + peli.getSpanishTitle() + "</big></b><br />" + 
//	                  DetallesCompra.getInstance().getCensura() + "<br />" + DetallesCompra.getInstance().getFecha() +  "<br />"
//	                  + nombreCine + "-" + salaNombre));
	     
//	     ((TextView)getView().findViewById(R.id.lugarCine)).setText(Html.fromHtml("<small> Cines Unidos</small><br />" + "CineId: " + String.valueOf(DetallesCompra.getInstance().getCineId())));
	        
	     
	     getView().findViewById(R.id.visaView).setOnClickListener(this);
	     getView().findViewById(R.id.masterView).setOnClickListener(this);
	     getView().findViewById(R.id.americanView).setOnClickListener(this);
	     getView().findViewById(R.id.cedulaButton).setOnClickListener(this);
	     getView().findViewById(R.id.editTextMes).setOnClickListener(this);		     
	     getView().findViewById(R.id.editTextAno).setOnClickListener(this);
//	     ((EditText)getView().findViewById(R.id.cedulaButton)).setText("  V");
	     ((Button)getView().findViewById(R.id.button1)).setOnClickListener(this);
	     
	     ( (EditText) getView().findViewById(R.id.editTextNumero) ).setText("");
	     ( (EditText) getView().findViewById(R.id.editTextCodigo) ).setText("");
	     
	     
	     ((TextView)getView().findViewById(R.id.textViewPeliculaNombre)).setText(peli.getSpanishTitle());
	     ((TextView)getView().findViewById(R.id.textViewCineNombre)).setText(nombreCine + " - " + salaNombre);
	     ((TextView)getView().findViewById(R.id.textViewFecha)).setText(DetallesCompra.getInstance().getFecha());





	     ((TextView)getView().findViewById(R.id.totalEntradas_text)).setText(String.valueOf(DetallesCompra.getInstance().getNumeroTotalEntradas()));
	     ((TextView)getView().findViewById(R.id.valorentradas_text)).setText(String.format("Bs. %.2f", DetallesCompra.getInstance().getTotalEntradasValor()));
	     ((TextView)getView().findViewById(R.id.cargosServicios_text)).setText(String.format("Bs. %.2f", DetallesCompra.getInstance().getBookingFeeWithTaxInCents()));
		((TextView)getView().findViewById(R.id.cargosServiciosWeb_text)).setText(String.format("Bs. %.2f", DetallesCompra.getInstance().getCargosWeb()));
		((TextView)getView().findViewById(iva_text)).setText(String.format("Bs. %.2f", DetallesCompra.getInstance().getTaxInCents()));
	     ((TextView)getView().findViewById(R.id.total_text)).setText(String.format("Bs. %.2f", DetallesCompra.getInstance().getCostoTotal()));
	     
	     if(PrefManager.getInstance().getUserCheckBoxStatus() == true){
	    	 ((TextView)getView().findViewById(R.id.editTextNombre)).setText(PrefManager.getInstance().getUserPaymentName());	    
	         ((TextView)getView().findViewById(R.id.editTextCedula)).setText(PrefManager.getInstance().getUserPaymentCI());	     
	         if(!PrefManager.getInstance().getUserPaymentCIType().equals("")){
	        	 ((TextView)getView().findViewById(R.id.cedulaButton)).setText("  " + PrefManager.getInstance().getUserPaymentCIType());
	        	 tipoCedula = PrefManager.getInstance().getUserPaymentCIType();
	         }	     
	         ((TextView)getView().findViewById(R.id.codTel)).setText(PrefManager.getInstance().getUserPaymentTelCode());	     
	         ((TextView)getView().findViewById(R.id.editTextTelefono)).setText(PrefManager.getInstance().getUserPaymentTel());
	         ((CheckBox) getView().findViewById(R.id.checkBox1)).setChecked(true);
	     }
	     
	     ((CheckBox) getView().findViewById(R.id.checkBox1)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked == false){
					PrefManager.getInstance().cleanUserPaymentInfo();
				}else{
					PrefManager.getInstance().putUserPaymentCI(( (EditText) getView().findViewById(R.id.editTextCedula) ).getText().toString());
					PrefManager.getInstance().putUserPaymentCIType(tipoCedula);
					PrefManager.getInstance().putUserPaymentName(( (EditText) getView().findViewById(R.id.editTextNombre) ).getText().toString());
					PrefManager.getInstance().putUserPaymentTelCode(( (EditText) getView().findViewById(R.id.codTel) ).getText().toString());
					PrefManager.getInstance().putUserPaymentTel(( (EditText) getView().findViewById(R.id.editTextTelefono) ).getText().toString());
					PrefManager.getInstance().putUserCheckBoxStatus(true);
				}
			}
		});
	     
	     
	     
	     
	     if(visa){
	    	 ImageView v = (ImageView) getView().findViewById(R.id.visaView);
	    	 v.setImageDrawable(getResources().getDrawable(R.drawable.visapressed));
	     }else if(master){
	    	 ImageView v = (ImageView) getView().findViewById(R.id.masterView);
	    	 v.setImageDrawable(getResources().getDrawable(R.drawable.masterpressed));
	     }else if(american){
	    	 ImageView v = (ImageView) getView().findViewById(R.id.americanView);
	    	 v.setImageDrawable(getResources().getDrawable(R.drawable.americanpressed));
	     }
	     
	     
	     numero_tarjeta = (EditText) getView().findViewById(R.id.editTextNumero);
	     
	     if(visa == false && master == false && american == false){
	    	 numero_tarjeta.setFocusable(false);
	     }
	     
	     numero_tarjeta.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if(visa == false && master == false && american == false){
					 
					 AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity()); 
		    	     alertDialog.setTitle("Cines Unidos"); 
		    	     alertDialog.setMessage("Por favor seleccione primero el tipo de tarjeta de crédito");
		    	     alertDialog.setIcon(R.drawable.icono); 
		    	     alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		    	          @Override
		    			public void onClick(DialogInterface dialog,int which) { 
		    	        	
		    	        	dialog.cancel();
		  	            		    	        	  	    	        	
		    	          }
		    	      }); 
		    	      alertDialog.show();
					
				}
				
			}
		});
	     
	     numero_tarjeta.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
				if(!hasFocus) {
					numero_tarjeta.setTransformationMethod(new PasswordTransformationMethod());
				} else {
					numero_tarjeta.setTransformationMethod(null);
				}
				
			}
	     });
	     
	     //Separador de numeros de tarjeta segun su tipo.
	    /* numero_tarjeta.addTextChangedListener(new TextWatcher() {
			
	    	int count = 0;
	    	 
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				String result = s.toString();
				
				if(american){
					if (result.length() == 4 && count < result.length()) {
						result += "-";
						numero_tarjeta.setText(result);
						numero_tarjeta.setSelection(result.length());
					}else if (result.length() == 11 && count < result.length()) {
					 	result += "-";
					 	numero_tarjeta.setText(result);
					 	numero_tarjeta.setSelection(result.length());
					}else if (result.length() > 17) {
						result = result.substring(0, result.length() - 1);
						numero_tarjeta.setText(result);
						numero_tarjeta.setSelection(result.length());
					}else if (result.length() == 4 && count > result.length()) {
						result = result.substring(0, 3);
						numero_tarjeta.setText(result);
						numero_tarjeta.setSelection(result.length());
					}
					else if (result.length() == 11 && count > result.length()) {
						result = result.substring(0, 10);
						numero_tarjeta.setText(result);
						numero_tarjeta.setSelection(result.length());
					}else if (result.length() == 17 && count > result.length()) {
						result = result.substring(0, 16);
						numero_tarjeta.setText(result);
						numero_tarjeta.setSelection(result.length());
					}
					
					count = result.length();
				}
				
				if(visa || master){
					if (result.length() == 4 && count < result.length()) {
						result += "-";
						numero_tarjeta.setText(result);
						numero_tarjeta.setSelection(result.length());			        
					}else if (result.length() == 9 && count < result.length()) {
						result += "-";
						numero_tarjeta.setText(result);
						numero_tarjeta.setSelection(result.length());			        
					}else if (result.length() == 14 && count < result.length()) {
						result += "-";
						numero_tarjeta.setText(result);
						numero_tarjeta.setSelection(result.length());			        
					}else if (result.length() > 19) {
						result = result.substring(0, result.length() - 1);
						numero_tarjeta.setText(result);
						numero_tarjeta.setSelection(result.length());			        
					}else if (result.length() == 4 && count > result.length()) {
						result = result.substring(0, 3);
						numero_tarjeta.setText(result);
						numero_tarjeta.setSelection(result.length());
					}
					else if (result.length() == 9 && count > result.length()) {
						result = result.substring(0, 8);
						numero_tarjeta.setText(result);
						numero_tarjeta.setSelection(result.length());
					}else if (result.length() == 14 && count > result.length()) {
						result = result.substring(0, 13);
						numero_tarjeta.setText(result);
						numero_tarjeta.setSelection(result.length());
					}else if (result.length() == 19 && count > result.length()) {
						result = result.substring(0, 18);
						numero_tarjeta.setText(result);
						numero_tarjeta.setSelection(result.length());
					}
					
					count = result.length();				
				}
			}				
			
		});*/
		          
	        
		
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		ImageView cambiar;
		Time today = new Time(Time.getCurrentTimezone());		
		today.setToNow();
		int tipoTarjeta;
		
		if(R.id.visaView == v.getId()){
			
			((ImageView) v).setImageDrawable(getResources().getDrawable(R.drawable.visapressed));
			cambiar = (ImageView) getView().findViewById(R.id.masterView);
			cambiar.setImageDrawable(getResources().getDrawable(R.drawable.master));
			cambiar = (ImageView) getView().findViewById(R.id.americanView);
			cambiar.setImageDrawable(getResources().getDrawable(R.drawable.american));
			
			visa = true;
			master = american = false;
			
			numero_tarjeta.setOnClickListener(null);			
			numero_tarjeta.setFocusableInTouchMode(true);
			numero_tarjeta.setText("");
			
			
			
		}else if(R.id.masterView == v.getId()){
			
			((ImageView) v).setImageDrawable(getResources().getDrawable(R.drawable.masterpressed));
			cambiar = (ImageView) getView().findViewById(R.id.visaView);
			cambiar.setImageDrawable(getResources().getDrawable(R.drawable.visa));
			cambiar = (ImageView) getView().findViewById(R.id.americanView);
			cambiar.setImageDrawable(getResources().getDrawable(R.drawable.american));
			
			master = true;
			visa = american = false;
			
			numero_tarjeta.setOnClickListener(null);			
			numero_tarjeta.setFocusableInTouchMode(true);
			numero_tarjeta.setText("");
			
		}else if(R.id.americanView == v.getId()){
			
			((ImageView) v).setImageDrawable(getResources().getDrawable(R.drawable.americanpressed));
			cambiar = (ImageView) getView().findViewById(R.id.masterView);
			cambiar.setImageDrawable(getResources().getDrawable(R.drawable.master));
			cambiar = (ImageView) getView().findViewById(R.id.visaView);
			cambiar.setImageDrawable(getResources().getDrawable(R.drawable.visa));
			
			american = true;
			master = visa = false;
			
			numero_tarjeta.setOnClickListener(null);			
			numero_tarjeta.setFocusableInTouchMode(true);
			numero_tarjeta.setText("");
			
		}else if(R.id.cedulaButton == v.getId()){
			seleccionarCedula();
		}else if(R.id.editTextMes == v.getId()){
			seleccionarMes();
		}else if(R.id.editTextAno == v.getId()){
			seleccionarAno();
		}else if(R.id.button1 == v.getId()){				
			String tarjeta = null;
			paga = Pagar.getInstance();
			comprar = DetallesCompra.getInstance();
			InformacionUsuario user = LoginManager.getInstance().getUserInfo();
			
			paga.setcinemaId(String.valueOf(comprar.getCineId()));
			
			paga.setUserId(user.getSecureId());
			paga.setUserFirstName(user.getName());
			paga.setUserLastName(user.getLastName());
			paga.setUserEmail(user.getLogin());
			paga.setUserNationalId(user.getExternalId());
			paga.setTypeNationalId(tipoCedula);
						
			if(DetallesCompra.getInstance().getSeleccionAsientos()){
				paga.setSeleccionButacas(true);
				paga.setOrderId(ButacasManager.getInstance().getOrderId());
				paga.setUserSelectsSeats("true");
				paga.setUserSeatSelection(butacas());
			}else{
				paga.setSeleccionButacas(false);
				paga.setperformanceId(String.valueOf(comprar.getShowTimeId()));
				paga.setUserSelectsSeats("false");
				paga.setUserSeatSelection("");
			}
			paga.setUserTicketSelection(DetallesCompra.getInstance().getSeleccionVoletos());
			
			if(visa == true){
				tarjeta = "0";
				tipoTarjeta = VISA;
			}
			else if(master == true){
				tarjeta = "1";
				tipoTarjeta = MASTERCARD;
			}
			else if(american == true){
				tarjeta = "Americanexpress";
				tipoTarjeta = AMEX;
			}else{
				Toast.makeText(getActivity(), "Debe seleccionar un tipo de tarjeta.", Toast.LENGTH_SHORT).show();
				return;	
			}
			
					
			 if(( (EditText) getView().findViewById(R.id.editTextMes) ).getText().toString().equals("Mes")){
				Toast.makeText(getActivity(), "Debe seleccionar el mes de vencimiento.", Toast.LENGTH_SHORT).show();
				return;				
			 }
			 String mes = ( (EditText) getView().findViewById(R.id.editTextMes) ).getText().toString();
			
			 if(( (EditText) getView().findViewById(R.id.editTextAno) ).getText().toString().equals("Año")){
				Toast.makeText(getActivity(), "Debe seleccionar el año de vencimiento.", Toast.LENGTH_SHORT).show();
				return;				
			 }
			 String anio = ( (EditText) getView().findViewById(R.id.editTextAno) ).getText().toString();
			 
			int year = Integer.parseInt(anio);
			int month = Integer.parseInt(mes);
				
			if(year == (today.year - 2000) && month < today.month + 1){
				Toast.makeText(getActivity(), "Tarjeta de crédito vencida.", Toast.LENGTH_SHORT).show();
				return;											
				}
			
			 if(( (EditText) getView().findViewById(R.id.editTextNumero) ).getText().toString().equals("")){
				 Toast.makeText(getActivity(), "Debe introducir el número de tarjeta.", Toast.LENGTH_SHORT).show();
					return;					
			 }
			 String numero = ( (EditText) getView().findViewById(R.id.editTextNumero) ).getText().toString().replaceAll("-", "");;

				
			 
			 if(( (EditText) getView().findViewById(R.id.editTextCodigo) ).getText().toString().equals("")){
				 Toast.makeText(getActivity(), "Debe introducir el código de seguridad de su tarjeta. Estos son los últimos 3 o 4 dígitos en el reverso de la tarjeta.", Toast.LENGTH_SHORT).show();
				return;					
			 }
			 String codigo = ( (EditText) getView().findViewById(R.id.editTextCodigo) ).getText().toString();
					
			 if(( (EditText) getView().findViewById(R.id.editTextNombre) ).getText().toString().equals("")){
				Toast.makeText(getActivity(), "Debe introducir el nombre que aparece en su tarjeta.", Toast.LENGTH_SHORT).show();
				return;	
			 }
			 String nombre = ( (EditText) getView().findViewById(R.id.editTextNombre) ).getText().toString();
							 
		     if(( (EditText) getView().findViewById(R.id.editTextCedula) ).getText().toString().equals("")){
		    	Toast.makeText(getActivity(), "Debe introducir su número de cédula.", Toast.LENGTH_SHORT).show();
				return;			     
		     }
			 String cedula = ( (EditText) getView().findViewById(R.id.editTextCedula) ).getText().toString();
				
			 if(( (EditText) getView().findViewById(R.id.editTextTelefono) ).getText().toString().equals("")){
				Toast.makeText(getActivity(), "Debe introducir su número telefónico.", Toast.LENGTH_SHORT).show();
				return;			     
			 }
			 String telefono = ( (EditText) getView().findViewById(R.id.editTextTelefono) ).getText().toString();
			 
			 if(( (EditText) getView().findViewById(R.id.codTel) ).getText().toString().equals("")){
					Toast.makeText(getActivity(), "Debe introducir el prefijo para el número telefónico.", Toast.LENGTH_SHORT).show();
					return;			     
				 }
				 String prefijo = ( (EditText) getView().findViewById(R.id.codTel) ).getText().toString();
			 
			//Todo Descomentar validacion de tarjeta
			if(validate(numero, tipoTarjeta) == false){
				Toast.makeText(getActivity(), "El número de tarjeta no es valido", Toast.LENGTH_SHORT).show();
				return;
			}
			
			if(ConnectionManager.checkConn() == false){
				ConnectionManager.getInstance().mensajeError("No hay ninguna conexión a internet activa");
				return;
			}
			
								
			paga.setUserPhone(prefijo + telefono);
			paga.setCardExpiryMonth(mes);
			paga.setCardExpiryYear(anio);
			paga.setCardNumber(numero);
			paga.setCardHolder(nombre);				
						
			paga.setCardType(tarjeta);
			//paga.setClubNationalId(tipoCedula+cedula);
			paga.setClubNationalId(cedula);
			paga.setPaymentType("CreditCard");
			paga.setverificationCode(codigo);
			
			CheckBox check = (CheckBox) getView().findViewById(R.id.checkBox1);
			
			if(check.isChecked()){
				PrefManager.getInstance().putUserPaymentCI(cedula);
				PrefManager.getInstance().putUserPaymentCIType(tipoCedula);
				PrefManager.getInstance().putUserPaymentName(nombre);
				PrefManager.getInstance().putUserPaymentTelCode(prefijo);
				PrefManager.getInstance().putUserPaymentTel(telefono);
				PrefManager.getInstance().putUserCheckBoxStatus(true);
			}else
				PrefManager.getInstance().cleanUserPaymentInfo();
			
			
			
			
			new PayTask().execute();
		}
		
		
		
	}	
	
	public void seleccionarCedula(){
		
			
    		//Prepare the list dialog box

        	AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        	//Set its title

        	builder.setTitle("Seleccione el tipo de cédula.");
        	builder.setIcon(R.drawable.cedula);
       
        	builder.setSingleChoiceItems(cedulas, -1, new DialogInterface.OnClickListener() {

        		// Click listener

        		@Override
				public void onClick(DialogInterface dialog, int item) {
        			
        			switch(item){
        			case 0 :
        				tipoCedula = "V";
        				break;
        			case 1 :
        				tipoCedula = "E";
        				break;
        			case 2 :
        				tipoCedula = "J";
        				break;
        			case 3 :
        				tipoCedula = "G";
        				break;        				
        			}
        			
        			((EditText)getView().findViewById(R.id.cedulaButton)).setText("  " + tipoCedula);  			
        			Toast.makeText(getActivity(), "Cédula " + cedulas[item], Toast.LENGTH_SHORT).show();
        			
        			
         	        dialog.dismiss();

        	    }

        	});

        	AlertDialog alert = builder.create();
        	alert.show();

        }
	
	public void seleccionarMes(){
		
		final String []meses = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" };
		
		//Prepare the list dialog box

    	AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

    	//Set its title

    	builder.setTitle("Mes de vencimiento.");
    	builder.setIcon(R.drawable.calendar);
   
    	builder.setSingleChoiceItems(meses, -1, new DialogInterface.OnClickListener() {

    		// Click listener

    		@Override
			public void onClick(DialogInterface dialog, int item) {
    			
    			
    			
    			((EditText)getView().findViewById(R.id.editTextMes)).setText(meses[item]);      			
    			Toast.makeText(getActivity(), "Mes " + meses[item], Toast.LENGTH_SHORT).show();    			
    			
     	        dialog.dismiss();

    	    }

    	});

    	AlertDialog alert = builder.create();
    	alert.show();

    }
	
	
public void seleccionarAno(){
		
		final String []anios = new String[20];
		
		Time today = new Time(Time.getCurrentTimezone());
		
		today.setToNow();
		
		for(int i = 0; i < 20; i++){
			//anios[i] = String.valueOf(today.year + i - 2000);
			anios[i] = String.valueOf(today.year + i );
		}
		
				
		//Prepare the list dialog box
		
		

    	AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

    	//Set its title

    	builder.setTitle("Año de vencimiento.");
    	builder.setIcon(R.drawable.calendar);
   
    	builder.setSingleChoiceItems(anios, -1, new DialogInterface.OnClickListener() {

    		// Click listener

    		@Override
			public void onClick(DialogInterface dialog, int item) {
    			
    			
    			
    			((EditText)getView().findViewById(R.id.editTextAno)).setText(anios[item]);  			
    			Toast.makeText(getActivity(), "Año " + anios[item], Toast.LENGTH_SHORT).show();
    			
     	        dialog.dismiss();

    	    }

    	});

    	AlertDialog alert = builder.create();
    	alert.show();

    }
	
	public interface OnPagarListener{		
	       public void irConfirmacion();
	   }
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			pagado = (OnPagarListener) activity;
		} catch (ClassCastException e) {
		throw new ClassCastException(activity.toString()
              + " must implement a Listener");
  }
}
	
	
	private boolean validate(String number, int type) {
        switch(type) {
		
        case MASTERCARD:
            if (number.length() != 16 ||
                Integer.parseInt(number.substring(0, 2)) < 51 ||
                Integer.parseInt(number.substring(0, 2)) > 55)
            {
                return false;
            }
            break;
			
        case VISA:
            if ((number.length() != 13 && number.length() != 16) ||
                    Integer.parseInt(number.substring(0, 1)) != 4)
            {
                return false;
            }
            break;
			
        case AMEX:
            if (number.length() != 15 ||
                (Integer.parseInt(number.substring(0, 2)) != 34 &&
                    Integer.parseInt(number.substring(0, 2)) != 37))
            {
                return false;
            }
            break;
        }
        
        return isCreditCardValid(number);
	}
	
	
	public static boolean isCreditCardValid(String cardNumber) {
	    String digitsOnly = cardNumber;
	    int sum = 0;
	    int digit = 0;
	    int addend = 0;
	    boolean timesTwo = false;

	    for (int i = digitsOnly.length() - 1; i >= 0; i--) {
	        digit = Integer.parseInt(digitsOnly.substring(i, i + 1));
	        if (timesTwo) {
	            addend = digit * 2;
	            if (addend > 9) {
	                addend -= 9;
	            }
	        } else {
	            addend = digit;
	        }
	        sum += addend;
	        timesTwo = !timesTwo;
	    }

	    int modulus = sum % 10;
	    return modulus == 0;

	}

	class ConfirmTask extends AsyncTask<String, Void, Boolean> {

		private Exception exception;

		protected Boolean doInBackground(String... params) {
			try {
				return NewSeats.getInstance().confirmSeats();

			} catch (Exception e) {
				this.exception = e;

				return false;
			}
		}

		protected void onPostExecute(Boolean result) {
			if (!result)
			{
				ConnectionManager.getInstance().mensajeError("Error confirmando las butacas. Por favor intente de nuevo");
				getActivity().getSupportFragmentManager().popBackStack();
			}
		}
	}


	
	private class PayTask extends AsyncTask<String, Void, String> {
		
		
		ProgressDialog pd;
		 
		 
		 @Override
		protected void onPreExecute() {
			 
			
			pd = ProgressDialog.show(getActivity(), "Espere.", "Realizando Pago.", true, false);
			 
			 
	    	}
	     
		 @Override
		protected String doInBackground(String... params) {	 
			 paga.sendRequest();
			 if(paga.getLastError() == 0)
				 return paga.getResult();		
			 return null;
					
	     }	    

	     @Override
		protected void onPostExecute(String result) {
	    	
	    	 if(getView() == null)
	    		 return;
	    	 
	    	 Map <String,String> values = new HashMap<String,String> ();
	    	 
	    	 try {
		    	 pd.cancel();
		         pd = null;
		        } catch (Exception e) { }
	    	 
		     if(paga.getLastError() != 0){
		    	 
		    	 AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());	    	      
	    	     alertDialog.setTitle("Transacción Fallida");
	    	      alertDialog.setMessage("Error de conexión");
	    	      alertDialog.setIcon(R.drawable.warningico); 	    	     	    	      
	    	      alertDialog.setPositiveButton("Volver", new DialogInterface.OnClickListener() {
	    	          @Override
	    			public void onClick(DialogInterface dialog,int which) { 
	    	        	DetallesCompra.getInstance().fail = true;
	    	        	dialog.cancel();
	  	            	getActivity().getSupportFragmentManager().popBackStack(MainActivity.ST_COMPRAR, 0);	    	        	  	    	        	
	    	          }
	    	      }); 
	    	      alertDialog.show();
	    	      
	    	      return;
		    	 
		     }else if(result == null){
	    		 cancelTimer();
	    		 
	    		 AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity()); 
	    	      
	    	     alertDialog.setTitle("Transacción Fallida");
	    	     
	    	      
	    	      if(Pagar.getInstance().msgerr1 == null && Pagar.getInstance().msgerr2 == null)
	    	    	  alertDialog.setMessage("Error de respuesta en el servidor");
	    	      else{
	    	    	  
	    	    	  alertDialog.setMessage(Pagar.getInstance().msgerr1 + "\n\n" + Pagar.getInstance().msgerr2); 
	    	    	  
	    	    	  values.put("Mensaje", Pagar.getInstance().msgerr1 + "\n\n" + Pagar.getInstance().msgerr2);
	    	    	  
	    	    	  //LocalyticsManager.localyticsSession().tagEvent("Error Compra");
					  //FlurryAgent.logEvent("Error Compra");
	    	      }
	    	     
	    	      alertDialog.setIcon(R.drawable.warningico); 
	    	     	    	      
	    	      alertDialog.setPositiveButton("Volver", new DialogInterface.OnClickListener() {
	    	          @Override
	    			public void onClick(DialogInterface dialog,int which) { 
	    	        	DetallesCompra.getInstance().fail = true;
	    	        	dialog.cancel();
	  	            	getActivity().getSupportFragmentManager().popBackStack(MainActivity.ST_COMPRAR, 0);	    	        	  	    	        	
	    	          }
	    	      }); 
	    	      alertDialog.show();
	    	      
	    	      return;
					
				}
	    	 
	    	 	cancelTimer();	    	 	
				comprar.setBookingNumber(result);
				InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getView().getWindowToken(),0); 
				pagado.irConfirmacion();
				
	    	
	    	 }
	     }	
	
	
	
	public void cancelTimer(){
		if(contador == null)
			return;
		
		contador.cancel();
		contador = null;
		
	}
	
	public class MyCount extends CountDownTimer{
		
		public MyCount(long millisInFuture, long countDownInterval) {			
			super(millisInFuture, countDownInterval);
			
		}
		
		@Override
		public void onTick(long millisUntilFinished) {
			int seconds = (int) (millisUntilFinished / 1000);
			int minutes = seconds / 60;
			seconds     = seconds % 60;
			TextView text;
			try {
			 text = (TextView) getView().findViewById(R.id.Timer);
			} catch (Exception e){
			    return;
			  }
			
						
			text.setText(String.format("Pago Seguro - Tiempo Restante: %d:%02d", minutes, seconds));
			DetallesCompra.getInstance().setTiempoRestantes(minutes, seconds);
			Contador.getInstance().setTimer(millisUntilFinished);
			
		}
		
		@Override
		public void onFinish() {
			TextView text;
			//LocalyticsManager.localyticsSession().tagEvent("Tiempo de Compra Agotado");
			//FlurryAgent.logEvent("Tiempo de Compra Agotado");
			try {
				 text = (TextView) getView().findViewById(R.id.Timer);
				} catch (Exception e){
				    return;
				  }			
			text.setText("Selección de butacas - Tiempo Agotado.");
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());			        
	        alertDialog.setTitle("CinesUnidos"); 			        
	        alertDialog.setMessage("Se ha agotado el tiempo de reserva, vuelva a empezar."); 			       
	        alertDialog.setIcon(R.drawable.icono);
	        alertDialog.setCancelable(false);
	        alertDialog.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
	            @Override
				public void onClick(DialogInterface dialog,    int which) {
	            	dialog.cancel();
	            	getActivity().getSupportFragmentManager().popBackStack("comprar", 0);
	            }
	        });
	        alertDialog.show();	        
	        
	        
		}
	}	
	
private String butacas(){
		
		String seleccion = "";
		
		Iterator<Butaca> butacas = ButacasManager.getInstance().getNewVectorSelectionIterator();
		
		
		if(butacas == null)
			return null;
		
		Butaca dum;
		
		while(butacas.hasNext()){
			dum = butacas.next();
			seleccion += dum.getNombreButaca();
			if(butacas.hasNext())
				seleccion += ",";
		}
		
		return seleccion;
	}

/*public void onPause()
{
	
    LocalyticsManager.localyticsSession().upload();
    super.onPause();
}*/

	@Override
	public void onPause() {
		super.onPause();

		FlurryAgent.onEndSession(getActivity());
	}

	@Override
	public void onStop() {
		super.onStop();

		FlurryAgent.onEndSession(getActivity());
	}

}
