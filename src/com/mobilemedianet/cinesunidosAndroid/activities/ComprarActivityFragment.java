package com.mobilemedianet.cinesunidosAndroid.activities;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.mobilemedianet.cinesunidosAndroid.MainActivity;
import com.mobilemedianet.cinesunidosAndroid.R;
import com.mobilemedianet.cinesunidosAndroid.managers.ButacasManager;
import com.mobilemedianet.cinesunidosAndroid.managers.ConnectionManager;
import com.mobilemedianet.cinesunidosAndroid.managers.DetallesCompra;
import com.mobilemedianet.cinesunidosAndroid.managers.MovieManager;
import com.mobilemedianet.cinesunidosAndroid.objects.Boleto;
import com.mobilemedianet.cinesunidosAndroid.objects.Cines;
import com.mobilemedianet.cinesunidosAndroid.objects.Movie;
import com.mobilemedianet.cinesunidosAndroid.objects.ShowTime;
import com.mobilemedianet.cinesunidosAndroid.objects.Theaters;
import com.mobilemedianet.cinesunidosAndroid.utilities.Metrics;

import java.util.Iterator;
import java.util.List;

//import com.mobilemedianet.cinesunidosAndroid.managers.LocalyticsManager;


public class ComprarActivityFragment extends Fragment implements OnClickListener{

	
	List<Boleto> boletos;
	
	int entradas;
	float valor;
	float totalEntradas;
	float cargoServicios;
	int tipo;	
	String tipoEntrada = null;
	
	String censura = "";
    String hora = "";
    int cineId = 0;
    int salaId = 0;	
    Movie peli, peli2; 
    int showTimeid; 
	
	 
	List <Theaters> salas;
			
	OnPagarSelectedListener PagarSelected;
	
	static private ComprarActivityFragment instance;

	private static final String Flurry_API_Key = "GSW94MXD24S7D546NST6";

	static public ComprarActivityFragment getInstance(String i){
		String []values = i.split("-");
		
		Log.i("Cadena recibida", i);
		
		//String movie_id = Integer.parseInt(values[2]);
		String movie_id = values[2];
		Movie selected = MovieManager.getInstance().getMovieById(movie_id, 0);
		
		if(selected == null){
			return null;
		}
		
		String tituloPeli = selected.getSpanishTitle();
				
		DetallesCompra.getInstance().setCineId(values[0]);
		DetallesCompra.getInstance().setMovieId(movie_id);
		DetallesCompra.getInstance().setShowTimeId(values[1]);
		DetallesCompra.getInstance().setTittle(tituloPeli);
		DetallesCompra.getInstance().setSalaId(Integer.parseInt(values[3]));
		
		
		if(instance == null)
			instance = new ComprarActivityFragment();
		return instance;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
        super.onCreate(savedInstanceState);

		//Flurry Start Session
		FlurryAgent.init(getActivity(), Flurry_API_Key);
		FlurryAgent.onStartSession(getActivity());


	}
	
	
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
       View viewer = inflater.inflate(R.layout.comprar, container, false);        
       viewer.findViewById(R.id.pagarBoton).setOnClickListener(this); 
       viewer.findViewById(R.id.informacion).setOnClickListener(this); 
       
        
        return viewer;
    }  
	
	
	@Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        
        
             
    }
	
	@Override
    public void onResume (){
    	super.onResume();
    	
    	ConnectionManager.setContext(getActivity());
    	if(ConnectionManager.checkConn() == false){
    		
    		
    		AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
    		alertDialog.setTitle("CinesUnidos"); 
    		alertDialog.setMessage("No hay conexión a internet");
    		alertDialog.setIcon(R.drawable.icono); 
    		alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
    			
    			@Override
    			public void onClick(DialogInterface dialog,int which) {
    				dialog.cancel();
    				((MainActivity) getActivity()).goHome();
    				return;
    			}
  	      }); 
  	      
  	    
  	      alertDialog.show(); 
  		
  	      return;
    	}
    	    	
    	/*if(LocalyticsManager.localyticsSession() == null){
    		LocalyticsManager.newLocalytics(getActivity().getApplicationContext());
        	LocalyticsManager.localyticsSession().open();
    	}*/
    	//LocalyticsManager.localyticsSession().tagEvent("Comprar Entradas");
		FlurryAgent.logEvent("Comprar Entradas");
    	peli = MovieManager.getInstance().getSelectedMovie(0);
    	
    	new GetSeatsTask().execute();
    	
	}
	
	

	private void iniciar(){
		
		 ViewTreeObserver vto = getView().getViewTreeObserver();
	        vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
	            @Override
	            public void onGlobalLayout() {
	            	if(getView() == null)
	             		return;
	            	
	            	
	            	
	            	mostrarPagina();   	
	            	
	            	String nombreCine = Cines.getCineById(DetallesCompra.getInstance().getCineId())[1];
	            	//String salaNombre = SalasManager.getInstance().getSalaNameById(DetallesCompra.getInstance().getSalaId());

					int salaId = DetallesCompra.getInstance().getSalaId();
					DetallesCompra.getInstance().setPremium(true); //todo revisar el premium
					String salaNombre = DetallesCompra.getInstance().getSalaName();
	            	//DetallesCompra.getInstance().setPremium(SalasManager.getInstance().getSalaById(salaId).isPremium());
	            	//DetallesCompra.getInstance().setSeleccionAsientos(SalasManager.getInstance().getSalaById(DetallesCompra.getInstance().getSalaId()).getSeleccionAsiento());
	            	DetallesCompra.getInstance().setSeleccionAsientos(true);
//	            	((ImageView)getView().findViewById(R.id.ImagenPeli)).setImageDrawable(peli.getPoster());
	    	        ((TextView)getView().findViewById(R.id.TituloPeliDetalles)).setText(Html.fromHtml("<big><b>" + peli.getSpanishTitle() + "</b></big>" +  "<br />" + 
	    	                  "<font color=#000000><b>" + censura + "</b><br />" + hora + "<br />" + nombreCine + " - " + salaNombre + "</font>"));
	    	        
	    	       
//	    	        ((TextView)getView().findViewById(R.id.textViewCine)).setText(Html.fromHtml(nombreCine + " - " + salaNombre));

					((TextView)getView().findViewById(R.id.textViewCine)).setText("Butacas disponibles: " + ButacasManager.getInstance().getSeatsAvailability());

	    	        
	    	        DetallesCompra.getInstance().setCensura(censura);
	    	        DetallesCompra.getInstance().setFecha(hora);
	    	        DetallesCompra.getInstance().setShowTimeId(String.valueOf(showTimeid));
	    	        
	    	        mostrarEntradas();
	    		 
	            	
	                ViewTreeObserver obs = getView().getViewTreeObserver();
	                obs.removeGlobalOnLayoutListener(this);

	            }
	        });
		
	}
	
	 private void mostrarPagina(){
		 
		 actualizarPrecios();	      
	     
	    
	     peli2 = MovieManager.getInstance().getSelectedMovie(DetallesCompra.getInstance().getDia());
	     
	     ((TextView)getView().findViewById(R.id.textViewNormas)).setText(Html.fromHtml("<u><font color=#0050a0>" + "Normas de usuario" + "</font></u>"));
	     	
	    	
	        
	        
	        salas = peli2.getSalas();
	        
	        List<ShowTime> shows;
	 		
	 		Theaters dumSala;
	 		
	 		ShowTime dumShowTime;
	 		
	 		Iterator iterSalas, iterShows;
	 		
	 		iterSalas = salas.iterator();
	 		
	 		showTimeid = Integer.parseInt(DetallesCompra.getInstance().getShowTimeId());
	 		
	 		int show;
	 		Log.i("Buscando id", String.valueOf(showTimeid));
			while(iterSalas.hasNext()){
	 			dumSala = (Theaters) iterSalas.next();
				shows = dumSala.getShowTimes();
				iterShows = shows.iterator();
				
	 			while(iterShows.hasNext()){
	 				dumShowTime = (ShowTime) iterShows.next();
	 				show = dumShowTime.getShowTimeId();	 				
	 				if(show == showTimeid){
	 					censura = dumSala.getCensura();
	 					hora = dumShowTime.getFechaLarga();
	 					DetallesCompra.getInstance().setDate(dumShowTime.getAno(), dumShowTime.getMes(), dumShowTime.getDia());
	 					DetallesCompra.getInstance().setTime(dumShowTime.getHora(), dumShowTime.getMinutos(), dumShowTime.getMeridiano());	
	 					DetallesCompra.getInstance().setDiaSemana(dumShowTime.getDiaSemana());
	 					cineId = dumSala.getCineId();
	 					salaId = dumSala.getSalaId();
	 					return;
	 				}
	 				
	 			}
	 		}
	 		
	 		
	 }
	 

	 
	 private void mostrarEntradas(){
		 if(boletos == null)
			 return;
		 LinearLayout contenedor = (LinearLayout) getView().findViewById(R.id.contenedor);	
		 contenedor.removeAllViews();
		 RelativeLayout ly = null;
//		 FrameLayout fy = null;
		 Iterator iterador = boletos.iterator();
		 TextView tv = null;
		 Button bv = null;
		 LayoutParams params = null;
		 
		 
		 TextView entrada_tag = (TextView) getView().findViewById(R.id.entrada_tag);
		 TextView valor_tag = (TextView) getView().findViewById(R.id.valor_tag);
		 TextView cantidad_tag = (TextView) getView().findViewById(R.id.cantidad_tag);
		 
		 
		 ViewGroup.MarginLayoutParams mlp1 = (ViewGroup.MarginLayoutParams) entrada_tag.getLayoutParams();
		 ViewGroup.MarginLayoutParams mlp2 = (ViewGroup.MarginLayoutParams) valor_tag.getLayoutParams();
		 ViewGroup.MarginLayoutParams mlp3 = (ViewGroup.MarginLayoutParams) cantidad_tag.getLayoutParams();
		 
		 
		 ViewGroup.MarginLayoutParams mlp;
		 
		 Boleto dum;
		 
		 boolean secuence = false;
		 
		 while(iterador.hasNext()){
			 			 
			 ly = new RelativeLayout(getActivity());
			 bv = new Button(getActivity());
			 bv.setBackgroundResource(R.drawable.fondo_tickets);
			 
			 
			 tv = new TextView(getActivity());
			 
			 dum = (Boleto) iterador.next();
			 

			 
			 tv.setText(dum.getDescripcion());
			 tv.setTextSize(16);
			 tv.setMaxWidth(Metrics.dpToPixel(110));
			 ly.addView(tv);
			 
			
			 params = (LayoutParams)tv.getLayoutParams();
			 params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			 params.addRule(RelativeLayout.CENTER_VERTICAL);
			 
			 mlp = (ViewGroup.MarginLayoutParams)tv.getLayoutParams();
			 mlp.setMargins(Metrics.dpToPixel(20), 0, 0, 0);
			 
			 
			 
//			 tv.setBackgroundColor(0xff00ff00);
			 
			 tv = new TextView(getActivity());
			 
			 tv.setText(String.format("Bs. %.2f",dum.getFullPrice()));
			 tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
			 tv.setTypeface(null, Typeface.BOLD);
			 ly.addView(tv);
			
			 params = (LayoutParams)tv.getLayoutParams();
			 params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			 params.addRule(RelativeLayout.CENTER_VERTICAL);
			 
			 
			 mlp = (ViewGroup.MarginLayoutParams)tv.getLayoutParams();			 
			 mlp.setMargins(0, 0, Metrics.dpToPixel(20), 0);
			 
			 
			 
			 bv.setText(String.valueOf(dum.getCantidad()));			
			 bv.setOnClickListener(OnClickDoSomething(bv));
			 bv.setTag(dum.getDescripcion());
			 bv.setTextSize(16);
			 
						 
			 ly.addView(bv);
			 
			 params = (LayoutParams)bv.getLayoutParams();
			 params.addRule(RelativeLayout.CENTER_HORIZONTAL);
			 params.addRule(RelativeLayout.CENTER_VERTICAL);
			
			 if(secuence)				 
				 ly.setBackgroundColor(0xFFEFEFEF);
			 else		 
				 ly.setBackgroundColor(0xFFFFFFFF);
			 
			 
			 secuence ^= true;
			 
			 
			 
			 ly.setLayoutParams(new LinearLayout.LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT, Metrics.dpToPixel(60)));
//			 mlp = (ViewGroup.MarginLayoutParams)bv.getLayoutParams();
//			 mlp.setMargins(mlp3.leftMargin + (cantidad_tag.getWidth()/2) , 5, 0, 0);
			 
			 
			 
			 contenedor.addView(ly);
			 
		 }
		 
//		 mlp = (ViewGroup.MarginLayoutParams)contenedor.getLayoutParams();
//		 mlp.setMargins(0, 20, 0, 0);
	
			 
	 }
	 
	 
	 View.OnClickListener OnClickDoSomething(final Button button)  {
	        return new View.OnClickListener() {
	            @Override
				public void onClick(View v) {	            	
	            	tipoEntrada = v.getTag().toString();
	            	mostrar();            	
	            }
	        };
	    }
	 
	 
	 public void mostrar(){
	    	

	       
//			if (view == findViewById(R.id.button1)) {

	        	//List items
		
		 		int limiteEntradas = DetallesCompra.getInstance().getBookingPerTrans();
		 
				final String []entradasDisponibles = new String[limiteEntradas + 1];
				
				
				
				for(int i = 0; i <= limiteEntradas; i++){
					entradasDisponibles[i] = String.valueOf(i); 								
				}

	        	//Prepare the list dialog box

	        	AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

	        	//Set its title

	        	builder.setTitle(tipoEntrada);
	        	builder.setIcon(R.drawable.ticket);
	        	
	        	

	        	//Set the list items along with checkbox and assign with the click listener

	        	builder.setSingleChoiceItems(entradasDisponibles, -1, new DialogInterface.OnClickListener() {

	        		// Click listener

	        		@Override
					public void onClick(DialogInterface dialog, int item) {       			
	        			        				        	        
	        			
	        			Button bv = (Button) getView().findViewWithTag(tipoEntrada);	        			
	        			bv.setText(String.valueOf(item));
	        			DetallesCompra.getInstance().setNumeroEntradas(tipoEntrada, item);
	        			actualizarPrecios();
	        			
//	        			Toast.makeText(getActivity(), entradasDisponibles[item] + " " + tipoEntrada, Toast.LENGTH_SHORT).show();       			
	        			
	        	       	dialog.dismiss();

	        	    }

	        	});

	        	AlertDialog alert = builder.create();

	        	//display dialog box

	        	
//	        	WindowManager.LayoutParams lp = alert.getWindow().getAttributes();  
//	        	lp.dimAmount=0.0f; 
//	        	lp.alpha=0.6f;        	
//	        	alert.getWindow().setAttributes(lp);  
//	        	alert.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
	        	
	        	alert.show();

	        }
//	    }
	 
	
		
	
	@Override
	public void onClick(View arg0) {		
		// TODO Auto-generated method stub
		
		
		if(arg0.getId() == R.id.pagarBoton){

			if(DetallesCompra.getInstance().getNumeroTotalEntradas() == 0){
				Toast.makeText(getActivity(), "Seleccione al menos una entrada", Toast.LENGTH_SHORT).show();
				return;
			}else if(DetallesCompra.getInstance().getNumeroTotalEntradas() > DetallesCompra.getInstance().getBookingPerTrans()){
				Toast.makeText(getActivity(), "Usted no puede seleccionar más de " + DetallesCompra.getInstance().getBookingPerTrans() + " entradas", Toast.LENGTH_SHORT).show();
				return;
			}else if(DetallesCompra.getInstance().getNumeroTotalEntradas() > ButacasManager.getInstance().getSeatsAvailability()){
				Toast.makeText(getActivity(), "Solo hay disponibilidad para  " + ButacasManager.getInstance().getSeatsAvailability() + " entradas", Toast.LENGTH_SHORT).show();
				return;
			}
			
			PagarSelected.PagarSelected();

		}else if(arg0.getId() == R.id.informacion){
			Intent i = new Intent(Intent.ACTION_VIEW, 
				       Uri.parse("http://pda.cinesunidos.com/policies_bb.asp"));			
				startActivity(i);
		}
		
	}
	
	
	
	public interface OnPagarSelectedListener{
	       public void PagarSelected();
	   }
	
	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
        	PagarSelected = (OnPagarSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement a Listener");
        }
    }
	
	private void irAPagar(short id){
    	Bundle bundle = new Bundle();
    	bundle.putShort("ShowTimeId", id);
    	//bundle.
//    	Intent intent = new Intent(this, PagarActivityFragment.class);
//    	intent.putExtras(bundle);
//        startActivity(intent);     	
    }
	
	private void actualizarPrecios(){
		
		TextView totalEntradasView;
		TextView cargosServiciosView;
		TextView cargosServiciosWebView;
		TextView iva_text;
		TextView totalView;		
		TextView valorEntradaView;		
		
		
		int totalEntradas = DetallesCompra.getInstance().getNumeroTotalEntradas();
		float totalEntradasPagar = DetallesCompra.getInstance().getTotalEntradasValor();
		float cargos = DetallesCompra.getInstance().getBookingFeeInCents();
		float cargosWeb = DetallesCompra.getInstance().getCargosWeb();
		float cargos_sin_iva = DetallesCompra.getInstance().getBookingFeeWithTaxInCents();
		float iva_cargos = DetallesCompra.getInstance().getTaxInCents();
		
		float total = totalEntradasPagar + cargos + cargosWeb + iva_cargos;
		
			
				
		totalEntradasView = (TextView) getView().findViewById(R.id.totalEntradas_text);
		totalEntradasView.setText(String.valueOf(totalEntradas));
		
		cargosServiciosView = (TextView) getView().findViewById(R.id.cargosServicios_text);
		cargosServiciosView.setText(String.format("Bs. %.2f", cargos_sin_iva));

		cargosServiciosWebView = (TextView) getView().findViewById(R.id.cargosServiciosWeb_text);
		cargosServiciosWebView.setText(String.format("Bs. %.2f", cargosWeb));
		
		iva_text = (TextView) getView().findViewById(R.id.iva_text);
		iva_text.setText(String.format("Bs. %.2f", iva_cargos));
		
		valorEntradaView = (TextView) getView().findViewById(R.id.valorEntradas_text);
		valorEntradaView.setText(String.format("Bs. %.2f", totalEntradasPagar));
		
		totalView = (TextView) getView().findViewById(R.id.total_a_pagar_text);
		totalView.setText(String.format("Bs. %.2f", total));
		
		DetallesCompra.getInstance().setValorEntrada(totalEntradasPagar);
		DetallesCompra.getInstance().setCostoTotal(total);	
				
		
		
		
	}
	
	private class GetSeatsTask extends AsyncTask<String, Void, Integer> {
		 ProgressDialog pd;
		 String cinemaId, performanceId;
		 String asientos;
		 int error = 0;
		 
		 
		 @Override
		protected void onPreExecute() {
			 
			 if(getView() == null){
				 this.cancel(true);
				 return;
			 }
			 
			 cinemaId = DetallesCompra.getInstance().getCineId();
			 performanceId = DetallesCompra.getInstance().getShowTimeId();
			 
			 pd = ProgressDialog.show(getActivity(), "Espere.", "Consultando butacas disponibles.", true, true);				
			 
	    	}
	     
		 @Override
		protected Integer doInBackground(String... params) {
			
			 
			DetallesCompra.getInstance().loadBoletos();
			if(ConnectionManager.getInstance().getLastError() == -1)
				error = -1;
			
		    boletos = DetallesCompra.getInstance().getBoletos();
		         
			//if(ButacasManager.getInstance().LoadSeatsAvailability(cinemaId, performanceId) == false)
			//	return -1;
			
			asientos = String.valueOf(ButacasManager.getInstance().getSeatsAvailability());	
			if(ConnectionManager.getInstance().getLastError() == -1)
				error = -1;
			
			
			if(!peli.posterlsLoaded()){
				peli.setPoster();
			}
			
	        
	         
	        return 0;
	     }	    

	     @Override
		protected void onPostExecute(Integer result) {
	    	 
	    	 if(getView() != null)
	    		 iniciar();
	    	 else
	    		 return;
	    	 
	    	 TextView err = (TextView) getView().findViewById(R.id.textViewError);
	    	 
	    	 if(DetallesCompra.getInstance().fail == true){	    		 
	    	 	 err.setVisibility(View.VISIBLE);
	    		 err.setText("Transacción anterior fallida");
	    		 DetallesCompra.getInstance().fail = false;
	    	 }else
	    		 err.setVisibility(View.GONE);	 
	    	 
	    	 ((TextView)getView().findViewById(R.id.textViewCine)).setText("Butacas disponibles: " + asientos);
	    	 ((ImageView)getView().findViewById(R.id.icon)).setImageDrawable(peli.getPoster());
	    	 try {
		    	 pd.cancel();
		         pd = null;
		        } catch (Exception e) { }
        	   
        	
        	if(error == -1){
        		ConnectionManager.getInstance().mensajeError("Fallo de conexión.");
        		getActivity().getSupportFragmentManager().popBackStack();
        	}
        		
	         
	     }
	 }
	
	/*public void onPause()
	{
		
	    LocalyticsManager.localyticsSession().upload();
	    super.onPause();
	}*/
	@Override
	public void onPause() {
		super.onPause();

		FlurryAgent.onEndSession(getActivity());
	}

	@Override
	public void onStop() {
		super.onStop();

		FlurryAgent.onEndSession(getActivity());
	}

}