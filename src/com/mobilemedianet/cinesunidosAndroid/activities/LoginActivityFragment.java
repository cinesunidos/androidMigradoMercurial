package com.mobilemedianet.cinesunidosAndroid.activities;



import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.mobilemedianet.cinesunidosAndroid.R;
import com.mobilemedianet.cinesunidosAndroid.managers.ConnectionManager;
//import com.mobilemedianet.cinesunidosAndroid.managers.LocalyticsManager;
import com.mobilemedianet.cinesunidosAndroid.managers.LoginManager;
import com.mobilemedianet.cinesunidosAndroid.managers.PrefManager;



public class LoginActivityFragment extends Fragment implements OnClickListener{

	OnLoginListener registrar;
	OnShowTimeSelectedListener showTimeSelected;

	private static final String Flurry_API_Key = "GSW94MXD24S7D546NST6";

static private LoginActivityFragment instance;
	
	

	static public LoginActivityFragment getInstance(){
		
		if(instance == null)
			instance = new LoginActivityFragment();
		return instance;
	}
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
	    super.onCreate(savedInstanceState);

        //Flurry Start Session
        FlurryAgent.init(getActivity(), Flurry_API_Key);
        FlurryAgent.onStartSession(getActivity());
	
	}
	
	public void onResume (){
    	super.onResume();   	

	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		 EditText userView = null;		 
		 
         View viewer = inflater.inflate(R.layout.login, container, false);         
         viewer.findViewById(R.id.botonEnviar).setOnClickListener(this); 
         viewer.findViewById(R.id.registrarView).setOnClickListener(this); 
         
         userView = (EditText) viewer.findViewById(R.id.editText1);
         
         
         userView.setOnClickListener(this);
         
                  
         
        return viewer;
    }   
	
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		EditText userView = null;
		EditText passView = null;
		String user = null;
		
		
		if(arg0.getId() == R.id.botonEnviar){
			
			ConnectionManager.setContext(getActivity());
	    	if(ConnectionManager.checkConn() == false){
	    		
	    		
	    		AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
	    		alertDialog.setTitle("CinesUnidos"); 
	    		alertDialog.setMessage("No hay conexión a internet");
	    		alertDialog.setIcon(R.drawable.icono); 
	    		alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
	    			
	    			@Override
	    			public void onClick(DialogInterface dialog,int which) {
	    				dialog.cancel();
	    				
	    				return;
	    			}
	  	      }); 
	  	      
	  	    
	  	      alertDialog.show(); 
	  		
	  	      return;
	    	}
			
			userView = (EditText) getView().findViewById(R.id.editText1);
			passView = (EditText) getView().findViewById(R.id.editText2);
			
			if(userView.getText().toString().equals("") || passView.getText().toString().equals("")){
				 Toast.makeText(getActivity(), "Debe ingresar usuario y contraseña.", Toast.LENGTH_SHORT).show();
				 return;
			}
			
			user = userView.getText().toString();			
			PrefManager.getInstance().saveUser(user);
			
			new LoginTask().execute();			
						
			
		}else if(arg0.getId() == R.id.editText1){			
//			InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//			imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
		}else if(arg0.getId() == R.id.registrarView){
			registrar.regisrar();			
		}
		
	}
	
	
	
	
//	@Override
// public void onAttach(Activity activity) {
//     super.onAttach(activity);
//     try {
//    	 irAPagar = (OnLoginListener) activity;
//     } catch (ClassCastException e) {
//         throw new ClassCastException(activity.toString()
//                 + " must implement a Listener");
//     }
// }
	
	 public interface OnShowTimeSelectedListener{	   
	       public void showTimeSelected();
	   } 
	 
	 public interface OnLoginListener{
	       public void regisrar();
	   }
	 
	 
	    
	  
	    @Override
	    public void onAttach(Activity activity) {
	        super.onAttach(activity);
	        try {
	        	showTimeSelected = (OnShowTimeSelectedListener) activity;
	        	registrar = (OnLoginListener) activity;
	        } catch (ClassCastException e) {
	            throw new ClassCastException(activity.toString()
	                    + " must implement a Listener");
	        }
	    }
	
	
	private class LoginTask extends AsyncTask<String, Void, Integer> {
		EditText userView = null;
		EditText passView = null;
		String user = null;
		String pass = null;
		boolean success = false;
		ProgressDialog pd;
		 
		 
		 @Override
		protected void onPreExecute() {
			 
			 userView = (EditText) getView().findViewById(R.id.editText1);
			 passView = (EditText) getView().findViewById(R.id.editText2);
			 user = userView.getText().toString();
			 pass = passView.getText().toString();	
			 
			 
			 
			 pd = ProgressDialog.show(getActivity(), "Espere.", "Iniciando sesión.", true, true);
			 
			 
	    	}
	     
		 @Override
		protected Integer doInBackground(String... params) {	 
			 
					success = LoginManager.getInstance().login(user, pass);
					return 0;
	     }	    

	     @Override
		protected void onPostExecute(Integer result) {
	    	 
	    	 if(getView() == null)
	    		 return;
	    	 
	    	 try {
		    	 pd.cancel();
		         pd = null;
		        } catch (Exception e) { }
	    	 if(success == false){	    		 
	    		 Toast.makeText(getActivity(), "Usuario y/o contraseña incorrectos.", Toast.LENGTH_SHORT).show();
	    		 
	    		 return;
	    	 }
	    	 
	     	 /*if(LocalyticsManager.localyticsSession() == null){
	    		LocalyticsManager.newLocalytics(getActivity().getApplicationContext());
	        	LocalyticsManager.localyticsSession().open();
	    	 }*/
	     	
	    	 //LocalyticsManager.localyticsSession().tagEvent("Sesión Iniciada");
			 FlurryAgent.logEvent("Sesión Iniciada");
			 
	    	 InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			 imm.hideSoftInputFromWindow(getView().getWindowToken(),0); 
			 PrefManager.getInstance().saveUserInfo();
			 showTimeSelected.showTimeSelected();
					
	    	 }
	     }	
	
	/*public void onPause()
	{
		
	    LocalyticsManager.localyticsSession().upload();
	    super.onPause();
	}*/

	@Override
	public void onPause() {
		super.onPause();

		FlurryAgent.onEndSession(getActivity());
	}

	@Override
	public void onStop() {
		super.onStop();

		FlurryAgent.onEndSession(getActivity());
	}

}
