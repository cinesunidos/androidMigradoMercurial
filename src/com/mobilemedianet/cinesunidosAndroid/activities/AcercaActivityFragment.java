package com.mobilemedianet.cinesunidosAndroid.activities;

import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.mobilemedianet.cinesunidosAndroid.R;
//import com.mobilemedianet.cinesunidosAndroid.managers.LocalyticsManager;

public class AcercaActivityFragment extends Fragment{
	
	static private AcercaActivityFragment instance;
	private static final String Flurry_API_Key = "GSW94MXD24S7D546NST6";
	
	
	static public AcercaActivityFragment getInstance(){
		
		if(instance == null)
			instance = new AcercaActivityFragment();
		return instance;
	}
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
	    super.onCreate(savedInstanceState);

		//Flurry Start Session
		FlurryAgent.init(getActivity(), Flurry_API_Key);
		FlurryAgent.onStartSession(getActivity());
	}
	
	public void onResume(){
		super.onResume();
		/*if(LocalyticsManager.localyticsSession() == null){
    		LocalyticsManager.newLocalytics(getActivity().getApplicationContext());
        	LocalyticsManager.localyticsSession().open();
    	}*/
		//LocalyticsManager.localyticsSession().tagEvent("AcercaDe");
		FlurryAgent.logEvent("AcercaDe");
		
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		
         View viewer = inflater.inflate(R.layout.acerca_de, container, false);
         
         ViewTreeObserver vto = viewer.getViewTreeObserver();
         vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
			 @Override
			 public void onGlobalLayout() {
				 if (getView() == null)
					 return;

				 TextView textViewVersion = (TextView) getView().findViewById(R.id.textView3);

				 try {
					 String versionName = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
					 if (textViewVersion != null)
						 textViewVersion.setText("Versión " + versionName);
				 } catch (NameNotFoundException e) {
					 // TODO Auto-generated catch block
					 if (textViewVersion != null)
						 textViewVersion.setText("Versión ???");
				 }

				 ViewTreeObserver obs = getView().getViewTreeObserver();
				 obs.removeGlobalOnLayoutListener(this);

			 }
		 });
         
       return viewer;
    }   
	
	
    
	
	/*public void onPause()
	{
		
	    LocalyticsManager.localyticsSession().upload();
	    super.onPause();
	}*/
	@Override
	public void onPause() {
		super.onPause();

		FlurryAgent.onEndSession(getActivity());
	}

	@Override
	public void onStop() {
		super.onStop();

		FlurryAgent.onEndSession(getActivity());
	}



}
