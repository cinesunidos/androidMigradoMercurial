package com.mobilemedianet.cinesunidosAndroid;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.mobilemedianet.cinesunidosAndroid.managers.ApplicationStatus;
import com.mobilemedianet.cinesunidosAndroid.managers.CinesManager;
import com.mobilemedianet.cinesunidosAndroid.managers.MovieManager;
import com.mobilemedianet.cinesunidosAndroid.managers.PrefManager;
import com.mobilemedianet.cinesunidosAndroid.objects.Cines;
import com.mobilemedianet.cinesunidosAndroid.utilities.Metrics;

import java.util.HashMap;
import java.util.Map;

//import com.mobilemedianet.cinesunidosAndroid.managers.LocalyticsManager;

public class SeleccionCiudadesActivity extends Activity {
    /** Called when the activity is first created. */
	SharedPreferences settings;
	
	private boolean first = false;
	private static final String Flurry_API_Key = "GSW94MXD24S7D546NST6";
	
	Map <String,Boolean> seleccion = new HashMap<String,Boolean> ();
	
	
	Dialog dialog;
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		//Flurry Start Session
		FlurryAgent.init(this, Flurry_API_Key);
		FlurryAgent.onStartSession(this);

        Metrics.setContext(this);
        if(PrefManager.getInstance().isAnyCinemaChecked() == false){
        	first = true;
        }else
        	first = false;
        setContentView(R.layout.ciudades);   
        RelativeLayout volver = (RelativeLayout) findViewById(R.id.botonAceptar);
        volver.setOnClickListener(OnClickDoSomething(volver));
        ExpandableListAdapter mAdapter;
        ExpandableListView epView = (ExpandableListView) findViewById(R.id.expandableListView1);
       
        mAdapter = new MyExpandableListAdapter();
        epView.setAdapter(mAdapter);  
        
        String cinemastring = PrefManager.getInstance().getCinemaString();
        
    	if(cinemastring != ""){
    		String[] cinesid = PrefManager.getInstance().getCinemaString().split(",");
    		
    		for(int i = 0; i < cinesid.length; i++){
    			seleccion.put(cinesid[i], true);
    		}
          		
    	}
       
        
    }
    
    private void alerta(){
    	
    	AlertDialog.Builder alertDialog = new AlertDialog.Builder(this); 
        
        alertDialog.setTitle("CinesUnidos"); 
        
        alertDialog.setMessage("No seleccionó ningún cine.\n¿Desea Salir?");
       
        alertDialog.setIcon(R.drawable.icono); 
       
        alertDialog.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
            @Override
			public void onClick(DialogInterface dialog,int which) { 
            	
            	Intent myIntent = new Intent(SeleccionCiudadesActivity.this, ListaActivity.class);
        		myIntent.putExtra("salir", true);                	
            	myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            	startActivity(myIntent);
            	finish();                    	   
            	
            }
        }); 
        
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
			public void onClick(DialogInterface dialog,    int which) {

            dialog.cancel();
            }
        });
 
       
        alertDialog.show();  
    }
    
    
    
    public class MyExpandableListAdapter extends BaseExpandableListAdapter
	{
      
//    	private String[] groups = {"Barquisimeto",	"Caracas", "Guatire", "Maracaibo", "Maracay", "Margarita",
//                "Maturín", "Puerto La Cruz","Puerto Ordaz", "San Cristóbal", "Valencia"};

    
//    	private String[][] children = {{ "Las Trinitarias", "Sambil Barquisimeto" },
//    		{ "El Marqués", "Líder", "Los Naranjos", "Millennium", "Galerías Ávila", "Galerías Paraíso", "Metrocenter", "Sambil Caracas" , "VISTA1PIR"},
//    		{"Guatire Plaza"}, { "Centro Sur", "Sambil Maracaibo" },  { "Hyper Jumbo", "Las Américas" }, {"Sambil Margarita"},
//    		{"Petroriente"}, {"Regina"}, {"Orinokia"}, {"San Cristóbal"}, { "La Granja", "Metrópolis", "Sambil Valencia" }};


//		private String [] CINES_CARACAS = {"1008", "1027", "1001", "1026", "1020", "1009", "1002", "1005", "1028"};
//		private String [] CINES_VALENCIA = {"1013", "1011", "1014"};
//		private String [] CINES_MARACAY = {"1015", "1010"};
//		private String [] CINES_MARACAIBO = {"1016", "1017"};
//		private String [] CINES_BARQUISIMETO = {"1006", "1025"};
//		private String [] CINES_GUATIRE = {"1003"};
//		private String [] CINES_MATURIN = {"1007"};
//		private String [] CINES_PORLAMAR = {"1021"};
//		private String [] CINES_PUERTOLACRUZ = {"1019"};
//		private String [] CINES_PUERTOORDAZ = {"1023"};
//		private String [] CINES_SANCRISTOBAL = {"1024"};

		@Override
		public Object getChild(int groupPosition, int childPosition) {

			return CinesManager.getInstance().getCine(groupPosition, childPosition);
			//return children[groupPosition][childPosition];
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return childPosition;
		}

		@Override
		public int getChildrenCount(int groupPosition)
		{
			int i = 0;
			try {
				i = CinesManager.getInstance().getCantidadCinesCiudad(groupPosition);
			//i = children[groupPosition].length;

			} catch (Exception e) {
			}

			return i;
		}

		public TextView getGenericView() {
			// Layout parameters for the ExpandableListView
			AbsListView.LayoutParams lp = new AbsListView.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT, Metrics.dpToPixel(60));

			TextView textView = new TextView(SeleccionCiudadesActivity.this);
			textView.setLayoutParams(lp);
			// Center the text vertically
			textView.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
			//textView.setTextColor(R.color.marcyred);
			// Set the text starting position
			textView.setPadding(Metrics.dpToPixel(50), 0, 0, 0);
			return textView;
		}

		@Override
		public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

			AbsListView.LayoutParams lp = new AbsListView.LayoutParams(
					ViewGroup.LayoutParams.MATCH_PARENT, Metrics.dpToPixel(60));

			String key = String.valueOf(groupPosition) + "-" + String.valueOf(childPosition);

			CheckBox textView = new CheckBox(SeleccionCiudadesActivity.this);

			textView.setText(getChild(groupPosition, childPosition).toString());

			settings = getApplicationContext().getSharedPreferences("cines", 0);

		   /* if(settings.getBoolean(getCineId(groupPosition, childPosition), false))
				textView.setChecked(true);      */
			//TODO aqui hay que revisar si es true o false


			if(seleccion.get(CinesManager.getInstance().getCineId(groupPosition, childPosition)) != null){
				if(seleccion.get(CinesManager.getInstance().getCineId(groupPosition, childPosition)) == true)
					textView.setChecked(true);
				else
					textView.setChecked(false);
			}else
				textView.setChecked(false);


//			if(seleccion.get(getCineId(groupPosition, childPosition)) != null){
//				if(seleccion.get(getCineId(groupPosition, childPosition)) == true)
//					textView.setChecked(true);
//				else
//					textView.setChecked(false);
//			}else
//				textView.setChecked(false);



			textView.setLayoutParams(lp);

			textView.setTag(key);

			textView.setOnCheckedChangeListener(checkboxListener);

			return textView;
		}

		@Override
		public Object getGroup(int groupPosition) {

			return CinesManager.getInstance().getCiudades()[groupPosition];
			//return groups[groupPosition];
		}

		@Override
		public int getGroupCount() {
			return CinesManager.getInstance().getCiudades().length;
			//return groups.length;
		}

		@Override
		public long getGroupId(int groupPosition) {
			return groupPosition;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
			TextView textView = getGenericView();
			textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
			textView.setText(getGroup(groupPosition).toString());
			return textView;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

		private OnCheckedChangeListener checkboxListener = new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				String positions = buttonView.getTag().toString();
				int groupPosition = Integer.valueOf(positions.split("-")[0]);
				int childPosition = Integer.valueOf(positions.split("-")[1]);
				//seleccion.put(getCineId(groupPosition, childPosition), isChecked);
				seleccion.put(CinesManager.getInstance().getCineId(groupPosition, childPosition), isChecked);


				//PrefManager.getInstance().setCinemaStatus(getCineId(groupPosition, childPosition), isChecked);

			}
		};

//		private String getCineId(int parent, int child){
//
//
//
//
//			switch (parent){
//
//			case 0 :
//				return CINES_BARQUISIMETO[child];
//
//			case 1 :
//				return CINES_CARACAS[child];
//
//			case 2 :
//				return CINES_GUATIRE[child];
//
//			case 3 :
//				return CINES_MARACAIBO[child];
//
//			case 4 :
//				return CINES_MARACAY[child];
//
//			case 5 :
//				return CINES_PORLAMAR[child];
//
//			case 6 :
//				return CINES_MATURIN[child];
//
//			case 7 :
//				return CINES_PUERTOLACRUZ[child];
//
//			case 8 :
//				return CINES_PUERTOORDAZ[child];
//
//			case 9 :
//				return CINES_SANCRISTOBAL[child];
//
//			case 10 :
//				return CINES_VALENCIA[child];
//
//
//			};
//
//			return null;
//
//		}
    

    }
    
    
//    private String getCineZone(int parent){
//
//    	switch (parent){
//
//    	case 0 :
//    		return "BQM";
//
//    	case 1 :
//    		return "CCS,CCSE";
//
//
//    	case 2 :
//    		return "GUA";
//
//    	case 3 :
//    		return "MBO";
//
//    	case 4 :
//    		return "MCA";
//
//    	case 5 :
//    		return "POR";
//
//    	case 6 :
//    		return "MAT";
//
//    	case 7 :
//    		return "PLC";
//
//    	case 8 :
//    		return "PDZ";
//
//    	case 9 :
//    		return "SCR";
//
//    	case 10 :
//    		return "VAL";
//
//
//    	};
//
//		return null;
//
//
//    }
    
    @Override
    public void onBackPressed() {

    	if(first){
    		Intent myIntent = new Intent(SeleccionCiudadesActivity.this, SplashActivity.class);
    		startActivity(myIntent);
        	finish();         	
    	}else{
    		Intent myIntent = new Intent(SeleccionCiudadesActivity.this, ListaActivity.class);
    		//MovieManager.getInstance().needToRefresh = true;
        	myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        	startActivity(myIntent);
        	finish();      
    	}
    }
    
    View.OnClickListener OnClickDoSomething(final View v)  {
        return new View.OnClickListener() {
            @Override
			public void onClick(View v) {  
            	
            	int count = 0;
            	String []cinesid = null;
            	String cines = "";
            	
            	for (Map.Entry<String, Boolean> entry : seleccion.entrySet()) {
            	   
            	    if(entry.getValue())
            	    	count++;
                    	                	    
            	}
            	
            	if(count == 0){            		
            		alerta();
            		return;
            	}else if(count == 0 && seleccion.isEmpty()){
            		Intent myIntent = new Intent(SeleccionCiudadesActivity.this, ListaActivity.class);            		         	
                	myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                	startActivity(myIntent);
                	finish();              
                	return;
            	}            	
       		
            	
            	for (Map.Entry<String, Boolean> entry : seleccion.entrySet()) {
            	    String key = entry.getKey();
            	    Boolean value = entry.getValue(); 
            	    PrefManager.getInstance().setCinemaStatus(key, value); 
            	}
           	
                String cinemastring = PrefManager.getInstance().getCinemaString();
            	
            	if(cinemastring != "")
            		cinesid = cinemastring.split(",");
            	else{
            		alerta();
            		return;            		
            	}
            	
            	for(int i = 0; i < cinesid.length; i++){
            		cines += Cines.getCineById(cinesid[i])[1] + ", ";
            	}
            	
            	cines = cines.substring(0, cines.length() - 2);             	
            	
            	
            	//if(LocalyticsManager.localyticsSession() == null)
            		//LocalyticsManager.newLocalytics(getApplicationContext());
            	
            	//LocalyticsManager.localyticsSession().open();
            	
            	Map <String,String> values = new HashMap<String,String> ();
            	
      	    	values.put("Cantidad", String.valueOf(PrefManager.getInstance().getCinemaChecked()));
      	    	values.put("Cines", cines);
      	    	
            	if(first){
            		//LocalyticsManager.localyticsSession().tagEvent("Cines Favoritos Inicial", values);
					//FlurryAgent.logEvent("Cines Favoritos Inicial");
            		Intent myIntent = new Intent(SeleccionCiudadesActivity.this, SplashActivity.class);
            		startActivity(myIntent);
                	finish();
                	return;
            	}else{
            		//LocalyticsManager.localyticsSession().tagEvent("Cambio de Cines Favoritos", values);
					//FlurryAgent.logEvent("Cambio de Cines Favoritos");
            		Intent myIntent = new Intent(SeleccionCiudadesActivity.this, ListaActivity.class);
            		MovieManager.getInstance().needToRefresh = true;                	
                	myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                	startActivity(myIntent);
                	finish();  
                	return;
            	}
            	
            }
        };
    }
    
    @Override
    public void onResume(){    	    	   	
    	super.onResume();
    	
    	if(first){
    		    		
    		dialog = new Dialog(SeleccionCiudadesActivity.this);
    		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog);
            //dialog.setCancelable(true);
           
            Button button = (Button) dialog.findViewById(R.id.button1);
            button.setOnClickListener(new OnClickListener() {
            @Override
                public void onClick(View v) {
                   dialog.cancel();
                }
            });

            dialog.show();
    	}
    	
    	if(ApplicationStatus.getStatus() == false){
    		Intent reLaunchMain=new Intent(this, SplashActivity.class);
    		reLaunchMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    		startActivity(reLaunchMain);
    	}    	

 	    
 	   // LocalyticsManager.localyticsSession().tagEvent("Seleecion de Ciudades Favoritas");
		//FlurryAgent.logEvent("Seleccion de Ciudades Favoritas");
 	    
    }
    
    /*@Override
	public void onPause()
	{
    	
	    LocalyticsManager.localyticsSession().upload();
	    super.onPause();
	}*/


	@Override
	protected void onPause() {
		super.onPause();

		FlurryAgent.onEndSession(this);
	}

	@Override
	protected void onStop() {
		super.onStop();

		FlurryAgent.onEndSession(this);
	}
    
    
    
}