package com.mobilemedianet.cinesunidosAndroid.utilities;


public class Contador {
	
	static public Contador instance = null;
//	private MyCount contador = null; 
	
	private long timer;
	
	
	static public Contador getInstance(){
		if(instance == null)
			instance = new Contador();
		return instance;
	}
	
	public void setTimer(long i){
		timer = i;
	}
	
	public long getTimer(){
		return timer;
	}
	
//	public void cancelTimer(){
//		if(contador == null)
//			return;
//		
//		contador.cancel();
//		contador = null;
//		
//	}
	
	
	
//	public class MyCount extends CountDownTimer{
//		
//		public MyCount(long millisInFuture, long countDownInterval) {			
//			super(millisInFuture, countDownInterval);
//			
//		}
//		
//		@Override
//		public void onTick(long millisUntilFinished) {
//			int seconds = (int) (millisUntilFinished / 1000);
//			int minutes = seconds / 60;
//			seconds     = seconds % 60;
//			TextView text;
//			try {
//			 text = (TextView) context.getView().findViewById(R.id.Timer);
//			} catch (Exception e){
//			    return;
//			  }
//			
//						
//			text.setText(String.format("Selección de butacas - Tiempo: %d:%02d", minutes, seconds));
//			Contador.getInstance().setTimer(millisUntilFinished);
//			
//		}
//		
//		@Override
//		public void onFinish() {
//			TextView text;
//			try {
//				 text = (TextView) context.getView().findViewById(R.id.Timer);
//				} catch (Exception e){
//				    return;
//				  }		
//			
//			
//			text.setText("Selección de butacas - Tiempo Agotado.");
//			AlertDialog.Builder alertDialog = new AlertDialog.Builder(context.getActivity());			        
//	        alertDialog.setTitle("CinesUnidos"); 			        
//	        alertDialog.setMessage("Se ha agotado el tiempo de reserva, vuelva a empezar."); 			       
//	        alertDialog.setIcon(R.drawable.icono);
//	        alertDialog.setCancelable(false);
//	        alertDialog.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
//	            public void onClick(DialogInterface dialog,    int which) {
//	            	dialog.cancel();
//	            	getActivity().getSupportFragmentManager().popBackStack("comprar", 0);
//	            }
//	        });
//	        alertDialog.show();	        
//	        
//	        
//		}
//		}
//	
//	
	
	
}
