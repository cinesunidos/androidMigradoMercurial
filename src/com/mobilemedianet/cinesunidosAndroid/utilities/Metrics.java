package com.mobilemedianet.cinesunidosAndroid.utilities;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.WindowManager;

public class Metrics {
	
	static Context context;
	
	public static void setContext(Context c){
		context = c;
	}
	
	public static int dpToPixel(int dp){
		
		
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);

        DisplayMetrics metrics = new DisplayMetrics();

        wm.getDefaultDisplay().getMetrics(metrics);
        
		return (int) (dp*(metrics.densityDpi/160f));
		
	}	


}
