package com.mobilemedianet.cinesunidosAndroid.utilities;


import java.util.Hashtable;

public class Zonas {
	
	private Hashtable<String,String> zonasPublicidad;
	
	public Zonas(){}
	
	public void createZones() {
	
		zonasPublicidad = new Hashtable<String,String>();
	
		//ListView
		zonasPublicidad.put("ALL_CCS","370");
		zonasPublicidad.put("ALL_BQM","372");
		zonasPublicidad.put("ALL_COR","373");
		zonasPublicidad.put("ALL_GUA","374");
		zonasPublicidad.put("ALL_MAT","375");
		zonasPublicidad.put("ALL_POR","376");
		zonasPublicidad.put("ALL_PLC","377");
		zonasPublicidad.put("ALL_VAL","378");
		zonasPublicidad.put("ALL_MCA","379");
		zonasPublicidad.put("ALL_MBO","380");
		zonasPublicidad.put("ALL_PDZ","381");
		zonasPublicidad.put("ALL_SCR","382");
		
		// Splash
		zonasPublicidad.put("SPLASH_CCS","419");
        zonasPublicidad.put("SPLASH_BQM","420");
        zonasPublicidad.put("SPLASH_COR","421");
        zonasPublicidad.put("SPLASH_GUA","422");
        zonasPublicidad.put("SPLASH_MAT","423");
        zonasPublicidad.put("SPLASH_POR","424");
        zonasPublicidad.put("SPLASH_PLC","425");
        zonasPublicidad.put("SPLASH_VAL","426");
        zonasPublicidad.put("SPLASH_MCA","427");
        zonasPublicidad.put("SPLASH_MBO","428");
        zonasPublicidad.put("SPLASH_PDZ","429");
        zonasPublicidad.put("SPLASH_SCR","430");
       
        // PostCompra
        zonasPublicidad.put("POST_CCS","431");
        zonasPublicidad.put("POST_BQM","432");
        zonasPublicidad.put("POST_COR","433");
        zonasPublicidad.put("POST_GUA","434");
        zonasPublicidad.put("POST_MAT","435");
        zonasPublicidad.put("POST_POR","436");
        zonasPublicidad.put("POST_PLC","437");
        zonasPublicidad.put("POST_VAL","438");
        zonasPublicidad.put("POST_MCA","439");
        zonasPublicidad.put("POST_MBO","440");
        zonasPublicidad.put("POST_PDZ","441");
        zonasPublicidad.put("POST_SCR","442");
        
        // Recordatorio
        zonasPublicidad.put("REMINDER_CCS","443");
        zonasPublicidad.put("REMINDER_BQM","444");
        zonasPublicidad.put("REMINDER_COR","445");
        zonasPublicidad.put("REMINDER_GUA","446");
        zonasPublicidad.put("REMINDER_MAT","447");
        zonasPublicidad.put("REMINDER_POR","448");
        zonasPublicidad.put("REMINDER_PLC","449");
        zonasPublicidad.put("REMINDER_VAL","450");
        zonasPublicidad.put("REMINDER_MCA","451");
        zonasPublicidad.put("REMINDER_MBO","452");
        zonasPublicidad.put("REMINDER_PDZ","453");
        zonasPublicidad.put("REMINDER_SCR","454");
	}
	
	public String getZona(String key) {
		return zonasPublicidad.get(key);
	}
}