package com.mobilemedianet.cinesunidosAndroid.utilities;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

public class Fechas {
	
	static String []meses = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", 
				"Octubre", "Noviembre", "Diciembre"};
	
	public static String []meses1 = {"Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep",	"Oct",
				"Nov", "Dic"};
	
	static int day;
	static int month;
	static int year;
	static int hour;
	
	
	static private void setValues(int dias){
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, dias);
		day = calendar.get(Calendar.DAY_OF_MONTH);
		month = calendar.get(Calendar.MONTH);
		year = calendar.get(Calendar.YEAR);
		hour = calendar.get(Calendar.HOUR_OF_DAY);
		
	}
	
	static public String getStringDate(int dia, int mes, int anio){
		day = dia;
		month = mes;
		year = anio;
		
		return String.valueOf(day) + " de " + meses1[month] + ". de " + String.valueOf(year);	
		
		
		
	}
	
	public static String fechaCompleta(int dias){
		
		setValues(dias);			
		
		String fecha = String.valueOf(day) + " de " + meses[month] + " de " + String.valueOf(year);		
		return fecha;
		
	}
	
	public static String fechaNormal(int dias){
		
		setValues(dias);
		
		String fecha = String.valueOf(day) + "/" + String.valueOf(month + 1) + "/" + String.valueOf(year);		
		return fecha;		
		
	}
	
	public static String getFecha(){		
		setValues(0);
		
		String fecha = day + "/" + month + "/" + year + "-" + hour;
		Log.i("fecha", fecha);		
		return fecha;		
	}	


	public static String getYearMonthDayJsonDate(String ackwardDate)
	{
		Calendar calendar = Calendar.getInstance();

		//String[] datePieces = ackwardDate.split('\+');

		String ackwardRipOff = ackwardDate.replace("/Date(", "").replace("+0000)/", "");
		Long timeInMillis = Long.valueOf(ackwardRipOff);


		calendar.setTimeInMillis(timeInMillis);
		Log.e("Fechas.java","Fecha a parsear " + ackwardDate + " " + timeInMillis);
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		format1.setTimeZone(TimeZone.getTimeZone("UTC"));
		return format1.format(calendar.getTime());
	}

	public static String getYearMonthDayTimeJsonDate(String ackwardDate)
	{
		Calendar calendar = Calendar.getInstance();

		//String[] datePieces = ackwardDate.split('\+');

		String ackwardRipOff = ackwardDate.replace("/Date(", "").replace("+0000)/", "");
		Long timeInMillis = Long.valueOf(ackwardRipOff);


		calendar.setTimeInMillis(timeInMillis);
		//Log.e("Fechas.java","Fecha a parsear " + ackwardDate + " " + timeInMillis);
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd h:mm:ss a");
		format1.setTimeZone(TimeZone.getTimeZone("UTC"));
		return format1.format(calendar.getTime());
	}
	

}
