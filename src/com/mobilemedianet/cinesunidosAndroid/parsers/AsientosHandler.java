package com.mobilemedianet.cinesunidosAndroid.parsers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.mobilemedianet.cinesunidosAndroid.managers.ButacasManager;
import com.mobilemedianet.cinesunidosAndroid.objects.Boleto;

public class AsientosHandler extends DefaultHandler{
	
	    private Boleto boleto;	    
	    private StringBuilder sbTexto;
	   
	  
	    @Override
	    public void characters(char[] ch, int start, int length)
	                   throws SAXException {
	 
	        super.characters(ch, start, length);
	 
	        if (this.boleto != null)
	        	sbTexto.append(ch, start, length);
	    }
	    
	    @Override
	    public void startElement(String uri, String localName,
	                   String name, Attributes attributes) throws SAXException {
	 
	        super.startElement(uri, localName, name, attributes);
	 
	        if (localName.equals("Performance")) {
	        	
	        	ButacasManager.getInstance().setSeatsAvailability(Integer.parseInt(attributes.getValue("SeatsAvailable")));
	        		        	     	 
	        }else if(localName.equals("Variable")){
	        	
	        }
	        sbTexto.setLength(0);
	    }
	 
	    @Override
	    public void endElement(String uri, String localName, String name)
	                   throws SAXException {
	 
	        super.endElement(uri, localName, name);
	 
	        if (this.boleto != null) {
	 
	            if (localName.equals("TicketType")) {
	            	
	            	
	            	
	            }
	 
	            sbTexto.setLength(0);
	        }
	    }
	 
	    @Override
	    public void startDocument() throws SAXException {
	 
	        super.startDocument();
	 	       
	        sbTexto = new StringBuilder();
	        
	    }
	 
	   

}
