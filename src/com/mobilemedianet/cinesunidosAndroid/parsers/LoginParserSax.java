package com.mobilemedianet.cinesunidosAndroid.parsers;

import java.io.InputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import com.mobilemedianet.cinesunidosAndroid.managers.ConnectionManager;
import com.mobilemedianet.cinesunidosAndroid.objects.InformacionUsuario;

public class LoginParserSax {
	
//	private URL rssUrl;
	String url;
	 
    public LoginParserSax(String url)
    {
    	this.url = url;
    }
 
    public int parse(InformacionUsuario usuario)
    {
        SAXParserFactory factory = SAXParserFactory.newInstance();
 
        InputStream stream = this.getInputStream();
        
        if(stream == null)
        	return -1;
        
        try
        {
            SAXParser parser = factory.newSAXParser();
            LoginHandler handler = new LoginHandler();
            handler.getParsed(usuario);
            parser.parse(this.getInputStream(), handler);
            
            
        }
        catch (Exception e)
        {
            return -1;
        }
        return 0;
    }
 
    private InputStream getInputStream(){
    	
        
        return ConnectionManager.getInstance().getConnection(url);
   
}

}
