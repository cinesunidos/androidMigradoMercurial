package com.mobilemedianet.cinesunidosAndroid.parsers;

import android.util.Log;

import com.mobilemedianet.cinesunidosAndroid.objects.Movie;
import com.mobilemedianet.cinesunidosAndroid.objects.ShowTime;
import com.mobilemedianet.cinesunidosAndroid.objects.Theaters;
import com.mobilemedianet.cinesunidosAndroid.utilities.Fechas;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MovieParserJson {

    private String TAG = this.getClass().getSimpleName();

    private List<Movie> peliculas;
	private InputStream file;
    private InputStream fileJson;

    public MovieParserJson()
    {
        peliculas = new ArrayList<Movie>();
    }
 
    public List<Movie> parse(String jsonRaw)
    {
        try {
            //JSONObject jsonObj = new JSONObject(jsonRaw);

            JSONArray jsonMovies = new JSONArray(jsonRaw);
            // Getting JSON Array node
            //JSONArray contacts = jsonObj.getJSONArray("contacts");

            // looping through All Contacts
            int ranking = jsonMovies.length()+1;
            for (int i = 0; i < jsonMovies.length(); i++)
            {

                JSONObject c = jsonMovies.getJSONObject(i);

                JSONObject movieObject = c.getJSONObject("Movie");

                String actor    = movieObject.getString("Actor");
                String country  = movieObject.getString("Country");
                String director = movieObject.getString("Director");
                String id       = movieObject.getString("Id");
                String sinopsis = movieObject.getString("Sinopsis");
                String title    = movieObject.getString("Title");
                //String movieDate = movieObject.getString("Date");

                // Information node is JSON Object
                JSONObject information = movieObject.getJSONObject("Information");
                String infoClass = information.getString("Class");
                String infoFormat = information.getString("Format");
                String gender = information.getString("Gender");
                String subtitle = information.getString("Subtitle");
                String time = information.getString("Time");
                String trailer = information.getString("Trailer");
                String web = information.getString("Web");


                Movie peli = new Movie(id);
                //peli.setPremier(true;);
                peli.setStarring(actor);
                //peli.setFirstExhibit(Fechas.getYearMonthDayJsonDate(movieDate));
                peli.setSinopsis(sinopsis);
                peli.setDirector(director);
                peli.setId(id);
                peli.setHO(id);
                peli.setSpanishTitle(title);
                peli.setOriginalTitle(title);
                peli.setDurationString(time);
                peli.setGenere(gender);
                peli.setDetailsLoaded(true);
                peli.setRanking(ranking);
                ranking--;


                JSONArray theatersObject = c.getJSONArray("Theaters");

                for (int j = 0; j < theatersObject.length(); j++)
                {
                    JSONObject theatersSessionsObject = theatersObject.getJSONObject(j);


                    if (theatersSessionsObject.has("Sessions")) {
                        JSONArray sessionsArrayObject = theatersSessionsObject.getJSONArray("Sessions");
                        JSONObject theaterObject = theatersSessionsObject.getJSONObject("Theater");

                        String idTheater = theaterObject.getString("Id");

                        Theaters sala = new Theaters();
                        sala.setCensura("");
                        sala.setCineId(Integer.valueOf(idTheater));
                        sala.setSalaId(0);

                        for (int k = 0; k < sessionsArrayObject.length(); k++) {
                            JSONObject sessionObject = sessionsArrayObject.getJSONObject(k);

                            String hallName = sessionObject.getString("HallName");
                            String hallNumber = sessionObject.getString("HallNumber");
                            String idSession = sessionObject.getString("Id");
                            String showtimeString = sessionObject.getString("ShowTime");

                            ShowTime showTime = new ShowTime();
                            showTime.setShowTimeId(Integer.valueOf(idSession));
                            showTime.setHorario(Fechas.getYearMonthDayTimeJsonDate(showtimeString));
                            sala.setNombreSala(hallName);
                            sala.addShowTime(showTime);
                        }
                        peli.addSala(sala);
                    }
                }



                peliculas.add(peli);

            }

            return peliculas;
        }
        catch (final JSONException e)
        {
            Log.e(TAG, "Json parsing error: " + e.getMessage());
            return null;
        }
    }



    public List<Movie> getMovies(){
        return peliculas;
    }
}


