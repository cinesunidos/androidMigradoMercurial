package com.mobilemedianet.cinesunidosAndroid.parsers;

import com.mobilemedianet.cinesunidosAndroid.managers.DetallesCompra;
import com.mobilemedianet.cinesunidosAndroid.objects.Boleto;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class BoletosHandler extends DefaultHandler{
	
	    private List<Boleto> boletos;
	    private Boleto boleto;	    
	    private StringBuilder sbTexto;
	   
	 
	    
	    public List<Boleto> getBoletos(){
	        return boletos;
	    }
	 
	    @Override
	    public void characters(char[] ch, int start, int length)
	                   throws SAXException {
	 
	        super.characters(ch, start, length);
	 
	        if (this.boleto != null)
	        	sbTexto.append(ch, start, length);
	    }
	    
	    @Override
	    public void startElement(String uri, String localName,
	                   String name, Attributes attributes) throws SAXException {
	 
	        super.startElement(uri, localName, name, attributes);
	 
	        if (localName.equals("TicketType")) {
	        	boleto = new Boleto();
	        	
	        	float value;
	        	
	        	boleto.setId(attributes.getValue("Id"));	
	        	value =  Float.parseFloat(attributes.getValue("PriceInCents").replace(",","."))/100;
	        	boleto.setPrice(value);
	        	value = Float.parseFloat(attributes.getValue("BookingFeeInCents").replace(",","."))/100;
	        	//boleto.setFee(value);
	        	value = Float.parseFloat(attributes.getValue("BookingFeeWithTaxInCents").replace(",","."))/100;
	        	//boleto.setBookingFeeWithTaxInCents(value);
	        	value = Float.parseFloat(attributes.getValue("TaxInCents").replace(",","."))/100;
	        	//boleto.setTaxInCents(value);
	        	
	        }else if(localName.equals("Variable")){
	        	if(attributes.getValue("Name").equals("ReservedSeats")){
	        		DetallesCompra.getInstance().setReservedSeats(Integer.parseInt(attributes.getValue("Value")));	        		
	        	}else if(attributes.getValue("Name").equals("BookingPerTrans")){
	        		DetallesCompra.getInstance().setBookingPerTrans(Integer.parseInt(attributes.getValue("Value")));
	        	}
	        }
	        sbTexto.setLength(0);
	    }
	 
	    @Override
	    public void endElement(String uri, String localName, String name)
	                   throws SAXException {
	 
	        super.endElement(uri, localName, name);
	 
	        if (this.boleto != null) {
	 
	            if (localName.equals("TicketType")) {
	            	
	            	boleto.setDescripcion(sbTexto.toString());
	            	boletos.add(boleto);
	            	
	            }
	 
	            sbTexto.setLength(0);
	        }
	    }
	 
	    @Override
	    public void startDocument() throws SAXException {
	 
	        super.startDocument();
	 
	        boletos = new ArrayList<Boleto>();
	        sbTexto = new StringBuilder();
	        
	    }
	 
	   

}
