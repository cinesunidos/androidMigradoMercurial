package com.mobilemedianet.cinesunidosAndroid.parsers;

import java.io.InputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import com.mobilemedianet.cinesunidosAndroid.managers.ConnectionManager;

public class AsientosParserSax {
	
	
	private String url;
	 
    public AsientosParserSax(String url)
    {
    	this.url = url;
//        try
//        {
//            this.rssUrl = new URL(url);
//        }
//        catch (MalformedURLException e)
//        {
//            throw new RuntimeException(e);
//        }
    }
 
    public int parse()
    {
        SAXParserFactory factory = SAXParserFactory.newInstance();
 
        InputStream stream = this.getInputStream();
        if(stream == null)
        	return -1;
        
        try
        {
            SAXParser parser = factory.newSAXParser();
            AsientosHandler handler = new AsientosHandler();
            InputStream conexion = this.getInputStream();
            if(conexion == null){
            	
            }
            parser.parse(stream, handler);
            return 0;
        }
        catch (Exception e)
        {
            return -1;
        }
    }
 
    private InputStream getInputStream()
    {
//        try
//        {
//            return rssUrl.openConnection().getInputStream();
//        }
//        catch (IOException e)
//        {
//            throw new RuntimeException(e);
//        }
    	return ConnectionManager.getInstance().getConnection(url);
    }

}
