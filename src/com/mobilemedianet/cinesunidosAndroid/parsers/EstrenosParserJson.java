package com.mobilemedianet.cinesunidosAndroid.parsers;

import android.util.Log;

import com.mobilemedianet.cinesunidosAndroid.objects.Movie;
import com.mobilemedianet.cinesunidosAndroid.utilities.Fechas;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;



public class EstrenosParserJson {

    private String TAG = EstrenosParserJson.class.getSimpleName();
    private List<Movie> peliculas;

    public EstrenosParserJson()
    {
        peliculas = peliculas = new ArrayList<Movie>();

    }
 
    public List<Movie> parse(String jsonRaw)
    {
        try {
            //JSONObject jsonObj = new JSONObject(jsonRaw);

            JSONArray jsonMovies = new JSONArray(jsonRaw);
            // Getting JSON Array node
            //JSONArray contacts = jsonObj.getJSONArray("contacts");

            // looping through All Contacts
            for (int i = 0; i < jsonMovies.length(); i++)
            {



                JSONObject c = jsonMovies.getJSONObject(i);



                String actor = c.getString("Actor");
                String country = c.getString("Country");
                String director = c.getString("Director");
                String id = c.getString("Id");
                String sinopsis = c.getString("Sinopsis");
                String title = c.getString("Title");
                String movieDate = c.getString("Date");






                // Information node is JSON Object
                JSONObject information = c.getJSONObject("Information");
                String infoClass = information.getString("Class");
                String infoFormat = information.getString("Format");
                String gender = information.getString("Gender");
                String subtitle = information.getString("Subtitle");
                String time = information.getString("Time");
                String trailer = information.getString("Trailer");
                String web = information.getString("Web");


                Movie peli = new Movie(id);
                //peli.setPremier(true;);
                peli.setStarring(actor);
                peli.setFirstExhibit(Fechas.getYearMonthDayJsonDate(movieDate));
                peli.setSinopsis(sinopsis);
                peli.setDirector(director);
                peli.setId(id);
                peli.setHO(id);
                peli.setSpanishTitle(title);
                peli.setOriginalTitle(title);
                peli.setDurationString(time);
                peli.setGenere(gender);
                peli.setDetailsLoaded(true);

                peliculas.add(peli);

            }

            return peliculas;
        }
        catch (final JSONException e)
        {
            Log.e(TAG, "Json parsing error: " + e.getMessage());
            return null;
        }


    }
}
