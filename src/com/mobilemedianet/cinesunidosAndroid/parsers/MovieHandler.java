package com.mobilemedianet.cinesunidosAndroid.parsers;

import android.util.Log;

import com.mobilemedianet.cinesunidosAndroid.objects.Movie;
import com.mobilemedianet.cinesunidosAndroid.objects.ShowTime;
import com.mobilemedianet.cinesunidosAndroid.objects.Theaters;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class MovieHandler extends DefaultHandler{
	
	    private List<Movie> peliculas;
	    private Movie peli;
	    private Theaters sala;
	    private ShowTime show;
	    private StringBuilder sbTexto;
	    private boolean flag;
	    private int index;
	 
	    public List<Movie> getMovies(){
	        return peliculas;
	    }
	    
	    public MovieHandler(int i){
	    	index = i;
	    }
	 
	    @Override
	    public void characters(char[] ch, int start, int length)
	                   throws SAXException {
	 
	        super.characters(ch, start, length);
	 
	        if (this.peli != null)
	        	sbTexto.append(ch, start, length);
	    }
	    
	    @Override
	    public void startElement(String uri, String localName,
	                   String name, Attributes attributes) throws SAXException {
	 
	        super.startElement(uri, localName, name, attributes);
	 
	        if (localName.equals("Movie")) {
//	            //peli = new Movie(index);
	            //peli.setId(Integer.parseInt(attributes.getValue("I")));
	            peli.setRanking((Integer.parseInt(attributes.getValue("R"))));
	            peli.setHO((attributes.getValue("HO").toString()));
	            sbTexto.setLength(0);
	        }else if(localName.equals("T") && attributes.getLength() != 0){
	        	sala = new Theaters();	        	
	        	sala.setCineId(Integer.parseInt(attributes.getValue("I")));
	        	sala.setSalaId(Integer.parseInt(attributes.getValue("H")));
	        	sala.setCensura(attributes.getValue("R"));
	        	sbTexto.setLength(0);
	        	flag = true;
	        }else if(localName.equals("F")){
	        	show = new ShowTime();	        	
	        }
	        sbTexto.setLength(0);
	    }
	 
	    @Override
	    public void endElement(String uri, String localName, String name)
	                   throws SAXException {
	 
	        super.endElement(uri, localName, name);
	 
	        if (this.peli != null) {
	 
	            if (localName.equals("N")) {
	            	
	            	peli.setSpanishTitle(sbTexto.toString());
	            	
	            }else if (localName.equals("T")) {
	            	
	            	if(flag == false){
	            		Log.i("Titulo original", sbTexto.toString());
	            		peli.setOriginalTitle(sbTexto.toString());
	            	}else{
	            		peli.addSala(sala);
	            		flag = false;
	            	}
	            	
	            }else if (localName.equals("IF3D")) {
	            	
	            	if(sbTexto.toString().equals("1"))
	            		peli.setFor3D(true);
	            	else
	            		peli.setFor3D(false);
	            	
	            } else if (localName.equals("L")) {
	            	
	            	peli.setLanguage(sbTexto.toString());
	            	
	            } else if (localName.equals("I")) {
	            	
	            	show.setShowTimeId(Integer.parseInt(sbTexto.toString()));
	            	
	            } else if (localName.equals("P")) {
	            	
	            	show.setHorario(sbTexto.toString());
	            	
	            } else if (localName.equals("F")) {
	            	
	            	sala.addShowTime(show);
	            	
	            }else if (localName.equals("Movie")) {
	            	
//	            	peli.setPoster();
					//Log.d("MovieHandler: ", "" + peli.getOriginalTitle());
	            	peliculas.add(peli);
	            	
	            } 
	 
	            sbTexto.setLength(0);
	        }
	    }
	 
	    @Override
	    public void startDocument() throws SAXException {
	 
	        super.startDocument();
	 
	        peliculas = new ArrayList<Movie>();
	        sbTexto = new StringBuilder();
	        flag = false;
	    }
	 
	   

}
