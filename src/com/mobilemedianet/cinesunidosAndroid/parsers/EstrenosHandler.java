package com.mobilemedianet.cinesunidosAndroid.parsers;

import com.mobilemedianet.cinesunidosAndroid.objects.Movie;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class EstrenosHandler extends DefaultHandler{
	
	    private List<Movie> peliculas;
	    private Movie peli;	    
	    private StringBuilder sbTexto;	   
	    private String index;
	 
	    public List<Movie> getMovies(){
	        return peliculas;
	    }
	    
	    public EstrenosHandler(){
	    	
	    }
	 
	    @Override
	    public void characters(char[] ch, int start, int length)
	                   throws SAXException {
	 
	        super.characters(ch, start, length);
	 
	        if (this.peli != null)
	        	sbTexto.append(ch, start, length);
	    }
	    
	    @Override
	    public void startElement(String uri, String localName,
	                   String name, Attributes attributes) throws SAXException {
	 
	        super.startElement(uri, localName, name, attributes);
	 
	        if (localName.equals("MoviePremier"))
	            peli = new Movie(index);
	            
	        sbTexto.setLength(0);
	    }
	 
	    @Override
	    public void endElement(String uri, String localName, String name)
	                   throws SAXException {
	 
	        super.endElement(uri, localName, name);
	 
	        if (this.peli != null) {
	        	
	        	if(localName.equals("ID")){
	        		//peli.setId(Integer.parseInt(sbTexto.toString()));
	        	}else if(localName.equals("HO")){
	        		peli.setHO(sbTexto.toString());
	        	}else if(localName.equals("SpanishTitle")){
	        		peli.setSpanishTitle(sbTexto.toString());
	        	}else if(localName.equals("Title")){
	        		peli.setOriginalTitle(sbTexto.toString());
	        	}else if(localName.equals("FirstExhibit")){
	        		peli.setFirstExhibit(sbTexto.toString());
	        	}else if(localName.equals("PreSale")){
	        		peli.setPreSale(sbTexto.toString());
	        	}else if(localName.equals("MoviePremier")){
	        		peliculas.add(peli);
	        		peli = null;	        		
	        	}
	            		            	
	            } 
	 
	            sbTexto.setLength(0);
	       
	    }
	 
	    @Override
	    public void startDocument() throws SAXException {
	 
	        super.startDocument();
	 
	        peliculas = new ArrayList<Movie>();
	        sbTexto = new StringBuilder();
	      
	    }
	 
	   

}
