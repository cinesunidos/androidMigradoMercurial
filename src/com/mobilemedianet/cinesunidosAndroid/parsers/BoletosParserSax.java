package com.mobilemedianet.cinesunidosAndroid.parsers;

import java.io.InputStream;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import com.mobilemedianet.cinesunidosAndroid.managers.ConnectionManager;
import com.mobilemedianet.cinesunidosAndroid.objects.Boleto;

public class BoletosParserSax {
	
	
	private String url;
	private InputStream stream = null;
	 
    public BoletosParserSax(String url)
    {
    	this.url = url;
    	
//        try
//        {
//            this.rssUrl = new URL(url);
//        }
//        catch (MalformedURLException e)
//        {
//            throw new RuntimeException(e);
//        }
    }
 
    public List<Boleto> parse()
    {
        SAXParserFactory factory = SAXParserFactory.newInstance();
 
        try
        {
            SAXParser parser = factory.newSAXParser();
            BoletosHandler handler = new BoletosHandler();
            stream = this.getInputStream();
            if(stream == null)
            	return null;
            parser.parse(stream, handler);
            return handler.getBoletos();
        }
        catch (Exception e)
        {
        	return null;
        }
    }
 
    private InputStream getInputStream()
    {
//        try
//        {
//            return rssUrl.openConnection().getInputStream();
//        }
//        catch (IOException e)
//        {
//            throw new RuntimeException(e);
//        }
    	return ConnectionManager.getInstance().getConnection(url);
    }

}
