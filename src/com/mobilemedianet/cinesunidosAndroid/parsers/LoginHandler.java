package com.mobilemedianet.cinesunidosAndroid.parsers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.mobilemedianet.cinesunidosAndroid.objects.InformacionUsuario;

public class LoginHandler extends DefaultHandler{
	
	    private InformacionUsuario usuario;
	    private StringBuilder sbTexto;
	   
	 
	    public void getParsed(InformacionUsuario a){
	        this.usuario = a;
	    }
	 
	    @Override
	    public void characters(char[] ch, int start, int length)
	                   throws SAXException {
	 
	        super.characters(ch, start, length);
	 
	        if (this.usuario != null)
	        	sbTexto.append(ch, start, length);
	    }
	    
	    @Override
	    public void startElement(String uri, String localName,
	                   String name, Attributes attributes) throws SAXException {
	 
	        super.startElement(uri, localName, name, attributes);
	            sbTexto.setLength(0);
	        
	    }
	 
	    @Override
	    public void endElement(String uri, String localName, String name)
	                   throws SAXException {
	 
	        super.endElement(uri, localName, name);
	 
	        if (this.usuario != null) {
	 
	            if (localName.equals("Status")) {	   
	            	
	            	if(sbTexto.toString().equals("true"))
	            		usuario.setStatus(true);
	            	else
	            		usuario.setStatus(false);            		
	             	
	            }else if (localName.equals("Text")) {	
	            	
	            	usuario.setText(sbTexto.toString());
	            	
	            }else if (localName.equals("Birthdate")) {	
	            	
	            	usuario.setBirthdate(sbTexto.toString());
	            	
	            }else if (localName.equals("Sex")) {	
	            	
	            	usuario.setSex(sbTexto.toString());
	            	
	            }else if (localName.equals("Login")) {	
	            	
	            	usuario.setLogin(sbTexto.toString());
	            	
	            }else if (localName.equals("Name")) {	
	            	
	            	usuario.setName(sbTexto.toString());
	            	
	            }else if (localName.equals("LastName")) {	
	            	
	            	usuario.setLastName(sbTexto.toString());
	            	
	            }else if (localName.equals("Phone")) {	
	            	
	            	usuario.setPhone(sbTexto.toString());
	            	
	            }else if (localName.equals("SecureId")) {	
	            	
	            	usuario.setSecureId(sbTexto.toString());
	            	
	            }else if (localName.equals("ExternalId")) {	
	            	
	            	usuario.setExternalId(sbTexto.toString());
	            	
	            }else if (localName.equals("Token")) {	
	            	
	            	usuario.setToken(sbTexto.toString());
	            	
	            }
	           
	        }
	    }
	 
	    @Override
	    public void startDocument() throws SAXException {
	 
	        super.startDocument();
	 	       
	        sbTexto = new StringBuilder();
	       
	    }
	 
	   

}
