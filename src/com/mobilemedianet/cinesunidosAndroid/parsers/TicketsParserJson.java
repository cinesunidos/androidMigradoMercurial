package com.mobilemedianet.cinesunidosAndroid.parsers;

import android.util.Log;

import com.mobilemedianet.cinesunidosAndroid.objects.Boleto;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class TicketsParserJson {

    private String TAG = TicketsParserJson.class.getSimpleName();
    private String hallName;
    private int seatsAvailable, maxSeatsPerTransaction;

    //private List<Movie> peliculas;

    public TicketsParserJson()
    {
        //peliculas = peliculas = new ArrayList<Movie>();
        hallName = "";
        seatsAvailable = 0;
        maxSeatsPerTransaction = 0;
    }
 
    public List<Boleto> parse(String jsonRaw)
    {

        List<Boleto> boletos = new ArrayList<Boleto>();

        try {
            JSONObject jsonObj = new JSONObject(jsonRaw);

            hallName = jsonObj.getString("HallName");
            seatsAvailable = jsonObj.getInt("SeatsAvailable");
            maxSeatsPerTransaction = jsonObj.getInt("MaxSeatsPerTransaction");
            seatsAvailable = jsonObj.getInt("SeatsAvailable");
            JSONArray jsonTickets = jsonObj.getJSONArray("Tickets");


            for (int i = 0; i < jsonTickets.length(); i++)
            {
                JSONObject ticket = jsonTickets.getJSONObject(i);

                Boleto boleto = new Boleto();
                boleto.setBasePrice(ticket.getDouble("BasePrice"));
                boleto.setBookingFee(ticket.getDouble("BookingFee"));
                boleto.setId(ticket.getString("Code"));
                boleto.setFullPrice(ticket.getDouble("FullPrice"));
                boleto.setDescripcion(ticket.getString("Name"));
                boleto.setPrice(ticket.getDouble("Price"));
                boleto.setServiceFee(ticket.getDouble("ServiceFee"));
                boleto.setServiceFeeWithTax(ticket.getDouble("ServiceFeeWithTax"));
                boleto.setTax(ticket.getDouble("Tax"));

                boletos.add(boleto);

            }

            return boletos;
        }
        catch (final JSONException e)
        {
            Log.e(TAG, "Json parsing error: " + e.getMessage());
            return null;
        }


    }

    public String getHallName() {
        return hallName;
    }

    public int getMaxSeatsPerTransaction() {
        return maxSeatsPerTransaction;
    }

    public int getSeatsAvailable() {
        return seatsAvailable;
    }
}
