package com.mobilemedianet.cinesunidosAndroid.parsers;

import java.io.InputStream;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import com.mobilemedianet.cinesunidosAndroid.managers.ConnectionManager;
import com.mobilemedianet.cinesunidosAndroid.objects.Movie;

public class EstrenosParserSax {
	
	
	String url;
	 
    public EstrenosParserSax(String url, int index)
    {
    	this.url = url;
//    	file = ConnectionManager.getInstance().getInputStreamFrom(url, "GetPremier.xml");
//        try
//        {
//            this.rssUrl = new URL(url);
//        }
//        catch (MalformedURLException e)
//        {
//            throw new RuntimeException(e);
//        }
    }
 
    public List<Movie> parse()
    {
//    	if(file == null)
//    		return null;
    	InputStream stream = this.getInputStream();
    	
    	if(stream == null)
    		return null;
    	
        SAXParserFactory factory = SAXParserFactory.newInstance();
 
        try
        {
            SAXParser parser = factory.newSAXParser();  
            EstrenosHandler handler = new EstrenosHandler();
            parser.parse(stream, handler);
            return handler.getMovies();
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }
 
    private InputStream getInputStream(){
    	
       
            return ConnectionManager.getInstance().getConnection(url);
       
    }

}
