package com.mobilemedianet.cinesunidosAndroid.parsers;

import java.io.InputStream;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import com.mobilemedianet.cinesunidosAndroid.managers.ConnectionManager;
import com.mobilemedianet.cinesunidosAndroid.objects.Hall;

public class SalasParserSax {
	
//	private URL rssUrl;
	private InputStream file;
	 
    public SalasParserSax(String url)
    {
    	file = ConnectionManager.getInstance().getInputStreamFrom(url, ConnectionManager.GetTheaters);
//        try
//        {
//            this.rssUrl = new URL(url);
//        }
//        catch (MalformedURLException e)
//        {
//            throw new RuntimeException(e);
//        }
    }
 
    public List<Hall> parse()
    {
    	if(file == null)
    		return null;
    	
        SAXParserFactory factory = SAXParserFactory.newInstance();
 
        try
        {
            SAXParser parser = factory.newSAXParser();
            SalasHandler handler = new SalasHandler();
            parser.parse(file, handler);
            return handler.getSalas();
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }
 
//    private InputStream getInputStream()
//    {
//        try
//        {
//            return rssUrl.openConnection().getInputStream();
//        }
//        catch (IOException e)
//        {
//            throw new RuntimeException(e);
//        }
//    }

}
