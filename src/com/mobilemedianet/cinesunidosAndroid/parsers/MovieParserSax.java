package com.mobilemedianet.cinesunidosAndroid.parsers;

import android.util.Log;

import com.mobilemedianet.cinesunidosAndroid.managers.ConnectionManager;
import com.mobilemedianet.cinesunidosAndroid.objects.Movie;

import java.io.InputStream;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class MovieParserSax {
	
//	private URL rssUrl;
	private InputStream file;
    private InputStream fileJson;
	 
    public MovieParserSax(String url, int index)
    {
        Log.d("MovieParserSax: ", "" + url);
        if(index >= 0)
    		file = ConnectionManager.getInstance().getInputStreamFrom(url, index + ConnectionManager.GetCinemaMoviesDay);
    	else
    		file = ConnectionManager.getInstance().getConnection(url);    		
//        try
//        {
//            this.rssUrl = new URL(url);
//        }
//        catch (MalformedURLException e)
//        {
//            throw new RuntimeException(e);
//        }
    }
 
    public List<Movie> parse(int index)
    {
    	if(file == null)
    		return null;
    	
        SAXParserFactory factory = SAXParserFactory.newInstance();
 
        try
        {
            SAXParser parser = factory.newSAXParser();
            MovieHandler handler = new MovieHandler(index);
            parser.parse(file, handler);
            if(handler.getMovies().isEmpty()){
                Log.d("MovieManager", "Está vacio");
                //return null;
            }
            return handler.getMovies();
        }
        catch (Exception e)
        {
            return null;
        }
    }
 
//    private InputStream getInputStream()
//    {
//        try
//        {
//            return rssUrl.openConnection().getInputStream();
//        }
//        catch (IOException e)
//        {
//            throw new RuntimeException(e);
//        }
//    }

}
