package com.mobilemedianet.cinesunidosAndroid.parsers;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class RegistroParserJson {

    private String TAG = this.getClass().getSimpleName();


    public RegistroParserJson()
    {

    }
 
    public boolean parse(String jsonRaw)
    {
        try {
            JSONObject jsonObj = new JSONObject(jsonRaw);

            boolean isOk = jsonObj.getBoolean("isOk");


            return isOk;
        }
        catch (final JSONException e)
        {
            Log.e(TAG, "Json parsing error: " + e.getMessage());
            return false;
        }
    }
}


