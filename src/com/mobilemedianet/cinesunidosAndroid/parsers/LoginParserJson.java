package com.mobilemedianet.cinesunidosAndroid.parsers;

import android.util.Log;

import com.mobilemedianet.cinesunidosAndroid.objects.InformacionUsuario;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginParserJson {

    private String TAG = this.getClass().getSimpleName();

    private InformacionUsuario usuario;

    public LoginParserJson(InformacionUsuario infoUser)
    {
        usuario = infoUser;
    }
 
    public InformacionUsuario parse(String jsonRaw)
    {


        try {
            JSONObject jsonObj = new JSONObject(jsonRaw);

            String hash = null;

            hash = jsonObj.getString("Hash");

            if (hash != null && hash.compareTo("") != 0)
            {
                if (usuario == null)
                {
                    usuario = new InformacionUsuario();
                }
                usuario.setSecureId(hash);
                usuario.setStatus(true);
            }

            return usuario;
        }
        catch (final JSONException e)
        {
            Log.e(TAG, "Json parsing error: " + e.getMessage());
            return null;
        }
    }
}


