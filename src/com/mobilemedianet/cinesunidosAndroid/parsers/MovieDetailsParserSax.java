package com.mobilemedianet.cinesunidosAndroid.parsers;

import java.io.InputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import com.mobilemedianet.cinesunidosAndroid.managers.ConnectionManager;
import com.mobilemedianet.cinesunidosAndroid.objects.Movie;

public class MovieDetailsParserSax {
	
//	private URL rssUrl;
	private InputStream file;
	 
    public MovieDetailsParserSax(String url, String filename)
    {
    	if(filename != "")
    		file = ConnectionManager.getInstance().getInputStreamFrom(url, filename);
    	else
    		file = ConnectionManager.getInstance().getConnection(url);
//        try
//        {
//            this.rssUrl = new URL(url);
//        }
//        catch (MalformedURLException e)
//        {
//            throw new RuntimeException(e);
//        }
    }
 
    public boolean parse(Movie peli)
    {
    	if(file == null)
    		return false;
    	
        SAXParserFactory factory = SAXParserFactory.newInstance();
 
        try
        {
            SAXParser parser = factory.newSAXParser();
            MovieDetailsHandler handler = new MovieDetailsHandler();
            handler.getParsed(peli);
            parser.parse(file, handler);
            
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);           
        }
        return true;
    }
 
//    private InputStream getInputStream()
//    {
//        try
//        {
//            return rssUrl.openConnection().getInputStream();
//        }
//        catch (IOException e)
//        {
//            throw new RuntimeException(e);
//        }
//    }

}
