package com.mobilemedianet.cinesunidosAndroid.parsers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.mobilemedianet.cinesunidosAndroid.objects.Movie;

public class MovieDetailsHandler extends DefaultHandler{
	
	    private Movie peli;
	    private StringBuilder sbTexto;
	   
	 
	    public void getParsed(Movie a){
	        this.peli = a;
	    }
	 
	    @Override
	    public void characters(char[] ch, int start, int length)
	                   throws SAXException {
	 
	        super.characters(ch, start, length);
	 
	        if (this.peli != null)
	        	sbTexto.append(ch, start, length);
	    }
	    
	    @Override
	    public void startElement(String uri, String localName,
	                   String name, Attributes attributes) throws SAXException {
	 
	        super.startElement(uri, localName, name, attributes);
	            sbTexto.setLength(0);
	        
	    }
	 
	    @Override
	    public void endElement(String uri, String localName, String name)
	                   throws SAXException {
	 
	        super.endElement(uri, localName, name);
	 
	        if (this.peli != null) {
	 
	            if (localName.equals("Runtime")) {	   
	            	
	            	peli.setRuntime(sbTexto.toString());          	
	            	
	            }else if (localName.equals("OfficialSite")) {	
	            	
	            	peli.setOfficialSite(sbTexto.toString());
	            	
	            }else if (localName.equals("Director")) {
	            	
	            	peli.setDirector(sbTexto.toString());	
	            	
	            }else if (localName.equals("Producer")) {	
	            	
	            	peli.setProducer(sbTexto.toString());	
	            	
	            }else if (localName.equals("Starring")) {	
	            	
	            	peli.setStarring(sbTexto.toString());	
	            	
	            }else if (localName.equals("Sinopsis")) {	
	            	
	            	peli.setSinopsis(sbTexto.toString());	 
	            	
	            }else if (localName.equals("Company")) {	
	            	
	            	peli.setCompany(sbTexto.toString());	 
	            	
	            }else if (localName.equals("Genre")) {	
	            	
	            	peli.setGenere(sbTexto.toString());	 
	            	
	            }else if (localName.equals("Writer")) {	
	            	
	            	peli.setWriter(sbTexto.toString());	 
	            	
	            }else if (localName.equals("Premier")) {	
	            	
	            	peli.setPremier(sbTexto.toString());	 
	            	
	            }
	            sbTexto.setLength(0);
	           
	        }
	        
	    }
	 
	    @Override
	    public void startDocument() throws SAXException {
	 
	        super.startDocument();
	 	       
	        sbTexto = new StringBuilder();
	       
	    }
	 
	   

}
