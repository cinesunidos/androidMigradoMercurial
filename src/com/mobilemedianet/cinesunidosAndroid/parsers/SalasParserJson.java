package com.mobilemedianet.cinesunidosAndroid.parsers;

import android.util.Log;

import com.mobilemedianet.cinesunidosAndroid.managers.CinesManager;
import com.mobilemedianet.cinesunidosAndroid.objects.CineInfo;
import com.mobilemedianet.cinesunidosAndroid.objects.Hall;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SalasParserJson {

    private String TAG = this.getClass().getSimpleName();

    private ArrayList<Hall> salas;
    private Hall sala;
    private CineInfo cine;

    public SalasParserJson()
    {
        salas = new ArrayList<Hall>();
    }
 
    public List<Hall> parse(String jsonRaw)
    {
        try {
            //JSONObject jsonObj = new JSONObject(jsonRaw);

            JSONArray jsonCines = new JSONArray(jsonRaw);
            // Getting JSON Array node
            //JSONArray contacts = jsonObj.getJSONArray("contacts");

            // looping through All Cines
            for (int i = 0; i < jsonCines.length(); i++)
            {

                JSONObject objectCine = jsonCines.getJSONObject(i);


                cine = new CineInfo();
                cine.setCineId(Integer.parseInt(objectCine.getString("Id")));
                cine.setName(objectCine.getString("Name"));
                cine.setCity(objectCine.getString("City"));

                JSONArray hallsArray = objectCine.getJSONArray("Halls");

                for(int j=0; j < hallsArray.length(); j++)
                {
                    JSONObject objectHall = hallsArray.getJSONObject(j);

                    sala = new Hall(objectHall.getInt("Hall_intId"));
                    sala.setName(objectHall.getString("Hall_strName"));
                    sala.setSeats(objectHall.getInt("Hall_intSeat"));
                    sala.setSeleccionAsiento(true);

                    String hall_type = objectHall.getString("Hall_strType");
                    sala.setPremium(false);
                    if (hall_type.compareTo("Premium") == 0)
                    {
                        sala.setPremium(true);
                    }

                    salas.add(sala);
                }


                CinesManager.getInstance().addCine(cine);

            }

            return salas;
        }
        catch (final JSONException e)
        {
            Log.e(TAG, "Json parsing error: " + e.getMessage());
            return null;
        }
    }



//    public List<Hall> getMovies(){
//        return salas;
//    }
}


