package com.mobilemedianet.cinesunidosAndroid.parsers;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.mobilemedianet.cinesunidosAndroid.managers.CinesManager;
import com.mobilemedianet.cinesunidosAndroid.objects.CineInfo;
import com.mobilemedianet.cinesunidosAndroid.objects.Hall;

public class SalasHandler extends DefaultHandler{
	
	    private ArrayList<Hall> salas;	        
	    private Hall sala;
	    private CineInfo cine;
	    private StringBuilder sbTexto;

	 
	    public List<Hall> getSalas(){
	        return salas;
	    }
	 
	    @Override
	    public void characters(char[] ch, int start, int length)
	                   throws SAXException {
	 
	        super.characters(ch, start, length);
	 
	        sbTexto.append(ch, start, length);
	    }
	    
	    @Override
	    public void startElement(String uri, String localName,
	                   String name, Attributes attributes) throws SAXException {
	 
	        super.startElement(uri, localName, name, attributes);
	        
	        
	 
	        if (localName.equals("Hall")) {
	            sala = new Hall(Integer.parseInt(attributes.getValue("I")));
	            sala.setSeats(Integer.parseInt(attributes.getValue("S")));
	            sala.setName(attributes.getValue("N"));	 
	            
	        }else if(localName.equals("Theater")){
	        	cine = new CineInfo();
	        	cine.setCineId(Integer.parseInt(attributes.getValue("I")));	 
	        	
	        }
	        
	        sbTexto.setLength(0);
	    }
	 
	    @Override
	    public void endElement(String uri, String localName, String name)
	                   throws SAXException {
	 
	        super.endElement(uri, localName, name);
	 
	        if (this.sala != null) {
	 
	            if (localName.equals("HT")) {	            	
	            	
	            	if(sbTexto.toString().equals("N")){
	            		sala.setPremium(false);
	            	}else
	            		sala.setPremium(true);
	            	
	            	salas.add(sala);
	            	
	            }else if(localName.equals("SA")){
	            	
	            	if(sbTexto.toString().equals("Y")){
	            		sala.setSeleccionAsiento(true);
	            	}else
	            		sala.setSeleccionAsiento(false);
	            		
	            	
	            }
	 
	            
	        }
	        
	        if(localName.equals("P")){
            	
            	if(sbTexto.toString().equals("0")){
            		cine.setPremium(false);
            	}else
            		cine.setPremium(true);
            	
            	CinesManager.getInstance().addCine(cine);
            	
            	
            	
            }	        
	        
	        
	    }
	 
	    @Override
	    public void startDocument() throws SAXException {
	 
	        super.startDocument();
	 
	        salas = new ArrayList<Hall>();	        
	        sbTexto = new StringBuilder();	  
	        
	        
	        
	    }
	 
	   

}
