package com.mobilemedianet.cinesunidosAndroid;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.mobilemedianet.cinesunidosAndroid.activities.ListaActivityFragment.OnListaSelectedListener;
import com.mobilemedianet.cinesunidosAndroid.managers.ApplicationStatus;
import com.mobilemedianet.cinesunidosAndroid.managers.ConnectionManager;
import com.mobilemedianet.cinesunidosAndroid.managers.DetallesCompra;
import com.mobilemedianet.cinesunidosAndroid.managers.LoginManager;
import com.mobilemedianet.cinesunidosAndroid.managers.MovieManager;
import com.mobilemedianet.cinesunidosAndroid.managers.PrefManager;
import com.mobilemedianet.cinesunidosAndroid.objects.Movie;
import com.mobilemedianet.cinesunidosAndroid.utilities.Metrics;
import com.mobilemedianet.cinesunidosAndroid.utilities.Zonas;

import net.londatiga.android.ActionItem;
import net.londatiga.android.QuickAction;

import java.util.ArrayList;

//import com.google.android.gms.ads.AdSize;
//import com.google.android.gms.ads.AdView;

//import com.mobilemedianet.cinesunidosAndroid.managers.LocalyticsManager;
//import com.mobmedianet.adutil.AdView;


public class ListaActivity extends Activity {


    private ArrayList<MovieEntry> pelis = null;
    private ArrayList<Movie> pelis2;
    private IconListViewAdapter m_adapter;
    private Zonas zona;
    private ListView lv = null;
    ProgressDialog pd;

    private static final int ID_SELECT = 1;
    private static final int ID_REFRESH = 2;
    private static final int ID_LOGOUT = 3;
    private static final int ID_ACERCA = 4;
    private static final int ID_COMPARTIR = 5;
    private static final int ID_ESTRENOS = 6;

    private static final String Flurry_API_Key = "GSW94MXD24S7D546NST6";


    Gallery myGallery;

    int posterHeight;
    int posterWidth;
    int key;

    View selected = null;
    int colortmp;

    OnListaSelectedListener onListaSelected;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        int tim;

        super.onCreate(savedInstanceState);

        //Flurry Start Session
        FlurryAgent.init(this, Flurry_API_Key);
        FlurryAgent.onStartSession(this);

        Bundle bundle = getIntent().getExtras();
        //MANEJO DE PARAMETRO ADICIONAL
        tim = (int) bundle.getInt("timeout");
        if (tim == -1) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Error en la conexión, intente la recarga de nuevo-")
                    .setTitle("Atención!")
                    .setCancelable(false)
                    .setNegativeButton("Recargar", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            ConnectionManager.getInstance().clearCache(null);
                            MovieManager.getInstance().needToRefresh = true;
                            new RefreshTask().execute("");
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }


        // Instantiate the object


    }

    @Override
    public void onResume() {
        super.onResume();


        if (ApplicationStatus.getStatus() == false) {
            Intent reLaunchMain = new Intent(this, SplashActivity.class);
            reLaunchMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(reLaunchMain);
            this.finish();
        }

		/*if(LocalyticsManager.localyticsSession() == null){
			LocalyticsManager.newLocalytics(getApplicationContext());
	    	LocalyticsManager.localyticsSession().open();
		}*/


        Bundle bundle = getIntent().getExtras();

        if (bundle != null && bundle.getBoolean("salir", false)) {
            ListaActivity.this.finish();
            android.os.Process.killProcess(android.os.Process.myPid());

        }


        Metrics.setContext(this);
        ConnectionManager.setContext(this);
        MovieManager.getInstance().setContext(this);
        PrefManager.getInstance().setAplicationContext(this);
        DetallesCompra.getInstance().setOrigen("Peliculas");

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            if (MovieManager.getInstance().needToRefresh == true)
                return;

            //LocalyticsManager.localyticsSession().tagEvent("Carrusel");
            //FlurryAgent.logEvent("Carrusel");
            carrusel();
        } else {
            //LocalyticsManager.localyticsSession().tagEvent("Lista de Peliculas Todas");
            //FlurryAgent.logEvent("Lista de Peliculas Todas");
            setContentView(R.layout.lista);
            RelativeLayout home = (RelativeLayout) findViewById(R.id.botonHome);
            RelativeLayout cines = (RelativeLayout) findViewById(R.id.botonCine);
            RelativeLayout peliculas = (RelativeLayout) findViewById(R.id.botonPeli);
            /***************************************Envio de Logs******************************/
    		/*Button sender = (Button) findViewById(R.id.sendLogs);
    		sender.setOnClickListener(OnClickDoSomething(sender));*/
            /**********************************************************************************/
            cines.setOnClickListener(OnClickDoSomething(cines));
            peliculas.setOnClickListener(OnClickDoSomething(peliculas));
            home.setOnClickListener(OnClickDoSomething(home));
            generarMenu();
            inicializarLista();
            if (MovieManager.getInstance().needToRefresh == true) {
                ConnectionManager.getInstance().clearCache(null);
                new RefreshTask().execute("");
            }
        }

    }

    @SuppressWarnings("deprecation")
    private void carrusel() {
        try {
            setContentView(R.layout.carrusel);
        } catch (Exception e) {
            return;
        }
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        posterHeight = (int) (metrics.heightPixels * 0.6);
        posterWidth = (int) (posterHeight * 0.8);


        int radius;

        if (metrics.widthPixels > metrics.heightPixels)
            radius = metrics.widthPixels;
        else
            radius = metrics.heightPixels;

        //@+id/mainly

        LinearLayout fondo = (LinearLayout) findViewById(R.id.mainly);
        GradientDrawable g = new GradientDrawable(Orientation.TL_BR, new int[]{0xff818181, 0xff0A0A0A});
        g.setGradientType(GradientDrawable.RADIAL_GRADIENT);
        g.setGradientRadius(radius / 2);
        g.setGradientCenter(0.5f, 0.5f);
        fondo.setBackgroundDrawable(g);


        MovieManager.getInstance().resetRankingSearch();

        Movie pelicula;
        pelis2 = new ArrayList<Movie>();
        while ((pelicula = MovieManager.getInstance().getNextMovieRank(0)) != null) {
            pelis2.add(pelicula);
        }

        myGallery = (Gallery) findViewById(R.id.gallery1);


        myGallery.setAdapter(new ImageAdapter(this));


        myGallery.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View v,
                                       int position, long id) {
//					i.seLayoutParams(posterWidth, posterHeight);
//					v.setLayoutParams(new Gallery.LayoutParams(posterWidth*(1 + 1/3), posterHeight*(1 + 1/3)));
                TextView nombrePeli = (TextView) findViewById(R.id.textView1);
                if (nombrePeli != null)
                    nombrePeli.setText(pelis2.get(position).getSpanishTitle());

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

        myGallery.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position,
                                    long id) {

                //LocalyticsManager.localyticsSession().tagEvent("Película Seleccionada desde el Carrusel");
                //FlurryAgent.logEvent("Película Seleccionada desde el Carrusel");
                String movid = pelis2.get(position).getId();

                Intent myIntent = new Intent(ListaActivity.this, MainActivity.class);
                myIntent.putExtra("action", MainActivity.ID_DETAILS);
                myIntent.putExtra("param", movid);
                myIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(myIntent);


            }

        });


    }


    public class ImageAdapter extends BaseAdapter {
        int mGalleryItemBackground;
        private Context mContext;

        public ImageAdapter(Context c) {
            mContext = c;
            TypedArray a = obtainStyledAttributes(R.styleable.HelloGallery);
//            TypedArray a = c.obtainStyledAttributes(R.styleable.Gallery1);
            mGalleryItemBackground = a.getResourceId(
                    R.styleable.HelloGallery_android_galleryItemBackground, 0);
            a.recycle();
        }

        @Override
        public int getCount() {
            return pelis2.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView i = new ImageView(mContext);
            if (pelis2.get(position).posterlsLoaded() == true) {
                i.setImageDrawable(pelis2.get(position).getPoster());
            } else {
                i.setImageResource(R.drawable.image_na);
                new DownloadImageTask2().execute(String.valueOf(position));
            }

            i.setLayoutParams(new Gallery.LayoutParams(posterWidth, posterHeight));
            i.setScaleType(ImageView.ScaleType.FIT_XY);
            i.setBackgroundResource(mGalleryItemBackground);
//            i.setTag(position);
//            i.setBackgroundColor(0x0000);
            return i;
        }
    }

    private class DownloadImageTask2 extends AsyncTask<String, Void, Drawable> {
        int index;
        String tag;

        @Override
        protected Drawable doInBackground(String... position) {
            index = Integer.parseInt(position[0]);
            tag = position[0];

            pelis2.get(index).setPoster();

            return pelis2.get(index).getPoster();

        }

        @Override
        protected void onPostExecute(Drawable poster) {
            ImageView imagen;

            imagen = (ImageView) myGallery.getChildAt(index - myGallery.getFirstVisiblePosition());
//        	imagen = (ImageView) myGallery.getItemAtPosition(index);        	
            if (imagen != null) {
                imagen.setImageDrawable(poster);
                imagen.invalidate();
            }
        }
    }

    View.OnClickListener OnClickDoSomething(final View v) {
        return new View.OnClickListener() {


            @Override
            public void onClick(View v) {


                if (v.getId() == R.id.botonCine) {
                    Intent myIntent = new Intent(ListaActivity.this, MainActivity.class);
                    myIntent.putExtra("action", MainActivity.ID_CINEMAS);
                    myIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(myIntent);


                } else if (v.getId() == R.id.botonPeli) {
                    Toast.makeText(ListaActivity.this, "Ya se encuentra en la lista de películas", Toast.LENGTH_SHORT).show();
//            		cambiarFragment(0, "lista", ListaActivityFragment.getInstance());
//            		
                } else if (v.getId() == R.id.botonHome) {

                    Toast.makeText(ListaActivity.this, "Ya se encuentra en home", Toast.LENGTH_SHORT).show();

                }/* else if (v.getId() == R.id.sendLogs) {
            		ACRA.getErrorReporter().handleSilentException(null);
            		ACRA.getErrorReporter().handleException(null, false);
            	}*/

            }
        };
    }


    public static class MovieEntry {
        public MovieEntry(Movie a) {
            peli = a;


        }

        public Movie getPeli() {
            return peli;
        }

        private Movie peli;


    }


    public void inicializarLista() {
        MovieEntry entry;
        Movie pelicula;

        MovieManager.getInstance().setListSelector(MovieManager.listMovies);

        pelis = new ArrayList<MovieEntry>();
        this.m_adapter = new IconListViewAdapter(this, R.layout.list_item_icon_text, pelis);

        m_adapter.clear();
        MovieManager.getInstance().resetRankingSearch();

        int i = 0;

        //Log.d("Pelis Array Size: ", "" + MovieManager.getInstance().getPelis().length);
        //Log.d("Adapter 0: ", "" + i++);
        Movie movieValidation = MovieManager.getInstance().getNextMovieRank(0);
        if (movieValidation == null) {
            //Log.d("Adapter 0: ", "" + i++);
            //return ;
            //Toast.makeText(getApplicationContext(), "No hay peliculas.", Toast.LENGTH_SHORT).show();
//			AlertDialog.Builder builder = new AlertDialog.Builder(this);
//			builder.setTitle("Error al cargar");
//			builder.setMessage("Presiona recargar para volver a intentarlo");
//			builder.setPositiveButton("Recargar", new DialogInterface.OnClickListener() {
//				public void onClick(DialogInterface dialog,int which) {
//					// TODO Auto-generated method stub
//					ConnectionManager.getInstance().clearCache(null);
//					MovieManager.getInstance().needToRefresh = true;
//					new RefreshTask().execute("");
//					dialog.cancel();
//				}
//			});
//			builder.create();
//			builder.setIcon(R.drawable.icono);
//			builder.show();

            //ListContainer colocar en este contedor el texto...
            FrameLayout fl = (FrameLayout) findViewById(R.id.ListContainer);
            View view;
            LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.default_msj_empty_list, null);
            fl.addView(view);

			/*TextView text = new TextView(getApplicationContext());
			text.setText("No hay películas disponibles");
			text.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
			text.setTextColor(Color.BLACK);
			text.setTextSize(16);
			fl.addView(text);*/

            ((FrameLayout) findViewById(R.id.ListContainer)).setVisibility(View.VISIBLE);

        }
        else
        {
            entry = new MovieEntry(movieValidation);
            m_adapter.add(entry);
            while ((pelicula = MovieManager.getInstance().getNextMovieRank(0)) != null) {
                entry = new MovieEntry(pelicula);
                m_adapter.add(entry);
                //Log.d("Adapter: ", "" + i++);
            }
        }
//        if(ConnectionManager.getInstance().getLastError() == -1){
//        	mensajeError("El caché ha expirado, necesita de una conexión a internet para poder actualizar las listas.");
//        	return;
//        }

        FrameLayout fl = (FrameLayout) findViewById(R.id.ListContainer);

        lv = new ListView(this);
        fl.addView(lv);

        lv.setAdapter(this.m_adapter);
        lv.setOnItemClickListener(new OnItemClickListener() {
            //TODO AQUI ES DONDE VAN LOS CLICKS DEL LISTVIEW
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
                // TODO Auto-generated method stub
                String movid = (m_adapter.getItem(position)).getPeli().getId();
                //LocalyticsManager.localyticsSession().tagEvent("Película Seleccionada desde Todas");
                //FlurryAgent.logEvent("Película Seleccionada desde Todas");
                Intent myIntent = new Intent(ListaActivity.this, MainActivity.class);
                myIntent.putExtra("action", MainActivity.ID_DETAILS);
                myIntent.putExtra("param", movid);
                myIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(myIntent);
//	        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);

            }

        });
//       lv.onListItemClick((OnClickListener) onListaSelected);


        PrefManager pref = PrefManager.getInstance();
        pref.setAplicationContext(this);
        String aux = pref.getCityWithMostSelections();
        Log.d("CIUDAD-ListActivity", "" + aux);

//	    String key = "";

        String key;
        if (aux.equals("CCSE,CCSO")) {
            key = "ALL_CCS";
        } else {
            key = "ALL_" + aux;
        }
//	    Zonas zona = new Zonas();
        zona = new Zonas();
        zona.createZones();

        Log.d("Connection Error num: ", "" + ConnectionManager.getInstance().getLastError());
    }

    public void refreshList() {
        MovieEntry entry;
        Movie pelicula;

        if (m_adapter == null)
            return;

        m_adapter.clear();


        MovieManager.getInstance().resetRankingSearch();

        while ((pelicula = MovieManager.getInstance().getNextMovieRank(0)) != null) {
            entry = new MovieEntry(pelicula);
            m_adapter.add(entry);

        }

        m_adapter.notifyDataSetChanged();


    }

    public class IconListViewAdapter extends ArrayAdapter<MovieEntry> {

        private ArrayList<MovieEntry> items;

        public IconListViewAdapter(Context context, int textViewResourceId, ArrayList<MovieEntry> items) {
            super(context, textViewResourceId, items);
            this.items = items;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View v = lv.getChildAt(position - lv.getFirstVisiblePosition()); //convertView;
            LinearLayout contenedor;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.list_item_icon_text, null);
            }

            contenedor = (LinearLayout) v.findViewById(R.id.contenedor);

            if (position % 2 == 0) {
                contenedor.setBackgroundResource(R.drawable.custom_button_dos);
            } else {
                contenedor.setBackgroundResource(R.drawable.custom_button_tres);
            }

            Movie pelicula = items.get(position).getPeli();

            if (pelicula != null) {

                //poblamos la lista de elementos

                ViewHolder holder = null;
                TextView tesp = null;
                TextView torg = null;
                ImageView im = null;

                if (v.getTag() == null) {
                    holder = new ViewHolder();
                    holder.icon = (ImageView) v.findViewById(R.id.icon);
                    holder.tituloesp = (TextView) v.findViewById(R.id.titulo_espanol);
                    holder.tituloorg = (TextView) v.findViewById(R.id.titulo_original);

                    tesp = holder.tituloesp;
                    torg = holder.tituloorg;
                    im = holder.icon;
                    v.setTag(holder);
                } else {
                    holder = (ViewHolder) v.getTag();
                    tesp = holder.tituloesp;
                    torg = holder.tituloorg;
                    im = holder.icon;
                }


                if (tesp != null) {
                    tesp.setText(pelicula.getSpanishTitle());
                }

                if (torg != null) {
                    torg.setText(pelicula.getOriginalTitle());
                }

                if (im != null) {
                    if (pelicula.posterlsLoaded() == true) {
                        im.setImageDrawable(pelicula.getPoster());
                    } else {
                        new DownloadImageTask().execute(position);

                    }

                }

                if (position == 2) {
                    PrefManager pref = new PrefManager();
                    pref.setAplicationContext(getBaseContext());
                    String aux = pref.getCityWithMostSelections();
                    Log.d("CIUDAD - ListaActivity:", "" + aux);

                    String key = "";
                    if (aux.equals("CCSE,CCSO")) {
                        key = "ALL_CCS";
                    } else {
                        key = "ALL_" + aux;
                    }
                    Zonas zona = new Zonas();
                    zona.createZones();

                    //MobileX
                    //AdView ad = (AdView) v.findViewById(R.id.adViewAll);
                    //ad.setZoneId(zona.getZona(key));
                    //ad.request();
                    //((RelativeLayout) v.findViewById(R.id.addViewContainer)).setVisibility(View.VISIBLE);

                    //Admob
                    RelativeLayout rl = ((RelativeLayout) v.findViewById(R.id.addViewContainer));


                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.gravity = Gravity.CENTER_HORIZONTAL;


                    PublisherAdView mAdView = new PublisherAdView(getContext());

                    rl.setLayoutParams(layoutParams);

                    PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();
                    mAdView.setAdUnitId(getResources().getString(R.string.banner_ad_unit_id));
                    mAdView.setAdSizes(AdSize.BANNER);



                    //AdView mAdView = new AdView(getContext());
                   //mAdView.setAdSize(AdSize.BANNER);
                    //mAdView.setAdUnitId(getResources().getString(R.string.banner_ad_unit_id));
                    //mAdView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, Metrics.dpToPixel(55)));
                    rl.addView(mAdView);

                    mAdView.loadAd(adRequest);

                    //AdRequest adRequest = new AdRequest.Builder().build();
                    //mAdView.loadAd(adRequest);
                    ((RelativeLayout) v.findViewById(R.id.addViewContainer)).setVisibility(View.VISIBLE);

                } else {
                    //Activar si se usa MobileX
                    //AdView ad = (AdView) v.findViewById(R.id.adViewAll);
                    //ad.setVisibility(View.INVISIBLE);

                    //AdView mAdView = (AdView) findViewById(R.id.adView);
                    //((RelativeLayout) v.findViewById(R.id.addViewContainer)).setVisibility(View.INVISIBLE);
//                            	((ImageView) v.findViewById(R.id.adds)).setImageResource(0);
//                            	((View) v.findViewById(R.id.separador)).setVisibility(v.INVISIBLE);
                }

            }
//                    v.setTag(String.valueOf(position));


            return v;
        }

        private class ViewHolder {
            TextView tituloesp;
            TextView tituloorg;
            ImageView icon;
        }
    }

    private class DownloadImageTask extends AsyncTask<Integer, Void, Drawable> {
        int index;


        @Override
        protected Drawable doInBackground(Integer... position) {
            index = position[0];

            (m_adapter.getItem(index)).getPeli().setPoster();

            return (m_adapter.getItem(index)).getPeli().getPoster();

        }

        @Override
        protected void onPostExecute(Drawable poster) {
            View itemView;


            if (lv != null)
                itemView = lv.getChildAt(index - lv.getFirstVisiblePosition());
            else
                return;

            if (itemView != null) {
                ImageView itemImageView = (ImageView) itemView.findViewById(R.id.icon);
                if (itemImageView != null)
                    itemImageView.setImageDrawable(poster);
            }


        }
    }


    private void generarMenu() {

        ActionItem refreshItem = new ActionItem(ID_REFRESH, "\nRefrescar\n", null);
        ActionItem estrenosCinemaItem = new ActionItem(ID_ESTRENOS, "\nPróximos Estrenos\n", null);
        ActionItem selectCinemaItem = new ActionItem(ID_SELECT, "\nCines Favoritos\n", null);
        ActionItem compartirItem = new ActionItem(ID_COMPARTIR, "\nCompartir\n", null);
        ActionItem logoutItem = new ActionItem(ID_LOGOUT, "\nCerrar Sesión\n", null);
        ActionItem acercaDe = new ActionItem(ID_ACERCA, "\nAcerca de\n", null);


        final QuickAction quickAction = new QuickAction(this, QuickAction.VERTICAL);

        quickAction.addActionItem(refreshItem);
        quickAction.addActionItem(estrenosCinemaItem);
        quickAction.addActionItem(selectCinemaItem);
        quickAction.addActionItem(compartirItem);
        quickAction.addActionItem(logoutItem);
        quickAction.addActionItem(acercaDe);


        quickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction source, int pos, int actionId) {
                ActionItem actionItem = quickAction.getActionItem(pos);
                Intent myIntent;

                //here we can filter which action item was clicked with pos or actionId parameter
                if (actionId == ID_REFRESH) {
                    if (ConnectionManager.checkConn() == false) {
                        mensajeError("No hay conexión a internet.");
                        return;
                    }
                    if (PrefManager.getInstance().isAnyCinemaChecked() == false) {
                        mensajeError("No hay cines favoritos, por favor seleccione al menos un cine favorito");
                        return;
                    }
                    ConnectionManager.getInstance().clearCache(null);
                    MovieManager.getInstance().needToRefresh = true;
                    new RefreshTask().execute("");

                } else if (actionId == ID_LOGOUT) {
                    PrefManager.getInstance().cleanUserInfo();
                    LoginManager.getInstance().setLoginStatus(false);
                    Toast.makeText(getApplicationContext(), "Datos de sesión eliminados", Toast.LENGTH_SHORT).show();
                } else if (actionId == ID_SELECT) {
//					checkTimersOnBack();
                    myIntent = new Intent(ListaActivity.this, SeleccionCiudadesActivity.class);
                    myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivityForResult(myIntent, 0);
                } else if (actionId == ID_ACERCA) {

                    myIntent = new Intent(ListaActivity.this, MainActivity.class);
                    myIntent.putExtra("action", ID_ACERCA);
                    myIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(myIntent);

//		        	cambiarFragment(0, "acercade", AcercaActivityFragment.getInstance());
                } else if (actionId == ID_COMPARTIR) {
                    //LocalyticsManager.localyticsSession().tagEvent("Compartir Aplicación");
                    //FlurryAgent.logEvent("Compartir Aplicación");
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    String shareBody = "Descarga CinesUnidos para Android visitando:\n\n www.cinesunidos.com/aplicativo";
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Descarga Cines para Unidos Android");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                    startActivity(Intent.createChooser(sharingIntent, "Share via"));
                } else if (actionId == ID_ESTRENOS) {
                    if (ConnectionManager.checkConn() == false) {
                        mensajeError("No hay conexión a internet.");
                        return;
                    }
                    myIntent = new Intent(ListaActivity.this, MainActivity.class);
                    myIntent.putExtra("action", ID_ESTRENOS);
                    myIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(myIntent);

//		        	cambiarFragment(0, "acercade", AcercaActivityFragment.getInstance());
                }

            }
        });

        RelativeLayout preferencias = (RelativeLayout) findViewById(R.id.botonMenu);
        preferencias.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                quickAction.show(v);
            }
        });


    }


    private void mensajeError(String error) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("CinesUnidos");
        builder.setMessage(error);
        builder.setPositiveButton("OK", null);
        builder.create();
        builder.setIcon(R.drawable.icono);
        builder.show();
    }


    private class RefreshTask extends AsyncTask<String, Void, Integer> {

        boolean resultado;
        private Movie pelicula;

        @Override
        protected void onPreExecute() {


            pd = ProgressDialog.show(ListaActivity.this, "Espere.", "Actualizando listas", true, true);

            if (lv != null && m_adapter != null) {
                m_adapter.clear();
                lv.setAdapter(m_adapter);
            }

        }

        @Override
        protected Integer doInBackground(String... params) {

            resultado = MovieManager.getInstance().refresh();
            Log.d("ListaActivity-Res", "" + resultado);

            if (resultado == false)
                return 0;


            MovieManager.getInstance().resetRankingSearch();

            while ((pelicula = MovieManager.getInstance().getNextMovieRank(0)) != null) {
                if (pelicula.isInCache())
                    pelicula.setPoster();
            }

            return 0;
        }

        @Override
        protected void onPostExecute(Integer result) {

            MovieManager.getInstance().needToRefresh = false;
            Log.d("onPostActivity-Res", "" + resultado);
            if (resultado == false) {
                try {
                    pd.cancel();
                    pd = null;
                } catch (Exception e) {
                }
	    		
	    		/*if (ConnectionManager.getInstance().getLastError() == -3) {
	    			Toast.makeText(ListaActivity.this, "Fallo de servidor", Toast.LENGTH_SHORT).show();
	    			refreshDialog();
	    			return;
	    		}*/


                Toast.makeText(ListaActivity.this, "Fallo de conexión", Toast.LENGTH_SHORT).show();
                refreshDialog();

                return;

            }

            refreshList();

            try {
                pd.cancel();
                pd = null;
            } catch (Exception e) {
            }
        }
    }

    private void refreshDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Error en la conexión, intente la recarga de nuevo--")
                .setTitle("Atención!")
                .setCancelable(false)
                .setNegativeButton("Recargar", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        ConnectionManager.getInstance().clearCache(null);
                        MovieManager.getInstance().needToRefresh = true;
                        new RefreshTask().execute("");
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void unbindDrawables(View view) {
        if (view == null)
            return;
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            try {
                ((ViewGroup) view).removeAllViews();
            } catch (UnsupportedOperationException e) {

            }
        }
    }

    @Override
    public void onPause() {

        //LocalyticsManager.localyticsSession().upload();

        try {
            pd.cancel();
            pd = null;
        } catch (Exception e) {
        }
        unbindDrawables(findViewById(R.id.mainly));

        super.onPause();

        FlurryAgent.onEndSession(this);

    }

    @Override
    protected void onStop() {
        super.onStop();

        FlurryAgent.onEndSession(this);
    }

    @Override
    public void onBackPressed() {

        //LocalyticsManager.localyticsSession().tagEvent("Aplicación Cerrada");
        //FlurryAgent.logEvent("Aplicación Cerrada");
//      localyticsSession.close();
        //LocalyticsManager.localyticsSession().upload();


        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        alertDialog.setTitle("CinesUnidos");

        alertDialog.setMessage("¿Está seguro que desea salir?");

        alertDialog.setIcon(R.drawable.icono);

        alertDialog.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ListaActivity.this.finish();
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        });

        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        });


        alertDialog.show();
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
    }


}
